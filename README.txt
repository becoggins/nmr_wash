nmr_wash README
Version 1.0.0, November 2013

About nmr_wash
--------------

nmr_wash is a collection of data-processing tools designed to remove
sampling artifacts from sparsely sampled NMR spectra. These tools
operate on frequency- domain data, i.e. as generated from the raw
spectrometer output using NMRPipe or another processing program. The
command-line programs "clean" and "scrub" remove artifacts using the
CLEAN and SCRUB algorithms, respectively. The "pipewash" component
provides access to the same algorithms from within NMRPipe. The nmr_wash
algorithms are also available as a C++ library which can be called from
other programs.

About SCRUB
-----------

SCRUB incorporates the CLEAN method for iterative artifact removal, but
applies an additional level of iteration, permitting real signals to be
distinguished from noise and allowing nearly all artifacts generated by
real signals to be eliminated. In favorable cases, SCRUB achieves a
dynamic range over 10000:1 (250× better artifact suppression than CLEAN)
and completely quantitative reproduction of signal intensities, volumes,
and line shapes.

For more information on SCRUB, please see:

  B.E. Coggins, J.W. Werner-Allen, A. Yan, and P. Zhou. J. Am. Chem.
  Soc., 134, 18619-18630 (2012).

Getting Started
---------------

To install nmr_wash, please see the installation instructions in the
INSTALL.txt file, included in this distribution.

Please see the user's manual in the doc directory for detailed
information on how to use these programs.

Questions? Problems?
--------------------

Please don't hesitate to write the author: bec2 -at- duke.edu

* * * * *

Copyright (c) 2013, Brian E. Coggins. All rights reserved.
