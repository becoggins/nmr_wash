% SCRUB and nmr_wash Documentation
%  
% Version 1.0.0, November 2013

### Readme, License, and Installation Instructions

| [Readme](readme.html)
| [License](license.html)
| [Installation Instructions](install.html)

### User's Manual

| [User's Manual](manual.html)

### Man Pages for Command-Line Programs

| [*scrub* Man Page](scrub_man.html)
| [*clean* Man Page](clean_man.html)
| [*pipewash* Man Page](pipewash_man.html)

###### {.copyright-block}

----------

Copyright (c) 2013, Brian E. Coggins.  All rights reserved.



