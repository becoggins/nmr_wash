%	Installation Instructions for *nmr_wash*
%
%	Version 1.0.0, November 2013


##	General Instructions

If the distribution you downloaded included precompiled binaries, these will be
found in the bin subdirectory.

If you did not receive precompiled binaries, you can compile the software on
your own system by typing the following command in the distribution directory:

	make
	
This should build all of the required libraries and then the software, placing
the binaries in the main distribution directory.


##	Installing Files

To install the program files, type either:

	make install-pc
	
to use precompiled binaries, or:

	make install-built
	
to use program files built on your system with the `make` command.  Note that 
`make install-built` will build the files if they are not already present.

To install the man pages, type:

	make install-man
	
The default installation location is `/usr/local/bin` for binaries and
`/usr/local/man` for man pages.  To override this, add `PREFIX=`*some_location*
to the end of the command line, where *some_location*`/bin` is your
preferred destination for binaries and *some_location*`/man` for man pages.


##	Other commands

To build the external libraries only:

	make libs
	
To build a specific component of the package:

	make scrub
	make clean
	make pipewash
	make libnmr_wash.a

Note that `clean` is the name of one of the binaries in this package, so
`make clean` does not have its usual meaning!

To remove all files generated in the build process:

	make cleanup


##	Building in Xcode

If you would like to build the software in Xcode 3 or 4:

1.  Change to the libs directory in your terminal, and type `make` to build
	the external libraries.

2.  Open `nmr_wash.xcodeproj` in Xcode.

3.  Open the Xcode preferences and choose "Source Trees" (which may be under
    "Locations," depending on the version of Xcode), then add the following:
    
		BLITZ_DIR = libs/blitz-0.10
		BOOST_INCLUDE_DIR = libs/boost
		BOOST_LIB_DIR = libs/boost/stage/lib
		CMINPACK_DIR = libs/cminpack-1.1.2
		CMINPACK_LIB_DIR = libs/cminpack-1.1.2
		SCITBX_DIR = libs/scitbx

4.	Choose the build scheme corresponding to the product you would like to build,
	and press Cmd-B (or choose Product -> Build) to build it.  The product will
	appear in your DerivedData directory, which might be hard to find; you can
	find it by looking at the build output in the log, or you can override it in
	the Xcode preferences and/or the project settings.


##	If You Downloaded the `git` Repository from BitBucket

...you will not have the external libraries.  You can download these from their
original source websites and build them yourself, then update the variables at the
top of the makefile (or the Xcode source trees) with the correct paths.  
Alternatively, you can download a TAR archive with only the external libraries 
from our website.  This should be unpacked and the products copied into your
nmr_wash directory, under `libs`.  You can then build using the (unchanged) 
makefile and/or the Xcode project with the source trees listed above.

###### {.copyright-block}

----------

Copyright (c) 2013, Brian E. Coggins.  All rights reserved.

