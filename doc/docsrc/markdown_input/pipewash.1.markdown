% PIPEWASH(1) Man Page
% Brian E. Coggins
% Version 1.0.0, November 2013

# NAME

pipewash - NMRPipe plug-in to remove artifacts from NMR spectra acquired with sparse 
sampling

# SYNPOSIS

pipewash -fn SCRUB -pattern *sampling-pattern* [*options*]

pipewash -fn CLEAN -pattern *sampling-pattern* [*options*]


# DESCRIPTION

*pipewash* is an NMRPipe plug-in providing access to the SCRUB and CLEAN algorithms for
removing sampling artifacts from NMR spectra acquired with sparse sampling. *pipewash*
can be inserted directly into NMRPipe processing scripts and the appropriate function
(either "SCRUB" or "CLEAN") called to carry out artifact suppression.

## Processing Scripts

The recommended way to use *pipewash* in the processing script for a 3-D NMR spectrum is
according to the following pattern:

\sps

###### {.pipe-script}


|     `xyz2pipe -in` *input* `-x \`
|	  ...
|	  Process the original X dimension...
|	  ...
|     `| nmrPipe -fn TP \`
|	  ...
|	  Process the original Y dimension...
|	  ...
|     `| nmrPipe -fn TP \`
|     `| nmrPipe -fn ZTP \`
|	  ...
|	  Process the original Z dimension...
|	  ...
|     `| pipewash -fn SCRUB -pattern` *pattern_file* *options* `\`
|     `| nmrPipe -fn ZTP \`
|     `| pipe2xyz -out` *output* `-x`

######	\fps

The *pipewash* processing functions require that the dimensions be transposed from their
normal order, shifting the indirect dimensions into the X, Y, and (when applicable) Z
positions.  This is the effect of the `ZTP` function in the example script above.

For a 4-D spectrum, follow the same approach, but use ATP before and after SCRUB:

\sps

###### {.pipe-script}


|     `xyz2pipe -in ` *input* `-x \`
|	  ...
|	  Process the original X dimension...
|	  ...
|     `| nmrPipe -fn TP \`
|	  ...
|	  Process the original Y dimension...
|	  ...
|     `| nmrPipe -fn ZTP \`
|	  ...
|	  Process the original Z dimension...
|	  ...
|     `| nmrPipe -fn ZTP \`
|     `| nmrPipe -fn ATP \`
|	  ...
|	  Process the original A dimension...
|	  ...
|     `| pipewash -fn SCRUB -pattern` *pattern_file* *options* `\`
|     `| nmrPipe -fn ATP \`
|     `| pipe2xyz -out` *output* `-x`

######		\fps

## Sampling Patterns

The *sampling-pattern* file should be a text file with one line for each sampling point.
Within each line, whitespace- and/or comma-separated numbers indicate the coordinates for
the sampling point, normally as integer multiples of the dwell time in each indirect
dimension for "on-grid" experiments.  There should be one column for each sparsely
sampled dimension.  For "off-grid" experiments, the coordinates should be given as
floating-point values denoting evolution times in seconds.  An additional column of
floating-point weighting values may be included after the coordinates.  Comment lines
beginning with the "#" character may be included.  Sampling patterns with extra columns
of numbers can be parsed using the special override options described below.

## Dimension Assignments

The matching of sampling pattern dimensions to experimental dimensions may need
adjustment using the `-x`, `-y`, and `-z` options.  It is recommended that you carefully
examine the information reported at the start of each calculation to verify that the
assignments are correct.

# OPTIONS

Options may be given with either one or two dashes (`-x u` and `--x u` are
both acceptable) and with either a space or an equal sign between the option name and
value (`--x u` and `--x=u` are both acceptable).  In this man page, we
follow NMRPipe custom and list options with a single dash and a space between the option
name and value.  For technical reasons, the program's usage information may display the
options in a different manner.

## General Options

-h, -help
:	Show usage information

-version
:	Show version information

-pattern *PATTERN*
:	The sampling pattern used to collect the data.

-q, -quiet
:	Suppress information about the configuration and progress of the calculation.

-l *LOGFILE*, -log *LOGFILE*
:	Record a log to the text file *LOGFILE*
	
-verbose-log
:	Record complete details of the calculation to the log file; without this option, more
	abbreviated information is recorded.

-output-components-only, --output-components-only
:	Output the reconstructed signals only, while omitting residual noise and unprocessed
	signals (for diagnostic purposes only).
	
-output-residuals-only, --output-residuals-only
:	Output the residuals only, while omitting the processed, reconstructed signals (for
	diagnostic purposes only).

	
## Dimension Assignment Options

Note: providing any of the following dimension assignment options turns off automatic
dimension assignment.  If you need to provide one of these options, go ahead and provide
the full set for all of the dimensions in the experiment.

-z *ASSIGNMENT*
:	Dimension assignment for the current Z dimension of the experiment, matching it to
	one of the sparse sampling dimensions in the sampling pattern (U, V, or W,
	corresponding to the first, second, or third column in pattern file, respectively).
	Valid *ASSIGNMENT* values are `u`, `v`, `w`.

-y *ASSIGNMENT*
:	Dimension assignment for the current Y dimension of the experiment, matching it to
	one of the sparse sampling dimensions in the sampling pattern (U, V, or W,
	corresponding to the first, second, or third column in pattern file, respectively).
	Valid *ASSIGNMENT* values are `u`, `v`, `w`.

-x *ASSIGNMENT*
:	Dimension assignment for the current X dimension of the experiment, matching it to
	one of the sparse sampling dimensions in the sampling pattern (U, V, or W,
	corresponding to the first, second, or third column in pattern file, respectively).
	Valid *ASSIGNMENT* values are `u`, `v`, `w`.

## Sampling Pattern Parsing Options

Note: providing one or more of the `-u-col`, `-v-col`, `-w-col`, or `-weight-col` 
options turns off automatic parsing of the sampling pattern.  In such cases, supply as
many of the other options as are needed to interpret the sampling pattern.

-u-col *COL*
:	The column of the sampling pattern containing the U dimension.  The first column is
	numbered 0, the second 1, etc.

-v-col *COL*
:	The column of the sampling pattern containing the V dimension, if applicable.  The
	first column is	numbered 0, the second 1, etc.

-w-col *COL*
:	The column of the sampling pattern containing the W dimension, if applicable.  The
	first column is	numbered 0, the second 1, etc.

-weight-col *COL*
:	The column of the sampling pattern containing the weighting information, if
	applicable.  The first column is numbered 0, the second 1, etc.
	
-off-grid
:	Interpret the sampling pattern as off-grid.

-ignore-weights
:	Ignore any weighting information found in the sampling pattern.

## SCRUB Options

-g *GAIN*, -gain *GAIN*
:	The gain, in percent.  The default is 10% for spectra with one or two sparse
	dimensions and 50% for those with three sparse dimensions.
	
-b *BASE*, -base *BASE*
:	Parameter indicating how far to continue subtraction during SCRUB processing.  SCRUB
	will continue to subtract until the estimated residual artifacts from all signals
	currently being processed are less than *BASE* times the current estimated noise
	level.  The default is 0.01.

## CLEAN Options

-g *GAIN*, -gain *GAIN*
:	The gain, in percent.  The default is 10% for spectra with one or two sparse
	dimensions and 50% for those with three sparse dimensions.
	
-snr-threshold *SNR*
:	Signal-to-noise stopping threshold: CLEAN stops when the signal-to-noise ratio is
	less than or equal to this many standard deviations of the noise.  Default is 5
	(=5 times the standard deviation of the noise).
	
-noise-change-threshold *CHANGE*
:	Noise-change threshold: CLEAN stops when the average noise level has not changed more
	than this percentage for 25 consecutive iterations.  Default is 5 (=5%).
	
-max-iter *ITER*
:	The maximum number of CLEAN iterations to allow for each position in the spectrum that
	is processed.  The default is unlimited iterations.

## Linearity and Pure Component Calculation Options

-linearity *MODE*, --linearity==*MODE*
:	The method used to scale reconstructed signals, where *MODE* may equal `etd` (the
	default), `fd`, or `td`.

-pure-comp-mode *MODE*
:	The method used to determine the pure component, where *MODE* may equal 
	`contour-irregular`, `contour-ellipsoid` (the default), or `fixed-ellipsoid`.
	
-pc-contour-irregular-level *LEVEL*
:	The contour level for the contour-irregular calculation.

-pc-contour-irregular-margin *MARGIN*
:	If *MARGIN* is nonzero, add a one-point margin around the contour; otherwise (the
	default), do not add a margin.

-pc-contour-ellipsoid-level *LEVEL*
:	The contour level for the contour-ellipsoid calculation.

-pc-contour-ellipsoid-margin *MARGIN*
:	Expand the ellipsoid by a margin of *MARGIN* percent beyond what is needed to
	circumscribe the contour.  Default is no margin.
	
-pc-fixed-ellipsoid-size *SIZE*
:	Size of the ellipsoid for fixed-ellipsoid calculations.  Each semiaxis is *SIZE*
	percent of the spectral width.

## PSF and Pure Component Output

-psf *PSFFILE*
:	Save the calculated PSF to the spectrum file *PSFFILE*

-pure-comp *PURECOMPFILE*
:	Save the calculated pure component to the spectrum file *PURECOMPFILE*

# SEE ALSO

scrub(1), clean(1)

HTML and PDF user's manuals are provided in the doc subdirectory of the *nmr_wash*
distribution, and at the nmr_wash web page.

# COPYRIGHT {.copyright-block}

Copyright (c) 2013, Brian E. Coggins.  All rights reserved.

