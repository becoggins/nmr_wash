%	Template for generating the nmr_wash man pages with pandoc
%	Intended for use with xelatex or luatex
%	Copyright (c) 2013, Brian E. Coggins.
%	Derived from the default pandoc LaTeX template.

\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript

%	Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}

%	Default fonts
\usepackage{fontspec}
\ifxetex
  \usepackage{xltxtra,xunicode}
\fi
\defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
\newcommand{\euro}{€}
\setmainfont[UprightFont={* Light}, BoldFont={* Regular}, BoldItalicFont={* Italic}, ItalicFont={* Light Italic}]{Roboto}
\setmonofont{Menlo}

%	Page geometry
\usepackage[top=1in, bottom=1in, left=0.75in, right=0.75in, headheight=15pt, footskip=36pt]{geometry}

%	Page headings
%	"overallheading" trick is needed due to titlesec clobbering \thetitle after each \section, \subsection, etc.
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\lhead{\overalltitle}
\rhead{\thepage}

%	Set up light gray color to be used for backgrounds in code blocks
\usepackage{color}
\definecolor{light-gray}{gray}{0.95}

%	Formatting of title page
\usepackage{titling}
\setlength{\droptitle}{24pt}
\pretitle{\thispagestyle{empty}\fontspec{Roboto Bold}\huge}
\posttitle{\par\vskip 1.5em}
\preauthor{\fontspec{Roboto Regular}\LARGE}
\postauthor{\par}
\predate{\fontspec{Roboto Regular}\LARGE}
\postdate{\clearpage}

%	Formatting of section headings
\usepackage{titlesec}
\titleformat{\section}[display]{\thispagestyle{fancy}\fontspec{Roboto Medium}\Large}{}{0pt}{}
\titlespacing{\section}{0pt}{12pt}{16pt}
\titleformat{\subsection}[hang]{\bfseries\large}{}{0pt}{}
\titlespacing{\subsection}{0pt}{12pt}{12pt}

%	Use microtype if available
\IfFileExists{microtype.sty}{\usepackage{microtype}}{}

%	Bibliography
$if(natbib)$
\usepackage{natbib}
\bibliographystyle{plainnat}
$endif$
$if(biblatex)$
\usepackage{biblatex}
$if(biblio-files)$
\bibliography{$biblio-files$}
$endif$
$endif$

%	Code listings
$if(listings)$
\usepackage{listings}
\lstset{breaklines=true} 
\lstset{numbers=none, numberstyle=\fontsize{9.75pt}{1.2em}\ttfamily, numbersep=10pt, captionpos=b} 
\lstset{backgroundcolor=\color{light-gray}}
\lstset{basicstyle=\fontsize{9.75pt}{1.2em}\ttfamily}
\lstset{framesep=4pt}
\lstset{aboveskip=12pt}
\lstset{belowskip=0pt}
$endif$
$if(lhs)$
\lstnewenvironment{code}{\lstset{language=Haskell,basicstyle=\small\ttfamily}}{}
$endif$
$if(highlighting-macros)$
$highlighting-macros$
$endif$

%	Special formatting for NMRPipe scripts
\usepackage{mdframed}
\newmdenv[backgroundcolor=light-gray, font=\fontsize{9.75pt}{1.2em}, rightline=false, leftline=false, topline=false, bottomline=false, innertopmargin=18pt]{pipescriptbox}
\newcommand{\sps}{\begin{pipescriptbox}}
\newcommand{\fps}{\end{pipescriptbox}}

%	Special formatting for the end of the acknowledgements
\newcommand{\sas}{\begin{flushright}\itshape}
\newcommand{\fas}{\end{flushright}}

%	Tables
$if(tables)$
\usepackage{longtable}
$endif$

%	Graphics
$if(graphics)$
\usepackage{graphicx}
% We will generate all images so they have a width \maxwidth. This means
% that they will get their normal width if they fit onto the page, but
% are scaled down if they would overflow the margins.
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth
\else\Gin@nat@width\fi}
\makeatother
\let\Oldincludegraphics\includegraphics
\renewcommand{\includegraphics}[1]{\Oldincludegraphics[width=\maxwidth]{#1}}
$endif$

%	Hyperlinks
\ifxetex
  \usepackage[setpagesize=false, % page size defined by xetex
              unicode=false, % unicode breaks when used with xetex
              xetex]{hyperref}
\else
  \usepackage[unicode=true]{hyperref}
\fi
\hypersetup{breaklinks=true,
            bookmarks=true,
            pdfauthor={$author-meta$},
            pdftitle={$title-meta$},
            colorlinks=true,
            urlcolor=$if(urlcolor)$$urlcolor$$else$blue$endif$,
            linkcolor=$if(linkcolor)$$linkcolor$$else$magenta$endif$,
            pdfborder={0 0 0}}
\urlstyle{same}  % don't use monospace font for urls
$if(links-as-notes)$
% Make links footnotes instead of hotlinks:
\renewcommand{\href}[2]{#2\footnote{\url{#1}}}
$endif$

%	Strikeout
$if(strikeout)$
\usepackage[normalem]{ulem}
% avoid problems with \sout in headers with hyperref:
\pdfstringdefDisableCommands{\renewcommand{\sout}{}}
$endif$

%	Paragraph formatting
\setlength{\parindent}{0pt}
\setlength{\parskip}{12pt plus 2pt minus 1pt}
%\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines

%	Section numbering
$if(numbersections)$
\setcounter{secnumdepth}{5}
$else$
\setcounter{secnumdepth}{0}
$endif$

%	Verbatim in footnotes
$if(verbatim-in-note)$
\VerbatimFootnotes % allows verbatim text in footnotes
$endif$

%	Language
$if(lang)$
\ifxetex
  \usepackage{polyglossia}
  \setmainlanguage{$mainlang$}
\else
  \usepackage[$lang$]{babel}
\fi
$endif$

%	CONTENT

$for(header-includes)$
$header-includes$
$endfor$

$if(title)$
\title{$title$}
\let\overalltitle\thetitle
$endif$
\author{$for(author)$$author$$sep$ \and $endfor$}
\date{$date$}

\begin{document}
$if(title)$
\maketitle
$endif$

$for(include-before)$
$include-before$

$endfor$
$if(toc)$
{
\hypersetup{linkcolor=black}
\setcounter{tocdepth}{$toc-depth$}
\tableofcontents
}
$endif$
$body$

$if(natbib)$
$if(biblio-files)$
$if(biblio-title)$
$if(book-class)$
\renewcommand\bibname{$biblio-title$}
$else$
\renewcommand\refname{$biblio-title$}
$endif$
$endif$
\bibliography{$biblio-files$}

$endif$
$endif$
$if(biblatex)$
\printbibliography$if(biblio-title)$[title=$biblio-title$]$endif$

$endif$
$for(include-after)$
$include-after$

$endfor$
\end{document}
