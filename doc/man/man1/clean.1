.TH CLEAN 1 "Version 1.0.0, November 2013" "Man Page"
.SH NAME
.PP
clean \- removes artifacts from NMR spectra acquired with sparse
sampling
.SH SYNPOSIS
.PP
clean \f[I]sampling\-pattern\f[] \f[I]input\-file\f[]
[\f[I]output\-file\f[]] [\f[I]options\f[]]
.SH DESCRIPTION
.PP
The \f[I]clean\f[] program uses the CLEAN algorithm to remove sampling
artifacts from NMR spectra acquired with sparse sampling.
For most applications, SCRUB is a better choice, and we recommend using
it instead.
.PP
The \f[I]sampling\-pattern\f[] file should be a text file with one line
for each sampling point.
Within each line, whitespace\- and/or comma\-separated numbers indicate
the coordinates for the sampling point, normally as integer multiples of
the dwell time in each indirect dimension for "on\-grid" experiments.
There should be one column for each sparsely sampled dimension.
For "off\-grid" experiments, the coordinates should be given as
floating\-point values denoting evolution times in seconds.
An additional column of floating\-point weighting values may be included
after the coordinates.
Comment lines beginning with the "#" character may be included.
Sampling patterns with extra columns of numbers can be parsed using the
special override options described below.
.PP
The \f[I]input\-file\f[] may be in NMRPipe, NMRView, SPARKY/UCSF, or
XEASY format; \f[I]clean\f[] will identify the format from the file
extension.
.PP
To process the input data in\-place, use the \f[C]\-\-in\-place\f[]
option; otherwise, provide an \f[I]output\-file\f[] to receive the
processed data.
This file is created if it does not exist, with the format determined
from the \f[I]output\-file\f[]\[aq]s file extension; the output format
need not be the same as the input format.
If the \f[I]output\-file\f[] already exists, the
\f[C]\-w/\-\-overwrite\f[] option must be given to indicate that it is
safe to overwrite it.
.PP
By default, \f[I]clean\f[] assumes that the dimensions of the experiment
correspond to the dimensions of the sampling pattern, with the F1
dimension matching to the first column (called "U" by \f[I]clean\f[]),
F2 matching to the second column ("V"), etc.
Any leftover experimental dimensions are treated as non\-sparse.
This mapping can be overridden using the dimension assignment options
described below.
.SH OPTIONS
.PP
Options may be given with either one or two dashes
(\f[C]\-threads\ 3\f[] and \f[C]\-\-threads\ 3\f[] are both acceptable)
and with either a space or an equal sign between the option name and
value (\f[C]\-\-threads\ 3\f[] and \f[C]\-\-threads=3\f[] are both
acceptable).
In this man page, we follow custom and separate the option name and
value by a space when there is one dash and by an equal sign when there
are two.
.SS General Options
.TP
.B \-h, \-help, \-\-help
Show usage information
.RS
.RE
.TP
.B \-\-version
Show version information
.RS
.RE
.TP
.B \-\-in\-place
Process the input spectrum in\-place.
.RS
.RE
.TP
.B \-w, \-overwrite, \-\-overwrite
Overwrite an existing output file with the same name.
.RS
.RE
.TP
.B \-q, \-quiet, \-\-quiet
Suppress information about the configuration and progress of the
calculation.
.RS
.RE
.TP
.B \-t \f[I]THREADS\f[], \-threads \f[I]THREADS\f[], \-\-threads
\f[I]THREADS\f[]
Use \f[I]THREADS\f[] threads for parallel computation.
The default is to set the number of threads to match the number of
processor cores.
.RS
.RE
.SS Dimension Assignment Options
.PP
Note: providing any of the following dimension assignment options turns
off automatic dimension assignment.
If you need to provide one of these options, go ahead and provide the
full set for all of the dimensions in the experiment.
.TP
.B \-1 \f[I]ASSIGNMENT\f[], \-f1 \f[I]ASSIGNMENT\f[],
\-\-f1=\f[I]ASSIGNMENT\f[]
Dimension assignment for the F1 dimension of the experiment, matching it
to one of the sparse sampling dimensions in the sampling pattern (U, V,
or W, corresponding to the first, second, or third column in pattern
file, respectively) or indicating that it is not sparse (an "index"
dimension).
Valid \f[I]ASSIGNMENT\f[] values are \f[C]u\f[], \f[C]v\f[], \f[C]w\f[],
and \f[C]index\f[].
.RS
.RE
.TP
.B \-2 \f[I]ASSIGNMENT\f[], \-f2 \f[I]ASSIGNMENT\f[],
\-\-f2=\f[I]ASSIGNMENT\f[]
Dimension assignment for the F2 dimension of the experiment, matching it
to one of the sparse sampling dimensions in the sampling pattern (U, V,
or W, corresponding to the first, second, or third column in pattern
file, respectively) or indicating that it is not sparse (an "index"
dimension).
Valid \f[I]ASSIGNMENT\f[] values are \f[C]u\f[], \f[C]v\f[], \f[C]w\f[],
and \f[C]index\f[].
.RS
.RE
.TP
.B \-3 \f[I]ASSIGNMENT\f[], \-f3 \f[I]ASSIGNMENT\f[],
\-\-f3=\f[I]ASSIGNMENT\f[]
Dimension assignment for the F3 dimension of the experiment, matching it
to one of the sparse sampling dimensions in the sampling pattern (U, V,
or W, corresponding to the first, second, or third column in pattern
file, respectively) or indicating that it is not sparse (an "index"
dimension).
Valid \f[I]ASSIGNMENT\f[] values are \f[C]u\f[], \f[C]v\f[], \f[C]w\f[],
and \f[C]index\f[].
.RS
.RE
.TP
.B \-4 \f[I]ASSIGNMENT\f[], \-f4 \f[I]ASSIGNMENT\f[],
\-\-f4=\f[I]ASSIGNMENT\f[]
Dimension assignment for the F4 dimension of the experiment, matching it
to one of the sparse sampling dimensions in the sampling pattern (U, V,
or W, corresponding to the first, second, or third column in pattern
file, respectively) or indicating that it is not sparse (an "index"
dimension).
Valid \f[I]ASSIGNMENT\f[] values are \f[C]u\f[], \f[C]v\f[], \f[C]w\f[],
and \f[C]index\f[].
.RS
.RE
.SS Dimension Apodization Information
.TP
.B \-f1\-apod \f[I]APOD\f[], \-\-f1\-apod=\f[I]APOD\f[]
Apodization information for the F1 dimension.
Required for each indirect dimension that was apodized during
post\-processing, \f[I]unless\f[] the data are in NMRPipe format, in
which case this information can be extracted from the file header
automatically.
Specify the apodization function by putting the corresponding NMRPipe
command in quotes, e.g.
.RS
.IP
.nf
\f[C]
\-\-f1\-apod="\-fn\ EM\ \-lb\ 10\ \-c\ 0.5"
\f[]
.fi
.PP
for an exponential function with 10 Hz line\-broadening and a
first\-point correction of 0.5.
.RE
.TP
.B \-f2\-apod \f[I]APOD\f[], \-\-f2\-apod=\f[I]APOD\f[]
Apodization information for the F2 dimension.
Required for each indirect dimension that was apodized during
post\-processing, \f[I]unless\f[] the data are in NMRPipe format, in
which case this information can be extracted from the file header
automatically.
Specify the apodization function by putting the corresponding NMRPipe
command in quotes, e.g.
.RS
.IP
.nf
\f[C]
\-\-f2\-apod="\-fn\ EM\ \-lb\ 10\ \-c\ 0.5"
\f[]
.fi
.PP
for an exponential function with 10 Hz line\-broadening and a
first\-point correction of 0.5.
.RE
.TP
.B \-f3\-apod \f[I]APOD\f[], \-\-f3\-apod=\f[I]APOD\f[]
Apodization information for the F3 dimension.
Required for each indirect dimension that was apodized during
post\-processing, \f[I]unless\f[] the data are in NMRPipe format, in
which case this information can be extracted from the file header
automatically.
Specify the apodization function by putting the corresponding NMRPipe
command in quotes, e.g.
.RS
.IP
.nf
\f[C]
\-\-f3\-apod="\-fn\ EM\ \-lb\ 10\ \-c\ 0.5"
\f[]
.fi
.PP
for an exponential function with 10 Hz line\-broadening and a
first\-point correction of 0.5.
.RE
.TP
.B \-f4\-apod \f[I]APOD\f[], \-\-f4\-apod=\f[I]APOD\f[]
Apodization information for the F4 dimension.
Required for each indirect dimension that was apodized during
post\-processing, \f[I]unless\f[] the data are in NMRPipe format, in
which case this information can be extracted from the file header
automatically.
Specify the apodization function by putting the corresponding NMRPipe
command in quotes, e.g.
.RS
.IP
.nf
\f[C]
\-\-f4\-apod="\-fn\ EM\ \-lb\ 10\ \-c\ 0.5"
\f[]
.fi
.PP
for an exponential function with 10 Hz line\-broadening and a
first\-point correction of 0.5.
.RE
.TP
.B \-f1\-fpc \f[I]FPC\f[], \-\-f1\-fpc=\f[I]FPC\f[]
First\-point correction for the F1 dimension, if a correction is needed
but not otherwise specified either in the file header or an f1\-apod
flag.
\f[I]FPC\f[] should be a number, typically 0.5.
.RS
.RE
.TP
.B \-f2\-fpc \f[I]FPC\f[], \-\-f2\-fpc=\f[I]FPC\f[]
First\-point correction for the F2 dimension, if a correction is needed
but not otherwise specified either in the file header or an f2\-apod
flag.
\f[I]FPC\f[] should be a number, typically 0.5.
.RS
.RE
.TP
.B \-f3\-fpc \f[I]FPC\f[], \-\-f3\-fpc=\f[I]FPC\f[]
First\-point correction for the F3 dimension, if a correction is needed
but not otherwise specified either in the file header or an f3\-apod
flag.
\f[I]FPC\f[] should be a number, typically 0.5.
.RS
.RE
.TP
.B \-f4\-fpc \f[I]FPC\f[], \-\-f4\-fpc=\f[I]FPC\f[]
First\-point correction for the F4 dimension, if a correction is needed
but not otherwise specified either in the file header or an f4\-apod
flag.
\f[I]FPC\f[] should be a number, typically 0.5.
.RS
.RE
.SS Sampling Pattern Parsing Options
.PP
Note: providing one or more of the \f[C]\-\-u\-col\f[],
\f[C]\-\-v\-col\f[], \f[C]\-\-w\-col\f[], or \f[C]\-\-weight\-col\f[]
options turns off automatic parsing of the sampling pattern.
In such cases, supply as many of the other options as are needed to
interpret the sampling pattern.
.TP
.B \-u\-col \f[I]COL\f[], \-\-u\-col=\f[I]COL\f[]
The column of the sampling pattern containing the U dimension.
The first column is numbered 0, the second 1, etc.
.RS
.RE
.TP
.B \-v\-col \f[I]COL\f[], \-\-v\-col=\f[I]COL\f[]
The column of the sampling pattern containing the V dimension, if
applicable.
The first column is numbered 0, the second 1, etc.
.RS
.RE
.TP
.B \-w\-col \f[I]COL\f[], \-\-w\-col=\f[I]COL\f[]
The column of the sampling pattern containing the W dimension, if
applicable.
The first column is numbered 0, the second 1, etc.
.RS
.RE
.TP
.B \-weight\-col \f[I]COL\f[], \-\-weight\-col=\f[I]COL\f[]
The column of the sampling pattern containing the weighting information,
if applicable.
The first column is numbered 0, the second 1, etc.
.RS
.RE
.TP
.B \-off\-grid, \-\-off\-grid
Interpret the sampling pattern as off\-grid.
.RS
.RE
.TP
.B \-ignore\-weights, \-\-ignore\-weights
Ignore any weighting information found in the sampling pattern.
.RS
.RE
.SS Options for Calculating Only Part of an Input Spectrum
.TP
.B \-position \f[I]POS1\f[] [additional \f[I]POS\f[]],
\-\-position=\f[I]POS\f[] [additional \f[I]POS\f[]]
Instead of processing the entire input spectrum, process one or more
specific vectors, planes, or cubes (depending on the number of sparse
indirect dimensions).
Since each position on the index dimension(s) is independent in terms of
artifacts from all other index dimension positions, one can choose
individual index dimension positions for processing.
.RS
.PP
Specify a position by giving one integer for each index dimension in the
spectrum, separated by commas but no white space, where each integer
designates a data point ranging from zero at the low\-frequency end to
the number of points on that dimension minus one at the high\-frequency
end.
The dimensions should be listed in the order of least\-frequently
changing to most\-frequently changing.
.PP
To process multiple positions, repeat this option for each position to
calculate (e.g.
\f[C]\-position\ 10\ \-position\ 14\f[]) OR list the positions,
separated by spaces, after the flag (e.g.
\f[C]\-position\ 10\ 14\f[]).
.PP
Example: in a 3\-D spectrum with two sparse dimensions, process the 8th
F1/F2 plane:
.IP
.nf
\f[C]
\-position\ 7
\f[]
.fi
.PP
Example: in a 4\-D spectrum with three sparse dimensions, process the
63rd and 64th F1/F2/F3 cubes:
.IP
.nf
\f[C]
\-position\ 62\ 63
\f[]
.fi
.PP
Example: in a 4\-D spectrum where F2 and F3 are sparse and F1 and F4 are
conventional, process the F2/F3 planes numbered 14\-16 at F4 position 9:
.IP
.nf
\f[C]
\-position\ 14,9\ 15,9\ 16,9
\f[]
.fi
.RE
.TP
.B \-insert\-in\-place, \-\-insert\-in\-place
When processing specific positions, write the results either:
.RS
.IP \[bu] 2
back to the input file, overwriting the original data, if
\f[C]\-\-in\-place\f[] is also provided on the command\-line
.IP \[bu] 2
to the corresponding locations in \f[I]output\-file\f[], creating the
file if need be, otherwise overwriting the existing data
.RE
.TP
.B \-separate\-outputs, \-\-separate\-outputs
When processing specific positions, write the result for each position
into a separate lower\-dimensional file.
These files are named based on the \f[I]output\-file\f[] parameter, with
an underscore and one or more position numbers added.
.RS
.RE
.SS CLEAN Options
.TP
.B \-g \f[I]GAIN\f[], \-gain \f[I]GAIN\f[], \-\-gain=\f[I]GAIN\f[]
The gain, in percent.
The default is 10% for spectra with one or two sparse dimensions and 50%
for those with three sparse dimensions.
.RS
.RE
.TP
.B \-snr\-threshold \f[I]SNR\f[], \-\-snr\-threshold=\f[I]SNR\f[]
Signal\-to\-noise stopping threshold: CLEAN stops when the
signal\-to\-noise ratio is less than or equal to this many standard
deviations of the noise.
Default is 5 (=5 times the standard deviation of the noise).
.RS
.RE
.TP
.B \-noise\-change\-threshold \f[I]CHANGE\f[],
\-\-noise\-change\-threshold=\f[I]CHANGE\f[]
Noise\-change threshold: CLEAN stops when the average noise level has
not changed more than this percentage for 25 consecutive iterations.
Default is 5 (=5%).
.RS
.RE
.TP
.B \-max\-iter \f[I]ITER\f[], \-\-max\-iter=\f[I]ITER\f[]
The maximum number of CLEAN iterations to allow for each position in the
spectrum that is processed.
The default is unlimited iterations.
.RS
.RE
.SS Pure Component Calculation Options
.TP
.B \-pure\-comp\-mode \f[I]MODE\f[], \-\-pure\-comp\-mode=\f[I]MODE\f[]
The method used to determine the pure component, where \f[I]MODE\f[] may
equal \f[C]contour\-irregular\f[], \f[C]contour\-ellipsoid\f[] (the
default), or \f[C]fixed\-ellipsoid\f[].
.RS
.RE
.TP
.B \-pc\-contour\-irregular\-level \f[I]LEVEL\f[],
\-\-pc\-contour\-irregular\-level=\f[I]LEVEL\f[]
The contour level for the contour\-irregular calculation.
.RS
.RE
.TP
.B \-pc\-contour\-irregular\-margin \f[I]MARGIN\f[],
\-\-pc\-contour\-irregular\-margin=\f[I]MARGIN\f[]
If \f[I]MARGIN\f[] is nonzero, add a one\-point margin around the
contour; otherwise (the default), do not add a margin.
.RS
.RE
.TP
.B \-pc\-contour\-ellipsoid\-level \f[I]LEVEL\f[],
\-\-pc\-contour\-ellipsoid\-level=\f[I]LEVEL\f[]
The contour level for the contour\-ellipsoid calculation.
.RS
.RE
.TP
.B \-pc\-contour\-ellipsoid\-margin \f[I]MARGIN\f[],
\-\-pc\-contour\-ellipsoid\-margin=\f[I]MARGIN\f[]
Expand the ellipsoid by a margin of \f[I]MARGIN\f[] percent beyond what
is needed to circumscribe the contour.
Default is no margin.
.RS
.RE
.TP
.B \-pc\-fixed\-ellipsoid\-size \f[I]SIZE\f[],
\-\-pc\-fixed\-ellipsoid\-size=\f[I]SIZE\f[]
Size of the ellipsoid for fixed\-ellipsoid calculations.
Each semiaxis is \f[I]SIZE\f[] percent of the spectral width.
.RS
.RE
.SS PSF and Pure Component Output
.TP
.B \-psf \f[I]PSFFILE\f[], \-\-psf=\f[I]PSFFILE\f[]
Save the calculated PSF to the spectrum file \f[I]PSFFILE\f[]
.RS
.RE
.TP
.B \-pure\-comp \f[I]PURECOMPFILE\f[],
\-\-pure\-comp=\f[I]PURECOMPFILE\f[]
Save the calculated pure component to the spectrum file
\f[I]PURECOMPFILE\f[]
.RS
.RE
.SS Diagnostic Options
.TP
.B \-l \f[I]LOGFILE\f[], \-log \f[I]LOGFILE\f[],
\-\-log=\f[I]LOGFILE\f[]
Record a log to the text file \f[I]LOGFILE\f[]
.RS
.RE
.TP
.B \-verbose\-log, \-\-verbose\-log
Record complete details of the calculation to the log file; without this
option, more abbreviated information is recorded.
.RS
.RE
.TP
.B \-r \f[I]REPORT\f[], \-noise\-report\-csv \f[I]REPORT\f[],
\-\-noise\-report\-csv=\f[I]REPORT\f[]
Record a report on noise (artifact) reduction to a CSV file
\f[I]REPORT\f[]
.RS
.RE
.SH SEE ALSO
.PP
scrub(1), pipewash(1)
.PP
HTML and PDF user\[aq]s manuals are provided in the doc subdirectory of
the \f[I]nmr_wash\f[] distribution, and at the nmr_wash web page.
.SH COPYRIGHT
.PP
Copyright (c) 2013, Brian E.
Coggins.
All rights reserved.
.SH AUTHORS
Brian E. Coggins.
