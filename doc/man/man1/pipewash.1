.TH PIPEWASH 1 "Version 1.0.0, November 2013" "Man Page"
.SH NAME
.PP
pipewash \- NMRPipe plug\-in to remove artifacts from NMR spectra
acquired with sparse sampling
.SH SYNPOSIS
.PP
pipewash \-fn SCRUB \-pattern \f[I]sampling\-pattern\f[]
[\f[I]options\f[]]
.PP
pipewash \-fn CLEAN \-pattern \f[I]sampling\-pattern\f[]
[\f[I]options\f[]]
.SH DESCRIPTION
.PP
\f[I]pipewash\f[] is an NMRPipe plug\-in providing access to the SCRUB
and CLEAN algorithms for removing sampling artifacts from NMR spectra
acquired with sparse sampling.
\f[I]pipewash\f[] can be inserted directly into NMRPipe processing
scripts and the appropriate function (either "SCRUB" or "CLEAN") called
to carry out artifact suppression.
.SS Processing Scripts
.PP
The recommended way to use \f[I]pipewash\f[] in the processing script
for a 3\-D NMR spectrum is according to the following pattern:
.PP
.SS 
.PP
\ \ \ \ \f[C]xyz2pipe\ \-in\f[] \f[I]input\f[] \f[C]\-x\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ Process the original X dimension...
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ TP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ Process the original Y dimension...
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ TP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ ZTP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ Process the original Z dimension...
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ pipewash\ \-fn\ SCRUB\ \-pattern\f[]
\f[I]pattern_file\f[] \f[I]options\f[] \f[C]\\\f[]
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ ZTP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ pipe2xyz\ \-out\f[] \f[I]output\f[] \f[C]\-x\f[]
.SS 
.PP
The \f[I]pipewash\f[] processing functions require that the dimensions
be transposed from their normal order, shifting the indirect dimensions
into the X, Y, and (when applicable) Z positions.
This is the effect of the \f[C]ZTP\f[] function in the example script
above.
.PP
For a 4\-D spectrum, follow the same approach, but use ATP before and
after SCRUB:
.PP
.SS 
.PP
\ \ \ \ \f[C]xyz2pipe\ \-in\f[] \f[I]input\f[] \f[C]\-x\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ Process the original X dimension...
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ TP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ Process the original Y dimension...
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ ZTP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ Process the original Z dimension...
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ ZTP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ ATP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ Process the original A dimension...
.PD 0
.P
.PD
\ \ \ \ ...
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ pipewash\ \-fn\ SCRUB\ \-pattern\f[]
\f[I]pattern_file\f[] \f[I]options\f[] \f[C]\\\f[]
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ nmrPipe\ \-fn\ ATP\ \\\f[]
.PD 0
.P
.PD
\ \ \ \ \f[C]|\ pipe2xyz\ \-out\f[] \f[I]output\f[] \f[C]\-x\f[]
.SS 
.SS Sampling Patterns
.PP
The \f[I]sampling\-pattern\f[] file should be a text file with one line
for each sampling point.
Within each line, whitespace\- and/or comma\-separated numbers indicate
the coordinates for the sampling point, normally as integer multiples of
the dwell time in each indirect dimension for "on\-grid" experiments.
There should be one column for each sparsely sampled dimension.
For "off\-grid" experiments, the coordinates should be given as
floating\-point values denoting evolution times in seconds.
An additional column of floating\-point weighting values may be included
after the coordinates.
Comment lines beginning with the "#" character may be included.
Sampling patterns with extra columns of numbers can be parsed using the
special override options described below.
.SS Dimension Assignments
.PP
The matching of sampling pattern dimensions to experimental dimensions
may need adjustment using the \f[C]\-x\f[], \f[C]\-y\f[], and
\f[C]\-z\f[] options.
It is recommended that you carefully examine the information reported at
the start of each calculation to verify that the assignments are
correct.
.SH OPTIONS
.PP
Options may be given with either one or two dashes (\f[C]\-x\ u\f[] and
\f[C]\-\-x\ u\f[] are both acceptable) and with either a space or an
equal sign between the option name and value (\f[C]\-\-x\ u\f[] and
\f[C]\-\-x=u\f[] are both acceptable).
In this man page, we follow NMRPipe custom and list options with a
single dash and a space between the option name and value.
For technical reasons, the program\[aq]s usage information may display
the options in a different manner.
.SS General Options
.TP
.B \-h, \-help
Show usage information
.RS
.RE
.TP
.B \-version
Show version information
.RS
.RE
.TP
.B \-pattern \f[I]PATTERN\f[]
The sampling pattern used to collect the data.
.RS
.RE
.TP
.B \-q, \-quiet
Suppress information about the configuration and progress of the
calculation.
.RS
.RE
.TP
.B \-l \f[I]LOGFILE\f[], \-log \f[I]LOGFILE\f[]
Record a log to the text file \f[I]LOGFILE\f[]
.RS
.RE
.TP
.B \-verbose\-log
Record complete details of the calculation to the log file; without this
option, more abbreviated information is recorded.
.RS
.RE
.SS Dimension Assignment Options
.PP
Note: providing any of the following dimension assignment options turns
off automatic dimension assignment.
If you need to provide one of these options, go ahead and provide the
full set for all of the dimensions in the experiment.
.TP
.B \-z \f[I]ASSIGNMENT\f[]
Dimension assignment for the current Z dimension of the experiment,
matching it to one of the sparse sampling dimensions in the sampling
pattern (U, V, or W, corresponding to the first, second, or third column
in pattern file, respectively).
Valid \f[I]ASSIGNMENT\f[] values are \f[C]u\f[], \f[C]v\f[], \f[C]w\f[].
.RS
.RE
.TP
.B \-y \f[I]ASSIGNMENT\f[]
Dimension assignment for the current Y dimension of the experiment,
matching it to one of the sparse sampling dimensions in the sampling
pattern (U, V, or W, corresponding to the first, second, or third column
in pattern file, respectively).
Valid \f[I]ASSIGNMENT\f[] values are \f[C]u\f[], \f[C]v\f[], \f[C]w\f[].
.RS
.RE
.TP
.B \-x \f[I]ASSIGNMENT\f[]
Dimension assignment for the current X dimension of the experiment,
matching it to one of the sparse sampling dimensions in the sampling
pattern (U, V, or W, corresponding to the first, second, or third column
in pattern file, respectively).
Valid \f[I]ASSIGNMENT\f[] values are \f[C]u\f[], \f[C]v\f[], \f[C]w\f[].
.RS
.RE
.SS Sampling Pattern Parsing Options
.PP
Note: providing one or more of the \f[C]\-u\-col\f[], \f[C]\-v\-col\f[],
\f[C]\-w\-col\f[], or \f[C]\-weight\-col\f[] options turns off automatic
parsing of the sampling pattern.
In such cases, supply as many of the other options as are needed to
interpret the sampling pattern.
.TP
.B \-u\-col \f[I]COL\f[]
The column of the sampling pattern containing the U dimension.
The first column is numbered 0, the second 1, etc.
.RS
.RE
.TP
.B \-v\-col \f[I]COL\f[]
The column of the sampling pattern containing the V dimension, if
applicable.
The first column is numbered 0, the second 1, etc.
.RS
.RE
.TP
.B \-w\-col \f[I]COL\f[]
The column of the sampling pattern containing the W dimension, if
applicable.
The first column is numbered 0, the second 1, etc.
.RS
.RE
.TP
.B \-weight\-col \f[I]COL\f[]
The column of the sampling pattern containing the weighting information,
if applicable.
The first column is numbered 0, the second 1, etc.
.RS
.RE
.TP
.B \-off\-grid
Interpret the sampling pattern as off\-grid.
.RS
.RE
.TP
.B \-ignore\-weights
Ignore any weighting information found in the sampling pattern.
.RS
.RE
.SS SCRUB Options
.TP
.B \-g \f[I]GAIN\f[], \-gain \f[I]GAIN\f[]
The gain, in percent.
The default is 10% for spectra with one or two sparse dimensions and 50%
for those with three sparse dimensions.
.RS
.RE
.TP
.B \-b \f[I]BASE\f[], \-base \f[I]BASE\f[]
Parameter indicating how far to continue subtraction during SCRUB
processing.
SCRUB will continue to subtract until the estimated residual artifacts
from all signals currently being processed are less than \f[I]BASE\f[]
times the current estimated noise level.
The default is 0.01.
.RS
.RE
.SS CLEAN Options
.TP
.B \-g \f[I]GAIN\f[], \-gain \f[I]GAIN\f[]
The gain, in percent.
The default is 10% for spectra with one or two sparse dimensions and 50%
for those with three sparse dimensions.
.RS
.RE
.TP
.B \-snr\-threshold \f[I]SNR\f[]
Signal\-to\-noise stopping threshold: CLEAN stops when the
signal\-to\-noise ratio is less than or equal to this many standard
deviations of the noise.
Default is 5 (=5 times the standard deviation of the noise).
.RS
.RE
.TP
.B \-noise\-change\-threshold \f[I]CHANGE\f[]
Noise\-change threshold: CLEAN stops when the average noise level has
not changed more than this percentage for 25 consecutive iterations.
Default is 5 (=5%).
.RS
.RE
.TP
.B \-max\-iter \f[I]ITER\f[]
The maximum number of CLEAN iterations to allow for each position in the
spectrum that is processed.
The default is unlimited iterations.
.RS
.RE
.SS Pure Component Calculation Options
.TP
.B \-pure\-comp\-mode \f[I]MODE\f[]
The method used to determine the pure component, where \f[I]MODE\f[] may
equal \f[C]contour\-irregular\f[], \f[C]contour\-ellipsoid\f[] (the
default), or \f[C]fixed\-ellipsoid\f[].
.RS
.RE
.TP
.B \-pc\-contour\-irregular\-level \f[I]LEVEL\f[]
The contour level for the contour\-irregular calculation.
.RS
.RE
.TP
.B \-pc\-contour\-irregular\-margin \f[I]MARGIN\f[]
If \f[I]MARGIN\f[] is nonzero, add a one\-point margin around the
contour; otherwise (the default), do not add a margin.
.RS
.RE
.TP
.B \-pc\-contour\-ellipsoid\-level \f[I]LEVEL\f[]
The contour level for the contour\-ellipsoid calculation.
.RS
.RE
.TP
.B \-pc\-contour\-ellipsoid\-margin \f[I]MARGIN\f[]
Expand the ellipsoid by a margin of \f[I]MARGIN\f[] percent beyond what
is needed to circumscribe the contour.
Default is no margin.
.RS
.RE
.TP
.B \-pc\-fixed\-ellipsoid\-size \f[I]SIZE\f[]
Size of the ellipsoid for fixed\-ellipsoid calculations.
Each semiaxis is \f[I]SIZE\f[] percent of the spectral width.
.RS
.RE
.SS PSF and Pure Component Output
.TP
.B \-psf \f[I]PSFFILE\f[]
Save the calculated PSF to the spectrum file \f[I]PSFFILE\f[]
.RS
.RE
.TP
.B \-pure\-comp \f[I]PURECOMPFILE\f[]
Save the calculated pure component to the spectrum file
\f[I]PURECOMPFILE\f[]
.RS
.RE
.SH SEE ALSO
.PP
scrub(1), clean(1)
.PP
HTML and PDF user\[aq]s manuals are provided in the doc subdirectory of
the \f[I]nmr_wash\f[] distribution, and at the nmr_wash web page.
.SH COPYRIGHT
.PP
Copyright (c) 2013, Brian E.
Coggins.
All rights reserved.
.SH AUTHORS
Brian E. Coggins.
