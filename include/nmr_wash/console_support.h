//  Copyright (c) 2012, Brian E. Coggins, Ph.D.  All rights reserved.

#ifndef NMR_WASH_CONSOLE_SUPPORT
#define NMR_WASH_CONSOLE_SUPPORT

#include <stdio.h>
#include <vector>
#include <string>
#include <exception>
#include <boost/exception/all.hpp>
#include <boost/any.hpp>
#include <nmrdata/nmrdata.h>
#include <bec_misc/becmisc.h>
#include "nmr_wash.h"


std::vector< std::vector< int > > parse_position_list( std::vector< std::string > pos_list_str, int num_of_index_dims );
std::vector< std::string > generate_position_filenames( std::vector< std::vector< int > > pos_list, std::string output_filename );
void create_position_files( std::vector< std::string > output_filenames, BEC_Misc::owned_ptr_vector< BEC_NMRData::NMRData > &output_files, const BEC_NMRData::NMRData &input, nmr_wash::dimension_map dm, bool overwrite );


template< typename StatusReporterClass, typename JobInfoClass >
class console_status
{
public:
    console_status( StatusReporterClass &sr_ );
    
    void operator()();
    
private:
    bool init;
    int char_count;
    StatusReporterClass &sr;
};

typedef console_status< nmr_wash::status_reporter_scrub, nmr_wash::job_info_scrub > console_status_scrub;
typedef console_status< nmr_wash::status_reporter_clean, nmr_wash::job_info_clean > console_status_clean;

class log_relay
{
    template< typename T > friend log_relay & operator<<( log_relay &l, T value );
    friend log_relay & operator<<( log_relay &l, std::ostream & ( *manip )( std::ostream & ) );
    
public:
    log_relay( FILE* log_, std::ostream &cstr_, bool quiet_ ) : log( log_ ), cstr( cstr_ ), quiet( quiet_ ) {  }
    
private:
    bool quiet;
    FILE *log;
    std::stringstream ss;
    std::ostream &cstr;
};

template< typename T >
log_relay & operator<<( log_relay &l, T value )
{
    if( !l.quiet ) l.cstr << value;
    
    if( l.log )
    {
        l.ss.str( std::string( "" ) );
        l.ss << value;
        fprintf( l.log, "%s", l.ss.str().c_str() );
    }
    
    return l;
}

log_relay & operator<<( log_relay &l, std::ostream & ( *manip )( std::ostream & ) );

struct invalid_pos_string : virtual std::exception, virtual boost::exception
{
    invalid_pos_string( std::string pos_str_ ) : pos_str( pos_str_ ) {  }
    virtual ~invalid_pos_string() throw() {  }
    
    virtual const char *what() const throw()
    {
        return "One or more of the supplied position strings were invalid.";
    }
    
    std::string pos_str;
};

struct file_exists : virtual std::exception, virtual boost::exception
{
    file_exists( std::string filename_ ) : filename( filename_ ) {  }
    virtual ~file_exists() throw() {  }
    
    virtual const char *what() const throw()
    {
        return "The requested output file already exists, and the overwrite option was not given.";
    }
    
    std::string filename;
};

namespace nmr_wash
{
    void validate( boost::any &v, const std::vector< std::string > &values, nmr_wash::dimension_apod *target_type, int dummy );
}

#endif
