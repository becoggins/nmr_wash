//  Copyright (c) 2012, Brian E. Coggins, Ph.D.  All rights reserved.

#ifndef NMR_WASH_DATA_GENERATOR
#define NMR_WASH_DATA_GENERATOR

#include <complex>
#include <boost/shared_array.hpp>
#include "nmr_wash.h"

class data_generator
{
public:
    data_generator( const nmr_wash::sampling_pattern &sp_, nmr_wash::dimension_map dm_, int xsize_, int ysize_, int zsize_, unsigned int seed_, nmr_wash::pure_component_modes pure_component_mode, float pc_param_a, float pc_param_b );
    data_generator( const nmr_wash::sampling_pattern &sp_, nmr_wash::dimension_map dm_, int xsize_, int ysize_, int zsize_, unsigned int seed_, boost::shared_array< float > env_ );
    ~data_generator();
    
    boost::shared_array< float > get_env() const;
    
    void add_signal( float xfreq = 0.0F, float yfreq = 0.0F, float zfreq = 0.0F, float intensity = 1.0F, float xtau = 0.0F, float ytau = 0.0F, float ztau = 0.0F );
    void add_noise( float mean = 0.0F, float std_dev = 1.0F );
    boost::shared_array< float > fft_sparse();
    boost::shared_array< float > fft_full();
    
private:
    int xsize;
    int ysize;
    int zsize;
    size_t size;
    const nmr_wash::sampling_pattern &sp;
    nmr_wash::dimension_map dm;
    unsigned int seed;
    
    std::complex< float > *sparse_input;
    std::complex< float > *full_input;
    boost::shared_array< float > env;
};

void make_snapshot( const float *data, std::string filename, int dims, int xs, int ys, int zs, float scale );

class z_snapshot_recorder
{
public:
    z_snapshot_recorder( std::string filename_stem_, int xsize_, int ysize_, int zsize_, int ypos_, int interval_ );
    
    void operator()( bool new_outer, float current_threshold, float noise_floor, float noise_std_dev, float clean_level, size_t accepted_positions, size_t components, const float * data );
    
    void set_y_pos( int new_y_pos );
    
private:
    std::string filename_stem;
    int xsize;
    int ysize;
    int zsize;
    int ypos;
    int dims;
    
    size_t count;
    size_t outer;
    size_t middle;
    int interval;
};

#endif
