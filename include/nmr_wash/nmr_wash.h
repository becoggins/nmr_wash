//  Copyright (c) 2006-2014 Brian E. Coggins, Ph.D.  All rights reserved.


#ifndef NMR_WASH_INCLUDED
#define NMR_WASH_INCLUDED

#include <stdio.h>
#include <stddef.h>
#include <vector>
#include <set>
#include <map>
#include <deque>
#include <string>
#include <algorithm>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/exception_ptr.hpp>
#include <boost/thread.hpp>
#include <nmrdata/nmrdata.h>
#include <bec_misc/becmisc.h>
#include <bec_misc/bec_minpack.h>

namespace nmr_wash
{
    static const std::string version( "1.1.0" );
    static const std::string copyright( "Copyright (c) 2006-2014 Brian E. Coggins, Ph.D.  All rights reserved." );
    
    namespace details
    {
        template< typename JobInfoClass, typename JobStatusRelayClass > class status_reporter_impl;
        class job_status_relay_clean;
        class job_status_relay_scrub;
        class job_queue;
        class job;
        class job_impl;
        class job_callback_wrapper;
        class job_callback_wrapper_scrub;
        class job_callback_wrapper_clean;
        class logger;
        struct position;
		class position_list;
		struct component;
		struct component_offset_equal;
		struct component_offset_less;
        struct sorted_data_entry_greater;
		struct noise_estimator;
        
		typedef std::vector< std::pair< float, size_t > > sorted_data_vector;
        typedef std::map< size_t, position > position_map;
        typedef std::pair< size_t, position > position_map_entry;
    }
    
    class sampling_pattern;
    class cleaner;
    class scrubber;
    
    struct job_info_clean
    {
        job_info_clean( int id_, const BEC_NMRData::NMRData *input_ ) : id( id_ ), started( false ), done( false ), input( input_ ), use_position( false ), iteration( 0 ), signal( 0.0F ), noise_floor( 0.0F ), noise_std_dev( 0.0F ), criteria_a( 0.0F ), criteria_b( 0 ) {  }
        job_info_clean( int id_, const BEC_NMRData::NMRData *input_, std::vector< int > position_ ) : id( id_ ), started( false ), done( false ), input( input_ ), use_position( true ), position( position_ ), iteration( 0 ), signal( 0.0F ), noise_floor( 0.0F ), noise_std_dev( 0.0F ), criteria_a( 0.0F ), criteria_b( 0 ) {  }
        
        int id;
        bool started;
        bool done;
        
        const BEC_NMRData::NMRData *input;
        bool use_position;
        std::vector< int > position;
        
        int iteration;
        float signal;
        float noise_floor;
        float noise_std_dev;
        float criteria_a;
        int criteria_b;
        float noise_floor_initial;
    };
    
    struct job_info_scrub
    {
        job_info_scrub( int id_, const BEC_NMRData::NMRData *input_ ) : id( id_ ), started( false ), done( false ), input( input_ ), use_position( false ), current_threshold( 0.0F ), noise_floor( 0.0F ), noise_std_dev( 0.0F ), clean_level( 0.0F ), num_of_accepted_positions( 0 ), num_of_clean_components( 0 ), noise_floor_initial( 0.0F ) {  }
        job_info_scrub( int id_, const BEC_NMRData::NMRData *input_, std::vector< int > position_ ) : id( id_ ), started( false ), done( false ), input( input_ ), use_position( true ), position( position_ ), current_threshold( 0.0F ), noise_floor( 0.0F ), noise_std_dev( 0.0F ), clean_level( 0.0F ), num_of_accepted_positions( 0 ), num_of_clean_components( 0 ), noise_floor_initial( 0.0F ) {  }
        
        int id;
        bool started;
        bool done;
        
        const BEC_NMRData::NMRData *input;
        bool use_position;
        std::vector< int > position;
        
        float current_threshold;
        float noise_floor;
        float noise_std_dev;
        float clean_level;
        float num_of_accepted_positions;
        float num_of_clean_components;
        float noise_floor_initial;
    };
    
    class status_reporter_clean
    {
        friend class cleaner;
        template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >friend class washer_base;
        
    public:
        status_reporter_clean();
        ~status_reporter_clean();
        
        int get_num_of_workers() const;
        const std::map< int, job_info_clean > get_snapshot() const;
        void cancel();
        
    private:
        status_reporter_clean( const status_reporter_clean &src );
        status_reporter_clean & operator=( const status_reporter_clean &rhs );
        
        details::status_reporter_impl< job_info_clean, details::job_status_relay_clean > *impl;
    };
    
    class status_reporter_scrub
    {
        friend class scrubber;
        template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >friend class washer_base;
        
    public:
        status_reporter_scrub();
        ~status_reporter_scrub();
        
        int get_num_of_workers() const;
        const std::map< int, job_info_scrub > get_snapshot() const;
        void cancel();
        
    private:
        status_reporter_scrub( const status_reporter_scrub &src );
        status_reporter_scrub & operator=( const status_reporter_scrub &rhs );
        
        details::status_reporter_impl< job_info_scrub, details::job_status_relay_scrub > *impl;
    };
    
    enum dimension_assignment
    {
        unused = -1,
        index_dim = 0,
        u = 1,
        v = 2,
        w = 3
    };
    
    enum expt_dimension
    {
        not_present = -1,
        F1 = 0,
        F2 = 1,
        F3 = 2,
        F4 = 3
    };
    
    enum apod_method
    {
        none = 0,
        sp = 1,
        em = 2,
        gm = 3,
        gmb = 4,
        tm = 5,
        tri = 6,
        jmod = 7
    };
    
    struct dimension_apod
    {
        dimension_apod() : method( none ), p1( 0.0F ), p2( 0.0F ), p3( 0.0F ), size( -1 ), first_point_correction( 1.0F ), sw( -1.0F ) {  }
        dimension_apod( apod_method method_, float p1_, float p2_, float p3_, int size_, float fpc_, float sw_ ) : method( method_ ), p1( p1_ ), p2( p2_ ), p3( p3_ ), size( size_ ), first_point_correction( fpc_ ), sw( sw_ ) {  }
        dimension_apod( const BEC_NMRData::DimensionApodInfo &x );
        
        float operator()( float position ) const;
        
        apod_method method;
        float p1;
        float p2;
        float p3;
        int size;
        float first_point_correction;
        
        float sw;
    };
    
    struct dimension_map
    {
        dimension_map() : F1( unused ), F2( unused ), F3( unused ), F4( unused ) {  }
        dimension_map( dimension_assignment F1_, dimension_assignment F2_, dimension_assignment F3_, dimension_assignment F4_ ) : F1( F1_ ), F2( F2_ ), F3( F3_ ), F4( F4_ ) {  }
        
        int get_num_of_dims() const;
        int get_num_of_index_dims() const;
        int get_num_of_sparse_dims() const;
        
        expt_dimension get_x_expt_dim() const;
        expt_dimension get_y_expt_dim() const;
        expt_dimension get_z_expt_dim() const;
        
        expt_dimension get_u_expt_dim() const;
        expt_dimension get_v_expt_dim() const;
        expt_dimension get_w_expt_dim() const;
        
        int get_x_expt_dim_num() const;
        int get_y_expt_dim_num() const;
        int get_z_expt_dim_num() const;
        
        std::vector< int > get_index_dim_nums() const;
        std::vector< int > get_sparse_dim_nums() const;
        
        bool is_valid() const;
        
        int x( int u_, int v_ = 0, int w_ = 0 ) const;
        int y( int u_, int v_ = 0, int w_ = 0 ) const;
        int z( int u_, int v_ = 0, int w_ = 0 ) const;

        int get_x_grid_size( const sampling_pattern &sp ) const;
        int get_y_grid_size( const sampling_pattern &sp ) const;
        int get_z_grid_size( const sampling_pattern &sp ) const;
        
        float xf( float u_, float v_ = 0, float w_ = 0 ) const;
        float yf( float u_, float v_ = 0, float w_ = 0 ) const;
        float zf( float u_, float v_ = 0, float w_ = 0 ) const;
        
        float x_apod( float position ) const;
        float y_apod( float position ) const;
        float z_apod( float position ) const;
        
        dimension_assignment F1;
        dimension_assignment F2;
        dimension_assignment F3;
        dimension_assignment F4;
        
        dimension_apod F1_apod;
        dimension_apod F2_apod;
        dimension_apod F3_apod;
        dimension_apod F4_apod;
    };
    
    struct sampling_point
    {
        sampling_point( int u_, int v_ = 0, int w_ = 0, float weight_ = 1.0F ) : u( u_ ), uf( 0.0F ), v( v_ ), vf( 0.0F ), w( w_ ), wf( 0.0F ), weight( weight_ )  {  }
        sampling_point( float u_, float v_ = 0.0F, float w_ = 0.0F, float weight_ = 1.0F ) : u( 0 ), uf( u_ ), v( 0 ), vf( v_ ), w( 0 ), wf( w_ ), weight( weight_ )  {  }
        
        int u;
        float uf;
        int v;
        float vf;
        int w;
        float wf;
        float weight;
    };
    
    class sampling_pattern
    {
    public:
        sampling_pattern();
        explicit sampling_pattern( std::string filename );
        sampling_pattern( std::string filename, int u_is_col, int v_is_col, int w_is_col, int weight_is_col, bool is_on_grid );
        
        void load( std::string filename );
        void load( std::string filename, int u_is_col, int v_is_col, int w_is_col, int weight_is_col, bool is_on_grid );
        void save( std::string filename, bool number_points = false, bool include_col_headings = false, char comment_char = '#', const char *delimiter_string = "\t" ) const;
        
        int get_num_of_dims() const;
        bool got_weights() const;
        bool is_on_grid() const;
        int get_u_grid_size() const;
        int get_v_grid_size() const;
        int get_w_grid_size() const;
        float get_u_max_value() const;
        float get_v_max_value() const;
        float get_w_max_value() const;
        
        void add_point( sampling_point new_point );
        void clear();
        void clear_weights();
        const std::vector< sampling_point > & get_points() const;
        std::vector< sampling_point > & get_points();
        std::vector< sampling_point > get_points_copy() const;
        void set_options( int num_of_dims_, bool weights_, bool on_grid_ );
        size_t size() const;
        
        sampling_point & operator[]( size_t index );
        const sampling_point & operator[]( size_t index ) const;
        
        //void calculate_voronoi_weights( std::string graphical_output_file = std::string( "" ) );

    private:
        void build_from_spectrum_representation( std::string filename );
        void build_from_text_file( std::string filename );
        void build_from_text_file_with_guidance( std::string filename, int u_is_col, int v_is_col, int w_is_col, int weight_is_col, bool is_on_grid );

        std::vector< sampling_point > points;
        
        int dims;
        bool weights;
        bool on_grid;
        
        int u_max;
        int v_max;
        int w_max;
        float uf_max;
        float vf_max;
        float wf_max;
    };
    
    enum linearity_modes
    {
        fd,
        tde,
        td
    };
    
    enum pure_component_modes
    {
        contour_irregular,
        contour_ellipsoid,
        fixed_ellipsoid,
        delta_function
    };
    
    class psf_calc
    {
    public:
        psf_calc( const sampling_pattern &pattern, const dimension_map dim_map, int xsize_, int ysize_, int zsize_, float xsw_ = 0.0F, float ysw_ = 0.0F, float zsw_ = 0.0F );
        
        boost::shared_array< float > psf();
        boost::shared_array< float > pure_component_contour_irregular( float level, bool margin );
        boost::shared_array< float > pure_component_contour_ellipsoid( float level, float margin );
        boost::shared_array< float > pure_component_fixed_ellipsoid( float radius );
        boost::shared_array< float > pure_component_delta_function();
        
        void set_psf( boost::shared_array< float > psf_ );
        
        size_t get_psf_size() const;
        
        float comp_scale;
        
    private:
        std::vector< details::position > get_neighboring_positions( details::position p );
        float calc_pc_scale_factor( boost::shared_array< float > pure_comp );
        void debug_snapshot_xy( const float *data, const char *filename, int xs, int ys, bool xc, bool yc, float scale = 1.0F );
        void debug_snapshot_xyz( const float *data, const char *filename, int xs, int ys, int zs, bool xc, bool yc, bool zc, float scale = 1.0F );
        
        const sampling_pattern &sp;
        const dimension_map dm;
        int xsize;
        int ysize;
        int zsize;
        float xtmax;
        float ytmax;
        float ztmax;
        float xsw;
        float ysw;
        float zsw;
        int xis;
        int yis;
        int size;
        
        boost::shared_array< float > stored_psf;
    };
    
    boost::shared_ptr< BEC_NMRData::NMRData > convert_psf_or_pure_comp_to_spectrum( const boost::shared_array< float > input, int xsize, int ysize, int zsize );
    
    template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
    class washer_base
    {
        typedef details::status_reporter_impl< JobInfo, JobStatusRelay > sri_t;
        
    public:
        washer_base( const sampling_pattern &pattern, const dimension_map dim_map, StatusReporter *reporter = 0 );
        
        void operator()( BEC_NMRData::NMRData &input );
        void operator()( const BEC_NMRData::NMRData &input, BEC_NMRData::NMRData &output );
        void operator()( BEC_NMRData::NMRData &input, std::vector< std::vector< int > > position_list );
        void operator()( const BEC_NMRData::NMRData &input, BEC_NMRData::NMRData &output, std::vector< std::vector< int > > position_list );
        void operator()( const BEC_NMRData::NMRData &input, BEC_Misc::ptr_vector< BEC_NMRData::NMRData > outputs, std::vector< std::vector< int > > position_list );
        
        void set_reporter( StatusReporter *new_reporter = 0 );
        boost::shared_array< float > get_psf();
        boost::shared_array< float > get_pure_component();
        int get_xsize();
        int get_ysize();
        int get_zsize();
        
        linearity_modes linearity_mode;
        
        pure_component_modes pure_component_mode;
        float pure_comp_ci_level;
        float pure_comp_ci_margin;
        float pure_comp_ce_level;
        float pure_comp_ce_margin;
        float pure_comp_fe_size;
        
        bool components_only;
        bool residuals_only;
        
        int num_of_threads;
        
        FILE *log_file;
        bool verbose_log;
        
    protected:
        void setup( const BEC_NMRData::NMRData &input );
        details::position get_position( size_t offset );
        void subtract_psf( details::position p, float *out, float scale );
        void add_pure_components( float *out, std::vector< details::component > &components );
        
        boost::exception_ptr worker_exception;
        boost::mutex worker_exception_mutex;
        
        void log_start( FILE *log, details::job &j );
        
        void debug_snapshot( const float *data, const char *filename, int xs, int ys, int zs, float scale = 1.0F );
        
        const sampling_pattern &sp;
        dimension_map dm;
        sri_t *sri;
        int xsize;
        int ysize;
        int zsize;
        
        linearity_modes cached_linearity_mode;
        
        pure_component_modes cached_pure_component_mode;
        float cached_pure_comp_ci_level;
        float cached_pure_comp_ci_margin;
        float cached_pure_comp_ce_level;
        float cached_pure_comp_ce_margin;
        float cached_pure_comp_fe_size;
        
        boost::shared_array< float > psf;
        boost::shared_array< float > pure_comp;
        size_t psf_size;
        float comp_scale;
    };

    class cleaner : public washer_base< cleaner, job_info_clean, details::job_status_relay_clean, details::job_callback_wrapper_clean, status_reporter_clean >
    {
        friend class details::job_callback_wrapper_clean;
        
    public:
        cleaner( const sampling_pattern &pattern, const dimension_map dim_map, status_reporter_clean *reporter = 0 );
        
        float gain;
        float threshold_noise;
        float threshold_intensity;
        int max_iter;
        
    private:
        void clean( details::job &j );
    };
    
    class scrubber : public washer_base< scrubber, job_info_scrub, details::job_status_relay_scrub, details::job_callback_wrapper_scrub, status_reporter_scrub >
    {
        friend class details::job_callback_wrapper_scrub;
        
    public:
        scrubber( const sampling_pattern &pattern, const dimension_map dim_map, status_reporter_scrub *reporter = 0 );
        
        float gain;
        float stopping_threshold;
        float base;

        boost::function< void ( bool, float, float, float, float, size_t, size_t, const float * ) > snapshot_recorder;

    private:
        void scrub( details::job &j );
        void scrub_v1( details::job &j );
        details::sorted_data_vector sort_data( const float *data, float clean_level, float threshold, const details::position_list &accepted_positions, const details::position_list &excluded_positions );
    };
    
    namespace details
    {
        template< typename JobInfoClass, typename JobStatusRelayClass >
        class status_reporter_impl
        {
            typedef typename std::map< int, JobStatusRelayClass * >::iterator relay_map_iterator;
            typedef typename std::map< int, JobStatusRelayClass * >::const_iterator const_relay_map_iterator;
            typedef typename std::map< int, JobInfoClass >::iterator job_map_iterator;
            typedef typename std::map< int, JobInfoClass >::const_iterator const_job_map_iterator;
            
        public:
            status_reporter_impl() : num_of_workers( 0 ), next_job_num( 0 ), cancel_signal( false ) {  }
            ~status_reporter_impl();
            
            //  Interface to status_reporter_scrub/status_reporter_clean, for client use
            int get_num_of_workers() const;
            std::map< int, JobInfoClass > get_snapshot() const;
            void cancel();
            
            //  Interface to worker
            void clear();
            int get_new_job_id();
            void register_job( int job_id, job j );
            void register_jobs( std::vector< job > &jobs );
            JobStatusRelayClass * start_job( int job_id );
            void end_job( int job_id );
            
        private:
            status_reporter_impl( const status_reporter_impl &src );                //  Disallow copying
            status_reporter_impl & operator=( const status_reporter_impl &rhs );    //  Disallow assignment
            
            std::map< int, JobInfoClass > jobs;
            std::map< int, JobStatusRelayClass * > relays;
            int num_of_workers;
            int next_job_num;
            bool cancel_signal;
            mutable boost::shared_mutex mtx;
        };
        
        class job_status_relay_clean
        {
        public:
            job_status_relay_clean( std::map< int, job_info_clean >::iterator it_ ) : it( it_ ), cancel_signal( false ) {  }
            
            //  Interface to cleaner
            bool post( int iteration, float signal, float noise_floor, float noise_std_dev, float criteria_a, float criteria_b );
            
            //  Interface to status_reporter_impl
            void cancel();
            boost::shared_mutex & get_mutex();
            
        private:
            std::map< int, job_info_clean >::iterator it;
            bool cancel_signal;
            mutable boost::shared_mutex mtx;
        };
        
        class job_status_relay_scrub
        {
        public:
            job_status_relay_scrub( std::map< int, job_info_scrub >::iterator it_ ) : it( it_ ), cancel_signal( false ) {  }
            
            //  Interface to scrubber
            bool post( float current_threshold, float noise_floor, float noise_std_dev, float clean_level, int accepted_positions, int components );
            
            //  Interface to status_reporter_impl
            void cancel();
            boost::shared_mutex & get_mutex();
            
        private:
            std::map< int, job_info_scrub >::iterator it;
            bool cancel_signal;
            mutable boost::shared_mutex mtx;
        };
        
        template< typename Pred >
        std::vector< job > make_jobs_all_pos( const BEC_NMRData::NMRData *in, Pred &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l );
        
        template< typename Pred >
        std::vector< job > make_jobs_pos_list( std::vector< std::vector< int > > pos_list, const BEC_NMRData::NMRData *in, Pred &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l );
        
        class make_job_pos_inplace
        {
        public:
            make_job_pos_inplace( BEC_NMRData::NMRData *data, boost::mutex *data_mutex, dimension_map dim_map );
            
            boost::shared_ptr< job_impl > operator()( int id, FILE *log_file, std::vector< int > pos );
            
        private:
            BEC_NMRData::NMRData *d;
            boost::mutex *d_mtx;
            dimension_map dm;
        };
        
        class make_job_pos
        {
        public:
            make_job_pos( const BEC_NMRData::NMRData *input, boost::mutex *input_mutex, BEC_NMRData::NMRData *output, boost::mutex *output_mutex, dimension_map dim_map );
            
            boost::shared_ptr< job_impl > operator()( int id, FILE *log_file, std::vector< int > pos );
            
        private:
            const BEC_NMRData::NMRData *in;
            boost::mutex *in_mtx;
            BEC_NMRData::NMRData *out;
            boost::mutex *out_mtx;
            dimension_map dm;
        };
        
        class make_job_pos_to_full
        {
        public:
            make_job_pos_to_full( const BEC_NMRData::NMRData *input, boost::mutex *input_mutex, BEC_Misc::ptr_vector< BEC_NMRData::NMRData > outputs_, dimension_map dim_map );
            
            boost::shared_ptr< job_impl > operator()( int id, FILE *log_file, std::vector< int > pos );
            
        private:
            int i;
            const BEC_NMRData::NMRData *in;
            boost::mutex *in_mtx;
            BEC_Misc::ptr_vector< BEC_NMRData::NMRData > outputs;
            dimension_map dm;
        };
        
        class job
        {
        public:
            job( boost::shared_ptr< job_impl > ji, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq = 0 );
            void operator()();
            
            //  Copyable/assignable via the default copy constructor
            
            int get_job_id();
            float * get_input();
            FILE * get_log();
            void write_log_section_header();
            void post_output( float *output );
            
            bool is_full();
            const BEC_NMRData::NMRData *get_input_src();
            std::vector< int > *get_position();
            
        private:
            boost::shared_ptr< job_impl > ji;
            boost::shared_ptr< job_callback_wrapper > cbw;
            job_queue *queue;
        };
        
        class job_queue
        {
        public:
            job_queue( int num_of_threads = 0 );
            
            void operator()();
            
            void add_job( job j );
            int get_num_of_threads();
            int get_num_of_running_threads();
            void set_num_of_threads( int num );
            
            void thread_signal_end();
            
        private:
            job_queue( const job_queue &src );                  //  Disallow copying
            job_queue & operator=( const job_queue &rhs );      //  Disallow assignment
            
            std::queue< job > q;
            boost::thread_group tg;
            int num_running;
            int num_allowed;
            boost::mutex m;
            boost::condition_variable thread_done;
        };
        
        class job_callback_wrapper
        {
        public:
            virtual ~job_callback_wrapper() {  }
            void start_calc( job &j );
            int get_new_job_id();
            
        private:
            virtual void do_start_calc( job &j ) = 0;
            virtual int do_get_new_job_id() = 0;
        };
        
        class job_callback_wrapper_scrub : public job_callback_wrapper
        {
        public:
            job_callback_wrapper_scrub( scrubber *s );
            
        private:
            virtual void do_start_calc( job &j );
            int do_get_new_job_id();
            
            scrubber *obj;
        };
        
        class job_callback_wrapper_clean : public job_callback_wrapper
        {
        public:
            job_callback_wrapper_clean( cleaner *c );
            
        private:
            virtual void do_start_calc( job &j );
            int do_get_new_job_id();
            
            cleaner *obj;
        };
        
        class job_impl
        {
        public:
            job_impl( int id_, FILE *log_file_ );
            virtual ~job_impl() {  }
            
            int get_job_id();
            float * get_input();
            FILE * get_log();
            void write_log_section_header();
            void post_output( float *output );
            
            bool is_full();
            const BEC_NMRData::NMRData *get_input_src();
            std::vector< int > *get_position();
            
        protected:
            int id;
            FILE *log_file;
            
        private:
            job_impl( const job_impl &src );                //  Disallow copying
            job_impl & operator=( const job_impl &rhs );    //  Disallow assignment
            virtual int do_get_job_id();
            virtual float * do_get_input() = 0;
            virtual FILE * do_get_log();
            virtual void do_write_log_section_header();
            virtual void do_post_output( float *output ) = 0;

            virtual bool do_is_full() = 0;
            virtual const BEC_NMRData::NMRData *do_get_input_src() = 0;
            virtual std::vector< int > *do_get_position() = 0;
        };
        
        class job_full_inplace : public job_impl
        {
        public:
            job_full_inplace( int id_, FILE *log_file_, BEC_NMRData::NMRData *data, boost::mutex *data_mutex );
            virtual ~job_full_inplace();
            
        private:
            virtual float * do_get_input();
            virtual void do_post_output( float *output );
            
            virtual bool do_is_full();
            virtual const BEC_NMRData::NMRData *do_get_input_src();
            virtual std::vector< int > *do_get_position();
           
            BEC_NMRData::NMRData *d;
            boost::mutex *d_mtx;
            float *data_block;
        };
        
        class job_full : public job_impl
        {
        public:
            job_full( int id_, FILE *log_file_, const BEC_NMRData::NMRData *input, boost::mutex *input_mutex, BEC_NMRData::NMRData *output, boost::mutex *output_mutex );
            virtual ~job_full();
            
        private:
            virtual float * do_get_input();
            virtual void do_post_output( float *output );
            
            virtual bool do_is_full();
            virtual const BEC_NMRData::NMRData *do_get_input_src();
            virtual std::vector< int > *do_get_position();
           
            const BEC_NMRData::NMRData *in;
            boost::mutex *in_mtx;
            BEC_NMRData::NMRData *out;
            boost::mutex *out_mtx;
            float *data_block;
        };
        
        class job_pos_inplace : public job_impl
        {
        public:
            job_pos_inplace( int id_, FILE *log_file_, BEC_NMRData::NMRData *data, boost::mutex *data_mutex, std::vector< int > pos, dimension_map dim_map );
            virtual ~job_pos_inplace();
            
        private:
            virtual float * do_get_input();
            virtual void do_post_output( float *output );
            
            virtual bool do_is_full();
            virtual const BEC_NMRData::NMRData *do_get_input_src();
            virtual void do_write_log_section_header();
            virtual std::vector< int > *do_get_position();
           
            BEC_NMRData::NMRData *d;
            boost::mutex *d_mtx;
            std::vector< int > p;
            dimension_map dm;
            BEC_NMRData::Subset *s;
            float *data_block;
        };
        
        class job_pos : public job_impl
        {
        public:
            job_pos( int id_, FILE *log_file_, const BEC_NMRData::NMRData *input, boost::mutex *input_mutex, BEC_NMRData::NMRData *output, boost::mutex *output_mutex, std::vector< int > pos, dimension_map dim_map );
            virtual ~job_pos();
            
        private:
            virtual float * do_get_input();
            virtual void do_post_output( float *output );
            
            virtual bool do_is_full();
            virtual const BEC_NMRData::NMRData *do_get_input_src();
            virtual void do_write_log_section_header();
            virtual std::vector< int > *do_get_position();
            
            const BEC_NMRData::NMRData *in;
            boost::mutex *in_mtx;
            BEC_NMRData::NMRData *out;
            boost::mutex *out_mtx;
            std::vector< int > p;
            dimension_map dm;
            std::vector< int > c1;
            std::vector< int > c2;
            float *data_block;
        };
        
        class job_pos_to_full : public job_impl
        {
        public:
            job_pos_to_full( int id_, FILE *log_file_, const BEC_NMRData::NMRData *input, boost::mutex *input_mutex, BEC_NMRData::NMRData *output, std::vector< int > pos, dimension_map dim_map );
            virtual ~job_pos_to_full();
            
        private:
            virtual float * do_get_input();
            virtual void do_post_output( float *output );
            
            virtual bool do_is_full();
            virtual const BEC_NMRData::NMRData *do_get_input_src();
            virtual void do_write_log_section_header();
            virtual std::vector< int > *do_get_position();
           
            const BEC_NMRData::NMRData *in;
            boost::mutex *in_mtx;
            BEC_NMRData::NMRData *out;
            dimension_map dm;
            std::vector< int > p;
            float *data_block;
        };
        
        class logger
        {
        public:
            logger( FILE *log_file_ ) : log_file( log_file_ ) {  }
            ~logger();
            
            FILE * register_job( int id );
            
        private:
            FILE *log_file;
            std::map< int, FILE * > tmp_files;
        };
        
		struct position
		{
			position( size_t o, int x, int y, int z ) : 
            offset( o ), xpos( x ), ypos( y ), zpos( z ) {  }
            
			size_t offset;
			int xpos;
			int ypos;
			int zpos;
 		};
        
		class position_list
		{
		public:
			position_list( void )  {  }
            
            void add( position p )
            {
                o_index.insert( p.offset );
            }
            
            void add_note_neighbors( position p, int xsize, int ysize, int zsize )
            {
                o_index.insert( p.offset );
                
                if( n_index.find( p.offset ) != o_index.end() )
                    n_index.erase( n_index.find( p.offset ) );
                
                for( int x = p.xpos - 1; x <= p.xpos + 1; x++ )
                    for( int y = p.ypos - 1; y <= p.ypos + 1; y++ )
                        for( int z = p.zpos - 1; z <= p.zpos + 1; z++ )
                            if( x >= 0 && x < xsize && y >= 0 && 
                               y < ysize && z >= 0 && z < zsize && 
                               !( x == p.xpos && y == p.ypos && z == p.zpos ) )
                            {
                                if( n_index.find( x * ysize * zsize + y * zsize + z ) == n_index.end() )
                                    n_index.insert( x * ysize * zsize + y * zsize + z );
                            }
            }
            
            bool is_on_list( position p ) const
            {
                if( o_index.find( p.offset ) != o_index.end() ) return true;
                else return false;
            }
            
            bool is_on_list( size_t o ) const
            {
                if( o_index.find( o ) != o_index.end() ) return true;
                else return false;
            }
            
            bool is_neighbor( position p ) const
            {
                if( n_index.find( p.offset ) != n_index.end() ) return true;
                else return false;
            }
            
            bool is_neighbor( size_t o ) const
            {
                if( n_index.find( o ) != n_index.end() ) return true;
                else return false;
            }
            
            size_t size() const
            {
                return o_index.size();
            }
            
		private:
            std::set< size_t > o_index;
            std::set< size_t > n_index;
		};
        
		struct component
		{
			component( position p, float s ) : pos( p ), scale ( s )  {  }
            
			position pos;
			float scale;
		};
        
		struct component_offset_equal
		{
			bool operator()( const component &lhs, const component &rhs )
			{
				return lhs.pos.offset == rhs.pos.offset;
			}
		};
        
		struct component_offset_less
		{
			bool operator()( const component &lhs, const component &rhs )
			{
				return lhs.pos.offset < rhs.pos.offset;
			}
		};
        
		struct sorted_data_entry_greater
		{
			bool operator()( const std::pair< float, size_t > &lhs, const std::pair< float, size_t > &rhs )
			{
				return lhs.first > rhs.first;
			}
		};
        
		class noise_estimator
		{
        public:
			noise_estimator( size_t size, int total_size, int bins_ = 200 );            
			typedef std::pair< float, int > bin_element;
            
			void operator()( const float *data );
            
			float noise_floor;
            float noise_avg;
			float noise_std_dev;
            float min;
            float max;
            float avg;
            float std_dev;
            float nf_factor;
			size_t n;
            size_t total_n;
            
        private:
            int bins;
            std::vector< bin_element > histogram;
            std::vector< double > xvalues;
            std::vector< double > yvalues;
            std::vector< double > guess;
            std::vector< double > fit;
            BEC_MINPACK::DualGaussianFunctionEvaluator eval;
            BEC_MINPACK::FitDataToModel_LM_AnalyticJacobian< BEC_MINPACK::DualGaussianFunctionEvaluator > fitter;
		};
        
		class vector_noise_estimator
		{
        public:
			vector_noise_estimator( int bins_ = 200 ) : noise_mean( 0.0F ), noise_std_dev( 0.0F ), bins( bins_ ), histogram( bins_ ), xvalues( bins_ ), yvalues( bins_ ), guess( 6 ), fit( 6 ), fitter( bins_, 6, eval, 10000 )  {  }
            
			typedef std::pair< float, int > bin_element;
            
			void operator()( const std::vector< float > data );
            
			float noise_mean;
			float noise_std_dev;
            
        private:
            int bins;
            std::vector< bin_element > histogram;
            std::vector< double > xvalues;
            std::vector< double > yvalues;
            std::vector< double > guess;
            std::vector< double > fit;
            BEC_MINPACK::DualGaussianFunctionEvaluator eval;
            BEC_MINPACK::FitDataToModel_LM_AnalyticJacobian< BEC_MINPACK::DualGaussianFunctionEvaluator > fitter;
		};
        
        class grid_hc_fft
        {
        public:
            grid_hc_fft( int xsize_, int ysize_, int zsize_, bool fd = false );
            
            float &operator()( int x, int y, int z );
            boost::shared_array< float > get_data();
            void set_data( boost::shared_array< float > input );
            void scale_data( float scale_factor );
            float integrate_real();
            
            void ft_di_x();
            void ft_di_y();
            void ft_di_z();
            
            void ift_ad_x();
            void ift_ad_y();
            void ift_ad_z();
            
            void ift_ht_ad_x();
            void ift_ht_ad_y();
            void ift_ht_ad_z();
            
            void ht( float *data, int size );
            
        private:
            int xsize;
            int ysize;
            int zsize;
            bool xc;
            bool yc;
            bool zc;
            int comp;
            boost::shared_array< float > data;
        };
                
        class plane_snapshot_recorder
        {
        public:
            plane_snapshot_recorder( std::string filename_, int xsize_, int ysize_, int interval_ );
            ~plane_snapshot_recorder();
            
            void operator()( int outer_iteration_num, float current_threshold, float noise_floor, float noise_std_dev, float clean_level, size_t accepted_positions, size_t components, const float *current_data );
            
        private:
            std::string filename;
            int xsize;
            int ysize;
            int interval;
            int count;
            std::vector< boost::shared_array< float > > data_record;
        };
        
        class vector_snapshot_recorder
        {
        public:
            vector_snapshot_recorder( int xsize_, int ysize_, int xpos_, std::string x_filename, std::string x_diff_filename, int ypos_, std::string y_filename, std::string y_diff_filename, int interval_ );
            ~vector_snapshot_recorder();
            
            void operator()( int outer_iteration_num, float current_threshold, float noise_floor, float noise_std_dev, float clean_level, size_t accepted_positions, size_t components, const float *current_data );
            
        private:
            FILE *x_file;
            FILE *x_diff_file;
            FILE *y_file;
            FILE *y_diff_file;
            int xsize;
            int ysize;
            int xpos;
            int ypos;
            int interval;
            int count;
            boost::shared_array< float > prev_data;
        };
        
    }
    
    std::istream & operator>>( std::istream &in, dimension_assignment &da );
    std::ostream & operator<<( std::ostream &out, dimension_assignment &da );
    std::istream & operator>>( std::istream &in, pure_component_modes &pcm );
    std::istream & operator>>( std::istream &in, linearity_modes &lm );
    std::istream & operator>>( std::istream &in, apod_method &am );
    std::ostream & operator<<( std::ostream &out, apod_method &am );
    std::ostream & operator<<( std::ostream &out, dimension_apod &da );

    namespace exceptions
    {
        struct base_exception : virtual std::exception, virtual boost::exception
        {
            virtual const char *what() const throw()
            {
                return "An error has occurred.";
            }
        };
        
        struct file_not_found : base_exception
        {
            file_not_found( const std::string &filename_ ) : filename( filename_ ) {  }
            virtual ~file_not_found() throw () {  }
            
            virtual const char *what() const throw()
            {
                return "The file could not be found.";
            }
            
            std::string filename;
        };

        struct cant_open_file : base_exception
        {
            cant_open_file( const std::string &filename_ ) : filename( filename_ ) {  }
            virtual ~cant_open_file() throw () {  }
            
            virtual const char *what() const throw()
            {
                return "The file could not be opened.";
            }
            
            std::string filename;
        };
        
        struct dimension_not_present : base_exception
        {
            virtual const char *what() const throw()
            {
                return "The requested dimension is not present in this dataset.";
            }
        };
        
        struct inconsistent_parameters : base_exception
        {
            virtual const char *what() const throw()
            {
                return "The input parameters are not consistent with one another.";
            }
        };
        
        struct bad_parameters : base_exception
        {
            virtual const char *what() const throw()
            {
                return "One or more input parameters are not valid.";
            }
        };
        
        struct in_and_out_not_compatible : base_exception
        {
            virtual const char *what() const throw()
            {
                return "The output dataset is not compatible with the input dataset.";
            }
        };
        
        struct nmrdata_library_exception : base_exception
        {
            nmrdata_library_exception( const std::string &msg_ ) : msg( msg_ ) {  }
            virtual ~nmrdata_library_exception() throw() {  }
            
            virtual const char *what() const throw()
            {
                return "An error occurred in the NMRData library.";
            }
            
            std::string msg;
        };
        
        struct bad_dimension_assignment_string : base_exception
        {
            virtual const char *what() const throw()
            {
                return "The dimension assignment string was not valid.";
            }
        };
        
        struct bad_apod_method_string : base_exception
        {
            virtual const char *what() const throw()
            {
                return "The apodization method string was not valid.";
            }
        };
        
        struct bad_linearity_mode_string : base_exception
        {
            virtual const char *what() const throw()
            {
                return "The linearity mode string was not valid.";
            }
        };
        
        struct bad_pure_component_mode_string : base_exception
        {
            virtual const char *what() const throw()
            {
                return "The pure component mode string was not valid.";
            }
        };
        
        namespace sampling_pattern
        {
            struct sampling_pattern_error : base_exception
            {
                virtual const char *what() const throw()
                {
                    return "An error occurred while processing a sampling pattern.";
                }
            };
            
            struct file_format_not_understood : sampling_pattern_error
            {
                file_format_not_understood( const std::string &filename_ ) : filename( filename_ ) {  }
                virtual ~file_format_not_understood() throw() {  }
                
                virtual const char *what() const throw()
                {
                    return "The sampling pattern file could not be interpreted: the file's format could not be discerned.";
                }
                
                std::string filename;
            };
            
            struct file_format_not_consistent : sampling_pattern_error
            {
                file_format_not_consistent( const std::string &filename_ ) : filename( filename_ ) {  }
                virtual ~file_format_not_consistent() throw() {  }
                
                virtual const char *what() const throw()
                {
                    return "The sampling pattern file could not be interpreted: the file's format is not consistent.";
                }
                
                std::string filename;
            };

            struct file_format_doesnt_match : sampling_pattern_error
            {
                file_format_doesnt_match( const std::string &filename_ ) : filename( filename_ ) {  }
                virtual ~file_format_doesnt_match() throw() {  }
                
                virtual const char *what() const throw()
                {
                    return "The sampling pattern file could not be interpreted: the file's format does not match the specification provided.";
                }
                
                std::string filename;
            };

            struct bad_file_format_guidance : sampling_pattern_error
            {
                bad_file_format_guidance( const std::string &filename_ ) : filename( filename_ ) {  }
                virtual ~bad_file_format_guidance() throw() {  }
                
                virtual const char *what() const throw()
                {
                    return "The sampling pattern file could not be interpreted: the file format guidance is not valid.";
                }
                
                std::string filename;
            };
            
            struct cant_use_spectrum_representation_for_off_grid_pattern : sampling_pattern_error
            {
                cant_use_spectrum_representation_for_off_grid_pattern( const std::string &filename_ ) : filename( filename_ ) {  }
                virtual ~cant_use_spectrum_representation_for_off_grid_pattern() throw() {  }
                
                virtual const char *what() const throw()
                {
                    return "The filename describes a spectrum, but a spectrum representation cannot be used for an off-grid sampling pattern.";
                }
                
                std::string filename;
           };
            
        }
        
    }

}

#endif
