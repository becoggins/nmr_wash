//  Copyright (c) 2012, Brian E. Coggins.  All rights reserved.

#ifndef NMR_WASH_PIPEWASH_INCLUDED
#define NMR_WASH_PIPEWASH_INCLUDED

#ifdef __cplusplus
extern "C"
{
    struct pipewash_clean_handle;
    struct pipewash_scrub_handle;
    struct pipewash_options_handle;
#else
    typedef struct pipewash_clean_handle pipewash_clean_handle;
    typedef struct pipewash_scrub_handle pipewash_scrub_handle;
    typedef struct pipewash_options_handle pipewash_options_handle;
#endif
    
    pipewash_options_handle *pipewash_clean_params( int argc, char *argv[] );
    pipewash_clean_handle * pipewash_clean_init( pipewash_options_handle *pwo, int dim_count, int pipe_x_size, int pipe_y_size, int pipe_z_size, int pipe_a_size, char *pipe_x_label, char *pipe_y_label, char *pipe_z_label, char *pipe_a_label, float pipe_x_sw, float pipe_y_sw, float pipe_z_sw, float pipe_x_apodcode, float pipe_x_q1, float pipe_x_q2, float pipe_x_q3, float pipe_x_apodsize, float pipe_x_c1, float pipe_y_apodcode, float pipe_y_q1, float pipe_y_q2, float pipe_y_q3, float pipe_y_apodsize, float pipe_y_c1, float pipe_z_apodcode, float pipe_z_q1, float pipe_z_q2, float pipe_z_q3, float pipe_z_apodsize, float pipe_z_c1 );
    int pipewash_clean_get_array_size( pipewash_clean_handle *pwh );
    int pipewash_clean_get_slices_per_array( pipewash_clean_handle *pwh );
    int pipewash_clean_proc( pipewash_clean_handle *pwh, float *data );
    int pipewash_clean_done( pipewash_clean_handle *pwh );
    
    pipewash_options_handle *pipewash_scrub_params( int argc, char *argv[] );
    pipewash_scrub_handle *pipewash_scrub_init( pipewash_options_handle *pwo, int dim_count, int pipe_x_size, int pipe_y_size, int pipe_z_size, int pipe_a_size, char *pipe_x_label, char *pipe_y_label, char *pipe_z_label, char *pipe_a_label, float pipe_x_sw, float pipe_y_sw, float pipe_z_sw, float pipe_x_apodcode, float pipe_x_q1, float pipe_x_q2, float pipe_x_q3, float pipe_x_apodsize, float pipe_x_c1, float pipe_y_apodcode, float pipe_y_q1, float pipe_y_q2, float pipe_y_q3, float pipe_y_apodsize, float pipe_y_c1, float pipe_z_apodcode, float pipe_z_q1, float pipe_z_q2, float pipe_z_q3, float pipe_z_apodsize, float pipe_z_c1 );
    int pipewash_scrub_get_array_size( pipewash_scrub_handle *pwh );
    int pipewash_scrub_get_slices_per_array( pipewash_scrub_handle *pwh );
    int pipewash_scrub_proc( pipewash_scrub_handle *pwh, float *data );
    int pipewash_scrub_done( pipewash_scrub_handle *pwh );
    
#ifdef __cplusplus
}
#endif

#endif
