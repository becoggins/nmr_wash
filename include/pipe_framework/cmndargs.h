/***/
/* Definitions for use of command-line argument extraction procedures.
/***/

#define CHECK_NONE  0
#define CHECK_FLAGS 1
#define CHECK_ALL   2
#define CHECK_DBG   3
#define CHECK_ENV   4

int  initArgs(), checkArgs();
char *getProgName();

int  isFlag(), flagLoc(), nextFlag();
int  intArgD(), dblArgD(), fltArgD(), strArgD(), ptrArgD();
int  logArgD(), notLogArgD();

float  fltArg();
int    intArgDN(), fltArgDN();

double dblArg();
char   *strArg(), *ptrArg(), *getNthArg();

int    charListArg();
int    *iListArg();
float  *rListArg();
char   **sListArg(), **strListArg(), **ptrListArg(), **copyArgv();

int badFlag;
extern int badFlag;
