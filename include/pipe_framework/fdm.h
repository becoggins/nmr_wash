
typedef struct {double rdata; double idata;} FComplex;
typedef int    FInt;
typedef int    FBoolean;
typedef double FDouble;

struct FDM1DInfo 
   {
    FInt     NSig;     /* Number of input time-domain points.                 */
    FInt     NSp;      /* Num of desired final points in spectrum.            */
    FInt     Nb0;      /* Num of basis func, (wmax-wmin)/SW*(Nsig+1)/2*rho.   */
    FInt     Nbc;      /* Num of coarse basis funcs, for multi-scale mode.    */
    FInt     Nb;       /* Num of fine basis funcs.                            */
    FInt     NWin;     /* Num of Windows                                      */
    float    sx1;      /* Window start, Pts                                   */
    float    sxn;      /* Window end, Pts                                     */
    FDouble  wmin;     /* Window start, Hz.                                   */
    FDouble  wmax;     /* Window end,  Hz.                                    */
    FDouble  dW;       /* Size of small window                                */
    FDouble  gamma;    /* Smoothing factor for spectral reconstruction.       */
    FDouble  delta;    /* Timestep, sec.                                      */
    FDouble  rho;      /* Basis Density.                                      */
    FBoolean mwFlag;   /* Use multi-window mode                               */
    FBoolean msFlag;   /* Use multi-scale mode.                               */
    FComplex *td;      /* 0:Nsig, Input Time-Domain.                          */
    FComplex *wk;      /* Nb, Frequencies, (Nb=Nb0+Nbc).                      */
    FComplex *dk;      /* Nb, Amplitudes, (Nb=Nb0+Nbc).                       */
    FComplex *U;       /* Nb,Nb,0:1 3D work array.                            */
    FComplex *f;       /* Nb 1D work array.                                   */
    FComplex *g;       /* Nb 1D work array.                                   */
    FComplex *diag;    /* Nb 1D work array.                                   */
    FComplex *z;       /* Nb 1D work array.                                   */
    FDouble  *beta;    /* Nb 1D work array.                                   */
    FComplex *coefW;   /* NSig 1D work array.                                 */
    FComplex *zz;      /* Nb,Nb 2D work array.                                */
    FComplex *U0;      /* Nb,Nb 2D work array.                                */
    FComplex *riSpec;  /* NSp 1D spectral output array.                       */
    FComplex *wSpec;   /* NSp 1D spectral work array.                         */
   };

struct FDM2DInfo
   {
    FInt     NSig1; 
    FInt     NSig2; 
    FInt     NSp1; 
    FInt     NSp2; 
    FInt     Nb1; 
    FInt     Nb2; 
    FInt     Nb; 
    FInt     NWin1;
    FInt     NWin2;
    float    sx1;
    float    sxn;
    float    sy1;
    float    syn;
    FDouble  wmin1; 
    FDouble  wmin2;
    FDouble  wmax1; 
    FDouble  wmax2;
    FDouble  dW1;
    FDouble  dW2;
    FDouble  delta1; 
    FDouble  delta2; 
    FDouble  gamma1; 
    FDouble  gamma2; 
    FDouble  rho1;  
    FDouble  rho2;  
    FDouble  q2;
    FBoolean mwxFlag;
    FBoolean mwyFlag;
    FBoolean singFlag;
    FBoolean ctFlag;
    FInt     iModel;
    FInt     riMode;
    FDouble  scale;
    FComplex *td; 
    FComplex *wk; 
    FComplex *dk;
    FComplex *U; 
    FComplex *zz; 
    FComplex *bk; 
    FComplex *coefW; 
    FComplex *f; 
    FComplex *g; 
    FComplex *gz; 
    FComplex *gsave; 
    FComplex *gzct;
    FComplex *z2_Mt; 
    FComplex *cm1; 
    FComplex *cm2; 
    FDouble  *beta;
    FComplex *wSpec;
    FComplex *riSpec;
    FComplex *v;
   };

struct FDMInfo
   {
    struct FDM1DInfo fInfo1D;
    struct FDM2DInfo fInfo2D;
    int dimCount;
   };

