/***/
/* Definition of dynamic memory wrappers:
/***/

#ifndef __nmr_memory_h

#define __nmr_memory_h

#ifdef LINUX
   void free();
#endif

#ifdef WINXP 
   void free();
#endif

   int    setAlloc();
   int    getMemUsed();

   float  *fltAlloc();
   int    *intAlloc();

   double   *dFltAlloc();
   long int *dIntAlloc();

   char  *strDup();
   char  *strDup2();
   char  *charMalloc();
   void  *voidMalloc();

   int   *intReAlloc();
   float *fltReAlloc();

   char  *charReMalloc();
   void  *voidReMalloc();

   float  **matAlloc();
   double **matAllocD();

   float  **mat2Ptr();
   float  **mat2PtrZ();
   double **mat2PtrD();

   float  ***mat2Ptr3D();
   float  ****mat2Ptr4D();

   int    matPtrFree3D();
   int    matPtrFree4D();

   int    matFree();
   int    matFreeD();

   void  unAlloc();

#ifdef VOID
#undef VOID
#endif

#ifdef SOLARIS
#define VOID   void
#define IOPTR  void 
#define IOSIZE size_t
#define SIZE_T size_t 
#define SIZE_DEFS
#endif

#if defined (WINNT) || defined (WIN95)
#define VOID   void
#define IOPTR  void 
#define IOSIZE size_t
#define SIZE_T size_t 
#define SIZE_DEFS
#endif

#ifdef SUN
#ifndef SIZE_DEFS
#define VOID   void
#define IOPTR  char
#define IOSIZE int
#define SIZE_T unsigned
#define SIZE_DEFS
#endif
#endif

#ifdef SGI
#define VOID   void
#define IOPTR  void 
#define IOSIZE unsigned
#define SIZE_T size_t 
#define SIZE_DEFS
#endif

#ifdef IBM 
#define VOID   void
#define IOPTR  void
#define IOSIZE size_t
#define SIZE_T size_t
#define SIZE_DEFS
#endif

#ifdef HP 
#define VOID   void
#define IOPTR  void
#define IOSIZE size_t
#define SIZE_T size_t
#define SIZE_DEFS
#endif

#ifdef ALPHA
#define VOID   void
#define IOPTR  char
#define IOSIZE size_t
#define SIZE_T size_t
#define SIZE_DEFS
#endif

#ifndef SIZE_DEFS
#define VOID   void
#define IOPTR  void 
#define IOSIZE size_t
#define SIZE_T size_t 
#define SIZE_DEFS
#endif
 
#define deAlloc(WHO,PTR,LEN)      unAlloc( WHO, (VOID *)PTR, (SIZE_T)(LEN) )
#define deAllocS(WHO,PTR,LEN)     unAllocS( WHO, (VOID *)PTR, (SIZE_T)(LEN) )

#define voidReAlloc(WHO,PTR,NEWLEN,OLDLEN) \
   voidReMalloc( WHO, (VOID *)PTR, (SIZE_T)(NEWLEN), (SIZE_T)(OLDLEN) )

#define charReAlloc(WHO,PTR,NEWLEN,OLDLEN) \
   charReMalloc( WHO, (char *)PTR, (SIZE_T)(NEWLEN), (SIZE_T)(OLDLEN) )

#define voidAlloc( WHO, LEN )    voidMalloc( WHO, (SIZE_T)(LEN) )
#define charAlloc( WHO, LEN )    charMalloc( WHO, (SIZE_T)(LEN) )

#define MTEST( TXT, EXP )                                                 \
   if (!(EXP))                                                            \
      {                                                                   \
       (void) fprintf( stderr, "%s Error allocating memory.\n", TXT );    \
       error = 1;                                                         \
       goto shutdown;                                                     \
      } error = 0

#endif /* __nmr_memory_h */
