/***/
/* nmrcodes.h: parameter definitions used for NMRPipe processing functions.
/***/

/***/
/* Definitions for parallel processing by nmrPipe client/servers:
/***/

#define SMODE_NONE   0     /* Normal I/O and data stream.                  */
#define SMODE_CLIENT 1     /* Process is a client with socket to servers.  */
#define SMODE_SERVER 2     /* Process is a server with a socket to client. */
#define SMODE_SOCKIO 3     /* Data stream is a socket.                     */

/***/
/* Parameters describing adjustment of data type during processing:
/***/

#define XIFLAG    0   /* Delete imaginaries of complex data.            */
#define DTFLAG    1   /* Delete imaginaries of hypercomplex data.       */
#define DIFLAG    2   /* Delete current imaginaries, complex or hyper.  */
#define AIFLAG    3   /* Add imaginaries to real data.                  */
#define ADFLAG    4   /* Add imaginaries to hypercomplex data.          */
#define ACFLAG    5   /* Add current imaginaries, real or complex.      */

#define MAXTYPE   6

/***/
/* Slice Code Values; positive slice code means slice number.
/***/

#define FN_UNKNOWN   -1       /* Unknown code; value before initiization.   */
#define FN_PARAMS    -2       /* Extract parameters from command line.      */
#define FN_INIT      -3       /* Initialize; adjust header if needed.       */
#define FN_SHUTDOWN  -4       /* Shutdown, free memory if needed.           */

/***/
/* Use of selected region limits for processing or pass-through:
/***/

#define SELECT_ALL     0      /* Process all data vectors.                  */
#define SELECT_INSIDE  1      /* Process only vectors inside the region.    */
#define SELECT_OUTSIDE 2      /* Process only vectors outside the region.   */
