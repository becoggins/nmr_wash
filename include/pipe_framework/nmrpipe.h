/***/
/* nmrPipe.h: structure definitions used for NMRPipe processing functions.
/***/

#include "dimloc.h"
#include "nmrcodes.h"

/***/
/* Specific to nmrPipe pipeline-based processing:
/***/

int initIO(),         /* Opens input or output files as needed.              */
    getHeader(),      /* Reads header, initializes sizes, etc.               */
    putHeader(),      /* Writes header at begining of stream.                */
    getSlice(),       /* Reads the next 1D slice from stream.                */
    procStream(),     /* Process all slices in input stream.                 */
    nextSlice(),      /* Determines if there is another slice to process.    */
    procDistribute(), /* Distributed processing of input stream.             */
    procSlice(),      /* Calls function to process 1D slice.                 */
    putSlice2(),      /* Actual function to write a 1D slice.                */
    flushSlice(),     /* Used by pipe to flush output buffer.                */
    sendSlice(),      /* Sends an unprocessed slice to a server.             */
    receiveSlice(),   /* Accepts a processed slice from a server.            */
    sysCmnd();        /* System call utility; allows performance timing.     */

/***/
/* General modules for use of nmrPipe processing library.
/***/

int initNMRAux(),     /* First-time call to initialize internals.            */
    initNMRSizes(),   /* Extracts data sizes and quad states.                */
    initInfo(),       /* Sets data and function pointers to NULL.            */
    updateHeader(),   /* Function initialize, header post-proc setup.        */
    allocSlice(),     /* Allocates rdata/idata slice memory.                 */
    showSlice(),      /* Show status of 1D slice process.                    */
    showInfo(),       /* Show an informational message via X11 or screen.    */
    freeSlice(),      /* Deallocation for pipe and function.                 */
    getparams(),      /* Gets command line arguments for pipe and function.  */
    putSlice(),       /* Used by functions to write or return a 1D slice.    */
    initSignal(),     /* Starts nmrPipe signal handling.                     */
    getUserSignal(),  /* Retrives User Signal for an application.            */
    setUserSignal();  /* Sets User Signal for an application.                */

void getSignal();     /* Called by OS when signal is received.               */
void nmrMathErr();    /* Handles math library errors.                        */

/***/
/* Index of function calls:
/***/

struct ProcFuncInfo
   {
    int  (*func)();           /* Pointer to the function itself.              */
    char *name;               /* Unique name for invoking the function.       */
    char *descrip;            /* Brief Description of the function.           */
    char *args;               /* Function arguments, if any.                  */
   };

/***/
/* Parameters describing data source and destination:
/**/

struct ProcDataInfo
   {
    float *rdata;             /* Pointer to 1D real data to process.          */
    float *idata;             /* Pointer to 1D imag data to process or zeros. */
    float *fdata;             /* Pointer to updated file header FDATA.        */
    float *fdataOrig;         /* Pointer to original file header FDATA.       */
    float *inBuff;            /* Pointer to array for buffered input.         */
    float *outBuff;           /* Pointer to array for buffered output.        */
    float *inBuffPtr;         /* Pointer to current slice in buffered input.  */
    float *outBuffPtr;        /* Pointer to current slice in buffered output. */
    int   quadState;          /* 1 = idata are zeroes, 2 = complex data.      */
    int   size;               /* Length of 1D data in rdata R|I.              */
    int   specnum;            /* Length of current Y dimension R+I.           */
    int   zSize;              /* Length of current Z dimension R+I.           */
    int   aSize;              /* Length of current A dimension R+I.           */
    int   dimCount;           /* Number of dimensions in this data set.       */
    int   sliceCount;         /* Total 1D slices to process.                  */
    int   varCountFlag;       /* Indicates variable slice-count data.         */
    int   maxSize;            /* Max 1D size of rdata during processing.      */
    int   outSize;            /* Final 1D size of rdata after processing.     */
    int   timeSize;           /* Apparent time domain size of rdata.          */
    int   outQuadState;       /* Final quad state of rdata/idata.             */
    int   ftFlag;             /* 1 = Freq Domain, 0 = Time Domain.            */
    int   transposed;         /* 2D Transpose state of original data.         */
    int   typeFlags[MAXTYPE]; /* Describes data type adjustments requested.   */
    int   inSwapFlag;         /* Indicates byte swap of header on input.      */
    int   outSwapFlag;        /* Indicates byte swap of header on output.     */
    float min;                /* Tally of minimum rdata value.                */
    float max;                /* Tally of maximum rdata value.                */
    int   scaleFlag;          /* 1=Max and Min are in use and valid.          */
    int   sliceCode;          /* Origin-1 Slice number or slice code (below). */
    int   slicesLeft;         /* Count of slices left in input stream.        */
    int   selectMode;         /* Describes how to use selected region limits. */
    int   selX1[MAXDIM];      /* Location of selected region.                 */
    int   selXN[MAXDIM];      /* Location of selected region.                 */
    int   locList[MAXDIM];    /* Location of current slice in data.           */
    int   firstPlane;         /* First plane to process, if restricted.       */
    int   lastPlane;          /* Last plane to process, if restricted.        */
    int   debug;              /* Code to determine debug level.               */
    int   verbose;            /* Code to determine verbose level.             */
    int   silentExit;         /* Flag to suppress error  messages on exit.    */
    int   verbInc;            /* Increment between slice reports.             */
    int   x11Flag;            /* Indicates if X11 graphics are available.     */
    int   argc;               /* From command line arguments.                 */
    char  **argv;             /* From command line arguments.                 */
    char  *inName;            /* Input name; NULL means stdin.                */
    char  *outName;           /* Output name; NULL means stdout.              */
    char  *inHdrName;         /* Optional Source of input header.             */
    char  *outHdrName;        /* Optional Destination of output header.       */
    char  *fnName;            /* User's function invocation name.             */
    char  *message;           /* Buffer for informational messages.           */
    UNIT  inUnit;             /* Unit connected to input; usually stdin.      */
    UNIT  outUnit;            /* Unit connected to output; usually stdout.    */
    int   maxTries;           /* Max retry count for blocking read/write.     */
    int   timeOut;            /* Timeout in usecs for blocking read/write.    */
    int   finalDelay;         /* Timeout in usec before exiting.              */
    int   rdFlag;             /* Flag to suppress read via nmrPipe.           */
    int   wrFlag;             /* Flag to suppress write via nmrPipe.          */
    int   ovFlag;             /* Flag to force overwrite of existing data.    */
    int   inPlace;            /* Flag to allow in-place processing.           */
    int   vecFlag;            /* Indicates function is strictly 1D vector.    */
    int   inCapacity;         /* 1D Slice Capacity of Input Buffer.           */
    int   outCapacity;        /* 1D Slice Capacity of Output Buffer.          */
    int   buffCountIn;        /* Current count of slices in input buffer.     */
    int   buffCountOut;       /* Current count of slices in output buffer.    */
    int   serverMode;         /* Defines client/server mode, as above.        */
    int   serverID;           /* Client's server ID for this process.         */
    int   serverCount;        /* Number of connected servers.                 */
    int   serverSock;         /* Socket used by server to start connection.   */
    int   serverMsgSock;      /* Socket used by server to get and put data.   */
    int   inSock;             /* Socket used as alternative to input file.    */
    int   outSock;            /* Socket used as alternative to output file.   */
    int   serverMaxTries;     /* Max retry count for blocking server I/O.     */
    int   serverTimeOut;      /* Timeout in usecs for blocking server I/O.    */
    Server server[MAXSOCK];   /* List of server information.                  */
   };

/***/
/*-------------- Addition of New Functions ---------------------------------
/***/

/***/
/*
/* The NMRPipe system will use the function:
/*
/*   struct ProcFuncInfo *getFuncList()
/*
/* to obtain a pointer to the table of valid functions;
/* the table and functions should be defined as below:
/*
/* Processing Function Codes, Names, and Descriptions.
/* Provide declarations as below to add a new function.
/*
/* The function should be of the form:
/*
/*   int procFunc( struct ProcDataInfo *dataInfo )
/*
/* It will be called repeatedly by the nmrPipe program to perform
/* initialization, processing, output (if needed) and cleanup procedures.
/* The action to be taken is indicated by dataInfo->sliceCode:
/*
/* FUNCTION RETURN VALUES:
/*
/*   Returns zero on sucessful completion.
/*   Returns non-zero on failure.
/*
/* USER INPUT/OUTPUT:
/*
/*   Use command line arguments for extracting user input.
/*   Use stderr to send informational messages to screen.
/*   Use the showInfo() function for special one-time-only messages.
/*
/* FN_PARAMS:
/*
/*   Extract any arguments from command line when sliceCode = FN_PARAMS. 
/*   Display help text if "-help" flag is found in argument list.
/*   Return non-zero if "-help" flag is found.
/*   Note: file header FDATA is not available at this stage.
/*
/* FN_INIT:
/*
/*   Adjust file header for post process state when sliceCode = FN_INIT.
/*   Perform pre-processing initialization when sliceCode = FN_INIT.
/*   Interpret command line args which require use of header array FDATA.
/*
/* FN_SHUTDOWN:
/*
/*   Perform shutdown and deallocation when sliceCode = FN_SHUTDOWN.
/*
/* PROCESSING:
/*
/*   Perform a 1D process when sliceCode is greater than zero.
/*   Write the 1D slice after 1D process when sliceCode is greater than zero.
/*   Use the putSlice() function to write the 1D slice.
/*   The sliceCode will start at 1 and end at dataInfo->sliceCount. 
/***/

/***/
/* Declaration 1: the function prototypes (if the function is not 
/*                defined somewhere else such as a header file):
/*
/*    int uNULL(), uFT();
/*
/* Declaration 2: the function descriptions:
/*
/*  C-Function-Name,        "Cmnd-Line-Invoke-Name",
/*   "Func-Descrption",     "[Cmnd-Line-Flags]", . . . 
/*
/*   static struct ProcFuncInfo funcInfoList[] = 
/*      {
/*       uNULL,                    "NULL",
/*        "Null Function",         "",
/*       uFT,                      "FT",
/*        "Fourier Transform",     "[-inv -dt]",
/*       NULL,                     (char *) NULL,
/*        (char *) NULL,           (char *) NULL
/*   };
/*
/*
/* Nota Bene: the function table must include a "uNULL" function,
/*            which is called when no function is explicitly
/*            specified.
/***/
