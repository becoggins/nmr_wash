/***/
/* Random number generation.
/***/

#include <stdlib.h>

#ifndef RAND_MAX 

#ifdef  IBM_FROM_HELL
#define RAND_MAX       32768.0
#else
#define RAND_MAX  2147483648.0 
#endif

#endif

#ifdef CUBE
#undef  RAND_MAX
#define RAND_MAX 32768.0
#endif

#if defined (WINNT) || defined (MAC_OSX) || defined(WINXP)
double drand48();
#endif

#define SRAND(ISEED)     (void)sRand(ISEED)
#define SRANDINIT(ISEED) (void)sRandInit(ISEED)

#define RANDOM1          (((float)rand())/(float)(RAND_MAX+1.0))
#define DRANDOM1         drand48()

/***/
/* Generates normally distributed random numbers with unit variance:
/***/

double gaussRandD();
float  gaussRand();
int    sRand();

