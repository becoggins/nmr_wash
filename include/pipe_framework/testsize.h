/***/
/* testsize.h: definitions for returning file and file system sizes;
/*             intended to accommodate sizes larger than those
/*             which can be represented by an int.
/***/

#define DEF_BLOCK_SIZE 1024

#define FILE_SIZE_FAIL  -1 /* Valid size could not be extracted.             */
#define FILE_SIZE_OK     0 /* Valid size is returned correctly as an int.    */
#define FILE_SIZE_OVER   1 /* Valid size can't be expressed as an int.       */

#define NMR_FILE         0 /* Size pertains to a single data file or stream. */
#define NMR_SERIES       1 /* Size pertains to a multi-file data set.        */

/***/
/* The total size in bytes will be expressed as:
/*
/*   dTotalBytes = (double)blockSize*blockCount + byteCount
/*   iTotalBytes =         blockSize*blockCount + byteCount
/*
/* The block size is arbitrary, not neccessarily the actual
/* block size used by the file system.
/****/

struct FileSize
   {
    int    blockSize;
    int    blockCount;
    int    byteCount;
    int    iTotalBytes;
    double dTotalBytes;
    int    status;
   };

double getFileBytes(), getFreeBytes(), getNMRBytes();
int    fileSizeInit();
