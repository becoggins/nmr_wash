/***/
/* Include file for string tokenizing functions:
/***/

    int   badToken;

    char  *getToken();
    char  *getTokenS();
    char  *getTokenC();

    float getTokenR();
    int   getTokenI();
    
    char  *leftToken();
    char  *rightToken();

    int   getTokenLoc();
    int   cntToken();
    int   cntTokenS();
    int   cntTokenC();
    int   freeToken();

    int   isInteger();
    int   isFloat();
