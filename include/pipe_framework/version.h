/***/
/* version.h: some convenient definitions for version display.
/***/

#define NMRPIPE_REV_TXT "2011.205.14.25"
#define NMRPIPE_VER_TXT "Version 5.97"

#define FPR_VERSION                                          \
   (void)fprintf( stderr,                                    \
                  " *** NMRPipe System %s Rev %s ***\n",     \
                  NMRPIPE_VER_TXT, NMRPIPE_REV_TXT )
