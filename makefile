#Makefile for nmr_wash

CC 							= gcc
CXX 						= g++
LD							= g++
CFLAGS 						= $(pipe_include_path) $(pipe_defs) -O3
CXXFLAGS 					= $(include_path) -O3
LDFLAGS 					= $(lib_path)
PREFIX						= /usr/local

boost_include_path 			= libs/boost
boost_lib_path				= libs/boost/stage/lib
scitbx_include_path 		= libs
cminpack_path 				= libs/cminpack-1.1.2
blitz_path              	= libs/blitz-0.10
becmisc_include_path		= libs
nmrdata_include_path		= libs/nmrdata/include
nmrdata_lib_path			= libs/nmrdata
include_path 				= -I$(boost_include_path) -I$(scitbx_include_path) -I$(cminpack_path) -I$(blitz_path) -I$(becmisc_include_path) -I$(nmrdata_include_path) -Iinclude/nmr_wash -Iinclude/pipe_framework
pipe_include_path 			= -Iinclude/pipe_framework -Iinclude/nmr_wash

uname						= $(shell uname)

ifeq ($(uname),Darwin)
pipe_defs					= -DMAC_OSX -DUSE_F77 -DNIH_YACC_PROTO -DUSE_TCLTK -DUSE_BLT
endif

ifeq ($(uname),Linux)
pipe_defs					= -DLINUX -DUSE_F77 -DNIH_YACC_PROTO -DUSE_TCLTK -DUSE_BLT -DSVR4 -DTRAD_C -DUSE_TCLTK8 -D__NO_STRING_INLINES
endif

boost_libs					= libboost_date_time libboost_exception libboost_filesystem libboost_program_options libboost_system libboost_thread
boost_lib_targets 			= $(addprefix $(boost_lib_path)/, $(addsuffix -mt.a, $(boost_libs) ) $(addsuffix -mt-d.a, $(boost_libs) ) )
lib_path 					= -L$(boost_lib_path) -L$(cminpack_path) -L$(nmrdata_lib_path)
LIBS 						= -lminpack -lboost_date_time-mt -lboost_exception-mt -lboost_filesystem-mt -lboost_program_options-mt -lboost_system-mt -lboost_thread-mt -lnmrdata -lm

ifeq ($(uname),Linux)
LIBS						+= -lpthread
endif

main_src_path				= src
main_build_path				= build
build_subdirs				= clean scrub scrub_simulator pipewash pipewash/pipe_framework console_support
build_dirs					= $(main_build_path) $(addprefix $(main_build_path)/, $(build_subdirs) )

washer_targets 				= cleaner.o jobs.o log_relay.o pos_list.o psf.o sampling.o scrubber.o status.o support.o washer_base.o
console_targets				= console_support/apod_cmd_line.o console_support/console_status.o 
pipe_targets 				= $(addprefix pipe_framework/, cmndargs.o dataio.o fdatap.o fixfdata.o inquire.o memory.o nmraux.o nmrpipe.o nmrsocket.o nmrtime.o raise.o rand.o rdtext.o specunit.o stralloc.o syscalls.o testsize.o token.o )
clean_target_list			= $(addprefix $(main_build_path)/, $(washer_targets) $(console_targets) clean/main.o )
scrub_target_list			= $(addprefix $(main_build_path)/, $(washer_targets) $(console_targets) scrub/main.o )
scrub_sim_target_list		= $(addprefix $(main_build_path)/, $(washer_targets) $(console_targets) scrub_simulator/data_generator.o scrub_simulator/main.o )
pipewash_target_list		= $(addprefix $(main_build_path)/, $(washer_targets) $(addprefix pipewash/, pipewash.o userproc.o $(pipe_targets) ) )
washer_lib_target_list		= $(addprefix $(main_build_path)/, $(washer_targets) )


everything : checkdirs libs all

$(boost_lib_targets) : 
	cd libs/boost; mkdir stage; ./bootstrap.sh --libdir=stage; ./b2 link=static threading=multi variant=release,debug --layout=tagged

$(cminpack_path)/libminpack.a :
	cd $(cminpack_path); make

$(blitz_path)/lib/libblitz.la :
	cd $(blitz_path); ./configure; make lib
	
$(nmrdata_lib_path)/libnmrdata.a :
	cd $(nmrdata_lib_path); make

libs : $(boost_lib_targets) $(cminpack_path)/libminpack.a $(blitz_path)/lib/libblitz.la $(nmrdata_lib_path)/libnmrdata.a

$(main_build_path)/%.o : $(main_src_path)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(main_build_path)/console_support/%.o : $(main_src_path)/console_support/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean : $(clean_target_list)
	$(LD) $(LDFLAGS) -o clean $^ $(LIBS)

$(main_build_path)/clean/%.o : $(main_src_path)/clean/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

scrub : $(scrub_target_list)
	$(LD) $(LDFLAGS) -o scrub $^ $(LIBS)

$(main_build_path)/scrub/%.o : $(main_src_path)/scrub/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

scrub_simulator : $(scrub_sim_target_list)
	$(LD) $(LDFLAGS) -o scrub_simulator $^ $(LIBS)

$(main_build_path)/scrub_simulator/%.o : $(main_src_path)/scrub_simulator/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

pipewash : $(pipewash_target_list) 
	$(LD) $(LDFLAGS) -o pipewash $^ $(LIBS)

$(main_build_path)/pipewash/%.o : $(main_src_path)/pipewash/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(main_build_path)/pipewash/%.o : $(main_src_path)/pipewash/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(main_build_path)/pipewash/pipe_framework/%.o : $(main_src_path)/nmr_wash/pipe_framework/pipewash/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

libnmr_wash.a : $(washer_lib_target_list)
	ar r $@ $(washer_lib_target_list); ranlib $@

all : clean scrub pipewash libnmr_wash.a

checkdirs : $(build_dirs)

$(build_dirs) :
	mkdir -p $@

install-built : clean scrub pipewash
	cp clean $(PREFIX)/bin
	cp scrub $(PREFIX)/bin
	cp pipewash $(PREFIX)/bin

install-pc :
	cp clean $(PREFIX)/bin
	cp scrub $(PREFIX)/bin
	cp pipewash $(PREFIX)/bin

install-man :
	cp doc/man/man1/* $(PREFIX)/man/man1

cleanup :
	rm -rf $(build_dirs)
	rm -f scrub
	rm -f clean
	rm -f scrub_simulator
	rm -f pipewash
	rm -f libnmr_wash.a

cleanup_libs :
	cd libs/boost; ./b2 --clean-all
	cd $(cminpack_path); make clean
	cd $(blitz_path); make clean
	cd $(nmrdata_lib_path); make clean

.PHONY: checkdirs cleanup all libs cleanup_libs install-built install-pc install-man


