//  Copyright (c) 2012-2014, Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <vector>
#include <string>
#include <iostream>
#include <sys/ioctl.h>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"
#include "console_support.h"

const std::string usage_info( "clean pattern_file input_file [options]\n" );
const std::string version_info( "clean " + nmr_wash::version + " compiled " + __DATE__ "\n" + nmr_wash::copyright + "\n" );

int main( int argc, const char * argv[] )
{
    namespace po = boost::program_options;
    using namespace BEC_NMRData;
    using namespace nmr_wash;
    
    try
    {
        winsize ws;
        ioctl( 0, TIOCGWINSZ, &ws );
        int disp_cols = ws.ws_col > 100 ? 100 : ws.ws_col;
        
        po::options_description parameters;
        parameters.add_options()
        ( "pattern", po::value< std::string >(), "sampling pattern file" )
        ( "input", po::value< std::string >(), "spectrum file to process" )
        ( "output", po::value< std::string >(), "destination for processed spectrum (unless --in-place is given)" );
        
        po::options_description general_options( "General options", disp_cols, disp_cols - 24 );
        general_options.add_options()
        ( "help,h", "show this help message" )
        ( "version", "show version information" )
        ( "in-place", "process the input spectrum in-place" )
        ( "overwrite,w", "overwrite an existing output file with the same name" )
        ( "quiet,q", "suppress information about the progress of the calculation" )
        ( "threads,t", po::value< int >(), "number of threads to use while calculating (default is to use one thread per processor core)" );
        
        po::options_description dim_options( "Dimension assignment", disp_cols, disp_cols - 24 );
        dim_options.add_options()
        ( "f1,1", po::value< dimension_assignment >(), "interpretation of F1 dimension (U, V, W, index).  Supply dimension assignment flags to override the automatic dimension assignment; if you override one, you will need to override the remaining ones up to the number of dimensions in the experiment.  Valid arg values are u, v, w, index." )
        ( "f2,2", po::value< dimension_assignment >(), "interpretation of F2 dimension (U, V, W, index)" )
        ( "f3,3", po::value< dimension_assignment >(), "interpretation of F3 dimension (U, V, W, index)" )
        ( "f4,4", po::value< dimension_assignment >(), "interpretation of F4 dimension (U, V, W, index)" );
        
        po::options_description apod_options( "Dimension apodization information", disp_cols, disp_cols - 24 );
        apod_options.add_options()
        ( "f1-apod", po::value< dimension_apod >(), "apodization for F1 dimension.  Required for each indirect dimension that was apodized during post-processing (unless the data are in NMRPipe format, where this information can be extracted from the file header automatically).  Specify the apodization function by putting the corresponding NMRPipe command in quotes, e.g.\n  --f1-apod \"-fn EM -lb 10 -c 0.5\"" )
        ( "f2-apod", po::value< dimension_apod >(), "apodization for F2 dimension" )
        ( "f3-apod", po::value< dimension_apod >(), "apodization for F3 dimension" )
        ( "f4-apod", po::value< dimension_apod >(), "apodization for F4 dimension" )
        ( "f1-fpc", po::value< float >(), "first point correction for F1 dimension.  Use this flag if a correction is needed but not otherwise specified either in the file header or in an f1-apod flag." )
        ( "f2-fpc", po::value< float >(), "first point correction for F2 dimension" )
        ( "f3-fpc", po::value< float >(), "first point correction for F3 dimension" )
        ( "f4-fpc", po::value< float >(), "first point correction for F4 dimension" );
        
        po::options_description pattern_parsing_options( "Sampling pattern parsing options", disp_cols, disp_cols - 24 );
        pattern_parsing_options.add_options()
        ( "u-col", po::value< int >(), "the column of the sampling pattern containing the U dimension (N.B. choosing any of these overrides automatic parsing of the sampling pattern)" )
        ( "v-col", po::value< int >(), "the column of the sampling pattern containing the V dimension" )
        ( "w-col", po::value< int >(), "the column of the sampling pattern containing the W dimension" )
        ( "weight-col", po::value< int >(), "the column of the sampling pattern containing the weighting information, if applicable" )
        ( "off-grid", "the sampling pattern is NOT defined on a grid" )
        ( "ignore-weights", "ignore any weights found in the sampling pattern file" );
        
        po::options_description position_options( "Options for calculating only part of an input spectrum", disp_cols, disp_cols - 24 );
        position_options.add_options()
        ( "position", po::value< std::vector< std::string > >()->composing()->multitoken(), "process only the specific position(s) in the input spectrum, rather than the entire file (repeat this option for each position to calculate, OR list the positions separated by spaces after the --position flag).  Specify a position by giving one integer for each index dimension in the spectrum, separated by commas but no white space, where each integer designates a data point ranging from zero at the low-frequency end to the number of points on that dimension minus one at the high-frequency end.  The dimensions should be listed in the order of least-frequently changing to most-frequently changing (e.g. if F1 is an index dimension, list it first, then F2, etc.)." )
        ( "insert-in-place", "write the result for each position at the corresponding position in either the original input file, if no output filename is provided, or a single output file, if an output filename is provided on the command line.  If an output file is named on the command line and does not exist, it will be created and all positions not listed for processing will be zeroed." )
        ( "separate-outputs", "write the result for each position into a separate lower-dimensional file, generated from the output name by adding an underscore and one or more position numbers" );
        
        po::options_description clean_options( "CLEAN options" );
        clean_options.add_options()
        ( "gain,g", po::value< float >()->default_value( 10.0F ), "gain (%)" )
        ( "snr-threshold", po::value< float >()->default_value( 5.0F ), "CLEAN stops when the signal-to-noise ratio is less than or equal to this many standard deviations of the noise" )  //  N.B. This maps to threshold_intensity
        ( "noise-change-threshold", po::value< float >()->default_value( 5.0F ), "CLEAN stops when the average noise level has not changed more than this percentage for 25 consecutive iterations" )   //  N.B. This maps to threshold_noise
        ( "max-iter", po::value< int >(), "The maximum number of CLEAN iterations to allow for each position in the spectrum that is processed(the default is unlimited)" );
        
        po::options_description pure_comp_options( "Linearity and pure component calculation options", disp_cols, disp_cols - 24 );
        pure_comp_options.add_options()
        ( "linearity", po::value< linearity_modes >()->default_value( tde ), "linearity mode (fd, tde, td; default is tde)" )
        ( "pure-comp-mode", po::value< pure_component_modes >()->default_value( contour_ellipsoid ), "calculation mode (contour-irregular, contour-ellipsoid, fixed-ellipsoid, delta-function); default is contour-ellipsoid; N.B. TD linearity mode requires delta functions for pure components and will override this setting" )
        ( "pc-contour-irregular-level", po::value< float >()->default_value( 1.0F ), "contour level for contour-irregular calculations (% of central peak intensity)" )
        ( "pc-contour-irregular-margin", po::value< bool >()->default_value( false ), "for contour-irregular calculations, whether or not to add a one point margin around the irregular contour" )
        ( "pc-contour-ellipsoid-level", po::value< float >()->default_value( 1.0F ), "contour level for contour-ellipsoid calculations (% of central peak intensity)" )
        ( "pc-contour-ellipsoid-margin", po::value< float >()->default_value( 0.0F ), "margin width for contour-ellipsoid calculations (after ellipsoid is circumscribed around contour, it is enlarged by this % of its own size)" )
        ( "pc-fixed-ellipsoid-size", po::value< float >()->default_value( 2.0F ), "size of ellipsoid for fixed-ellipsoid calculations (each semiaxis is this % of the spectral width)" );
        
        po::options_description psf_options( "PSF and pure component output", disp_cols, disp_cols - 24 );
        psf_options.add_options()
        ( "psf", po::value< std::string >(), "save the calculated PSF to a spectrum file" )
        ( "pure-comp", po::value< std::string >(), "save the calculated pure component to a spectrum file" );
        
        po::options_description diag_options( "Diagnostic options", disp_cols, disp_cols - 24 );
        diag_options.add_options()
        ( "log,l", po::value< std::string >(), "record a log to a text file" )
        ( "verbose-log", "record complete details of the calculation to the log file" )
        ( "noise-report-csv,r", po::value< std::string >(), "record a report on noise (artifact) reduction to a CSV file" )
        ( "output-components-only", "write only the components to output" )
        ( "output-residuals-only", "write only the residuals to output" );
        
        po::options_description all_options( disp_cols, disp_cols - 24 );
        all_options.add( parameters ).add( general_options ).add( dim_options ).add( apod_options ).add( position_options ).add( clean_options ).add( pure_comp_options ).add( psf_options ).add( diag_options );
        
        po::options_description options_for_help( disp_cols, disp_cols - 24 );
        options_for_help.add( general_options ).add( dim_options ).add( apod_options ).add( position_options ).add( clean_options ).add( pure_comp_options ).add( psf_options ).add( diag_options );
        
        po::positional_options_description positional_options;
        positional_options.add( "pattern", 1 );
        positional_options.add( "input", 1 );
        positional_options.add( "output", 1 );
        
        po::variables_map vm;
        
        //  *********************************************************
        //  Parse command line
        
        try
        {
            po::store( po::command_line_parser( argc, argv ).options( all_options ).positional( positional_options ).style( ( po::command_line_style::unix_style | po::command_line_style::allow_long_disguise ) ^ po::command_line_style::allow_guessing ).run(), vm );
            po::notify( vm );
        }
        catch( exceptions::bad_dimension_assignment_string &e )
        {
            std::cerr << "clean: An invalid dimension assignment was provided.\n";
            return 1;
        }
        catch( exceptions::bad_pure_component_mode_string &e )
        {
            std::cerr << "clean: An invalid pure component calculation mode was provided.\n";
            return 1;
        }
        catch( po::unknown_option &e )
        {
            std::cerr << "clean: Unrecognized option " << e.get_option_name() << ".\n";
            return 1;
        }
        catch( po::invalid_option_value &e )
        {
            std::cerr << "clean: An invalid option value was provided.\n";
            return 1;
        }
        catch( po::validation_error &e )
        {
            std::cerr << "clean: Value for option " << e.get_option_name() << " is not valid: " << e.what() << "\n";
            return 1;
        }
        catch( po::error &e )
        {
            std::cerr << "clean: An error occurred while parsing the command line: " << e.what() << "\n";
            return 1;
        }
        
        //  *********************************************************
        //  Respond to information flags
        
        if( vm.count( "help" ) )
        {
            std::cout << usage_info << options_for_help << "\n";
            return 1;
        }
        if( vm.count( "version" ) )
        {
            std::cout << version_info << "\n";
            return 1;
        }
        
        //  *********************************************************
        //  Check for required parameters
        
        if( !vm.count( "pattern" ) )
        {
            std::cerr << "clean: No sampling pattern.\n";
            return 1;
        }
        if( !vm.count( "input" ) )
        {
            std::cerr << "clean: No input file.\n";
            return 1;
        }
        if( !vm.count( "position" ) && !vm.count( "output" ) && !vm.count( "in-place" ) )
        {
            std::cerr << "clean: Either an output file or the --in-place (-i) option must be provided.\n";
            return 1;
        }
        if( vm.count( "position" ) && !vm.count( "insert-in-place" ) && !( vm.count( "output" ) && vm.count( "separate-outputs" ) ) )
        {
            std::cerr << "clean: When processing specific positions, choose either to write the results back to the input (--insert-in-place), to write them to the corresponding positions in an output file (--output and --insert-in-place), or to write them to separate output files for each position processed (--output and --separate-outputs).\n";
            return 1;
        }
        if( vm.count( "output-components-only" ) && vm.count( "output-residuals-only" ) )
        {
            std::cerr << "scrub: Both --output-components-only and --output-residuals-only were provided; please choose only one at a time.\n";
            return 1;
        }
        
        //  *********************************************************
        //  Get quiet flag
        bool quiet = vm.count( "quiet" ) ? true : false;
        
        FILE *log = 0;
        if( vm.count( "log" ) )
        {
            std::string log_filename( vm[ "log" ].as< std::string >() ); 
            log = fopen( log_filename.c_str(), "w+" );
            if( !log )
            {
                std::cerr << "clean: The log file " << log_filename << "could not be created.\n";
                return 1;
            }
        }
        log_relay log_and_cout( log, std::cout, quiet );
        
        if( !quiet || log ) log_and_cout << version_info << "\n" << std::flush;
        
        //  *********************************************************
        //  Load sampling pattern
        
        std::string pattern_filename = vm[ "pattern" ].as< std::string >();
        if( !quiet || log ) log_and_cout << "Parsing sampling pattern file " << pattern_filename << "..." << std::flush;
        sampling_pattern sp;
        try
        {
            if( vm.count( "u-col" ) || vm.count( "v-col" ) || vm.count( "w-col" ) || vm.count( "weight-col" ) || vm.count( "off-grid" ) )
            {
                int u_col = vm.count( "u-col" ) ? vm[ "u-col" ].as< int >() - 1 : -1;
                int v_col = vm.count( "v-col" ) ? vm[ "v-col" ].as< int >() - 1 : -1;
                int w_col = vm.count( "v-col" ) ? vm[ "w-col" ].as< int >() - 1 : -1;
                int weight_col = vm.count( "weight-col" ) ? vm[ "weight-col" ].as< int >() - 1 : -1;
                bool on_grid = vm.count( "off-grid" ) ? false : true;
                
                sp.load( pattern_filename, u_col, v_col, w_col, weight_col, on_grid );
            }
            else sp.load( vm[ "pattern" ].as< std::string >() );
        }
        catch( exceptions::file_not_found &e )
        {
            std::cerr << "\nclean: The pattern file " << e.filename << " could not be found.\n";
            return 1;
        }
        catch( exceptions::cant_open_file &e )
        {
            std::cerr << "\nclean: The pattern file " << e.filename << " could not be opened.\n";
            return 1;
        }
        catch( exceptions::sampling_pattern::file_format_not_understood &e )
        {
            std::cerr << "\nclean: The pattern file " << e.filename << " could not be interpreted automatically.\n";
            return 1;
        }
        catch( exceptions::sampling_pattern::file_format_not_consistent &e )
        {
            std::cerr << "\nclean: The pattern file " << e.filename << " does not have a consistent format: one or more entries have extra or missing data fields.\n";
            return 1;
        }
        catch( exceptions::sampling_pattern::file_format_doesnt_match &e )
        {
            std::cerr << "\nclean: The pattern file " << e.filename << " could not be processed according to the format options provided.\n";
            return 1;
        }
        catch( exceptions::sampling_pattern::bad_file_format_guidance &e )
        {
            std::cerr << "\nclean: Invalid options for the pattern file format.\n";
            return 1;
        }
        
        if( !quiet || log )
        {
            log_and_cout << "done.\n";
            log_and_cout << "Sampling pattern has " << sp.get_num_of_dims() << " dimensions, ";
            log_and_cout << sp.size() << " sampling points, ";
            if( sp.got_weights() ) log_and_cout << "and weighting information.\n";
            else log_and_cout << "and no weighting information.\n";
            if( vm.count( "ignore-weights" ) && sp.got_weights() )
                log_and_cout << "Ignoring weights.\n";
            log_and_cout << std::flush;
        }
        
        if( vm.count( "ignore-weights" ) && sp.got_weights() )
        {
            sp.clear_weights();
        }
        
        if( !quiet || log ) log_and_cout << "\n" << std::flush;
        
        //  *********************************************************
        //  Open input file
        
        std::string input_filename = vm[ "input" ].as< std::string >();
        bool open_read_only = !( vm.count( "in-place" ) || ( vm.count( "position" ) && vm.count( "insert-in-place" ) && !vm.count( "output" ) ) );
        
        boost::scoped_ptr< NMRData > input_p;
        try
        {
            input_p.reset( NMRData::GetObjForFile( input_filename, open_read_only ) );
        }
        catch( Exceptions::FileTypeNotRecognized &e )
        {
            std::cerr << "clean: The file type for the input file " << input_filename << " could not be recognized from its extension or is not a supported file type.\n";
            return 1;
        }
        catch( Exceptions::CantOpenFile &e )
        {
            boost::filesystem::path input_file_path( input_filename );
            if( !boost::filesystem::exists( input_file_path ) )
            {
                std::cerr << "clean: The input file " << input_filename << " could not be found.\n";
                return 1;
            }
            else
            {
                std::cerr << "clean: The input file " << input_filename << " could not be opened.\n";
                return 1;
            }
        }
        catch( Exceptions::PipeStreamFormatNotSupported &e )
        {
            std::cerr << "clean: The input file " << input_filename << " is in the NMRPipe multidimensional stream format, which is not currently supported.  Please modify your NMRPipe script to produce a set of files containing 2-D planes.\n";
            return 1;
        }
        catch( Exceptions::FileError &e )
        {
            std::cerr << "clean: An error occurred while reading the input file " << input_filename << ".\n";
            return 1;
        }
        NMRData &input = *input_p;
        
        if( input.GetNumOfDims() > 4 )
        {
            std::cerr << "clean: Spectra with more than four dimensions are not supported.\n";
            return 1;
        }
        
        if( !quiet || log )
        {
            log_and_cout << "Opened input file " << input_filename << " of type " << input.GetFileTypeStr() << "\n";
            log_and_cout << "Dimensions: \n";
            log_and_cout << "  F1: " << input.GetDimensionLabelStr( 0 ) << " (" << input.GetDimensionSize( 0 ) << " points)\n";
            if( input.GetNumOfDims() >= 2 ) log_and_cout << "  F2: " << input.GetDimensionLabelStr( 1 ) << " (" << input.GetDimensionSize( 1 ) << " points)\n";
            if( input.GetNumOfDims() >= 3 ) log_and_cout << "  F3: " << input.GetDimensionLabelStr( 2 ) << " (" << input.GetDimensionSize( 2 ) << " points)\n";
            if( input.GetNumOfDims() == 4 ) log_and_cout << "  F4: " << input.GetDimensionLabelStr( 3 ) << " (" << input.GetDimensionSize( 3 ) << " points)\n";
            log_and_cout << "\n";
            log_and_cout << std::flush;
        }
        
        //  *********************************************************
        //  Build dimension map
        
        if( sp.get_num_of_dims() > input.GetNumOfDims() )
        {
            std::cerr << "clean: The input file has fewer dimensions than the sampling pattern.\n";
            return 1;
        }
        
        dimension_map dm;
        switch( input.GetNumOfDims() )
        {
            case 1:
                dm.F1 = u;
                break;
            case 2:
                if( sp.get_num_of_dims() == 1 )
                {
                    dm.F1 = u;
                    dm.F2 = index_dim;
                }
                else if( sp.get_num_of_dims() == 2 )
                {
                    dm.F1 = u;
                    dm.F2 = v;
                }
                if( vm.count( "f1" ) ) dm.F1 = vm[ "f1" ].as< dimension_assignment >();
                if( vm.count( "f2" ) ) dm.F2 = vm[ "f2" ].as< dimension_assignment >();
                break;
            case 3:
                if( sp.get_num_of_dims() == 1 )
                {
                    dm.F1 = u;
                    dm.F2 = index_dim;
                    dm.F3 = index_dim;
                }
                else if( sp.get_num_of_dims() == 2 )
                {
                    dm.F1 = u;
                    dm.F2 = v;
                    dm.F3 = index_dim;
                }
                else if( sp.get_num_of_dims() == 3 )
                {
                    dm.F1 = u;
                    dm.F2 = v;
                    dm.F3 = w;
                }
                if( vm.count( "f1" ) ) dm.F1 = vm[ "f1" ].as< dimension_assignment >();
                if( vm.count( "f2" ) ) dm.F2 = vm[ "f2" ].as< dimension_assignment >();
                if( vm.count( "f3" ) ) dm.F3 = vm[ "f3" ].as< dimension_assignment >();
                break;
            case 4:
                if( sp.get_num_of_dims() == 1 )
                {
                    dm.F1 = u;
                    dm.F2 = index_dim;
                    dm.F3 = index_dim;
                    dm.F4 = index_dim;
                }
                else if( sp.get_num_of_dims() == 2 )
                {
                    dm.F1 = u;
                    dm.F2 = v;
                    dm.F3 = index_dim;
                    dm.F4 = index_dim;
                }
                else if( sp.get_num_of_dims() == 3 )
                {
                    dm.F1 = u;
                    dm.F2 = v;
                    dm.F3 = w;
                    dm.F4 = index_dim;
                }
                if( vm.count( "f1" ) ) dm.F1 = vm[ "f1" ].as< dimension_assignment >();
                if( vm.count( "f2" ) ) dm.F2 = vm[ "f2" ].as< dimension_assignment >();
                if( vm.count( "f3" ) ) dm.F3 = vm[ "f3" ].as< dimension_assignment >();
                if( vm.count( "f4" ) ) dm.F4 = vm[ "f4" ].as< dimension_assignment >();
                break;
            default:
                std::cerr << "clean: At this time, spectra with more than four dimensions are not supported.\n";
                return 1;
        }
        if( !dm.is_valid() )
        {
            std::cerr << "clean: The dimension assignment is not valid.\n";
            return 1;
        }
        
        if( !quiet || log )
        {
            log_and_cout << "Mapping of sampling pattern dimensions to experimental dimensions:\n";
            log_and_cout << "  F1: " << dm.F1 << "\n";
            if( input.GetNumOfDims() >= 2 ) log_and_cout << "  F2: " << dm.F2 << "\n";
            if( input.GetNumOfDims() >= 3 ) log_and_cout << "  F3: " << dm.F3 << "\n";
            if( input.GetNumOfDims() == 4 ) log_and_cout << "  F4: " << dm.F4 << "\n";
            log_and_cout << "\n";
            log_and_cout << std::flush;
        }
        
        //  *********************************************************
        //  Configure apodization information
        NMRData_nmrPipe *input_pipe = dynamic_cast< NMRData_nmrPipe * >( input_p.get() );
        if( input_pipe )
        {
            dm.F1_apod = dimension_apod( input_pipe->GetDimensionApodInfo( 0 ) );
            if( input.GetNumOfDims() >= 2 ) dm.F2_apod = dimension_apod( input_pipe->GetDimensionApodInfo( 1 ) );
            if( input.GetNumOfDims() >= 3 ) dm.F3_apod = dimension_apod( input_pipe->GetDimensionApodInfo( 2 ) );
            if( input.GetNumOfDims() == 4 ) dm.F4_apod = dimension_apod( input_pipe->GetDimensionApodInfo( 3 ) );
        }
        
        if( vm.count( "f1-apod" ) ) dm.F1_apod = vm[ "f1-apod" ].as< dimension_apod >();
        if( vm.count( "f2-apod" ) ) dm.F2_apod = vm[ "f2-apod" ].as< dimension_apod >();
        if( vm.count( "f3-apod" ) ) dm.F3_apod = vm[ "f3-apod" ].as< dimension_apod >();
        if( vm.count( "f4-apod" ) ) dm.F4_apod = vm[ "f4-apod" ].as< dimension_apod >();
        
        if( vm.count( "f1-fpc" ) ) dm.F1_apod.first_point_correction = vm[ "f1-fpc" ].as< float >();
        if( vm.count( "f2-fpc" ) ) dm.F2_apod.first_point_correction = vm[ "f2-fpc" ].as< float >();
        if( vm.count( "f3-fpc" ) ) dm.F3_apod.first_point_correction = vm[ "f3-fpc" ].as< float >();
        if( vm.count( "f4-fpc" ) ) dm.F4_apod.first_point_correction = vm[ "f4-fpc" ].as< float >();
        
        if( dm.F1_apod.size < 0 ) dm.F1_apod.size = input.GetDimensionSize( 0 );
        if( input.GetNumOfDims() >= 2 && dm.F2_apod.size < 0 ) dm.F2_apod.size = input.GetDimensionSize( 1 );
        if( input.GetNumOfDims() >= 3 && dm.F3_apod.size < 0 ) dm.F3_apod.size = input.GetDimensionSize( 2 );
        if( input.GetNumOfDims() == 4 && dm.F4_apod.size < 0 ) dm.F4_apod.size = input.GetDimensionSize( 3 );
        
        if( dm.F1_apod.sw < 0 ) dm.F1_apod.sw = input.GetDimensionSW( 0 );
        if( input.GetNumOfDims() >= 2 && dm.F2_apod.sw < 0 ) dm.F2_apod.sw = input.GetDimensionSW( 1 );
        if( input.GetNumOfDims() >= 3 && dm.F3_apod.sw < 0 ) dm.F3_apod.sw = input.GetDimensionSW( 2 );
        if( input.GetNumOfDims() == 4 && dm.F4_apod.sw < 0 ) dm.F4_apod.sw = input.GetDimensionSW( 3 );
        
        if( !quiet || log )
        {
            log_and_cout << "Apodization settings for sparse dimensions:\n";
            if( dm.F1 != index_dim ) log_and_cout << "  F1: " << dm.F1_apod << "\n";
            if( input.GetNumOfDims() >= 2 && dm.F2 != index_dim ) log_and_cout << "  F2: " << dm.F2_apod << "\n";
            if( input.GetNumOfDims() >= 3 && dm.F3 != index_dim) log_and_cout << "  F3: " << dm.F3_apod << "\n";
            if( input.GetNumOfDims() == 4 && dm.F4 != index_dim) log_and_cout << "  F4: " << dm.F4_apod << "\n";
            log_and_cout << "\n";
            log_and_cout << std::flush;
        }
        
        if( input.GetNumOfDims() == 3 && dm.F3 == index_dim )
            log_and_cout << "Note: Having an index dimension in F3 can make for vastly slower calculations due to the need to shuffle data in and out of memory.  It is strongly recommended that you transpose the data, preferably swapping F1 and F3 (in NMRPipe terminology, swap Z and X).\n\n";
        else if( input.GetNumOfDims() == 4 && dm.F4 == index_dim )
            log_and_cout << "Note: Having an index dimension in F4 can make for vastly slower calculations due to the need to shuffle data in and out of memory.  It is strongly recommended that you transpose the data, preferably swapping F1 and F4 (in NMRPipe terminology, swap A and X).\n\n";

        //  *********************************************************
        //  Construct status gatherer
        boost::scoped_ptr< status_reporter_clean > sr;
        boost::scoped_ptr< console_status_clean > cs;
        if( !quiet )
        {
            sr.reset( new status_reporter_clean );
            cs.reset( new console_status_clean( *sr ) );
        }
        
        //  *********************************************************
        //  Construct cleaner
        
        cleaner c( sp, dm, sr.get() );
        
        if( vm.count( "gain" ) ) c.gain = vm[ "gain" ].as< float >();
        if( vm.count( "snr-threshold" ) ) c.threshold_intensity = vm[ "snr-threshold" ].as< float >();
        if( vm.count( "noise-change-threshold" ) ) c.threshold_noise = vm[ "noise-change-threshold" ].as< float >();
        if( vm.count( "max-iter" ) ) c.max_iter = vm[ "max-iter" ].as< int >();
        
        if( vm.count( "linearity" ) ) c.linearity_mode = vm[ "linearity" ].as< linearity_modes >();
        if( vm.count( "pure-comp-mode" ) ) c.pure_component_mode = vm[ "pure-comp-mode" ].as< pure_component_modes >();
        if( vm.count( "pc-contour-irregular-level" ) ) c.pure_comp_ci_level = vm[ "pc-contour-irregular-level" ].as< float >();
        if( vm.count( "pc-contour-irregular-margin" ) ) c.pure_comp_ci_margin = vm[ "pc-contour-irregular-margin" ].as< bool >();
        if( vm.count( "pc-contour-ellipsoid-level" ) ) c.pure_comp_ce_level = vm[ "pc-contour-ellipsoid-level" ].as< float >();
        if( vm.count( "pc-contour-ellipsoid-margin" ) ) c.pure_comp_ce_margin = vm[ "pc-contour-ellipsoid-margin" ].as< float >();
        if( vm.count( "pc-fixed-ellipsoid-size" ) ) c.pure_comp_fe_size = vm[ "pc-fixed-ellipsoid-size" ].as< float >();
        
        c.log_file = log;
        if( vm.count( "verbose-log" ) ) c.verbose_log = true;
        if( vm.count( "threads" ) ) c.num_of_threads = vm[ "threads" ].as< int >();
        if( vm.count( "output-components-only" ) ) c.components_only = true;
        if( vm.count( "output-residuals-only" ) ) c.residuals_only = true;
        
        //  *********************************************************
        //  Process full spectrum in-place
        
        if( vm.count( "in-place" ) && !vm.count( "position" ) )
        {
            if( !quiet || log ) log_and_cout << "Processing with CLEAN...\n";
            
            boost::scoped_ptr< boost::thread > cst;
            if( !quiet ) cst.reset( new boost::thread( *cs ) );
            
            try
            {
                c( input );
            }
            catch( exceptions::base_exception &e )
            {
                std::cerr << "clean: An unexpected error occurred: " << e.what() << "\n";
                return 1;
            }
            
            if( !quiet ) cst->interrupt();
            
            if( !quiet || log ) log_and_cout << "\nCalculations completed.\n";
        }
        
        //  *********************************************************
        //  Process full spectrum to output file
        
        if( vm.count( "output" ) && !vm.count( "position" ) )
        {
            std::string output_filename( vm[ "output" ].as< std::string >() );
            boost::filesystem::path output_file_path( output_filename );
            if( boost::filesystem::exists( output_file_path ) && !vm.count( "overwrite" ) )
            {
                std::cerr << "clean: The requested output file " << output_filename << " already exists.  Use the --overwrite option to overwrite.\n";
                return 1;
            }
            
            boost::scoped_ptr< NMRData > output_p;
            try
            {
                output_p.reset( NMRData::GetObjForFileType( output_filename ) );
            }
            catch( Exceptions::FileTypeNotRecognized &e )
            {
                std::cerr << "clean: The file type for the output file " << output_filename << " could not be recognized from its extension or is not a supported file type.\n";
                return 1;
            }
            NMRData &output = *output_p;
            
            if( !quiet || log ) log_and_cout << "Building output file " << output_filename << "..." << std::flush;
            try
            {
                output.CopyParameters( input_p.get() );
                output.CreateFile( output_filename );
            }
            catch( Exceptions::CantOpenFile &e )
            {
                std::cerr << "\nclean: The output file " << output_filename << " could not be created.\n";
                return 1;
            }
            catch( Exceptions::FileError &e )
            {
                std::cerr << "\nclean: An error occurred while writing the output file " << output_filename << ".\n";
                return 1;
            }
            if( !quiet || log ) log_and_cout << "done.\n\n" << std::flush;
            
            if( !quiet || log ) log_and_cout << "Processing with CLEAN...\n";
            
            boost::scoped_ptr< boost::thread > cst;
            if( !quiet ) cst.reset( new boost::thread( *cs ) );
            
            try
            {
                c( input, output );
            }
            catch( exceptions::base_exception &e )
            {
                std::cerr << "clean: An unexpected error occurred: " << e.what() << "\n";
                return 1;
            }

            if( !quiet ) cst->interrupt();
            
            if( !quiet || log ) log_and_cout << "\nCalculations completed.\n";
        }
        
        //  *********************************************************
        //  Process position list in-place
        
        if( vm.count( "position" ) && vm.count( "insert-in-place" ) && !vm.count( "output" ) )
        {
            std::vector< std::vector< int > > pos_list;
            try
            {
                pos_list = parse_position_list( vm[ "position" ].as< std::vector< std::string > >(), dm.get_num_of_index_dims() );
            }
            catch( invalid_pos_string &e )
            {
                std::cerr << "clean: The position \"" << e.pos_str << "\" could not be parsed or is otherwise invalid.\n";
                return 1;
            }
            
            if( !quiet || log )
            {
                log_and_cout << "A total of " << pos_list.size() << " positions will be processed.\n";
                log_and_cout << "Processed subsets will be written back to the original input file.\n" << std::flush;
            }
            
            if( !quiet || log ) log_and_cout << "Processing with CLEAN...\n";
            
            boost::scoped_ptr< boost::thread > cst;
            if( !quiet ) cst.reset( new boost::thread( *cs ) );
            
            try
            {
                c( input, pos_list );
            }
            catch( exceptions::base_exception &e )
            {
                std::cerr << "clean: An unexpected error occurred: " << e.what() << "\n";
                return 1;
            }

            if( !quiet ) cst->interrupt();
            
            if( !quiet || log ) log_and_cout << "\nCalculations completed.\n";
        }
        
        //  *********************************************************
        //  Process position list to output file
        
        if( vm.count( "position" ) && vm.count( "insert-in-place" ) && vm.count( "output" ) )
        {
            std::vector< std::vector< int > > pos_list;
            try
            {
                pos_list = parse_position_list( vm[ "position" ].as< std::vector< std::string > >(), dm.get_num_of_index_dims() );
            }
            catch( invalid_pos_string &e )
            {
                std::cerr << "clean: The position \"" << e.pos_str << "\" could not be parsed or is otherwise invalid.\n";
                return 1;
            }
            
            std::string output_filename( vm[ "output" ].as< std::string >() );
            if( !quiet || log )
            {
                log_and_cout << "A total of " << pos_list.size() << " positions will be processed.\n";
                log_and_cout << "Processed subsets will be written to the requested output file " << output_filename << ".\n\n" << std::flush;
            }
            
            boost::scoped_ptr< NMRData > output_p;
            
            boost::filesystem::path output_file_path( output_filename );
            if( boost::filesystem::exists( output_file_path ) )
            {
                try
                {
                    output_p.reset( NMRData::GetObjForFile( output_filename ) );
                }
                catch( Exceptions::FileTypeNotRecognized &e )
                {
                    std::cerr << "clean: The file type for the output file " << output_filename << " could not be recognized from its extension or is not a supported file type.\n";
                    return 1;
                }
                catch( Exceptions::CantOpenFile &e )
                {
                    std::cerr << "\nclean: The output file " << output_filename << " could not be opened.\n";
                    return 1;
                }
                catch( Exceptions::FileError &e )
                {
                    std::cerr << "\nclean: An error occurred while attempting to access the output file " << output_filename << ".\n";
                    return 1;
                }
                
                if( output_p->GetNumOfDims() != input.GetNumOfDims() || 
                   output_p->GetDimensionSize( 0 ) != input.GetDimensionSize( 0 ) ||
                   ( input.GetNumOfDims() >= 2 && output_p->GetDimensionSize( 1 ) != input.GetDimensionSize( 1 ) ) ||
                   ( input.GetNumOfDims() >= 3 && output_p->GetDimensionSize( 2 ) != input.GetDimensionSize( 2 ) ) ||
                   ( input.GetNumOfDims() == 4 && output_p->GetDimensionSize( 3 ) != input.GetDimensionSize( 3 ) ) )
                {
                    std::cerr << "\nclean: To insert results into an existing output file, its dimensionality and dimension sizes must match those of the input file.\n";
                    return 1;
                }
            }
            else
            {
                if( !quiet || log ) log_and_cout << "Building output file " << output_filename << "..." << std::flush;
                try
                {
                    output_p->CopyParameters( input_p.get() );
                    output_p->CreateFile( output_filename );
                }
                catch( Exceptions::CantOpenFile &e )
                {
                    std::cerr << "\nclean: The output file " << output_filename << " could not be created.\n";
                    return 1;
                }
                catch( Exceptions::FileError &e )
                {
                    std::cerr << "\nclean: An error occurred while writing the output file " << output_filename << ".\n";
                    return 1;
                }
                if( !quiet || log ) log_and_cout << "done.\n\n" << std::flush;
            }
            
            NMRData &output = *output_p;
            
            if( !quiet || log ) log_and_cout << "Processing with CLEAN...\n";
            
            boost::scoped_ptr< boost::thread > cst;
            if( !quiet ) cst.reset( new boost::thread( *cs ) );
            
            try
            {
                c( input, output, pos_list );
            }
            catch( exceptions::base_exception &e )
            {
                std::cerr << "clean: An unexpected error occurred: " << e.what() << "\n";
                return 1;
            }

            if( !quiet ) cst->interrupt();
            
            if( !quiet || log ) log_and_cout << "\nCalculations completed.\n";
        }
        
        //  *********************************************************
        //  Process position list to multiple output files
        
        if( vm.count( "position" ) && vm.count( "separate-outputs" ) )
        {
            std::vector< std::vector< int > > pos_list;
            try
            {
                pos_list = parse_position_list( vm[ "position" ].as< std::vector< std::string > >(), dm.get_num_of_index_dims() );
            }
            catch( invalid_pos_string &e )
            {
                std::cerr << "clean: The position \"" << e.pos_str << "\" could not be parsed or is otherwise invalid.\n";
                return 1;
            }
            
            std::vector< std::string > output_filenames = generate_position_filenames( pos_list, vm[ "output" ].as< std::string >() );
            
            if( !quiet || log )
            {
                log_and_cout << "A total of " << pos_list.size() << " positions will be processed.\n";
                log_and_cout << "Processed subsets will be written to separate output files.\n" << std::flush;
            }
            
            if( !quiet || log ) log_and_cout << "Building output files..." << std::flush;
            BEC_Misc::owned_ptr_vector< NMRData > outputs;
            try
            {
                create_position_files( output_filenames, outputs, input, dm, vm.count( "overwrite" ) );
            }
            catch( file_exists &e )
            {
                std::cerr << "clean: The requested output file " << e.filename << " already exists.  Use the --overwrite option to overwrite.\n";
                return 1;
            }
            catch( Exceptions::FileTypeNotRecognized &e )
            {
                std::cerr << "clean: The file type for the output file could not be recognized from its extension or is not a supported file type.\n";
                return 1;
            }
            catch( Exceptions::CantOpenFile &e )
            {
                std::cerr << "\nclean: An output file could not be created.\n";
                return 1;
            }
            catch( Exceptions::FileError &e )
            {
                std::cerr << "\nclean: An error occurred while writing an output file.\n";
                return 1;
            }
            if( !quiet || log ) log_and_cout << "done.\n\n" << std::flush;
            
            if( !quiet || log ) log_and_cout << "Processing with CLEAN...\n";
            
            boost::scoped_ptr< boost::thread > cst;
            if( !quiet ) cst.reset( new boost::thread( *cs ) );
            
            try
            {
                c( input, outputs, pos_list );
            }
            catch( exceptions::base_exception &e )
            {
                std::cerr << "clean: An unexpected error occurred: " << e.what() << "\n";
                return 1;
            }

            if( !quiet ) cst->interrupt();
            
            if( !quiet || log ) log_and_cout << "\nCalculations completed.\n";
        }
        
        //  *********************************************************
        //  Collect and save PSF and/or pure component
        
        if( vm.count( "psf" ) )
        {
            boost::shared_ptr< NMRData > psf( convert_psf_or_pure_comp_to_spectrum( c.get_psf(), c.get_xsize(), c.get_ysize(), c.get_zsize() ) );
            
            boost::scoped_ptr< NMRData > psf_file;
            try
            {
                psf_file.reset( NMRData::GetObjForFileType( vm[ "psf" ].as< std::string >() ) );
            }
            catch( Exceptions::FileTypeNotRecognized &e )
            {
                std::cerr << "clean: The file type for the PSF output file could not be recognized from its extension or is not a supported file type.\n";
                return 1;
            }
            try
            {
                psf_file->CopyParameters( psf.get() );
                psf_file->CreateFile( vm[ "psf" ].as< std::string >() );
            }
            catch( Exceptions::CantOpenFile &e )
            {
                std::cerr << "\nclean: The PSF output file could not be created.\n";
                return 1;
            }
            catch( Exceptions::FileError &e )
            {
                std::cerr << "\nclean: An error occurred while writing to the PSF output file.\n";
                return 1;
            }
            
            *psf_file = *psf;
        }
        
        if( vm.count( "pure-comp" ) )
        {
            boost::shared_ptr< NMRData > pc( convert_psf_or_pure_comp_to_spectrum( c.get_pure_component(), c.get_xsize(), c.get_ysize(), c.get_zsize() ) );
            
            boost::scoped_ptr< NMRData > pc_file;
            try
            {
                pc_file.reset( NMRData::GetObjForFileType( vm[ "pure-comp" ].as< std::string >() ) );
            }
            catch( Exceptions::FileTypeNotRecognized &e )
            {
                std::cerr << "clean: The file type for the pure component output file could not be recognized from its extension or is not a supported file type.\n";
                return 1;
            }
            try
            {
                pc_file->CopyParameters( pc.get() );
                pc_file->CreateFile( vm[ "pure-comp" ].as< std::string >() );
            }
            catch( Exceptions::CantOpenFile &e )
            {
                std::cerr << "\nclean: The pure component output file could not be created.\n";
                return 1;
            }
            catch( Exceptions::FileError &e )
            {
                std::cerr << "\nclean: An error occurred while writing to the pure component output file.\n";
                return 1;
            }
            
            *pc_file = *pc;
        }
        
        //  *********************************************************
        //  Produce noise report and calculation of artifact suppresion.
        
        const std::map< int, job_info_clean > m( sr->get_snapshot() );
        if( !vm.count( "position" ) && ( !quiet || log ) )
        {
            std::vector< float > initial_noise_levels;
            for( std::map< int, job_info_clean >::const_iterator i = m.begin(); i != m.end(); ++i )
                initial_noise_levels.push_back( i->second.noise_floor_initial );
            details::vector_noise_estimator vne;
            vne( initial_noise_levels );
            float noise_level = vne.noise_mean + vne.noise_std_dev;
            
            float artifacts_initial = 0.0F, artifacts_final = 0.0F;
            for( std::map< int, job_info_clean >::const_iterator i = m.begin(); i != m.end(); ++i )
            {
                float nf_start = i->second.noise_floor_initial;
                float nf_end = i->second.noise_floor;
                artifacts_initial += nf_start > noise_level ? nf_start - noise_level : 0;
                artifacts_final += nf_end > noise_level ? nf_end - noise_level : 0;
            }
            float suppression = ( artifacts_initial - artifacts_final ) * 100.0F / artifacts_initial;
            if( suppression > 100.0F ) suppression = 100.0F;
            log_and_cout << "Approximate artifact suppression: " << std::fixed << std::setprecision( 0 ) << ( artifacts_initial - artifacts_final ) * 100.0F / ( artifacts_initial ) << "%\n";
        }
        if( vm.count( "noise-report-csv" ) )
        {
            FILE *noise_report = fopen( vm[ "noise-report-csv" ].as< std::string > ().c_str(), "w+" );
            for( std::map< int, job_info_clean >::const_iterator i = m.begin(); i != m.end(); ++i )
            {
                const std::vector< int > pos = i->second.position;
                for( std::vector< int >::const_iterator j = pos.begin(); j != pos.end(); ++j )
                    fprintf( noise_report, "%i, ", *j );
                float nf_start = i->second.noise_floor_initial;
                float nf_end = i->second.noise_floor;
                fprintf( noise_report, "%f, %f\n", nf_start, nf_end );
            }
            fclose( noise_report );
        }

        if( log ) fclose( log );
        return 0;
    }
    catch( exceptions::base_exception &e )
    {
        std::cerr << "clean: " << e.what() << "\n";
        return 1;
    }
    catch( Exceptions::BaseException &e )
    {
        std::cerr << "clean: " << e.what() << "\n";
        return 1;
    }
    
}

