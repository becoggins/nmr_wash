//  Copyright (c) 2006-2012, Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <memory.h>
#include <vector>
#include <numeric>
#include <boost/shared_array.hpp>
#include <boost/circular_buffer.hpp>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"

using namespace BEC_Misc;
using namespace BEC_NMRData;
using namespace nmr_wash;
using namespace nmr_wash::details;

cleaner::cleaner( const sampling_pattern &pattern, const dimension_map dim_map, status_reporter_clean *reporter ) : washer_base< cleaner, job_info_clean, job_status_relay_clean, job_callback_wrapper_clean, status_reporter_clean >( pattern, dim_map, reporter ), gain( 10.0F ), threshold_noise( 5.0F ), threshold_intensity( 5.0F ), max_iter( -1 )
{
    //
}

void cleaner::clean( job &j )
{
    try
    {
        job_status_relay_clean *jsr = sri ? sri->start_job( j.get_job_id() ) : 0;
        
        float gain_ratio = gain / 100.0F;
        float threshold_noise_ratio = 1.0F + threshold_noise / 100.0F;
        
        FILE *log = j.get_log();
        if( log )
            j.write_log_section_header();
        
        size_t size = xsize * ysize * zsize;
        
        float *out = j.get_input();
        
        size_t offset = 0;
        
        std::vector< component > components;
        boost::circular_buffer< float > noise_raw_record( 15 );
        boost::circular_buffer< float > noise_running_avg_record( 25 );
        
        noise_estimator ne( size > 32768 ? 32768 : size, size );
        ne( out );
        if( log )
            fprintf( log, "  Initial\n    noise_floor = %f\n    noise_std_dev = %f\n", ne.noise_floor, ne.noise_std_dev );
        
        for( int iteration = 0; max_iter > 0 ? iteration < max_iter : true; iteration++ )
        {
            for( size_t i = 0; i < size; i++ )
                if( fast_abs( out[ i ] ) > fast_abs( out[ offset ] ) )
                    offset = i;
            
            position p = get_position( offset );
            
            float signal_pre_subtract = out[ p.offset ];
            
            //  Criteria A = number of standard deviations by which the signal exceeds ( threshold_intensity * noise_std_dev )
            //  if Criteria A is above zero, the signal is above the threshold, and the calculation continues
            //  if Criteria A is at or below zero, the signal is at or below the threshold, and we stop
            float criteria_a = ( fast_abs( signal_pre_subtract ) / ne.noise_std_dev ) - threshold_intensity;
            if( criteria_a <= 0.0F )
                break;

            if( log && verbose_log )
                fprintf( log, "      Cleaning position %03i ( x = %03i, y = %03i, z = %03i, value = %f )\n", ( int ) p.offset, p.xpos, p.ypos, p.zpos, out[ p.offset ] );

            components.push_back( component( p, out[ p.offset ] * gain_ratio * comp_scale ) );
            subtract_psf( p, out, out[ p.offset ] * gain_ratio );
            
            ne( out );
            
            //  Criteria B measures how much the noise is decreasing over 25 iterations.
            //  First, a running average is computed looking at the noise levels for the last 15 iterations.
            //  Second, we iterate backwards over the running averages for the last 25 iterations and see whether all of them are within threshold_noise percent of the current running average.  If so, we conclude that the noise is no longer changing very much, and we stop.  The number of iterations for which the running average is within the specified range is reported as Criteria B.
            noise_raw_record.push_back( ne.noise_std_dev );
            float sum = std::accumulate( noise_raw_record.begin(), noise_raw_record.end(), 0.0F );
            float avg = sum / noise_raw_record.size();
            
            boost::circular_buffer< float >::reverse_iterator ri = noise_running_avg_record.rbegin();
            int count = 0;
            while( ri != noise_running_avg_record.rend() && *ri / avg <= threshold_noise_ratio )
                ++count, ++ri;
            if( count == 25 )
                break;
            noise_running_avg_record.push_back( avg );
            
            if( log && verbose_log )
                fprintf( log, "      New noise_floor = %f     noise_std_dev = %f     Criteria A = %f     Criteria B = %i\n", ne.noise_floor, ne.noise_std_dev, criteria_a, count );
            
            if( jsr ? jsr->post( iteration, signal_pre_subtract, ne.noise_floor, ne.noise_std_dev, criteria_a, count ) : false )
                return;
            if( worker_exception )
                return;
        }
        
        if( log )
            fprintf( log, "  Final\n    noise_floor = %f\n    noise_std_dev = %f\n    components = %i\n", 
                    ne.noise_floor, ne.noise_std_dev, ( int ) components.size() );
        
        if( components_only )
            memset( out, 0, sizeof( float ) * size );
        
        if( !residuals_only )
            add_pure_components( out, components );
        
        j.post_output( out );
        
        if( sri )
            sri->end_job( j.get_job_id() );
    }	//	try
    catch( std::exception &e )
    {
        boost::lock_guard< boost::mutex > lock( worker_exception_mutex );
        worker_exception = boost::current_exception();
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        boost::lock_guard< boost::mutex > lock( worker_exception_mutex );
        worker_exception = boost::copy_exception( nmr_wash::exceptions::nmrdata_library_exception( e.what() ) );
    }
}

