//  Copyright (c) 2013, Brian E. Coggins.  All rights reserved.

#include <boost/program_options.hpp>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"
#include "console_support.h"

using namespace nmr_wash;
namespace po = boost::program_options;

void nmr_wash::validate( boost::any &v, const std::vector< std::string > &values, dimension_apod *, int )
{
    po::validators::check_first_occurrence( v );
    
    if( values.size() != 1 )
        throw po::validation_error( po::validation_error::invalid_option_value );
    
    po::options_description apod_string_options( "apodization string options" );
    apod_string_options.add_options()
    ( "fn", po::value< apod_method >(), "apodization method (SP, EM, GM, GMB, TM, TRI, JMOD)" )
    ( "off", po::value< float >(), "starting point of the sine-bell for SP function (rad), starting point of modulation for JMOD (rad)" )
    ( "end", po::value< float >(), "ending point of the sine-bell for SP function (rad)" )
    ( "pow", po::value< float >(), "exponent of the sine-bell for SP function" )
    ( "lb", po::value< float >(), "line broadening for EM, GMB, and JMOD functions (Hz)" )
    ( "g1", po::value< float >(), "inverse exponential for GM function (Hz)" )
    ( "g2", po::value< float >(), "Gaussian for GM function (Hz)" )
    ( "g3", po::value< float >(), "position of the Gaussian function's maximum for GM function, from 0.0 (start) to 1.0 (end)" )
    ( "gb", po::value< float >(), "Gaussian factor for GMB function" )
    ( "t1", po::value< float >(), "number of points in the left edge of the trapezoid for TM function" )
    ( "t2", po::value< float >(), "number of points in the right edge of the trapezoid for TM function" )
    ( "loc", po::value< float >(), "point location of the triangle function's apex for TRI function" )
    ( "lHi", po::value< float >(), "height of the triangle function at the first point of the window for TRI" )
    ( "rHi", po::value< float >(), "height of the triangle function at the last point of the window for TRI" )
    ( "j", po::value< float>(), "frequency of J-modulation for JMOD" )
    ( "sin", "for JMOD, presets for sine modulation" )
    ( "cos", "for JMOD, presets for cosine modulation" )
    ( "qName", po::value< apod_method>(), "alternative way of specifying the apodization method" )
    ( "q1", po::value< float >(), "alternative way of specifying the first apodization parameter (Q1)" )
    ( "q2", po::value< float >(), "alternative way of specifying the second apodization parameter (Q2)" )
    ( "q3", po::value< float >(), "alternative way of specifying the third apodization parameter (Q3)" )
    ( "size", po::value< int >(), "number of points in the window function" )
    ( "c", po::value< float >(), "first-point correction" );
     
    po::positional_options_description positional_options;
    positional_options.add( "fn", 1 );

    po::variables_map vm;
    
    try
    {
        po::store( po::command_line_parser( po::split_unix( values[ 0 ] ) ).options( apod_string_options ).positional( positional_options ).style( po::command_line_style::unix_style | po::command_line_style::allow_long_disguise ).run(), vm );
        po::notify( vm );
    }
    catch( exceptions::bad_apod_method_string &e )
    {
        std::cerr << "scrub: An invalid apodization method was supplied in an apodization information string.\n";
        throw po::validation_error( po::validation_error::invalid_option_value );
    }
    catch( po::unknown_option &e )
    {
        std::cerr << "scrub: Unrecognized option " << e.get_option_name() << " was supplied in an apodization information string.\n";
        throw po::validation_error( po::validation_error::invalid_option_value );
    }
    catch( po::invalid_option_value &e )
    {
        std::cerr << "scrub: An invalid option value was provided in an apodization information string.\n";
        throw po::validation_error( po::validation_error::invalid_option_value );
    }
    catch( po::validation_error &e )
    {
        std::cerr << "scrub: Value for option " << e.get_option_name() << " in an apodization information string is not valid: " << e.what() << "\n";
        throw po::validation_error( po::validation_error::invalid_option_value );
    }
    catch( po::error &e )
    {
        std::cerr << "scrub: The apodization information string was not valid.\n";
        throw po::validation_error( po::validation_error::invalid_option_value );
    }
    
    dimension_apod da;
    
    if( vm.count( "fn" ) )
    {
        da.method = vm[ "fn" ].as< apod_method >();
    }
    else if( vm.count( "qName" ) )
    {
        da.method = vm[ "qName" ].as< apod_method >();
    }
    else
    {
        std::cerr << "scrub: No apodization method was specified in the apodization information string.\n";
        throw po::validation_error( po::validation_error::invalid_option_value );
    }
    
    switch( da.method )
    {
        case none:
            break;
        case sp:
            if( vm.count( "off" ) )
                da.p1 = vm[ "off" ].as< float >();
            else if( vm.count( "q1" ) )
                da.p1 = vm[ "q1" ].as< float >();
            else
                da.p1 = 0.0F;
            
            if( vm.count( "end" ) )
                da.p2 = vm[ "end" ].as< float >();
            else if( vm.count( "q2" ) )
                da.p2 = vm[ "q2" ].as< float >();
            else
                da.p2 = 1.0F;
            
            if( vm.count( "pow" ) )
                da.p3 = vm[ "pow" ].as< float >();
            else if( vm.count( "q3" ) )
                da.p3 = vm[ "q3" ].as< float >();
            else
                da.p3 = 1.0F;
            
            break;
        case em:
            if( vm.count( "lb" ) )
                da.p1 = vm[ "lb" ].as< float >();
            else if( vm.count( "q1" ) )
                da.p1 = vm[ "q1" ].as< float >();
            else
            {
                std::cerr << "scrub: The EM apodization function requires a line-broadening parameter (-lb).\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }
            
            break;
        case gm:
            if( vm.count( "g1" ) )
                da.p1 = vm[ "g1" ].as< float >();
            else if( vm.count( "q1" ) )
                da.p1 = vm[ "q1" ].as< float >();
            else
                da.p1 = 0.0F;
            
            if( vm.count( "g2" ) )
                da.p2 = vm[ "g2" ].as< float >();
            else if( vm.count( "q2" ) )
                da.p2 = vm[ "q2" ].as< float >();
            else
            {
                std::cerr << "scrub: The GM apodization function requires a line-broadening parameter (-g2).\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }
            
            if( vm.count( "g3" ) )
                da.p3 = vm[ "g3" ].as< float >();
            else if( vm.count( "q3" ) )
                da.p3 = vm[ "q3" ].as< float >();
            else
                da.p3 = 0.0F;
            
            break;
        case gmb:
            if( vm.count( "lb" ) )
                da.p1 = vm[ "lb" ].as< float >();
            else if( vm.count( "q1" ) )
                da.p1 = vm[ "q1" ].as< float >();
            else
                da.p1 = 0.0F;
            
            if( vm.count( "gb" ) )
                da.p2 = vm[ "gb" ].as< float >();
            else if( vm.count( "q2" ) )
                da.p2 = vm[ "q2" ].as< float >();
            else
                da.p2 = 0.0F;
                
            break;
        case nmr_wash::tm:
            if( vm.count( "t1" ) )
                da.p1 = vm[ "t1" ].as< float >();
            else if( vm.count( "q1" ) )
                da.p1 = vm[ "q1" ].as< float >();
            else
            {
                std::cerr << "scrub: The TM apodization function requires a -t1 parameter.\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }
            
            if( vm.count( "t2" ) )
                da.p2 = vm[ "t2" ].as< float >();
            else if( vm.count( "q2" ) )
                da.p2 = vm[ "q2" ].as< float >();
            else
            {
                std::cerr << "scrub: The TM apodization function requires a -t2 parameter.\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }

            break;
        case tri:
            if( vm.count( "loc" ) )
                da.p1 = vm[ "loc" ].as< float >();
            else if( vm.count( "q1" ) )
                da.p1 = vm[ "q1" ].as< float >();
            else
            {
                std::cerr << "scrub: The TRI apodization function requires a -loc parameter.\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }
            
            if( vm.count( "lHi" ) )
                da.p2 = vm[ "lHi" ].as< float >();
            else if( vm.count( "q2" ) )
                da.p2 = vm[ "q2" ].as< float >();
            else
            {
                std::cerr << "scrub: The GM apodization function requires a -lHi parameter.\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }
            
            if( vm.count( "rHi" ) )
                da.p3 = vm[ "rHi" ].as< float >();
            else if( vm.count( "q3" ) )
                da.p3 = vm[ "q3" ].as< float >();
            else
            {
                std::cerr << "scrub: The GM apodization function requires a -rHi parameter.\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }
            
            break;
        case jmod:
            if( vm.count( "off" ) )
                da.p1 = vm[ "off" ].as< float >();
            else if( vm.count( "q1" ) )
                da.p1 = vm[ "q1" ].as< float >();
            else if( vm.count( "cos" ) )
                da.p1 = 0.5F;
            else if( vm.count( "sin" ) )
                da.p1 = 0.0F;
            else
            {
                std::cerr << "scrub: The JMOD apodization function requires an -off parameter, or the -cos or -sin flag.\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }
            
            if( vm.count( "j" ) )
                da.p2 = vm[ "j" ].as< float >();
            else if( vm.count( "q2" ) )
                da.p2 = vm[ "q2" ].as< float >();
            else
            {
                std::cerr << "scrub: The JMOD apodization function requires a -j parameter.\n";
                throw po::validation_error( po::validation_error::invalid_option_value );
            }
            
            if( vm.count( "lb" ) )
                da.p3 = vm[ "lb" ].as< float >();
            else if( vm.count( "q3" ) )
                da.p3 = vm[ "q3" ].as< float >();
            else
                da.p3 = 0.0F;
            
            break;
    }
    
    if( vm.count( "size" ) )
        da.size = vm[ "size" ].as< int >();
    
    if( vm.count( "c" ) )
        da.first_point_correction = vm[ "c" ].as< float >();
    
    v = da;
}


