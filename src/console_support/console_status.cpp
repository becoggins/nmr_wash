//  Copyright (c) 2012, Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "nmr_wash.h"
#include "console_support.h"


using namespace nmr_wash;

template< typename StatusReporterClass, typename JobInfoClass >
console_status< StatusReporterClass, JobInfoClass >::console_status( StatusReporterClass &sr_ ) : sr( sr_ ), init( false ), char_count( 0 )
{
    //
}


template< typename StatusReporterClass, typename JobInfoClass >
void console_status< StatusReporterClass, JobInfoClass >::operator()()
{
    while( true )
    {
        std::map< int, JobInfoClass > status = sr.get_snapshot();
        
        int job_count = ( int ) status.size();
        if( job_count )
        {
            if( !init )
            { 
                std::cout << "                0%                                                100%\n";
                std::cout << "Calculating...  " << std::flush;
                init = true;
            }
            
            int jobs_done = 0;
            for( typename std::map< int, JobInfoClass >::iterator i = status.begin(); i != status.end(); ++i )
                if( i->second.done ) jobs_done++;
            
            int done_chars = ( int ) floorf( ( float ) jobs_done * 50.0F / ( float ) job_count );
            
            for( ; char_count < done_chars; char_count++ )
                std::cout << "X" << std::flush;
        }
        
        boost::thread::sleep( boost::get_system_time() + boost::posix_time::milliseconds( 250 ) );
    }
}

template class console_status< nmr_wash::status_reporter_scrub, nmr_wash::job_info_scrub >;
template class console_status< nmr_wash::status_reporter_clean, nmr_wash::job_info_clean >;
