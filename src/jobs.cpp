//  Copyright (c) 2012 Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <assert.h>
#include <vector>
#include <queue>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"

using namespace BEC_NMRData;
using namespace BEC_Misc;
using namespace nmr_wash;
using namespace nmr_wash::details;


template< typename Pred >
std::vector< job > nmr_wash::details::make_jobs_all_pos( const NMRData *in, Pred &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l )
{
    int index_dim_count = dm.get_num_of_index_dims();
    std::vector< int > coord( index_dim_count ), sizes( index_dim_count );
    std::vector< int > index_dims = dm.get_index_dim_nums();
    
    int num_of_jobs = 1;
    std::vector< job > jobs;
    
    for( int d = 0; d < index_dim_count; d++ )
    {
        coord[ d ] = 0;
        sizes[ d ] = in->GetDimensionSize( index_dims[ d ] );
        num_of_jobs *= sizes[ d ];
    }
    
    for( int i = 0; i < num_of_jobs; i++ )
    {
        int id = jcw->get_new_job_id();
        FILE * log_file = l.register_job( id );
        boost::shared_ptr< job_impl > ji = pred( id, log_file, coord );
        jobs.push_back( job( ji, jcw, jq ) );
        
        for( int d = index_dim_count - 1; d >= 0; d-- )
        {
            if( ++coord[ d ] == sizes[ d ] )
                coord[ d ] = 0;
            else
                break;
        }
    }
    
    return jobs;
}

template< typename Pred >
std::vector< job > nmr_wash::details::make_jobs_pos_list( std::vector< std::vector< int > > pos_list, const NMRData *in, Pred &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l )
{
    int index_dim_count = dm.get_num_of_index_dims();
    std::vector< int > sizes( index_dim_count );
    std::vector< int > index_dims = dm.get_index_dim_nums();
    
    std::vector< job > jobs;
    
    for( int d = 0; d < index_dim_count; d++ )
        sizes[ d ] = in->GetDimensionSize( index_dims[ d ] );
    
    for( std::vector< std::vector< int > >::iterator i = pos_list.begin(); i != pos_list.end(); ++i )
    {
        assert( i->size() == index_dim_count );
        for( int d = 0; d < index_dim_count; d++ )
            assert( i->at( d ) >= 0 && i->at( d ) < sizes[ d ] );
        
        int id = jcw->get_new_job_id();
        FILE *log_file = l.register_job( id );
        boost::shared_ptr< job_impl > ji = pred( id, log_file, *i );
        jobs.push_back( job( ji, jcw, jq ) );
    }
    
    return jobs;
}

make_job_pos_inplace::make_job_pos_inplace( NMRData *data, boost::mutex *data_mutex, dimension_map dim_map ) : d( data ), d_mtx( data_mutex ), dm( dim_map )
{
    //
}

boost::shared_ptr< job_impl > make_job_pos_inplace::operator()( int id, FILE *log_file, std::vector< int > pos )
{
    boost::shared_ptr< job_impl > ji( new job_pos_inplace( id, log_file, d, d_mtx, pos, dm ) );
    return ji;
}

make_job_pos::make_job_pos( const NMRData *input, boost::mutex *input_mutex, NMRData *output, boost::mutex *output_mutex, dimension_map dim_map ) : in( input ), in_mtx( input_mutex ), out( output ), out_mtx( output_mutex ), dm( dim_map )
{
    //
}

boost::shared_ptr< job_impl > make_job_pos::operator()( int id, FILE *log_file, std::vector< int > pos )
{
    boost::shared_ptr< job_impl > ji( new job_pos( id, log_file, in, in_mtx, out, out_mtx, pos, dm ) );
    return ji;
}

make_job_pos_to_full::make_job_pos_to_full( const NMRData *input, boost::mutex *input_mutex, ptr_vector< NMRData > outputs_, dimension_map dim_map ) : i( 0 ), in( input ), in_mtx( input_mutex ), outputs( outputs_ ), dm( dim_map )
{
    //
}

boost::shared_ptr< job_impl > make_job_pos_to_full::operator()( int id, FILE *log_file, std::vector< int > pos )
{
    boost::shared_ptr< job_impl > ji( new job_pos_to_full( id, log_file, in, in_mtx, outputs.ptr_at( i++ ), pos, dm ) );
    return ji;
}

job_queue::job_queue( int num_of_threads ) : num_running( 0 )
{
    set_num_of_threads( num_of_threads );
}

void job_queue::operator()()
{
    boost::unique_lock< boost::mutex > lock( m );
    
    while( !q.empty() )
    {
        if( num_running < num_allowed )
        {
            tg.create_thread( q.front() );
            q.pop();
            num_running++;
        }
        if( num_running == num_allowed )
            thread_done.wait( lock );
    }
    
    lock.unlock();
    tg.join_all();
}

void job_queue::add_job( job j )
{
    q.push( j );
}

int job_queue::get_num_of_threads()
{
    return num_allowed;
}

int job_queue::get_num_of_running_threads()
{
    return num_running;
}

void job_queue::set_num_of_threads( int num )
{
    if( num < 1 )
        num_allowed = boost::thread::hardware_concurrency();
    else
        num_allowed = num;
}

void job_queue::thread_signal_end()
{
    boost::lock_guard< boost::mutex > lock( m );
    
    num_running--;
    thread_done.notify_one();
}

job::job( boost::shared_ptr< job_impl > ji_, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq ) : ji( ji_ ), cbw( jcw ), queue( jq )
{
    //
}

void job::operator()()
{
    cbw->start_calc( *this );
    
    if( queue )
        queue->thread_signal_end();
}

int job::get_job_id()
{
    return ji->get_job_id();
}

float * job::get_input()
{
    return ji->get_input();
}

FILE * job::get_log()
{
    return ji->get_log();
}

void job::write_log_section_header()
{
    ji->write_log_section_header();
}

void job::post_output( float *output )
{
    ji->post_output( output );
}

bool job::is_full()
{
    return ji->is_full();
}

const NMRData *job::get_input_src()
{
    return ji->get_input_src();
}

std::vector< int > * job::get_position()
{
    return ji->get_position();
}

void job_callback_wrapper::start_calc( job &j )
{
    do_start_calc( j );
}

int job_callback_wrapper::get_new_job_id()
{
    return do_get_new_job_id();
}

job_callback_wrapper_scrub::job_callback_wrapper_scrub( scrubber *s ) : obj( s )
{
    //
}

void job_callback_wrapper_scrub::do_start_calc( job &j )
{
    obj->scrub( j );
}

int job_callback_wrapper_scrub::do_get_new_job_id()
{
    if( obj->sri )
        return obj->sri->get_new_job_id();
    else
        return 0;
}

job_callback_wrapper_clean::job_callback_wrapper_clean( cleaner *c ) : obj( c )
{
    //
}

void job_callback_wrapper_clean::do_start_calc( job &j )
{
    obj->clean( j );
}

int job_callback_wrapper_clean::do_get_new_job_id()
{
    if( obj->sri )
        return obj->sri->get_new_job_id();
    else
        return 0;
}

job_impl::job_impl( int id_, FILE *log_file_ ) : id( id_ ), log_file( log_file_ )
{
    //
}

int job_impl::get_job_id()
{
    return do_get_job_id();
}

float * job_impl::get_input()
{
    return do_get_input();
}

FILE * job_impl::get_log()
{
    return do_get_log();
}

void job_impl::write_log_section_header()
{
    do_write_log_section_header();
}

void job_impl::post_output( float *output )
{
    do_post_output( output );
}

int job_impl::do_get_job_id()
{
    return id;
}

FILE * job_impl::do_get_log()
{
    return log_file;
}

void job_impl::do_write_log_section_header()
{
    //
}

bool job_impl::is_full()
{
    return do_is_full();
}

const NMRData *job_impl::get_input_src()
{
    return do_get_input_src();
}

std::vector< int > * job_impl::get_position()
{
    return do_get_position();
}

job_full_inplace::job_full_inplace( int id_, FILE *log_file_, NMRData *data, boost::mutex *data_mutex ) : job_impl( id_, log_file_ ), d( data ), d_mtx( data_mutex ), data_block( 0 )
{
    //
}

job_full_inplace::~job_full_inplace()
{
    if( data_block )
        delete data_block;
}

float * job_full_inplace::do_get_input()
{
    boost::unique_lock< boost::mutex > lock;
    if( d_mtx ) lock = boost::unique_lock< boost::mutex >( *d_mtx );
    
    data_block = d->GetDataCopyPtr();
    return data_block;
}

void job_full_inplace::do_post_output( float *output )
{
    boost::unique_lock< boost::mutex > lock;
    if( d_mtx ) lock = boost::unique_lock< boost::mutex >( *d_mtx );
    
    d->SetDataFromCopyPtr( output );
}

bool job_full_inplace::do_is_full()
{
    return true;
}

const NMRData * job_full_inplace::do_get_input_src()
{
    return d;
}

std::vector< int > * job_full_inplace::do_get_position()
{
    return 0;
}

job_full::job_full( int id_, FILE *log_file_, const NMRData *input, boost::mutex *input_mutex, NMRData *output, boost::mutex *output_mutex ) : job_impl( id_, log_file_ ), in( input ), in_mtx( input_mutex ), out( output ), out_mtx( output_mutex ), data_block( 0 )
{
    //
}

job_full::~job_full()
{
    if( data_block )
        delete data_block;
    data_block = 0;
}

float * job_full::do_get_input()
{
    boost::unique_lock< boost::mutex > lock;
    if( in_mtx ) lock = boost::unique_lock< boost::mutex >( *in_mtx );
    
    data_block = in->GetDataCopyPtr();
    return data_block;
}

void job_full::do_post_output( float *output )
{
    boost::unique_lock< boost::mutex > lock;
    if( out_mtx ) lock = boost::unique_lock< boost::mutex >( *out_mtx );
    
    out->SetDataFromCopyPtr( output );
}

bool job_full::do_is_full()
{
    return true;
}

const NMRData * job_full::do_get_input_src()
{
    return in;
}

std::vector< int > * job_full::do_get_position()
{
    return 0;
}

job_pos_inplace::job_pos_inplace( int id_, FILE *log_file_, NMRData *data, boost::mutex *data_mutex, std::vector< int > pos, dimension_map dim_map ) : job_impl( id_, log_file_ ), d( data ), d_mtx( data_mutex ), p( pos ), dm( dim_map ), s( 0 ), data_block( 0 )
{
    //
}

job_pos_inplace::~job_pos_inplace()
{
    if( s )
        delete s;
    if( data_block )
        delete data_block;
    
    s = 0;
    data_block = 0;
}

float * job_pos_inplace::do_get_input()
{
    int dims = d->GetNumOfDims();
    std::vector< int > coord1( dims ), coord2( dims );

    int index_dim_count = 0;
    
    switch( dm.F1 )
    {
        case index_dim:
            coord1[ 0 ] = p[ index_dim_count ];
            coord2[ 0 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 0 ] = 0;
            coord2[ 0 ] = d->GetDimensionSize( 0 );
            break;
        case unused:
            break;
    }
    
    switch( dm.F2 )
    {
        case index_dim:
            coord1[ 1 ] = p[ index_dim_count ];
            coord2[ 1 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 1 ] = 0;
            coord2[ 1 ] = d->GetDimensionSize( 1 );
            break;
        case unused:
            break;
    }
   
    switch( dm.F3 )
    {
        case index_dim:
            coord1[ 2 ] = p[ index_dim_count ];
            coord2[ 2 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 2 ] = 0;
            coord2[ 2 ] = d->GetDimensionSize( 2 );
            break;
        case unused:
            break;
    }
    
    switch( dm.F4 )
    {
        case index_dim:
            coord1[ 3 ] = p[ index_dim_count ];
            coord2[ 3 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 3 ] = 0;
            coord2[ 3 ] = d->GetDimensionSize( 3 );
            break;
        case unused:
            break;
    }
    
    boost::unique_lock< boost::mutex > lock;
    if( d_mtx ) lock = boost::unique_lock< boost::mutex >( *d_mtx );
    
    s = new Subset( ( *d )( coord1, coord2 ) );
    
    data_block = s->GetDataCopyPtr();
    return data_block;
}

void job_pos_inplace::do_post_output( float *output )
{
    boost::unique_lock< boost::mutex > lock;
    if( d_mtx ) lock = boost::unique_lock< boost::mutex >( *d_mtx );
    
    s->SetDataFromCopyPtr( output );
}

bool job_pos_inplace::do_is_full()
{
    return false;
}

const NMRData * job_pos_inplace::do_get_input_src()
{
    return d;
}

void job_pos_inplace::do_write_log_section_header()
{
    assert( log_file );
    fprintf( log_file, "Position: " );
    for( int i = 0; i < p.size(); i++ )
    {
        fprintf( log_file, "%i", p[ i ] );
        if( i == p.size() - 1 ) fprintf( log_file, "\n" );
        else fprintf( log_file, ", " );
    }
}

std::vector< int > * job_pos_inplace::do_get_position()
{
    return &p;
}

job_pos::job_pos( int id_, FILE *log_file_, const NMRData *input, boost::mutex *input_mutex, NMRData *output, boost::mutex *output_mutex, std::vector< int > pos, dimension_map dim_map ) : job_impl( id_, log_file_ ), in( input ), in_mtx( input_mutex ), out( output ), out_mtx( output_mutex ), p( pos ), dm( dim_map )
{
    //
}

job_pos::~job_pos()
{
    if( data_block )
        delete data_block;
    data_block = 0;
}

float * job_pos::do_get_input()
{
    int dims = dm.get_num_of_dims();
    std::vector< int > coord1( dims ), coord2( dims );
    
    int index_dim_count = 0;
    
    switch( dm.F1 )
    {
        case index_dim:
            coord1[ 0 ] = p[ index_dim_count ];
            coord2[ 0 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 0 ] = 0;
            coord2[ 0 ] = in->GetDimensionSize( 0 );
            break;
        case unused:
            break;
    }
    
    switch( dm.F2 )
    {
        case index_dim:
            coord1[ 1 ] = p[ index_dim_count ];
            coord2[ 1 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 1 ] = 0;
            coord2[ 1 ] = in->GetDimensionSize( 1 );
            break;
        case unused:
            break;
    }
    
    switch( dm.F3 )
    {
        case index_dim:
            coord1[ 2 ] = p[ index_dim_count ];
            coord2[ 2 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 2 ] = 0;
            coord2[ 2 ] = in->GetDimensionSize( 2 );
            break;
        case unused:
            break;
    }
    
    switch( dm.F4 )
    {
        case index_dim:
            coord1[ 3 ] = p[ index_dim_count ];
            coord2[ 3 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 3 ] = 0;
            coord2[ 3 ] = in->GetDimensionSize( 3 );
            break;
        case unused:
            break;
    }
    
    c1 = coord1;
    c2 = coord2;
    
    boost::unique_lock< boost::mutex > lock;
    if( in_mtx ) lock = boost::unique_lock< boost::mutex >( *in_mtx );
    
    Subset s = ( *in )( coord1, coord2 );
    
    data_block = s.GetDataCopyPtr();
    return data_block;
}

void job_pos::do_post_output( float *output )
{
    boost::unique_lock< boost::mutex > lock;
    if( out_mtx ) lock = boost::unique_lock< boost::mutex >( *out_mtx );
    
    Subset s = ( *out )( c1, c2 );
    s.SetDataFromCopyPtr( output );
}

bool job_pos::do_is_full()
{
    return false;
}

const NMRData * job_pos::do_get_input_src()
{
    return in;
}

void job_pos::do_write_log_section_header()
{
    assert( log_file );
    fprintf( log_file, "Position: " );
    for( int i = 0; i < p.size(); i++ )
    {
        fprintf( log_file, "%i", p[ i ] );
        if( i == p.size() - 1 ) fprintf( log_file, "\n" );
        else fprintf( log_file, ", " );
    }
}

std::vector< int > * job_pos::do_get_position()
{
    return &p;
}

job_pos_to_full::job_pos_to_full( int id_, FILE *log_file_, const NMRData *input, boost::mutex *input_mutex, NMRData *output, std::vector< int > pos, dimension_map dim_map ) : job_impl( id_, log_file_ ), in( input ), in_mtx( input_mutex ), out( output ), p( pos ), dm( dim_map )
{
    //
}

job_pos_to_full::~job_pos_to_full()
{
    if( data_block )
        delete data_block;
    data_block = 0;
}

float * job_pos_to_full::do_get_input()
{
    int dims = dm.get_num_of_dims();
    std::vector< int > coord1( dims ), coord2( dims );
    
    int index_dim_count = 0;
    
    switch( dm.F1 )
    {
        case index_dim:
            coord1[ 0 ] = p[ index_dim_count ];
            coord2[ 0 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 0 ] = 0;
            coord2[ 0 ] = in->GetDimensionSize( 0 );
            break;
        case unused:
            break;
    }
    
    switch( dm.F2 )
    {
        case index_dim:
            coord1[ 1 ] = p[ index_dim_count ];
            coord2[ 1 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 1 ] = 0;
            coord2[ 1 ] = in->GetDimensionSize( 1 );
            break;
        case unused:
            break;
    }
    
    switch( dm.F3 )
    {
        case index_dim:
            coord1[ 2 ] = p[ index_dim_count ];
            coord2[ 2 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 2 ] = 0;
            coord2[ 2 ] = in->GetDimensionSize( 2 );
            break;
        case unused:
            break;
    }
    
    switch( dm.F4 )
    {
        case index_dim:
            coord1[ 3 ] = p[ index_dim_count ];
            coord2[ 3 ] = p[ index_dim_count ];
            index_dim_count++;
            break;
        case u:
        case v:
        case w:
            coord1[ 3 ] = 0;
            coord2[ 3 ] = in->GetDimensionSize( 3 );
            break;
        case unused:
            break;
    }
    
    coord1 = coord1;
    coord2 = coord2;
    
    boost::unique_lock< boost::mutex > lock;
    if( in_mtx ) lock = boost::unique_lock< boost::mutex >( *in_mtx );
    
    Subset s = ( *in )( coord1, coord2 );
    
    data_block = s.GetDataCopyPtr();
    return data_block;
}

void job_pos_to_full::do_post_output( float *output )
{
    out->SetDataFromCopyPtr( output );
}

bool job_pos_to_full::do_is_full()
{
    return false;
}

const NMRData * job_pos_to_full::do_get_input_src()
{
    return in;
}

void job_pos_to_full::do_write_log_section_header()
{
    assert( log_file );
    fprintf( log_file, "Position: " );
    for( int i = 0; i < p.size(); i++ )
    {
        fprintf( log_file, "%i", p[ i ] );
        if( i == p.size() - 1 ) fprintf( log_file, "\n" );
        else fprintf( log_file, ", " );
    }
}

std::vector< int > * job_pos_to_full::do_get_position()
{
    return &p;
}

namespace nmr_wash
{
    namespace details
    {
        template std::vector< job > make_jobs_all_pos< make_job_pos_inplace >( const BEC_NMRData::NMRData *in, make_job_pos_inplace &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l );
        template std::vector< job > make_jobs_all_pos< make_job_pos >( const BEC_NMRData::NMRData *in, make_job_pos &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l );
        template std::vector< job > make_jobs_pos_list< make_job_pos_inplace >( std::vector< std::vector< int > > pos_list, const BEC_NMRData::NMRData *in, make_job_pos_inplace &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l );
        template std::vector< job > make_jobs_pos_list< make_job_pos >( std::vector< std::vector< int > > pos_list, const BEC_NMRData::NMRData *in, make_job_pos &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l );
        template std::vector< job > make_jobs_pos_list< make_job_pos_to_full >( std::vector< std::vector< int > > pos_list, const BEC_NMRData::NMRData *in, make_job_pos_to_full &pred, dimension_map dm, boost::shared_ptr< job_callback_wrapper > jcw, job_queue *jq, logger &l );
    }
}

