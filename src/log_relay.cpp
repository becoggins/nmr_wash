//  Copyright (c) 2012, Brian E. Coggins, Ph.D.  All rights reserved.

#include <iostream>
#include "console_support.h"

log_relay & operator<<( log_relay &l, std::ostream & ( *manip )( std::ostream & ) )
{
    manip( l.cstr );
    manip( l.ss );
    
    return l;
}