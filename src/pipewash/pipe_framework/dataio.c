/******************************************************************************/
/*                                                                            */
/*                   ---- NIH NMR Software System ----                        */
/*                        Copyright 1992 and 1993                             */
/*                             Frank Delaglio                                 */
/*                   NIH Laboratory of Chemical Physics                       */
/*                                                                            */
/*               This software is not for distribution without                */
/*                  the written permission of the author.                     */
/*                                                                            */
/******************************************************************************/

/***/
/* DataIO: routines for binary I/O via C or system calls.
/*
/* error = DataRead( fileUnit, buffer, byteCount );
/*         DataWrite( fileUnit, buffer, byteCount );
/*         DataReadB( fileUnit, buffer, byteCount, maxTries, timeOut );
/*         DataWriteB( fileUnit, buffer, byteCount, maxTries, timeOut );
/*         DataRecvB( fileUnit, buffer, byteCount, maxTries, timeOut );
/*         DataSendB( fileUnit, buffer, byteCount, maxTries, timeOut );
/*         DataPos( fileUnit, bytePosition );
/*         DataPos2( fileUnit, bytePosition, buff, buffSize );
/*
/* error = dataOpen( fileName, fileUnit, openMode );
/*         dataOpen2( fileName, fileUnit, openStr );
/*         dataClose( fileUnit );
/*         dataCreate( fileName );
/*         dirCreate( dirName );
/**/

#include <stdio.h>
#include <stdlib.h>

#ifdef WIN95
#include <sys/stat.h>
#include <errno.h>
#include <winsock.h>
#include <direct.h>
#include <fcntl.h>

#define popen(S1,S2) _popen(S1,S2)
typedef unsigned int mode_t;
#else
#include <sys/errno.h>
#include <dirent.h>
#endif

#include <sys/types.h>

#ifdef MAC_OSX
#include <sys/stat.h>
#endif

#include <string.h>

#include "dataio2.h"
#include "dataio.h"

#ifdef NMRTIME
#include "nmrserver.h"
#include "nmrpipe.h"
#endif

#include "nmrtime.h"
#include "memory.h"

#ifdef FB_SYS_IO

#ifdef WIN95
#include <time.h>
#undef O_BINARY
#define O_BINARY _O_BINARY | _O_RANDOM
#else
#include <sys/time.h>
#include <sys/file.h>
#undef  O_BINARY
#define O_BINARY 0
#endif

#include <fcntl.h>
#include <errno.h>

#ifndef LINT_CHECK
#ifndef WIN95
#include <unistd.h>
#endif
#endif

#endif /* FB_SYS_IO */

#include "memory.h"
#include "prec.h"

#ifdef WINXP
#include <fcntl.h>
#endif

#if defined (WIN95) || defined (WINNT) || defined (MAC_OSX)
static int selectFlag = 0;
#else
static int selectFlag = 1;
#endif

static int defTimeOut = DATAIO_TIMEOUT;

static int autoSwapFlag = 1;
static int byteSwapFlag = 0;

#define FPR (void)fprintf

/***/
/* initDataIO: initialize I/O settings via environment variables.
/***/

int initDataIO()
{
    int  status;
    char *sPtr;

    char *getenv();

    status = 0;

    if (sPtr = getenv( "NMR_IO_SELECT" ))
       {
        selectFlag = atoi( sPtr );
        status += 1;
       }

    if (sPtr = getenv( "NMR_IO_TIMEOUT" ))
       {
        defTimeOut = atoi( sPtr );
        status += 2;
       }

    if (sPtr = getenv( "NMR_AUTOSWAP" ))
       {
        if (!strcasecmp( sPtr, "VERBOSE" )) autoSwapFlag = 2;
        if (!strcasecmp( sPtr, "YES" ))     autoSwapFlag = 1;
        if (!strcasecmp( sPtr, "TRUE" ))    autoSwapFlag = 1;
        if (!strcasecmp( sPtr, "1" ))       autoSwapFlag = 1;

        if (!strcasecmp( sPtr, "NO" ))      autoSwapFlag = 0;
        if (!strcasecmp( sPtr, "FALSE" ))   autoSwapFlag = 0;
        if (!strcasecmp( sPtr, "0" ))       autoSwapFlag = 0;

        status += 4;
       }

    return( status ); 
}

/***/
/* getDefTimeout: returns current default IO timeout.
/***/

int getDefTimeout()
{
   return( defTimeOut );
}

/***/
/* dataOpen: open a binary file named "fileName".
/***/

int dataOpen( fileName, fileUnit, openMode )

   char *fileName;
   int   openMode;

   FILE_UNIT( *fileUnit );

{
    struct stat buff;
    char   *modeName;
    int    error;
    mode_t mode;

/***/
/* Initialize error status:
/***/

    error = 0;
    mode  = 0666;

/***/
/* Test for I/O to one-way pipe:
/***/

   if (fileName && *fileName == '!')
      {
       error = dataPOpen( fileName+1, fileUnit, openMode );
       return( error );
      }

/***/
/* Test for existance of file and create it if needed:
/***/

    if (openMode == FB_UNKNOWN)
       {
        if (stat( fileName, &buff ) && ENOENT == errno)
           {
            if (error = dataCreate( fileName ))
               {
                return( error );
               }
           }

        openMode = FB_READWRITE;
       }

/***/
/* Open file using system call:
/***/

#ifdef FB_SYS_IO

    errno = 0;

    switch( openMode )
       {
        case FB_READ:
           *fileUnit = open( fileName, O_RDONLY | O_BINARY );
           modeName  = "Read";
           break;

        case FB_WRITE:
           *fileUnit = open( fileName, O_WRONLY | O_BINARY );
           modeName  = "Write";
           break;

        case FB_READWRITE:
           *fileUnit = open( fileName, O_RDWR | O_BINARY  );
           modeName  = "Read/Write";
           break;

        case FB_CREATE:

#ifdef WIN95
           if ((*fileUnit = creat( fileName, mode )) >= 0)
              {
               (void) close( *fileUnit );
              }

           *fileUnit = open( fileName, O_WRONLY | O_BINARY );
#else
#ifndef LINT_CHECK
           if ((*fileUnit = creat( fileName, mode )) < 0)
              {
               *fileUnit = open( fileName, O_WRONLY | O_BINARY );
              }
#endif
#endif

           modeName  = "Create";
           break;

        case FB_CREATE_KEEP:
/*  Had to add a file mode to the open() call on the next line--need to get a proper correction from Frank. -BEC */
           *fileUnit = open( fileName, O_RDWR | O_BINARY | O_CREAT, 644 );
           modeName  = "CreateKeep";
           break;

        default:
           *fileUnit = -1;
           modeName  = "???";
           break;
       }
     
    if (*fileUnit < 0)
       {
        (void) fprintf( stderr, "DATAIO SYSOPEN Error %d:\n", errno );
        (void) fprintf( stderr, "File: %s Access: %s\n", fileName, modeName );

        if (errno) (void) perror( "SYSOPEN Message" );

        error = 1;
        *fileUnit = UNIT_CAST( 0 );  
       }

/***/
/* Open file via C routines:
/***/

#else

    switch( openMode )
       {
        case FB_READ:
           *fileUnit = fopen( inName, "r" );
           break;
        case FB_WRITE:
           *fileUnit = fopen( inName, "w" );
           break;
        case FB_READWRITE:
           *fileUnit = fopen( inName, "r+" );
           break;
        case FB_CREATE:
           *fileUnit = fopen( inName, "w+" );
           break;
        default:
           *fileUnit = UNIT_CAST( 0 );
           break;
       }

    if (!*fileUnit)
       {
        (void) fprintf( stderr, "DATAIO C-OPEN Error:\n" );
        (void) fprintf( stderr, "File %s Mode %d`n", openMode );
        error = 1;
       }

#endif

/***/
/* Return open status either way:
/***/

    return( error );
}

/***/
/* dataPOpen: open a one-way pipe according to mode string.
/***/

int dataPOpen( cmndName, fileUnit, openMode )

   char *cmndName;
   int   openMode;

   FILE_UNIT( *fileUnit );

{
    FILE  *pUnit;
    int   error;

/***/
/* Initialize error status:
/***/

    error     = 0;
    *fileUnit = -1;

    switch( openMode )
       {
        case FB_READ:
           if ((pUnit = popen( cmndName, "r" )))
              {
               *fileUnit = fileno( pUnit );
              }

           break;

        case FB_WRITE:
        case FB_CREATE:

           if ((pUnit = popen( cmndName, "w" )))
              {
               *fileUnit = fileno( pUnit );
              }

           break;

        default:
           break;
       }

    if (*fileUnit < 0)
       {
        (void) fprintf( stderr, "DATAIO POPEN Error %d:\n", errno );
        (void) fprintf( stderr, "Cmnd: %s Access: %d\n", cmndName, openMode );

        if (errno) (void) perror( "SYSOPEN Message" );

        error = 1;
       }

    return( error );
}

/***/
/* dataOpen2: open a binary file named "fileName" according to mode string.
/***/

int dataOpen2( fileName, fileUnit, openStr )

   char *fileName, *openStr;
   int  *fileUnit;

{
    int    pipeMode, flags;
    int    error;
    mode_t mode;

    *fileUnit = -1;

    pipeMode  = FB_READ; 
    mode      = 0666;
    flags     = 0;
    error     = 0;

    if (strchr( openStr, 'r' ) || strchr( openStr, 'R' )) flags |= O_RDONLY;
    if (strchr( openStr, 'w' ) || strchr( openStr, 'W' )) flags |= O_WRONLY;

    if (flags == (O_RDONLY | O_WRONLY)) flags = O_RDWR;

#ifndef WIN95
    if (strchr( openStr, 'd' ) || strchr( openStr, 'D' )) flags |= O_NDELAY;
    if (strchr( openStr, 'y' ) || strchr( openStr, 'Y' )) flags |= O_NOCTTY;
#ifndef WINXP
#ifdef MAC_OSX
    if (strchr( openStr, 's' ) || strchr( openStr, 'S' )) flags |= O_FSYNC;
#else
    if (strchr( openStr, 's' ) || strchr( openStr, 'S' )) flags |= O_SYNC;
#endif
#endif
#endif
    if (strchr( openStr, 'a' ) || strchr( openStr, 'A' )) flags |= O_APPEND;
    if (strchr( openStr, 'c' ) || strchr( openStr, 'C' )) flags |= O_CREAT;
    if (strchr( openStr, 't' ) || strchr( openStr, 'T' )) flags |= O_TRUNC;
    if (strchr( openStr, 'e' ) || strchr( openStr, 'E' )) flags |= O_EXCL;

    if (!strcasecmp( openStr, "w" ) || !strcasecmp( openStr, "W" ))
       {
        flags = (O_RDWR | O_CREAT | O_TRUNC);
       }

    mode |= O_BINARY;

    if (flags | O_WRONLY) pipeMode = FB_WRITE;
    if (flags | O_APPEND) pipeMode = FB_WRITE;
    if (flags | O_CREAT)  pipeMode = FB_WRITE;

    if (fileName && *fileName == '!')
       {
        error = dataPOpen( fileName+1, fileUnit, pipeMode );
       }
    else
       {
        *fileUnit = open( fileName, flags, mode );
        if (*fileUnit < 0) (void) perror( "DataOpen2 File Error" );
       }

    return( error );
}

/***/
/* DataRead: read byteCount bytes from file inUnit.
/***/

int DataRead( inUnit, array, byteCount )

   FILE_UNIT( inUnit );

   REAL4 *array;
   INT4   byteCount;

{
    int  count, error;

/***/
/* Initialize error status:
/***/

    error = 0;

/***/
/* Read the data:
/***/

#ifdef FB_SYS_IO
    count = read( inUnit, (IOPTR *)array, (IOSIZE)byteCount );
#else
    count = fread( array, 1, byteCount, inUnit );
#endif

/***/
/* Interpret the status:
/***/

    if (count != byteCount)
       {
        (void) fprintf( stderr, "DATAIO File Read Error %d.\n", errno );
        (void) fprintf( stderr, "Request: %d Actual: %d.\n", byteCount, count );
        (void) perror( "SYSREAD Message" );
        error = 1;
       }

   if (byteSwapFlag) (void) byteSwapV( array, byteCount/sizeof(REAL4) );
 
   return( error );
}

/***/
/* Get and set byteswap modes.
/***/

int getAutoSwapFlag()
{
   return( autoSwapFlag );
}

int getByteSwapFlag()
{
   return( byteSwapFlag );
}

int setAutoSwapFlag( aFlag )

   int aFlag;
{
   autoSwapFlag = aFlag;
   return( aFlag );
}

int setByteSwapFlag( sFlag )

   int sFlag;
{
    byteSwapFlag = sFlag;
    return( sFlag );
}

/***/
/* DataReadB: Read byteCount bytes from file inUnit in pseudo-blocking mode.
/***/

int DataReadB( inUnit, rArray, bytesRequested, maxTries, timeOut )

   FILE_UNIT( inUnit );

   INT4   bytesRequested, maxTries, timeOut;
   REAL4 *rArray;

{
    int  count, tries, bytesLeft;
    char *array;

    array     = (char *)rArray;
    bytesLeft = bytesRequested;
    tries     = 0;

    while( bytesLeft && (maxTries < 0 || tries++ < maxTries) )
       {

#ifdef FB_SYS_IO
        if (selectFlag)
           {
            FD_ZERO2( &readfds );
            FD_SET( inUnit, &readfds );

            count = select( DTABLESIZE, &readfds, FDNULL, FDNULL, TONULL );
           }

        count = read( inUnit, (IOPTR *)array, (IOSIZE)bytesLeft );
#else
        count = fread( array, 1, bytesLeft, inUnit );
#endif

        bytesLeft -= count;

        if (bytesLeft && (maxTries < 0 || tries < maxTries))
           {
            array += count;
            USLEEP( timeOut );
            TIMER_INCR( readRetries );
           }
       }

    if (byteSwapFlag) (void) byteSwapV( rArray, bytesRequested/sizeof(REAL4) );

    if (bytesLeft) return( -1 );

    return( 0 );
}

/***/
/* DataRecvB: read byteCount bytes from socket inUnit in pseudo-blocking mode.
/***/

int DataRecvB( inUnit, rArray, bytesRequested, maxTries, timeOut )

   FILE_UNIT( inUnit );

   INT4   bytesRequested, maxTries, timeOut;
   REAL4 *rArray;

{
    int  count, tries, bytesLeft;
    char *array;

    array     = (char *)rArray;
    bytesLeft = bytesRequested;
    tries     = 0;

    while( bytesLeft && (maxTries < 0 || tries++ < maxTries) )
       {

#ifdef FB_SYS_IO
        if (selectFlag)
           {
            FD_ZERO2( &readfds );
            FD_SET( inUnit, &readfds );

            count = select( DTABLESIZE, &readfds, FDNULL, FDNULL, TONULL );
           }

        count = recv( inUnit, (IOPTR *)array, bytesLeft, 0 );
#else
        return( -666 );
#endif

        bytesLeft -= count;

        if (bytesLeft)
           {
            array += count;
            USLEEP( timeOut );
            TIMER_INCR( sReadRetries );
           }
       }

    if (byteSwapFlag) (void) byteSwapV( rArray, bytesRequested/sizeof(REAL4) );

    if (bytesLeft) return( -1 );

    return( 0 );
}

/***/
/* DataRecvCB: read byteCount chars from socket inUnit in pseudo-blocking mode.
/***/

int DataRecvCB( inUnit, array, bytesRequested, maxTries, timeOut )

   FILE_UNIT( inUnit );

   INT4   bytesRequested, maxTries, timeOut;
   char   *array;
{
    int count, tries, bytesLeft;

    bytesLeft = bytesRequested;
    tries     = 0;

    while( bytesLeft && (maxTries < 0 || tries++ < maxTries) )
       {
#ifdef FB_SYS_IO
        if (selectFlag)
           {
            FD_ZERO2( &readfds );
            FD_SET( inUnit, &readfds );

            count = select( DTABLESIZE, &readfds, FDNULL, FDNULL, TONULL );
           }

        count = recv( inUnit, (IOPTR *)array, bytesLeft, 0 );
#else
        return( -666 );
#endif

        bytesLeft -= count;

        if (bytesLeft)
           {
            array += count;
            USLEEP( timeOut );
            TIMER_INCR( sReadRetries );
           }
       }

    if (bytesLeft) return( -1 );

    return( 0 );
}

/***/
/* dataReadC: read byteCount bytes from file inUnit into a character array.
/***/

int dataReadC( inUnit, array, byteCount )

   FILE_UNIT( inUnit );

   char  *array;
   INT4  byteCount;

{
    int count, error;

/***/
/* Initialize error status:
/***/

    error = 0;
    errno = 0;

/***/
/* Read the data:
/***/

#ifdef FB_SYS_IO
    count = read( inUnit, (IOPTR *)array, (IOSIZE)byteCount );
#else
    count = fread( array, 1, byteCount, inUnit );
#endif

/***/
/* Interpret the status:
/***/

    if (count != byteCount)
       {
        (void) fprintf( stderr, "DATAIO File Read Error %d.\n", errno );
        (void) fprintf( stderr, "Request: %d Actual: %d.\n", byteCount, count );

        if (errno) (void) perror( "SYSTEM Message" );

        error = 1;
       }

   return( error );
}

/***/
/* dataWriteC: write byteCount bytes from file outUnit into a character array.
/***/

int dataWriteC( outUnit, array, byteCount )

   FILE_UNIT( outUnit );

   char  *array;
   INT4  byteCount;

{
    int count, error;

/***/
/* Initialize error status:
/***/

    error = 0;
    errno = 0;

/***/
/* Write the data:
/***/

#ifdef FB_SYS_IO
    count = write( outUnit, (IOPTR *)array, (IOSIZE)byteCount );
#else
    count = fwrite( array, 1, byteCount, outUnit );
#endif

/***/
/* Interpret the status:
/***/

    if (count != byteCount)
       {
        (void) fprintf( stderr, "DATAIO File Write Error %d.\n", errno );
        (void) fprintf( stderr, "Request: %d Actual: %d.\n", byteCount, count );

        if (errno) (void) perror( "SYSTEM Message" );

        error = 1;
       }

   return( error );
}

/***/
/* dataReadCB: Read byteCount bytes from file inUnit in pseudo-blocking mode.
/***/

int dataReadCB( inUnit, array, bytesRequested, maxTries, timeOut )

   FILE_UNIT( inUnit );

   INT4   bytesRequested, maxTries, timeOut;
   char   *array;

{
    int count, tries, bytesLeft;

    bytesLeft = bytesRequested;
    tries     = 0;

    while( bytesLeft && (maxTries < 0 || tries++ < maxTries) )
       {

#ifdef FB_SYS_IO
        if (selectFlag)
           {
            FD_ZERO2( &readfds );
            FD_SET( inUnit, &readfds );

            count = select( DTABLESIZE, &readfds, FDNULL, FDNULL, TONULL );
           }

        count = read( inUnit, (IOPTR *)array, (IOSIZE)bytesLeft );
#else
        count = fread( array, 1, bytesLeft, inUnit );
#endif

        bytesLeft -= count;

        if (bytesLeft && (maxTries < 0 || tries < maxTries))
           {
            array += count;
            USLEEP( timeOut );
            TIMER_INCR( readRetries );
           }
       }

    if (bytesLeft) return( -1 );

    return( 0 );
}

/***/
/* DataWrite: write byteCount bytes to file outUnit.
/***/

int DataWrite( outUnit, array, byteCount )

   FILE_UNIT( outUnit );

   REAL4 *array;
   INT4   byteCount;

{
    int count, error;

/***/
/* Initialize error status:
/***/

    error = 0;
    errno = 0;

/***/
/* Write the data:
/***/

#ifdef FB_SYS_IO
    count = write( outUnit, (IOPTR *)array, (IOSIZE)byteCount );
#else
    count = fwrite( array, 1, byteCount, outUnit );
#endif

/***/
/* Interpret the status:
/***/

    if (count != byteCount)
       {
        (void) fprintf( stderr, "DATAIO File Write Error %d.\n", errno );
        (void) fprintf( stderr, "Request: %d Actual: %d.\n", byteCount, count );

        if (errno) (void) perror( "SYSWRITE Message" );

        error = 1;
       }

   return( error );
}

/***/
/* DataWriteB: write byteCount bytes to file outUnit in pseudo-blocking mode.
/***/

int DataWriteB( outUnit, rArray, bytesRequested, maxTries, timeOut )

   FILE_UNIT( outUnit );

   INT4   bytesRequested, maxTries, timeOut;
   REAL4 *rArray;

{
    int  count, tries, bytesLeft;
    char *array;

    array     = (char *)rArray;
    bytesLeft = bytesRequested;
    tries     = 0;

    while( bytesLeft && (maxTries < 0 || tries++ < maxTries) )
       {

#ifdef FB_SYS_IO
        if (selectFlag)
           {
            FD_ZERO2( &writefds );
            FD_SET( outUnit, &writefds );

            count = select( DTABLESIZE, FDNULL, &writefds, FDNULL, TONULL );
           }

        count = write( outUnit, (IOPTR *)array, (IOSIZE)bytesLeft );
#else
        count = fwrite( array, 1, bytesLeft, outUnit );
#endif

        bytesLeft -= count;

        if (bytesLeft && (maxTries < 0 || tries < maxTries))
           {
            array += count;
            USLEEP( timeOut );
            TIMER_INCR( writeRetries );
           }
       }

    if (bytesLeft) return( -1 );

    return( 0 );
}

/***/
/* dataWriteCB: write byteCount chars to file outUnit in pseudo-blocking mode.
/***/

int dataWriteCB( outUnit, array, bytesRequested, maxTries, timeOut )

   FILE_UNIT( outUnit );

   INT4   bytesRequested, maxTries, timeOut;
   char   *array;

{
    int count, tries, bytesLeft;

    bytesLeft = bytesRequested;
    tries     = 0;

    while( bytesLeft && (maxTries < 0 || tries++ < maxTries) )
       {

#ifdef FB_SYS_IO
        if (selectFlag)
           {
            FD_ZERO2( &writefds );
            FD_SET( outUnit, &writefds );

            count = select( DTABLESIZE, FDNULL, &writefds, FDNULL, TONULL );
           }

        count = write( outUnit, (IOPTR *)array, (IOSIZE)bytesLeft );
#else
        count = fwrite( array, 1, bytesLeft, outUnit );
#endif

        bytesLeft -= count;

        if (bytesLeft && (maxTries < 0 || tries < maxTries))
           {
            array += count;
            USLEEP( timeOut );
            TIMER_INCR( writeRetries );
           }
       }

    if (bytesLeft) return( -1 );

    return( 0 );
}

/***/
/* DataSendB: write byteCount bytes to socket outUnit in pseudo-blocking mode.
/***/

int DataSendB( outUnit, rArray, bytesRequested, maxTries, timeOut )

   FILE_UNIT( outUnit );

   INT4   bytesRequested, maxTries, timeOut;
   REAL4 *rArray;

{
    int  count, tries, bytesLeft;
    char *array;

    array     = (char *)rArray;
    bytesLeft = bytesRequested;
    tries     = 0;

    while( bytesLeft && (maxTries < 0 || tries++ < maxTries) )
       {

#ifdef FB_SYS_IO
        if (selectFlag)
           {
            FD_ZERO2( &writefds );
            FD_SET( outUnit, &writefds );

            count = select( DTABLESIZE, FDNULL, &writefds, FDNULL, TONULL );
           }

        count = send( outUnit, (IOPTR *)array, bytesLeft, 0 );
#else
        return( -666 );
#endif

        bytesLeft -= count;

        if (bytesLeft && (maxTries < 0 || tries < maxTries))
           {
            array += count;
            USLEEP( timeOut );
            TIMER_INCR( sWriteRetries );
           }
       }

    if (bytesLeft) return( -1 );

    return( 0 );
}

/***/
/* DataSendCB: write byteCount chars to socket outUnit in pseudo-blocking mode.
/***/

int DataSendCB( outUnit, array, bytesRequested, maxTries, timeOut )

   FILE_UNIT( outUnit );

   INT4   bytesRequested, maxTries, timeOut;
   char   *array;

{
    int count, tries, bytesLeft;

    bytesLeft = bytesRequested;
    tries     = 0;

    while( bytesLeft && (maxTries < 0 || tries++ < maxTries) )
       {
#ifdef FB_SYS_IO
        if (selectFlag)
           {
            FD_ZERO2( &writefds );
            FD_SET( outUnit, &writefds );

            count = select( DTABLESIZE, FDNULL, &writefds, FDNULL, TONULL );
           }

        count = send( outUnit, (IOPTR *)array, bytesLeft, 0 );
#else
        return( -666 );
#endif

        bytesLeft -= count;

        if (bytesLeft && (maxTries < 0 || tries < maxTries))
           {
            array += count;
            USLEEP( timeOut );
            TIMER_INCR( sWriteRetries );
           }
       }

    if (bytesLeft) return( -1 );

    return( 0 );
}

/***/
/* DataPos: position fileUnit to byte "position".
/***/

int DataPos( fileUnit, position )

   FILE_UNIT( fileUnit );
   INT4 position;

{
    int error;
    
#ifdef FB_SYS_IO
    off_t istatus;
#endif

    error = 0;
    errno = 0;

#ifdef FB_SYS_IO
#ifndef LINT_CHECK
    istatus = lseek( fileUnit, (off_t) position, SEEK_SET );
    if (-1 == istatus) error = 1;
#endif
#else
    error = fseek( fileUnit, (long) position, SEEK_SET );
#endif

    if (error)
       {
        (void) fprintf( stderr, "DATAIO File Seek error %d.\n", errno );

        if (errno) (void) perror( "SYSTEM Message" );
       }
      
    return( error ); 
}

/***/
/* DataPos2: use read to move position of fileUnit forward by byteCount.
/***/

int DataPos2( fileUnit, byteCount, buffer, buffSize )

   FILE_UNIT( fileUnit );
   INT4 byteCount, buffSize;
   VOID *buffer;
{
    int bytesIn;

    while( byteCount > 0 )
       {
        bytesIn = byteCount > buffSize ? buffSize : byteCount;

        if (dataReadB( fileUnit, (float *)buffer, bytesIn, -1, 0 ))
           {
            (void) fprintf( stderr, "DATAPOS2 Error positioning stream.\n" );
            return( 1 );
           }

        byteCount -= bytesIn;
       }

    return( 0 );
}

/***/
/* byteSwapV: verbose version, perform 4-byte swap of elements in vec.
/***/

int byteSwapV( vec, length )

   float *vec;
   int   length;
{
    union fsw { float f; char s[4]; } in, out;

    if (autoSwapFlag > 1) FPR( stderr, " Byte Swap Data:   %6d\n", length );

    while( length-- )
       {
        in.f = *vec;

        out.s[0] = in.s[3];
        out.s[1] = in.s[2];
        out.s[2] = in.s[1];
        out.s[3] = in.s[0];

        *vec++ = out.f;
       }

    return( 0 );
}

/***/
/* dataClose: close file or socket at fileUnit.
/***/

int dataClose( fileUnit )

   FILE_UNIT( fileUnit );
{
    int error;

    error = 0;
    errno = 0;

    if (!fileUnit) return( 0 );

#ifdef FB_SYS_IO
    if (fileUnit > 0)
       error = close( fileUnit );
#else
    if (fileUnit)
       error = fclose( fileUnit );
#endif

    if (error)
       {
        (void) fprintf( stderr, "DATAIO File Close Error %d.\n", errno );

        if (errno) (void) perror( "SYSTEM Message" );
       }

    return( error );
}

/***/
/* dataCreate: create a binary file named "fileName".
/***/

int dataCreate( fileName )

   char *fileName;
{
    FILE_UNIT( fileUnit );
    int error;

    error = dataOpen( fileName, &fileUnit, FB_CREATE );

    if (error)
       (void) fprintf( stderr, "DATAIO File Create Error %d.\n", error );

    (void) dataClose( fileUnit );

    return( error );
}

/***/
/* dirCreate: create directory dirName.
/***/

int dirCreate( dirName )

   char *dirName;
{
    int  error;

#ifdef WIN95
    error = mkdir( dirName );
#else
    error = mkdir( dirName, 0755 );
#endif

    if (error)
       {
        (void) fprintf( stderr,
                        "DATAIO Error Creating Directory %s\n", dirName );

        (void) perror( "MKDIR Message" );
       }

    return( error );
}

/***/
/* delFile: delete a file.
/***/

int delFile( fileName )

   char *fileName;
{
    int error;

    error = 0;

#ifdef WIN95
    if (fileExists( fileName ))
       {
        (void) unlink( fileName );
       }
#else
    if (fileExists0( fileName ))
       {
        if (error = unlink( fileName ))
           {
            (void) perror( "DATAIO Error Deleting File" );
           }
       }
#endif

    return( error );
}

#ifndef WIN95

/***/
/* delDirContents: delete the regular files in the given directory.
/***/

int delDirContents( dirName )

   char *dirName;
{
    char   fullName[NAMELEN+1], ctemp[NAMELEN+1];
    DIR    *dirUnit;
    int    i;

    struct stat   statBuff;
    struct dirent *dirInfo;

#ifdef SOLARIS
    char buff[sizeof(struct dirent) + _POSIX_PATH_MAX + NAMELEN + 1];
    struct dirent *readdir_r(); /* Not used. */
#endif

    errno   = 0;
    dirUnit = (DIR *)NULL;
    dirInfo = (struct dirent *)NULL;

    if (!(dirUnit = opendir( dirName )))
       {
        (void) fprintf( stderr, "DATAIO Error opening dir %s\n", dirName );

        if (errno) (void) perror( "System Message" );
        return( 1 );
       }

    while( dirInfo = READDIR( dirUnit ))
       {
        for( i = 0; i < DIR_NAMELEN; i++ )
           {
            ctemp[i] = dirInfo->d_name[i];
           }

        ctemp[DIR_NAMELEN] = '\0';

        (void) sprintf( fullName, "%s/%s", dirName, ctemp );

        if (!stat( fullName, &statBuff ))
           {
            if (S_ISREG(statBuff.st_mode)) (void) delFile( fullName );
           }
       }

    (void) closedir( dirUnit );

    return( 0 );
}

#endif

/***/
/* Bottom
/***/
