/******************************************************************************/
/*                                                                            */
/*                   ---- NIH NMR Software System ----                        */
/*                        Copyright 1992 and 1993                             */
/*                             Frank Delaglio                                 */
/*                   NIH Laboratory of Chemical Physics                       */
/*                                                                            */
/*               This software is not for distribution without                */
/*                  the written permission of the author.                     */
/*                                                                            */
/******************************************************************************/

/***/
/* memory.c
/*
/* Procedure wrappers to allocate dynamic memory, for
/* monitoring and dubugging of memory use.
/*
/*  charMalloc( caller, length )
/*  voidMalloc( caller, length )
/*  charReMalloc( caller, ptr, newLength )
/*  voidReMalloc( caller, ptr, newLength )
/*  unAlloc( caller, ptr, knownLength )
/*
/*  getMemUsed( usedPtr, reusedPtr )
/*  setAlloc( verboseFlag )
/*  strDup( string )
/*  strDup2( caller, string )
/*  fltAlloc( caller, length )
/*  intAlloc( caller, length )
/*  dIntAlloc( caller, length )
/*  dFltAlloc( caller, length )
/*  matAlloc( caller, xLength, yLength )
/*  matAllocD( caller, xLength, yLength )
/*  matFree( matrix, xLength, yLength )
/*  matFreeD( matrix, xLength, yLength )
/*  intReAlloc( caller, ptr, newLength, oldLength )
/*  fltReAlloc( caller, ptr, newLength, oldLength )
/***/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "memory.h"

static  int bytesUsed = 0, bytesReused = 0, verboseAlloc = 0;

#define FPR (void)fprintf

#define ERRTEST( P ) \
   if (!(P)) (void)fprintf( stderr, "Memory allocation failure.\n" );

/***/
/* Adjust verbose flag:
/***/

int setAlloc( verboseFlag )

   int verboseFlag;
{
   return( verboseAlloc = verboseFlag );
}

/***/
/* Return memory in use:
/***/

int getMemUsed( usedPtr, reusedPtr )

   int *usedPtr, *reusedPtr;
{

   *usedPtr   = bytesUsed;
   *reusedPtr = bytesReused;

   return( 0 );
}

/***/
/* Show current memory usage.
/***/

int showAlloc()
{
    FPR( stderr,
         "REMARK Memory Allocation Status. Reused: %d In Use: %d\n",
         bytesReused, bytesUsed );

    return( 0 );
}
 

/***/
/* Allocate a float array:
/***/

float *fltAlloc( caller, length )

   char *caller;
   int   length;
{
   float *ptr;

   length    *= sizeof(float);
   bytesUsed += length;
   ptr        = (float *) malloc( (unsigned)length );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM FLT_ALLOC Name: %s Alloc: %p Add: %d Now: %d\n",
            caller, (void *)ptr, length, bytesUsed );
      }
#endif

   ERRTEST( ptr );
 
   return( ptr );
}

/***/
/* Allocate an int array:
/***/

int *intAlloc( caller, length )

   char *caller;
   int   length;
{
   int *ptr;

   length    *= sizeof(int);
   bytesUsed += length;
   ptr        = (int *) malloc( (unsigned)length );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM INT_ALLOC Name: %s Alloc: %p Add: %d Now: %d\n",
            caller, (void *)ptr, length, bytesUsed );
      }
#endif

   ERRTEST( ptr );

   return( ptr );
}

/***/
/* Allocate a double prec float array:
/***/

double *dFltAlloc( caller, length )

   char *caller;
   int   length;
{
   double *ptr;

   ptr        = (double *)NULL;
   length    *= sizeof(double);
   bytesUsed += length;

#ifndef LINT_CHECK
   ptr        = (double *) malloc( (unsigned)length );
#endif

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM DBL_ALLOC Name: %s Alloc: %p Add: %d Now: %d\n",
            caller, (void *)ptr, length, bytesUsed );
      }
#endif

   ERRTEST( ptr );

   return( ptr );
}

/***/
/* Allocate a long int array:
/***/

long int *dIntAlloc( caller, length )

   char *caller;
   int   length;
{
   long int *ptr;

   length    *= sizeof(long int);
   bytesUsed += length;
   ptr        = (long int *) malloc( (unsigned)length );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM LNG_ALLOC Name: %s Alloc: %p Add: %d Now: %d\n",
            caller, (void *)ptr, length, bytesUsed );
      }
#endif

   ERRTEST( ptr );

   return( ptr );
}

/***/
/* Allocate a character array:
/***/

char *charMalloc( caller, length )

   char    *caller;
   SIZE_T  length;
{
   char *ptr;

   length    *= sizeof(char);
   bytesUsed += length;
   ptr        = (char *) malloc( (unsigned)length );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM CHR_ALLOC Name: %s Alloc: %p Add: %d Now: %d\n",
            caller, (void *)ptr, (int)length, bytesUsed );
      }
#endif

   ERRTEST( ptr );

   return( ptr );
}

/***/
/* strFree: deallocate string created by strDup.
/***/

int strFree( string )

   char *string;
{
    if (!string) return( 0 );

    (void) deAlloc( "strDup", string, (int)(1+strlen(string)) );

    return( 0 );
}

/***/
/* strDup: allocate, create, and return a duplicate of string.
/***/

char *strDup( string )

   char *string;
{
    char *dup;

    if (!(dup = charAlloc( "strDup", (int)(1+strlen(string)) )))
       {
        ERRTEST( dup );
        return( dup );
       }

    (void) strcpy( dup, string );

    return( dup );
}

/***/
/* strDup2: allocate, create, and return a duplicate of string.
/***/

char *strDup2( caller, string )

   char *caller, *string;
{
    char *dup;

    if (!(dup = charAlloc( caller, (int)(1+strlen(string)) )))
       {
        ERRTEST( dup );
        return( dup );
       }

    (void) strcpy( dup, string );

    return( dup );
}

/***/
/* Allocate a type void array:
/***/

void *voidMalloc( caller, length )

   char   *caller;
   SIZE_T length;
{
   void *ptr;

   bytesUsed += length;
   ptr        = (void *) malloc( (unsigned)length );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM VOI_ALLOC Name: %s Alloc: %p Add: %d Now: %d\n",
            caller, (void *)ptr, (int)length, bytesUsed );
      }
#endif

   ERRTEST( ptr );

   return( ptr );
}

/***/
/* Allocate a float matrix.
/***/

float **matAlloc( caller, xLength, yLength )

   char *caller;
   int  xLength, yLength;
{
    float *work, **matrix;
    float **mat2Ptr();

    bytesUsed += sizeof(float)*xLength*yLength + sizeof(float *)*yLength;

    if (!(work = fltAlloc( caller, yLength*xLength )))
       {
        return( (float **) NULL );
       }

    if (!(matrix = mat2Ptr( work, xLength, yLength )))
       {
        (void) deAlloc( caller, work, sizeof(float)* yLength*xLength );
        return( (float **) NULL );
       }

    return( matrix );
}

/***/
/* Allocate a double matrix.
/***/

double **matAllocD( caller, xLength, yLength )

   char *caller;
   int  xLength, yLength;
{
    double *work, **matrix;
    double **mat2PtrD();

    bytesUsed += sizeof(float)*xLength*yLength + sizeof(float *)*yLength;

    if (!(work = dFltAlloc( caller, yLength*xLength )))
       {
        return( (double **) NULL );
       }

    if (!(matrix = mat2PtrD( work, xLength, yLength )))
       {
        (void) deAlloc( caller, work, sizeof(float)* yLength*xLength );
        return( (double **) NULL );
       }

    return( matrix );
}

/***/
/* Reallocate a type int array:
/***/

int *intReAlloc( caller, ptr, newLength, oldLength )

   int  newLength, oldLength;
   char *caller;
   int  *ptr;
{
   int *newPtr;

   if (!ptr) oldLength = 0;

   oldLength   *= sizeof(int);
   newLength   *= sizeof(int);

   bytesUsed   += newLength - oldLength;
   bytesReused += oldLength;

   if (ptr)
      newPtr = (int *) realloc( (char *)ptr, (unsigned)newLength );
   else
      newPtr = (int *) malloc( (unsigned)newLength );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr, 
            "\n_MEM INT_RE_AL Name: %s Free: %p Alloc: %p",
            caller, ptr, newPtr );

       FPR( stderr, 
            " Add: %d Delete: %d Now: %d\n",
            newLength, oldLength, bytesUsed );
      }
#endif

   ERRTEST( newPtr );

   return( newPtr );
}

/***/
/* Reallocate a type float array:
/***/

float *fltReAlloc( caller, ptr, newLength, oldLength )

   int    newLength, oldLength;
   char   *caller;
   float  *ptr;
{
   float *newPtr;

   if (!ptr) oldLength = 0;

   oldLength   *= sizeof(float);
   newLength   *= sizeof(float);

   bytesUsed   += newLength - oldLength;
   bytesReused += oldLength;

   if (ptr)
      newPtr = (float *) realloc( (char *) ptr, (unsigned)newLength );
   else
      newPtr = (float *) malloc( (unsigned)newLength );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM FLT_RE_AL Name: %s Free: %p Alloc: %p",
            caller, ptr, newPtr );

       FPR( stderr,
            " Add: %d Delete: %d Now: %d\n",
            newLength, oldLength, bytesUsed );
      }
#endif

   ERRTEST( newPtr );

   return( newPtr );
}

/***/
/* Reallocate a type char array:
/***/

char *charReMalloc( caller, ptr, newLength, oldLength )

   SIZE_T newLength, oldLength;
   char   *caller;
   char   *ptr;
{
   char *newPtr;

   if (!ptr) oldLength = 0;

   bytesUsed   += newLength - oldLength;
   bytesReused += oldLength;

   if (ptr)
      newPtr = (char *) realloc( (char *)ptr, (unsigned)newLength );
   else
      newPtr = (char *) malloc( (unsigned)newLength );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM CHR_RE_AL Name: %s Free: %p Alloc: %p",
            caller, ptr, newPtr );

       FPR( stderr,
            " Add: %d Delete: %d Now: %d\n",
            newLength, oldLength, bytesUsed );
      }
#endif

   ERRTEST( newPtr );

   return( newPtr );
}

/***/
/* Reallocate a type void array:
/***/

void *voidReMalloc( caller, ptr, newLength, oldLength )

   SIZE_T newLength, oldLength;
   char   *caller;
   VOID   *ptr;
{
   void *newPtr;

   if (!ptr) oldLength = 0;

   bytesUsed   += newLength - oldLength;
   bytesReused += oldLength;

   if (ptr)
      newPtr = (void *) realloc( (VOID *)ptr, newLength );
   else
      newPtr = (void *) malloc( newLength );

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM CHR_RE_AL Name: %s Free: %p Alloc: %p",
            caller, ptr, newPtr );

       FPR( stderr,
            " Add: %d Delete: %d Now: %d\n",
            newLength, oldLength, bytesUsed );
      }
#endif

   ERRTEST( newPtr );

   return( newPtr );
}

/***/
/* freeStrList: deallocate argv-style Null-ptr terminated string list.
/***/

int freeStrList( s, n )

   char **s;
   int  n;
{
    int i;

    if (!s || n < 1) return( 0 );
    if (!*s) return( 0 );
 
    for( i = 0; i < n; i++ )
       {
        if (s[i])
           { 
            (void) strFree( s[i] );
            s[i] = (char *)NULL;
           }
       } 

    (void) deAlloc( "strDup", s, sizeof(char *)*(n+1) );

    return( 0 );
}

/***/
/* mat2Ptr: allocates 2D pointer list given an allocated matrix.
/***/

float **mat2Ptr( matrix, xSize, ySize )

   int   xSize, ySize;
   float *matrix;
{
    float **mList, **mPtr;

    mList = (float **) voidAlloc( "mat2ptr", sizeof(float *)*ySize );

    if (!mList)
       {
        return( mList );
       }

    mPtr = mList;

    while( ySize-- )
       {
        *mPtr++ = matrix;
        matrix += xSize;
       }
 
    return( mList ); 
}

/***/
/* mat2Ptr3D: allocates 3D pointer list given an allocated matrix.
/***/

float ***mat2Ptr3D( matrix, xSize, ySize, zSize )

   int   xSize, ySize, zSize;
   float *matrix;
{
    int   iy, iz;
    float ***mList;

    mList = (float ***) voidAlloc( "mat2ptr", sizeof(float **)*zSize );

    if (!mList) return( mList );

    for( iz = 0; iz < zSize; iz++ )
       {
        mList[iz] = (float **) voidAlloc( "mat2ptr", sizeof(float *)*ySize );

        if (!mList[iz])
           {
            (void) deAlloc( "mat2ptr", mList, sizeof(float **)*zSize );
            return( (float ***)NULL );
           }

        for( iy = 0; iy < ySize; iy++ )
           {
            mList[iz][iy] = matrix;
            matrix        += xSize;
           }
       }

    return( mList );
}

/***/
/* mat2Ptr4D: allocates 4D pointer list given an allocated matrix.
/***/

float ****mat2Ptr4D( matrix, xSize, ySize, zSize, aSize )

   int   xSize, ySize, zSize, aSize;
   float *matrix;
{
    int   iy, iz, ia;
    float ****mList;

    mList = (float ****) voidAlloc( "mat2ptr", sizeof(float ***)*aSize );

    if (!mList) return( mList );

    for( ia = 0; ia < aSize; ia++ )
       {
        mList[ia] = (float ***) voidAlloc( "mat2ptr", sizeof(float **)*zSize );

        if (!mList[ia])
           {
            return( (float ****)NULL );
           }

        for( iz = 0; iz < zSize; iz++ )
           {
            mList[ia][iz] = (float **) 
               voidAlloc( "mat2ptr", sizeof(float *)*ySize );

            if (!mList[ia][iz])
               {
                return( (float ****)NULL );
               }

            for( iy = 0; iy < ySize; iy++ )
               {
                mList[ia][iz][iy]  = matrix;
                matrix            += xSize;
               }
           }
       }

    return( mList );
}

/***/
/* mat2PtrZ: generate pointer list for a sub-matrix.
/***/

float **mat2PtrZ( matrix, xSize, xOffset, yStart, yEnd )

   int   xSize, xOffset, yStart, yEnd; 
   float *matrix;
{
    float **mList, **mPtr;
    int   ySize;

    ySize = 1 + yEnd - yStart;

    mList = (float **) voidAlloc( "mat2ptr", sizeof(float *)*ySize );

    if (!mList)
       {
        return( mList );
       }

    matrix += (yStart - 1)*xSize + xOffset - 1;
    mPtr    = mList;

    while( ySize-- )
       {
        *mPtr++ = matrix;
        matrix += xSize;
       }

    return( mList );
}

/***/
/* mat2ptrD: alocates and returns a 2D pointer array describing
/*           an already-allocated 2D double precision matrix.
/***/

double **mat2PtrD( matrix, xSize, ySize )

   int   xSize, ySize;
   double *matrix;
{
    double **mList, **mPtr;

    mList = (double **) voidAlloc( "mat2ptr", sizeof(double *)*ySize );

    if (!mList)
       {
        return( mList );
       }

    mPtr = mList;

    while( ySize-- )
       {
        *mPtr++ = matrix;
        matrix += xSize;
       }

    return( mList );
}

/***/
/* matFree: deallocates a 2D matrix and associated pointers.
/***/

int matFree( matrixPtr, xSize, ySize )

   int   xSize, ySize;
   float **matrixPtr;
{

    if (matrixPtr)
       {
        (void) deAlloc( "matFree", *matrixPtr, sizeof(float)*xSize*ySize );
        (void) deAlloc( "matFree", matrixPtr, sizeof(float *)*ySize );
       }

    return( 0 );
}

/***/
/* matPtrFree3D: deallocates pointers associated with a 3D matrix.
/***/

int matPtrFree3D( matrixPtr, xSize, ySize, zSize )

   int   xSize, ySize, zSize;
   float ***matrixPtr;
{
    int iz;

    if (!matrixPtr) return( 0 );

    for( iz = 0; iz < zSize; iz++ )
       {
        (void) deAlloc( "mat2ptr", matrixPtr[iz], sizeof(float *)*ySize );
       }

    (void) deAlloc( "mat2ptr", matrixPtr, sizeof(float **)*zSize );
 
    return( 0 );
}

/***/
/* matPtrFree4D: deallocates pointers associated with a 4D matrix.
/***/

int matPtrFree4D( matrixPtr, xSize, ySize, zSize, aSize )

   int   xSize, ySize, zSize, aSize;
   float ****matrixPtr;
{
    int iz, ia;

    if (!matrixPtr) return( 0 );

    for( ia = 0; ia < aSize; ia++ )
       {
        if (!matrixPtr[ia]) break;

        for( iz = 0; iz < zSize; iz++ )
           {
            if (!matrixPtr[ia][iz]) break;

            (void) deAlloc( "mat2ptr",
                            matrixPtr[ia][iz],
                            sizeof(float *)*ySize );
           }

        (void) deAlloc( "mat2ptr", matrixPtr[ia], sizeof(float **)*zSize );
       }

    (void) deAlloc( "mat2ptr", matrixPtr, sizeof(float ***)*aSize );

    return( 0 );
}

/***/
/* matFreeD: deallocates a 2D double prec matrix and associated pointers.
/***/

int matFreeD( matrixPtr, xSize, ySize )

   int    xSize, ySize;
   double **matrixPtr;
{

    if (matrixPtr)
       {
        (void) deAlloc( "matFree", *matrixPtr, sizeof(double)*xSize*ySize );
        (void) deAlloc( "matFree", matrixPtr, sizeof(double *)*ySize );
       }

    return( 0 );
}

/***/
/* Deallocate memory:
/***/

void unAllocS( caller, ptr, length )

   SIZE_T  length;
   char    *caller;
   VOID    *ptr;
{
   if (!ptr || length < 1) return;
   (void) unAlloc( caller, ptr, length );
}

void unAlloc( caller, ptr, length )

   SIZE_T  length;
   char    *caller;
   VOID    *ptr;
{
   if (ptr) bytesUsed -= (int)length;

#ifndef LINT_CHECK
   if (verboseAlloc)
      {
       FPR( stderr,
            "\n_MEM %s Name: %s Free: %p Delete: %d Now: %d Reused: %d\n",
            ptr ? "DEALLOC" : "NOOP_AL", caller, ptr,
            (int)length, bytesUsed, bytesReused );
      }
#endif

   if (ptr) (void) free( ptr );
}

/***/
/* Bottom.
/***/
