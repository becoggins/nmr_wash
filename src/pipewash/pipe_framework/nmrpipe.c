/******************************************************************************/
/*                                                                            */
/*                   ---- NIH NMR Software System ----                        */
/*                        Copyright 1992 - 2010                               */
/*                             Frank Delaglio                                 */
/*                   NIH Laboratory of Chemical Physics                       */
/*                                                                            */
/*               This software is not for distribution without                */
/*                  the written permission of the author.                     */
/*                                                                            */
/******************************************************************************/

/***/
/* nmrPipe: process an NMR file as an I/O stream.
/***/

#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

#ifdef USE_IMSL_C
#include <imsl.h>
#endif

#include "cmndargs.h"
#include "version.h"
#include "inquire.h"
#include "memory.h"
#include "dataio.h"
#include "fdatap.h"
#include "nmrserver.h"
#include "token.h"
#include "prec.h"

#include "nmrpipe.h"
#include "nmrtime.h"

#define FPR    (void) fprintf
#define ARGC   dataInfo->argc
#define ARGV   dataInfo->argv
#define HDR    dataInfo->fdata
#define DI     dataInfo

static struct ProcFuncInfo *glbFuncInfoPtr = (struct ProcFuncInfo *) NULL;
static struct ProcDataInfo *glbDataInfoPtr = (struct ProcDataInfo *) NULL;

static int rMove(), rSet();
int    shutDownPipe();

int main( argc, argv, envp )

   int  argc;
   char **argv, **envp;
{
    char   pathName[NAMELEN+1], portName[NAMELEN+1], serverListName[NAMELEN+1];
    float  origFdata[FDATASIZE], fdata[FDATASIZE];
    int    checkFlag, ttyOk, error;

    struct ProcFuncInfo funcInfo;
    struct ProcDataInfo dataInfo;

/***/
/* Debug mode setup:
/*  Establish global environment for interrupts.
/*  Initialize argument checking according to environment variables.
/*  Verbose memory allocation ON or OFF.
/***/

#ifdef USE_NMR_PLUGIN
    if (nmrPlugin( argc, argv, envp )) return( 1 );
#endif

    glbFuncInfoPtr = &funcInfo;
    glbDataInfoPtr = &dataInfo;

    error          = 0;
    dataInfo.debug = 0;

    (void) initNMRAux( argc, argv, &funcInfo, &dataInfo, shutDownPipe );
    (void) initArgs( argc, argv, CHECK_ENV );
    (void) intArgD( argc, argv, "-dbg", &dataInfo.debug );
    (void) setAlloc( dataInfo.debug > 1 ? 1 : 0 );
    (void) initDataIO();

    if (dataInfo.debug)
       {
        FPR( stderr, "NMRPipe Debug Level %d.\n", dataInfo.debug );
       }

/***/
/* Begin performance timing.
/***/

    (void) initNmrTime();

    TIMER_ON( totalTime );
 
/***/
/* Initialize signal handling.
/* Initialize Info pointers.
/* Extract processing code and parameters.
/* Read and interpret header.
/* Allocate slice memory.
/* Update and write header.
/* Process slices in stream.
/* Flush output buffer.
/* Update header to reflect scaling if possible:
/*
/* Nota Bene: procedures which fail can send an abort signal to
/*            processes further on in the UNIX pipe, in the form
/*            of a null header.
/*
/*            This will only work if the header has not yet been
/*            written to the output stream.
/***/

    if (error = initSignal( 0, argc, argv, "NMRPipe" ))
       {
        (void) fprintf( stderr, "NMRPipe Error initializing signals.\n" );
        (void) shutDownPipe( error );
       }

    if (error = initInfo( argc, argv, fdata, &funcInfo, &dataInfo ))
       {
        (void) fprintf( stderr, "NMRPipe Error initializing structures.\n" );
        (void) shutDownPipe( error );
       }

    error = getparams( argc, argv,
                       &funcInfo, &dataInfo,
                       pathName, portName, serverListName,
                       &checkFlag, &ttyOk );

    if (error)
       {
        if (!dataInfo.silentExit)
           {
            (void) fprintf( stderr, "NMRPipe Error extracting arguments.\n" );
            (void) sendNullHdr( &dataInfo );
           }

        (void) shutDownPipe( error );
       }

    if (PASTDATE())
       {
        FPR( stderr, "NMRPipe Expiration Error, Aborting.\n" );
        return( 1 );
       }

    if (error = initIO( &dataInfo, pathName, portName, serverListName, ttyOk ))
       {
        (void) fprintf( stderr, "NMRPipe Error opening NMR streams.\n" );
        (void) sendNullHdr( &dataInfo );
        (void) shutDownPipe( error );
       }

    if (error = getHeader( origFdata, fdata, &dataInfo, checkFlag ))
       {
        if (!dataInfo.silentExit)
           {
            (void) fprintf( stderr, "NMRPipe Error while reading header.\n" );
           }

        (void) sendNullHdr( &dataInfo );
        (void) shutDownPipe( error );
       }

    if (error = updateHeader( &funcInfo, &dataInfo ))
       {
        (void) fprintf( stderr, "NMRPipe Error in function initialization.\n" );
        (void) sendNullHdr( &dataInfo );
        (void) shutDownPipe( error );
       }

    if (error = allocSlice( &dataInfo ))
       {
        (void) fprintf( stderr, "NMRPipe Error allocating slice memory.\n" );
        (void) sendNullHdr( &dataInfo );
        (void) shutDownPipe( error );
       }

    if (error = putHeader( origFdata, fdata, &dataInfo, checkFlag ))
       {
        (void) fprintf( stderr, "NMRPipe Error writing header.\n" );
        (void) sendNullHdr( &dataInfo );
        (void) shutDownPipe( error );
       }

    if (error = procStream( &dataInfo, &funcInfo ))
       {
        (void) fprintf( stderr, "NMRPipe Error processing stream.\n" );
        (void) shutDownPipe( error ); 
       }

    if (error = flushSlice( &dataInfo ))
       {
        (void) fprintf( stderr, "NMRPipe Error flushing output.\n" );
        (void) shutDownPipe( error );
       }

    if (error = updateScale( &dataInfo ))
       {
        (void) fprintf( stderr, "NMRPipe Error updating max/min\n" );
        (void) shutDownPipe( error );
       }

    (void) shutDownPipe( error );

    return( 0 );
}

/***/
/* shutDownPipe: NMRPipe Exit point.
/***/

int shutDownPipe( error )

    int error;
{

/***/
/* Report error status if needed.
/* If an error occurred, close and kill server connections with no waiting.
/* If no error occurred, wait to close client connections.
/* Free memory.
/* End performance timing.
/* Display performance timing.
/***/

    if (error && !glbDataInfoPtr->silentExit)
       {
        (void) fprintf( stderr, "NMRPipe Error Status: %d\n", error );
        (void) fprintf( stderr, "NMRPipe Function %s\n", glbFuncInfoPtr->name );
       }
 
    if (error)
       {
        (void) nmrCloseServers( glbDataInfoPtr, 0 );
        (void) nmrKillServers( glbDataInfoPtr );
       }
    else
       {
        (void) nmrCloseServers( glbDataInfoPtr, 1 );
       }

    (void) freeSlice( glbFuncInfoPtr, glbDataInfoPtr );

    TIMER_OFF( totalTime );

    (void) showNmrTime( glbFuncInfoPtr, glbDataInfoPtr );

    if (glbDataInfoPtr->debug)
       {
        (void) fprintf( stderr, "NMRPipe Exiting, Status: %d\n", error );
       }

    if (glbDataInfoPtr->finalDelay) 
       {
        USLEEP( glbDataInfoPtr->finalDelay );
       }

    exit( error );
}

/***/
/* initIO: open input and output files if needed.
/***/

int initIO( dataInfo, pathName, portName, serverListName, ttyOk )

   struct ProcDataInfo *dataInfo;
   char   *pathName, *portName, *serverListName;
   int    ttyOk;
{
    char c[4], hName[NAMELEN+1];
    int  error;

    error = 0;

/***/
/* I/O Mode must be via file descriptors and system calls:
/***/

#ifndef FB_SYS_IO
    if (dataInfo->serverMode != SMODE_NONE)
       {
        (void) fprintf( stderr, "NMRPipe Compile-Time Error: Bad I/O Mode.\n" );
        return( 1 );
       }
#endif

/***/
/* Suppress I/O retries for explicit input files (as opposed to pipes/sockets):
/***/

   if (dataInfo->inName)
       {
        dataInfo->maxTries = 1;
        dataInfo->timeOut  = 0;
       }

/***/
/* If input is from socket:
/*   Use the input socket ID as the input unit.
/*   Transmit the 'w' character to identify a writable connection.
/*   Transmit the 'b' character to identify both read/write connection.
/* 
/* If server mode, open the socket to the client.
/*
/* If the input file is explicitly named, open it.
/*
/* Otherwise, input comes from stdin; check it if needed.
/***/

    if (dataInfo->serverMode == SMODE_SOCKIO && dataInfo->inSock != -1)
       {
        if (error = gethostname( hName, NAMELEN ))
           {
            (void) fprintf( stderr, "NMRPipe Error Getting Host Name.\n" );
            return( error );
           }

        error = nmrConnect( hName, dataInfo->inSock, &dataInfo->inUnit );

        if (error)
           {
            (void) fprintf( stderr, "NMRPipe Error Opening InSocket.\n" );
            return( error );
           }

        if (dataInfo->inSock == dataInfo->outSock)
           c[0] = 'b';
        else
           c[0] = 'w';

        error = 
         dataSendCB( dataInfo->inUnit, c, sizeof(char), -1, dataInfo->timeOut );

        if (error)
           {
            (void) fprintf( stderr, "NMRPipe Error Identifying InSocket.\n" );
            return( error );
           }
       }
    else if (dataInfo->serverMode == SMODE_SERVER)
       {
        error = nmrAcceptClient( portName,
                                 &dataInfo->serverSock,
                                 &dataInfo->serverMsgSock,
                                 dataInfo->verbose );

       if (error)
          {
           (void) fprintf( stderr, "NMRPipe Error Accepting Client.\n" );
           return( error );
          }

        dataInfo->inUnit = dataInfo->serverMsgSock;
       }
    else if (dataInfo->inName)
       {
        error = dataOpen( dataInfo->inName, &dataInfo->inUnit, FB_READ );

        if (error)
           {
            (void) fprintf( stderr,
                            "NMRPipe Error opening input file %s\n",
                            dataInfo->inName );

            return( error );
           }
       }
    else
       {
        if (!ttyOk && isTTY( dataInfo->inUnit ))
           {
            (void) fprintf( stderr, "NMRPipe Error: input from a terminal:\n" );
            (void) fprintf( stderr, " Check for missing input argument.\n" );
            (void) fprintf( stderr, " Check for trailing  spaces  after\n" );
            (void) fprintf( stderr, " any \"\\\" line-continuations.\n" );
            return( 1 );
           }
       }
    
/***/
/* If output is to socket:
/*  Use the output socket ID as the output unit.
/*  Transmit a 'r' to indicate the connection is readable.
/*
/* If server mode, open the socket to the client.
/* In server mode, output goes to server message socket.
/*
/* If the output file is explicit (not stdout) post-process I/O is okay;
/* Therefore, its okay to update scale after processing (scaleFlag).
/* Also, if the output file is explicit, test for overwrite.
/*
/* If the output file is not explicit, test its type as needed.
/***/

    if (dataInfo->serverMode == SMODE_SOCKIO && dataInfo->outSock != -1)
       {
        if (error = gethostname( hName, NAMELEN ))
           {
            (void) fprintf( stderr, "NMRPipe Error Getting Host Name.\n" );
            return( error );
           }

        if (dataInfo->inSock == dataInfo->outSock)
           {
            dataInfo->outUnit = dataInfo->inUnit;
           }
        else
           {
            error = nmrConnect( hName, dataInfo->outSock, &dataInfo->outUnit );

            if (error)
               {
                (void) fprintf( stderr, "NMRPipe Error Opening OutSocket.\n" );
                return( error );
               }
           }

        c[0] = 'r';

        error = 
         dataSendCB( dataInfo->outUnit, c, 
                     sizeof(char), -1, 
                     dataInfo->timeOut );

        if (error)
           {
            (void) fprintf( stderr, "NMRPipe Error Identifying OutSocket.\n" );
            return( error );
           }
       }
    else if (dataInfo->serverMode == SMODE_SERVER)
       {
        dataInfo->outUnit = dataInfo->serverMsgSock;
       }
    else if (dataInfo->outName)
       {
        dataInfo->scaleFlag = 1;

        if (!dataInfo->ovFlag && fileExists( dataInfo->outName ))
           {
            (void) fprintf( stderr,
                            "NMRPipe Error: output file %s exists.\n",
                            dataInfo->outName );

            return( 1 );
           }

        if (dataInfo->inPlace)
           {
            error = dataOpen( dataInfo->outName,
                              &dataInfo->outUnit,
                              FB_UNKNOWN );
           }
        else
           {
            error = dataOpen( dataInfo->outName,
                              &dataInfo->outUnit,
                              FB_CREATE );
           }

        if (error)
           {
            (void) fprintf( stderr,
                            "NMRPipe Error opening output file %s\n",
                            dataInfo->outName );

            return( error );
           }
       }
   else
       {
        if (!ttyOk && isTTY( dataInfo->outUnit ))
           {
            (void) fprintf( stderr, "NMRPipe Error: output to a terminal:\n" );
            (void) fprintf( stderr, " Check for missing output argument.\n" );
            (void) fprintf( stderr, " Check for trailing  spaces  after\n" );
            (void) fprintf( stderr, " any \"\\\" line-continuations.\n" );
            return( 1 );
           }
       }

/***/
/* If this is a client process, start server processes:
/***/

    if (dataInfo->serverMode == SMODE_CLIENT)
       {
        error = nmrInitServers( dataInfo, serverListName, pathName );

        if (error)
           {
            (void) fprintf( stderr, "NMRPipe Error starting servers.\n" );
            return( 1 );
           }
       }

    return( 0 );
}

/***/
/* getHeader: reads FDATA header at the begining of NMR stream.
/***/

int getHeader( origFdata, fdata, dataInfo, checkFlag )

   float  origFdata[FDATASIZE], fdata[FDATASIZE];
   struct ProcDataInfo *dataInfo;
   int    checkFlag;
{
    int  swapDone, error;
    FILE *pipeUnit;
    UNIT hdrUnit;

/***/
/* Initialize:
/***/

    error    = 0;
    swapDone = 0;
    pipeUnit = (FILE *)NULL;
    hdrUnit  = (UNIT)NULL;

/***/
/* If an Input Header Source is Specified:
/*   If its a pipe (!) open the pipe, read the header, close the pipe.
/*   If its a file, open the read, read the header, close the file.
/*
/* Otherwise, read the header from the established input stream.
/***/

    if (dataInfo->inHdrName)
       {
        if (dataInfo->inHdrName[0] == '!') 
           {
            if (!(pipeUnit = popen( &dataInfo->inHdrName[1], "r" )))
               {
                FPR( stderr,
                     "NMRPipe Error opening input header pipe %s\n",
                     dataInfo->inHdrName );

                return( error );
               }

            hdrUnit = fileno( pipeUnit );

            error = readHdrB( hdrUnit, 
                              fdata, 
                              dataInfo->maxTries, 
                              dataInfo->timeOut,
                              &swapDone );

            if (error)
               {
                FPR( stderr,
                     "NMRPipe Error reading input header pipe %s\n",
                     dataInfo->inHdrName );

                return( error );
               }

            (void) pclose( pipeUnit );
           }
        else
           {
            error = dataOpen( dataInfo->inHdrName, &hdrUnit, FB_READ );

            if (error)
               {
                FPR( stderr,
                     "NMRPipe Error opening input header file %s\n",
                     dataInfo->inHdrName );

                return( error );
               }

            error = readHdr( hdrUnit, fdata, &swapDone );

            if (error)
               {
                FPR( stderr,
                     "NMRPipe Error reading input header file %s\n",
                     dataInfo->inHdrName );

                return( error );
               }

            (void) dataClose( hdrUnit );
           }
       }
    else
       {
        error = readHdrB( dataInfo->inUnit,
                          fdata, 
                          dataInfo->maxTries, 
                          dataInfo->timeOut,
                          &swapDone ); 

        if (error) return( error );
       }

/***/
/* Perform byte swap and header fix-up as needed.
/* If possible, check consistancy of indicated and actual input size.
/***/

    if (dataInfo->inSwapFlag) (void) swapHdr( fdata );
       
    if (error = fixfdata( fdata ))
       {
        dataInfo->silentExit = 1;
        return( error );
       }

    (void) rMove( fdata, origFdata, FDATASIZE );

    if (dataInfo->inName && checkFlag)
       {
        switch( testNMRin( dataInfo->inName, fdata, 0, 1 ))
           {
            case -1:
               (void) fprintf( stderr, "\n" );
               (void) fprintf( stderr, "NMRPipe Warning:" );
               (void) fprintf( stderr, " can't check size of file" );
               (void) fprintf( stderr, " %s\n\n", dataInfo->inName );
               break;
            case  1:
               (void) fprintf( stderr, "\n" );
               (void) fprintf( stderr, "NMRPipe Error:" );
               (void) fprintf( stderr, " header/size mismatch for file" ); 
               (void) fprintf( stderr, " %s\n\n", dataInfo->inName );
               return( 1 );
               break;
            default:
               break;
           }    
       }

    (void) initNMRSizes( fdata, dataInfo );

    return( 0 );
}

/***/
/* procStream: process all slices in data stream.
/***/

int procStream( dataInfo, funcInfo )

   struct ProcDataInfo *dataInfo;
   struct ProcFuncInfo *funcInfo;
{
    int slice, inLen, outLen, error;

/***/
/* If processing should and can be performed by servers, do that instead.
/* If this is a server, set I/O buffers for returning data to client. 
/***/

    if (dataInfo->serverMode == SMODE_CLIENT && dataInfo->vecFlag)
       {
        error = procDistribute( dataInfo, funcInfo );
        return( error );
       }
    else if (dataInfo->serverMode == SMODE_SERVER)
       {
        inLen  = dataInfo->inCapacity*dataInfo->size*dataInfo->quadState;
        outLen = dataInfo->outCapacity*dataInfo->outSize*dataInfo->outQuadState;

        (void) setServerIO( dataInfo->serverMsgSock, outLen, inLen );
       }

/***/
/* While there is a next slice to process:
/*   Display status of process as needed.
/*   Read the slice from the stream.
/*   Process and write the slice.
/***/

    slice = 0;

    while( nextSlice( &slice, dataInfo ))
       {
        if (error = showSlice( funcInfo, dataInfo ))
           {
            (void) fprintf( stderr, "NMRPipe Display Error, row %d.\n", slice );
            return( error );
           }

        if (error = getSlice( dataInfo ))
           {
            (void) fprintf( stderr, "NMRPipe Read Error, row %d.\n", slice );
            return( error );
           }

        if (error = procSlice( funcInfo, dataInfo ))
           {
            (void) fprintf( stderr, "NMRPipe Process Error, row %d.\n", slice );
            return( error ); 
           }
       }

    return( 0 );
}

/***/
/* nextSlice: determine if there is a next slice to process;
/*            Also increments slice count and slice locations.
/***/

int nextSlice( currentSlice, dataInfo )

   struct ProcDataInfo *dataInfo;
   int    *currentSlice;
{
    int  error;
    char c[4];

/***/
/* If processing variable slice-count data:
/*  Read the status data: '1' means more slices are left.
/*  Transmit the status data if needed.
/*  
/* Otherwise: 
/*  Test if current slice count is smaller that total slices.
/*  Update slice locations.
/***/

    dataInfo->sliceCode = ++(*currentSlice);

    if (dataInfo->varCountFlag)
       {
        error = dataReadCB( dataInfo->inUnit,
                            c, (int)sizeof(char),
                            -1, dataInfo->timeOut );

        if (error)
           {
            (void) fprintf( stderr, "NMRPipe Error Testing I/O Status.\n" );
            return( error );
           }

        if (!dataInfo->outName)
           {
            error = dataWriteCB( dataInfo->outUnit,
                                 c, (int)sizeof(char),
                                 -1, dataInfo->timeOut );

            if (error)
               {
                (void) fprintf( stderr, "NMRPipe Error Sending I/O Status.\n" );
                return( error );
               }
           }

        if (c[0] == '1')
           {
            dataInfo->locList[YLOC] = 1;
            return( 1 );
           }
       }
    else
       {
        if (dataInfo->locList[YLOC] == dataInfo->specnum)
           {
            dataInfo->locList[YLOC] = 1;
            dataInfo->locList[ZLOC]++;
           }
        else
           {
            dataInfo->locList[YLOC]++;
           }

        if (dataInfo->dimCount == 4)
           {
            if (dataInfo->locList[ZLOC] > dataInfo->lastPlane)
               {
                dataInfo->locList[ZLOC] = dataInfo->firstPlane;
                dataInfo->locList[ALOC]++; 
               }

/*
            if (dataInfo->locList[ZLOC] > dataInfo->zSize)
               {
                dataInfo->locList[ZLOC] = 1;
                dataInfo->locList[ALOC]++;
               }
*/
           }
 
        if (*currentSlice <= dataInfo->sliceCount)
           {
            return( 1 );
           }
       }

    return( 0 );
}
   
/***/
/* procDistribute: distributed processing of all slices in data stream.
/***/

int procDistribute( dataInfo, funcInfo )

   struct ProcDataInfo *dataInfo;
   struct ProcFuncInfo *funcInfo;
{
    int i, j, count, outLen, inLen, error;

/***/
/* Prepare buffer size for transmition of data to servers:
/***/

    outLen = dataInfo->inCapacity*dataInfo->size*dataInfo->quadState;
    inLen  = dataInfo->outCapacity*dataInfo->outSize*dataInfo->outQuadState;

    for( i = 0; i < dataInfo->serverCount; i++ )
       {
        (void) setServerIO( dataInfo->server[i].sock, outLen, inLen );
       }

/***/
/* While slices are left to read and process:
/*    For each server:
/*       Find number of slices to send to this server.
/*       Read a group of slices from the input stream.
/*       Write the group of slices to the server.
/*
/*    {Servers process their data.}
/*
/*    For each server:
/*       Retrieve the group of processed slices from the server.
/*       Write the group of processed slices to the output stream.
/*       Update slices left to process on this server.
/***/

    dataInfo->sliceCode = 1;

    while( dataInfo->slicesLeft )
       {
        for( i = 0; i < dataInfo->serverCount; i++ )
           { 
            if (dataInfo->server[i].partition > dataInfo->server[i].capacity)
               count = dataInfo->server[i].capacity;
            else
               count = dataInfo->server[i].partition;

            for( j = 0; j < count; j++ )
               {
                if (error = showSlice( funcInfo, dataInfo ))
                   {
                   (void) fprintf( stderr, "NMRPipe Display Error.\n" );
                   return( error );
                  }

                if (error = getSlice( dataInfo ))
                   {
                    (void) fprintf( stderr, "NMRPipe Read Error.\n" );
                    return( error );
                   }

                if (error = sendSlice( dataInfo, i ))
                   {
                    (void) fprintf( stderr, "NMRPipe Send Error.\n" );
                    return( error );
                   }

                dataInfo->sliceCode++;
               }
           }

        for( i = 0; i < dataInfo->serverCount; i++ )
           {
            if (dataInfo->server[i].partition > dataInfo->server[i].capacity)
               count = dataInfo->server[i].capacity;
            else
               count = dataInfo->server[i].partition;

            for( j = 0; j < count; j++ )
               {
                if (error = receiveSlice( dataInfo, i ))
                   {
                    (void) fprintf( stderr, "NMRPipe Receive Error.\n" );
                    return( error );
                   }

                if (error = putSlice( dataInfo ))
                   {
                    (void) fprintf( stderr, "NMRPipe Write Error.\n" );
                    return( error );
                   }
               }

            dataInfo->server[i].partition -= count;
           }
       }

    return( 0 );
}

/***/
/* updateScale: write a new post-process header with max/min if possible.
/***/

int updateScale( dataInfo )

   struct ProcDataInfo *dataInfo;
{
    int   error;
    float tmpFDATA[FDATASIZE];

/***/
/* Suppress if:
/*   Output is a stream, and can't be positioned.
/*   Scaling values are no good anyway.
/*   Header destination has been explicitly specified (it may be a pipe).
/***/

    if (!dataInfo->outName)   return( 0 );
    if (!dataInfo->scaleFlag) return( 0 );
    if (dataInfo->outHdrName) return( 0 );

    (void) setParm( dataInfo->fdata, FDSCALEFLAG, (float)1.0,           0 );
    (void) setParm( dataInfo->fdata, FDDISPMAX,   (float)dataInfo->max, 0 );
    (void) setParm( dataInfo->fdata, FDDISPMIN,   (float)dataInfo->min, 0 );
    (void) setParm( dataInfo->fdata, FDMAX,       (float)dataInfo->max, 0 );
    (void) setParm( dataInfo->fdata, FDMIN,       (float)dataInfo->min, 0 );

    if (error = dataPos( dataInfo->outUnit, 0 )) return( error );

    (void) rMove( dataInfo->fdata, tmpFDATA, FDATASIZE );

    if (dataInfo->outSwapFlag) (void) swapHdr( tmpFDATA );

    error = dataWrite( dataInfo->outUnit, tmpFDATA, sizeof(float)*FDATASIZE );

    return( error );
}

/***/
/* putHeader: writes FDATA header at the begining of NMR stream.
/***/

int putHeader( origFdata, fdata, dataInfo, checkFlag )

   struct ProcDataInfo *dataInfo;
   float  origFdata[FDATASIZE], fdata[FDATASIZE];
   int    checkFlag;
{
    int   i, length, error;
    float tmpFDATA[FDATASIZE];

    FILE  *pipeUnit;
    UNIT  hdrUnit;

/***/
/* Initialize:
/***/

    error    = 0;
    pipeUnit = (FILE *)NULL;
    hdrUnit  = (UNIT) NULL;

/***/
/* If possible, check consistancy of predicted and available output size:
/***/

    if (dataInfo->outName && checkFlag)
       {
        switch( testNMRout( dataInfo->outName, fdata, 0, 1 ))
           {
            case -1:
               (void) fprintf( stderr, "\n" );
               (void) fprintf( stderr, "NMRPipe Warning:" );
               (void) fprintf( stderr, " can't check size of file" );
               (void) fprintf( stderr, " %s\n\n", dataInfo->outName );
               break;
            case  1:
               (void) fprintf( stderr, "\n" );
               (void) fprintf( stderr, "NMRPipe Error:" );
               (void) fprintf( stderr, " insufficient space for file" );
               (void) fprintf( stderr, " %s\n\n", dataInfo->outName );
               return( 1 );
               break;
            default:
               break;
           }
       }

/***/
/* In client Mode:
/*   Write original header, with adjusted partition, to all servers.
/***/

    length = sizeof(float)*FDATASIZE;

    if (dataInfo->serverMode == SMODE_CLIENT)
       {
        for( i = 0; i < dataInfo->serverCount; i++ )
           {
            (void) setParm( origFdata,
                            FDPARTITION,  
                            (float)dataInfo->server[i].partition,
                            0 );

            error = dataSendB( dataInfo->server[i].sock,
                               origFdata, length,
                               dataInfo->serverMaxTries,
                               dataInfo->serverTimeOut );

            if (error)
               {
                (void) fprintf( stderr,
                                "NMRPipe Client Error writing header to %s.\n",
                                dataInfo->server[i].name );

                return( error );
               }
           }
       }

    (void) setParm( origFdata, FDPARTITION, (float)0, 0 );

/***/
/* Writing Header to final output stream:
/*  Suppress writing of header if destination is 'none'.
/*  Send header to pipeline if destination is !cmnd.
/*  Send header to existing output stream if destination is null.
/***/

    if (dataInfo->serverMode != SMODE_SERVER)
       {
        (void) rMove( fdata, tmpFDATA, FDATASIZE );

        if (dataInfo->outSwapFlag) (void) swapHdr( tmpFDATA );

        if (dataInfo->outHdrName)
           {
            if (!strcasecmp( dataInfo->outHdrName, "none" ))
               {
                error = 0;
               }
            else if (dataInfo->outHdrName[0] == '!')
               {
                if (!(pipeUnit = popen( &dataInfo->outHdrName[1], "w" )))
                   {
                    FPR( stderr,
                         "NMRPipe Error opening output header pipe %s\n",
                         dataInfo->outHdrName );
    
                    return( error );
                   }

                hdrUnit = fileno( pipeUnit );

                if (error = dataWrite( hdrUnit, tmpFDATA, length ))
                   {
                    FPR( stderr,
                         "NMRPipe Error writing output header pipe %s\n",
                         dataInfo->outHdrName );
    
                    return( error );
                   }
    
                (void) pclose( pipeUnit );
               }
            else
               {
                error = dataOpen( dataInfo->outHdrName, &hdrUnit, FB_UNKNOWN );

                if (error)
                   {
                    FPR( stderr,
                         "NMRPipe Error opening output header file %s\n",
                         dataInfo->outHdrName );

                    return( error );
                   }

                if (error = dataWrite( hdrUnit, tmpFDATA, length ))
                   {
                    FPR( stderr,
                         "NMRPipe Error writing output header file %s\n",
                         dataInfo->outHdrName );

                    return( error );
                   }

                (void) dataClose( hdrUnit );
               }
           }
        else
           {
            if (error = dataWrite( dataInfo->outUnit, tmpFDATA, length ))
               {
                return( error );
               }
           }
       }

    return( 0 );
}

/***/
/* sendNullHdr: writes null header to indicate error.
/***/

int sendNullHdr( dataInfo )

   struct ProcDataInfo *dataInfo;
{
    int i, error; 

    for( i = 0; i < FDATASIZE; i++ ) dataInfo->fdata[i] = 0.0;

/***/
/* In client Mode:
/*   Write a null  header to all servers.
/*
/* Write a null header to the output stream.
/***/

    if (dataInfo->serverMode == SMODE_CLIENT)
       {
        if (!dataInfo->silentExit)
           {
            (void) fprintf( stderr, "NMRPipe Aborting with null header.\n" );
           }

        for( i = 0; i < dataInfo->serverCount; i++ )
           {
            error = dataWrite( dataInfo->server[i].sock,
                              dataInfo->fdata,
                              sizeof(float)*FDATASIZE );

            if (error)
               {
                (void) fprintf( stderr,
                                "NMRPipe Client Error writing header to %s.\n",
                                dataInfo->server[i].name );

                return( error );
               }
           }
       }

    if (!dataInfo->outName && dataInfo->serverMode != SMODE_SERVER)
       {
        if (!dataInfo->silentExit)
           {
            (void) fprintf( stderr, "NMRPipe Aborting with null header.\n" );
           }

        error = dataWrite( dataInfo->outUnit,
                           dataInfo->fdata,
                           sizeof(float)*FDATASIZE );

        if (error)
           {
            return( error );
           }
       }

    return( 0 );
}

/***/
/* getSlice: read the next slice from the NMR stream; returns zeros
/*           in idata if input is not complex.
/***/

int getSlice( dataInfo )

   struct ProcDataInfo *dataInfo;
{
    int    count, length, error;

/***/
/* Null size or no read, nothing to get:
/***/

    if (dataInfo->size < 1 || !dataInfo->rdFlag) return( 0 );

/***/
/* If slice input is buffered:
/*  If no data is left in buffer: 
/*    Read a group of slices.
/*    Reset buff status.
/*
/*  Move the next real slice.
/*  Move or fill the next imaginary slice.
/*  Increment the buffer status.
/*
/* Else if slice input is not buffered:
/*   Read real data.
/*   Read or fill imaginary data.
/***/

    if (dataInfo->inCapacity > 0)
       {
        if (!dataInfo->buffCountIn)
           {
            if (dataInfo->inCapacity > dataInfo->slicesLeft)
               count = dataInfo->slicesLeft;
            else
               count = dataInfo->inCapacity;
 
            length = dataInfo->size*dataInfo->quadState*count;

            if (dataInfo->serverMode == SMODE_SERVER)
               {
                TIMER_ON( clientReadTime );

                if (error = dataRecvB( dataInfo->inUnit,
                                       dataInfo->inBuff,
                                       sizeof(float)*length,
                                       dataInfo->maxTries,
                                       dataInfo->timeOut )) return( error );

                TIMER_OFF( clientReadTime );
               }
            else
               {
                TIMER_ON( readTime );

                if (error = dataReadB( dataInfo->inUnit,
                                       dataInfo->inBuff,
                                       sizeof(float)*length,
                                       dataInfo->maxTries,
                                       dataInfo->timeOut )) return( error );

                TIMER_OFF( readTime );
               }

            dataInfo->buffCountIn  = dataInfo->inCapacity;
            dataInfo->inBuffPtr    = dataInfo->inBuff;
            dataInfo->slicesLeft  -= count;
           }

        if (dataInfo->quadState == 1)
           {
            (void) rMove( dataInfo->inBuffPtr,
                          dataInfo->rdata,
                          dataInfo->size );
 
            dataInfo->inBuffPtr += dataInfo->size;

            (void) rSet( dataInfo->idata, dataInfo->maxSize, (float)0.0 );
           }
        else
           {
            (void) rMove( dataInfo->inBuffPtr,
                          dataInfo->rdata,
                          dataInfo->size );

            dataInfo->inBuffPtr += dataInfo->size;

            (void) rMove( dataInfo->inBuffPtr,
                          dataInfo->idata,
                          dataInfo->size );

            dataInfo->inBuffPtr += dataInfo->size;
           }

        dataInfo->buffCountIn--;
       }
    else
       {
        TIMER_ON( readTime );

        if (dataInfo->quadState == 1)
           {
            if (error = dataReadB( dataInfo->inUnit,
                                   dataInfo->rdata,
                                   sizeof(float)*dataInfo->size,
                                   dataInfo->maxTries,
                                   dataInfo->timeOut )) return( error );

            (void) rSet( dataInfo->idata, dataInfo->maxSize, (float)0.0 );
           }
        else
           {
            if (error = dataReadB( dataInfo->inUnit,
                                   dataInfo->rdata,
                                   sizeof(float)*dataInfo->size,
                                   dataInfo->maxTries,
                                   dataInfo->timeOut )) return( error );

            if (error = dataReadB( dataInfo->inUnit,
                                   dataInfo->idata,
                                   sizeof(float)*dataInfo->size,
                                   dataInfo->maxTries,
                                   dataInfo->timeOut )) return( error );
           }

        TIMER_OFF( readTime );

        dataInfo->slicesLeft--;
       }
 
    return( 0 ); 
}

/***/
/* putSlice: called by processing functions to write slice data.
/***/

int putSlice( dataInfo )

   struct ProcDataInfo *dataInfo;
{
    int error;

/***/
/* If creating hypercomplex data:
/*  Slice must be written twice.
/* Otherwise:
/*  Write the slice.
/***/

    if (!dataInfo->wrFlag) return( 0 );

    if (dataInfo->typeFlags[ADFLAG])
       {
        if (error = putSlice2( dataInfo )) return( error );
        if (error = putSlice2( dataInfo )) return( error );
       }
    else
       {
        if (error = putSlice2( dataInfo )) return( error );
       }

    return( 0 );
}

/***/
/* putSlice2: write a slice to the NMR stream; update max/min if needed.
/***/

int putSlice2( dataInfo )

  struct ProcDataInfo *dataInfo;
{
    int    length, error;

/***/
/* Stop if there is no data to write.
/* Find display max and min if result is saved explicitly:
/***/

    if (dataInfo->outSize < 1) return( 0 );

    if (dataInfo->outName)
       {
        (void) minMax2( dataInfo->rdata,
                        dataInfo->outSize,
                        &dataInfo->min,
                        &dataInfo->max );
       }

/***/
/* If slice output is buffered:
/*   Move real data to buffer.
/*   Move imaginary data to buffer if needed.  
/*   If the buffer is full, flush it.
/*
/* Else if slice output is not buffered:
/*   Write real data.
/*   Write imaginary data if needed.
/***/

    if (dataInfo->outCapacity)
       {
        if (dataInfo->outQuadState == 1)
           {
            (void) rMove( dataInfo->rdata, 
                          dataInfo->outBuffPtr,
                          dataInfo->outSize );

            dataInfo->outBuffPtr += dataInfo->outSize;
           }
        else
           {
            (void) rMove( dataInfo->rdata, 
                          dataInfo->outBuffPtr,
                          dataInfo->outSize );

            dataInfo->outBuffPtr += dataInfo->outSize;

            (void) rMove( dataInfo->idata, 
                          dataInfo->outBuffPtr,
                          dataInfo->outSize );

            dataInfo->outBuffPtr += dataInfo->outSize;
           }

        dataInfo->buffCountOut++; 

        if (dataInfo->buffCountOut == dataInfo->outCapacity)
           {
            if (error = flushSlice( dataInfo )) return( error );
           }
       }
    else
       {
        TIMER_ON( writeTime );

        length = dataInfo->outSize;

        if (dataInfo->outQuadState == 1)
           {
            if (error = dataWrite( dataInfo->outUnit,
                                   dataInfo->rdata,
                                   sizeof(float)*length )) return( error );
           }
        else
           {
            if (error = dataWrite( dataInfo->outUnit,
                                   dataInfo->rdata,
                                   sizeof(float)*length )) return( error );

            if (error = dataWrite( dataInfo->outUnit,
                                   dataInfo->idata,
                                   sizeof(float)*length )) return( error );

           }

      TIMER_OFF( writeTime );
     }

  return( 0 );
}

/***/
/* sendSlice: transmit a slice to a server for processing.
/***/

int sendSlice( dataInfo, id )

   struct ProcDataInfo *dataInfo;
   int    id;
{
    int length, error;

    if (dataInfo->size < 1) return( 0 );

    TIMER_ON( serverWriteTime[id] );

    length = dataInfo->size;

    if (dataInfo->quadState == 1)
       {
        if (error = dataSendB( dataInfo->server[id].sock,
                               dataInfo->rdata,
                               sizeof(float)*length,
                               dataInfo->serverMaxTries,
                               dataInfo->serverTimeOut )) return( error );
       }
    else
       {
        if (error = dataSendB( dataInfo->server[id].sock,
                               dataInfo->rdata,
                               sizeof(float)*length,
                               dataInfo->serverMaxTries,
                               dataInfo->serverTimeOut )) return( error );

        if (error = dataSendB( dataInfo->server[id].sock,
                               dataInfo->idata,
                               sizeof(float)*length,
                               dataInfo->serverMaxTries,
                               dataInfo->serverTimeOut )) return( error );
       }

    TIMER_OFF( serverWriteTime[id] );

    return( 0 );
}

/***/
/* receiveSlice: accept a processed slice from a server.
/***/

int receiveSlice( dataInfo, id )

   struct ProcDataInfo *dataInfo;
   int    id;
{
    int length, error;

    if (dataInfo->outSize < 1) return( 0 );

    TIMER_ON( serverReadTime[id] );

    length = dataInfo->outSize;

    if (dataInfo->outQuadState == 1)
       {
        error = dataRecvB( dataInfo->server[id].sock,
                           dataInfo->rdata,
                           sizeof(float)*length,
                           dataInfo->serverMaxTries,
                           dataInfo->serverTimeOut  );

        if (error)
           {
            (void) perror( "NMRPipe Client Read Error" );
            return( error );
           }
       }
    else
       {
        error = dataRecvB( dataInfo->server[id].sock,
                           dataInfo->rdata,
                           sizeof(float)*length,
                           dataInfo->serverMaxTries,
                           dataInfo->serverTimeOut  );

        if (error)
           {
            (void) perror( "NMRPipe Client Read Error" );
            return( error );
           }

        error = dataRecvB( dataInfo->server[id].sock,
                           dataInfo->idata,
                           sizeof(float)*length,
                           dataInfo->serverMaxTries,
                           dataInfo->serverTimeOut  );

        if (error)
           {
            (void) perror( "NMRPipe Client Read Error" );
            return( error );
           }
       }

    TIMER_OFF( serverReadTime[id] );

    return( 0 );
}

/***/
/* flushSlice: write any buffered slices to the NMR stream.
/***/

int flushSlice( dataInfo )

  struct ProcDataInfo *dataInfo;
{
    int    pts, error;
    static int firstTime = 1;

    pts = dataInfo->outSize*dataInfo->outQuadState*dataInfo->buffCountOut;

/***/
/* If output is buffered:
/*   In server mode, send data back to client.
/*
/*   Otherwise:
/*     For the first in-place output, re-create the output file and header.
/*     Write the output buffer.
/***/

    if (dataInfo->outCapacity && dataInfo->buffCountOut)
       {
        if (dataInfo->serverMode == SMODE_SERVER)
           {
            TIMER_ON( clientWriteTime );

            if (error = dataSendB( dataInfo->outUnit,
                                   dataInfo->outBuff,
                                   sizeof(float)*pts,
                                   dataInfo->serverMaxTries,
                                   dataInfo->serverTimeOut )) return( error );

            TIMER_OFF( clientWriteTime );
           }
        else
           {
            if (firstTime && dataInfo->outName && dataInfo->inPlace)
               {
                (void) dataClose( dataInfo->outUnit );
            
                if (error = dataOpen( dataInfo->outName,
                                      &dataInfo->outUnit,
                                      FB_CREATE )) return(error);

                if (error = dataWrite( dataInfo->outUnit,
                                       dataInfo->fdata,
                                       sizeof(float)*FDATASIZE )) return(error);
               }

            TIMER_ON( writeTime );

            if (error = dataWrite( dataInfo->outUnit,
                                   dataInfo->outBuff,
                                   sizeof(float)*pts )) return( error );

            TIMER_OFF( writeTime );
           }

        dataInfo->outBuffPtr   = dataInfo->outBuff;
        dataInfo->buffCountOut = 0;
       }

    firstTime = 0;

    return( 0 );
}

/***/
/* procSlice: process a single 1D slice from the NMR stream.
/***/

int procSlice( funcInfo, dataInfo )

   struct ProcFuncInfo *funcInfo;
   struct ProcDataInfo *dataInfo;
{
    int i, procFlag, error; 

/***/
/* Test if the current slice should be passed or processed:
/***/

    if (dataInfo->selectMode != SELECT_ALL)
       {
        procFlag = 1;
 
        for( i = YLOC; i < dataInfo->dimCount; i++ )
           {
            if (dataInfo->selX1[i] == 0 && dataInfo->selXN[i] == 0)
               {
                continue;
               }
            else if (dataInfo->locList[i] < dataInfo->selX1[i] || 
                      dataInfo->locList[i] > dataInfo->selXN[i])
               {
                procFlag = 0;
               }
           }

        if (dataInfo->selectMode == SELECT_OUTSIDE) procFlag = !procFlag;
 
        if (!procFlag)
           {
            if (error = putSlice( dataInfo ))
               {
                (void) fprintf( stderr, "NMRPipe Pass-Through Write Error.\n" );
                return( error );
               }

            return( 0 );
           }
       }

/***/
/* Set timers if needed, process the slice:
/***/

    TIMER_ON( procTime );

    if (funcInfo->func)
       {
        if (error = (*funcInfo->func)( dataInfo )) return( error );
       }
    else
       {
        return( 1 );
       }

    TIMER_OFF( procTime );

    return( 0 );
}

/***/
/* nmrPlugin: transforms to new executable if -fn name matches plugin name.
/***/

int nmrPlugin( argc, argv, envp )

   int  argc;
   char **argv, **envp;
{
    char **newArgv, *fnPtr, *fnList, *exePtr, *exeList, *exeName;
    char fnName[NAMELEN+1];

    int  loc, i, n, nFn, nExe, dbFlag;

    newArgv   = (char **)NULL;
    fnName[0] = '\0';
    dbFlag    = 0;

    if (!(fnPtr = getenv( "NMR_PLUGIN_FN" ))) return( 0 );

    (void) strArgD( argc, argv, "-fn", fnName );
    if (!fnName[0]) return( 0 );

    if (!(fnList = strDup( fnPtr ))) 
       {
        FPR( stderr, "NMRPipe Plugin Memory Error.\n" ); 
        return( 1 );
       }

    (void) raiseStr( fnName );
    (void) raiseStr( fnList );

    n = strlen( fnList );
    for( i = 0; i < n; i++ ) if (fnList[i] == ':') fnList[i] = ' ';

    nFn = cntToken( fnList );
    loc = -1;

    for( i = 1; i <= nFn; i++ )
       {
        if (!strcmp( fnName, getToken( i, fnList )))
           {
            loc = i;
            break;
           }
       } 

    (void) strFree( fnList );

    if (loc < 0) return( 0 );

    if (!(exePtr = getenv( "NMR_PLUGIN_EXE" )))
       {
        FPR( stderr, "NMRPipe Plugin Error: EXE Environment is Missing.\n" );
        return( 1 );
       }

    exeList = strDup( exePtr );
    for( i = 0; i < n; i++ ) if (exeList[i] == ':') exeList[i] = ' ';

    nExe = cntToken( exeList );

    if (nFn != nExe)
       {
        FPR( stderr, "NMRPipe Plugin Error: FN/EXE Environment Mismatch.\n" );
        return( 1 );
       }
   
    exeName = getToken( loc, exeList );
    newArgv = copyArgv( argc, argv, fileTail( exeName ));

    if (!newArgv)
       {
        FPR( stderr, "NMRPipe Plugin Error Creating Argument List.\n" );
        return( 1 );
       }

    (void) intArgD( argc, argv, "-dbg", &dbFlag );

    if (dbFlag)
       {
        FPR( stderr, "NMRPipe Plugin Executing %s -fn %s\n", exeName, fnName );
       }

    (void) execvp( exeName, newArgv );

    FPR( stderr, 
         "NMRPipe Plugin Error Executing %s -fn %s\n", 
         exeName, fnName );

    return( 1 );
}

/***/
/* rMove: moves items from src to dest.
/***/

static int rMove( src, dest, length )

   register float *src, *dest;
   register int   length;
{
    while( length-- ) *dest++ = *src++;

    return( 0 );
}

/***/
/* rSet: set elements of vec to val.
/***/

static int rSet( vec, length, val )

   register float *vec, val;
   register int   length;
{
    while( length-- ) *vec++ = val;
    return( 0 );
}

/***/
/* Bottom.
/***/
