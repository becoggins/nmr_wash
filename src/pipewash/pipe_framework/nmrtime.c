/******************************************************************************/
/*                                                                            */
/*                   ---- NIH NMR Software System ----                        */
/*                        Copyright 1992 and 1993                             */
/*                             Frank Delaglio                                 */
/*                   NIH Laboratory of Chemical Physics                       */
/*                                                                            */
/*               This software is not for distribution without                */
/*                  the written permission of the author.                     */
/*                                                                            */
/******************************************************************************/


/***/
/* nmrtime.c: utilities for recording and reporting nmrPipe performance times.
/***/

#include <stdio.h>
#include <string.h>

#include "dataio.h"
#include "prec.h"

#include "nmrserver.h"
#include "nmrpipe.h"
#include "nmrtime.h"

/***/
/* initNmrTime: set various timers to zero.
/***/

int initNmrTime()
{
#ifdef NMRTIME

    int i;

/***/
/* Set total times to zero.
/* Set I/O retry count to zero.
/* Set times for each possible server to zero.
/***/

    procTime.total         = 0.0;
    readTime.total         = 0.0;
    writeTime.total        = 0.0;
    readWTime.total        = 0.0;
    writeWTime.total       = 0.0;
    clientReadTime.total   = 0.0;
    clientWriteTime.total  = 0.0;
    clientReadWTime.total  = 0.0;
    clientWriteWTime.total = 0.0;
    msgTime.total          = 0.0;
    connectTime.total      = 0.0;
    systemTime.total       = 0.0;
    totalTime.total        = 0.0;

    for( i = 0; i < MAXSOCK; i++ )
       {
        (serverReadTime   + i)->total = 0.0;
        (serverWriteTime  + i)->total = 0.0;
        (serverReadWTime  + i)->total = 0.0;
        (serverWriteWTime + i)->total = 0.0;
       }

#endif

    return( 0 );
}

/***/
/* showNmrTime: display various performance time tallies.
/***/

int showNmrTime( funcInfo, dataInfo )

   struct ProcFuncInfo *funcInfo;
   struct ProcDataInfo *dataInfo;
{
#ifdef NMRTIME

/***/
/* Shorthand for standard and client server messages:
/***/

#define SHO2( F, T ) (void) fprintf( stderr, "%-8s ", hostName );       \
                     (void) fprintf( stderr, "%-6s ", funcInfo->name ); \
                     (void) fprintf( stderr, F, T )

#define SHO3( F, T ) (void) fprintf( stderr, "%-8s ", hostName );       \
                     (void) fprintf( stderr, "%-6s ", funcInfo->name ); \
                     (void) fprintf( stderr, F, T );                    \
                     (void) fprintf( stderr, " [%02d]\n", i+1 );

    char  hostName[NAMELEN+1];
    float sum;
    int   i;

/***/
/* Stagger reports from servers:
/***/

    if (dataInfo->serverMode == SMODE_SERVER)
       {
        (void) sleep( (unsigned)dataInfo->serverID );
       }

/***/
/* Extract host name for report.
/* Calculate a total time.
/* Report on I/O and process statistics.
/* Report on client/server statistics.
/***/

    if (dataInfo->serverMode ==SMODE_SERVER)
       (void) gethostname( hostName, NAMELEN );
    else
       (void) strcpy( hostName, "client" );

    sum = procTime.total + readTime.total + writeTime.total + readWTime.total +
          writeWTime.total + clientReadTime.total + clientWriteTime.total     +
          clientReadWTime.total + clientWriteWTime.total + systemTime.total;
 
    SHO2( "Process:                %9.3f\n", procTime.total );
    SHO2( "Read:                   %9.3f\n", readTime.total );
    SHO2( "Write:                  %9.3f\n", writeTime.total );
    SHO2( "Read Wait:              %9.3f\n", readWTime.total );
    SHO2( "Write Wait:             %9.3f\n", writeWTime.total );

    if (dataInfo->serverMode == SMODE_SERVER)
       {
        SHO2( "Read From Client:       %9.3f\n", clientReadTime.total );
        SHO2( "Write To Client:        %9.3f\n", clientWriteTime.total );
        SHO2( "Read From Client Wait:  %9.3f\n", clientReadWTime.total );
        SHO2( "Write To Client Wait:   %9.3f\n", clientWriteWTime.total );
       }

    SHO2( "Message:                %9.3f\n", msgTime.total );
    SHO2( "Connect:                %9.3f\n", connectTime.total );
    SHO2( "System:                 %9.3f\n", systemTime.total );
    SHO2( "Timed Total:            %9.3f\n", totalTime.total );

    for( i = 0; i < dataInfo->serverCount; i++ )
       {
        SHO3( "Read From Server:       %9.3f", serverReadTime[i].total );
        SHO3( "Write to Server:        %9.3f", serverWriteTime[i].total );
        SHO3( "Read From Server Wait:  %9.3f", serverReadWTime[i].total );
        SHO3( "Write To Server Wait:   %9.3f", serverWriteWTime[i].total );
 
        sum += serverReadTime[i].total + serverWriteTime[i].total +
               serverReadWTime[i].total + serverWriteWTime[i].total;
       }

    SHO2( "Summed Total:           %9.3f\n", sum );
    SHO2( "Difference:             %9.3f\n", (float)totalTime.total-sum );

    SHO2( "Read Retries:           %9d\n", readRetries );
    SHO2( "Write Retries:          %9d\n", writeRetries );
    SHO2( "Socket Read Retries:    %9d\n", sReadRetries );
    SHO2( "Socket Write Retries:   %9d\n", sWriteRetries );

#endif
   
    return( 0 );
}
