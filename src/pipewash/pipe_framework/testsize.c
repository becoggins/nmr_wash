/******************************************************************************/
/*                                                                            */
/*                   ---- NIH NMR Software System ----                        */
/*                        Copyright 1992 and 1993                             */
/*                             Frank Delaglio                                 */
/*                   NIH Laboratory of Chemical Physics                       */
/*                                                                            */
/*               This software is not for distribution without                */
/*                  the written permission of the author.                     */
/*                                                                            */
/******************************************************************************/

/***/
/* testSize: utilities for testing sizes of input and output files.
/***/

#include <stdio.h>
#include <limits.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>

#define FPR (void)fprintf

#define USE_VFS

#ifdef WINNT
#include <sys/mount.h>
#include <sys/stat.h>
#undef USE_VFS
#endif

#ifdef MAC_OSX
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/mount.h>
#undef USE_VFS
#endif

#ifdef SOLARIS
#include <sys/statfs.h>
#define STATFS statvfs
#endif

#ifdef SGI
#include <sys/statfs.h>
#undef USE_VFS
#endif

#ifdef CRAY
#include <sys/statfs.h>
#undef USE_VFS
#endif

#ifdef ALPHA
#include <sys/mount.h>
#include <sys/ustat.h>
#undef USE_VFS
#endif

#ifdef IBM
#include <sys/statfs.h>
#endif

#ifdef WINXP
#include <sys/statvfs.h>
#define STATFS statvfs
#else
#ifdef USE_VFS 
#include <sys/vfs.h>
#endif
#endif

#ifndef STATFS
#define STATFS statfs
#endif

#include "fdatap.h"
#include "testsize.h"

/***/
/* testNMRin: tests that true size of input file matches header.
/*
/*            Returns: -1 if tests fail.
/*                      1 if true length is inconsistant with header.
/*                      0 if lengths are consistant.
/***/

int testNMRin( path, fdata, multiFile, verbose )

   char  *path;
   float fdata[FDATASIZE];
   int   multiFile, verbose;
{
    struct FileSize pInfo, tInfo;
    int    mismatchFlag;

/***/
/* Extract the predicted and true file sizes.
/* Stop if either extraction fails.
/***/

    (void) getNMRBytes( fdata, multiFile, &pInfo );
    (void) getFileBytes( path, &tInfo );

    if (pInfo.status == FILE_SIZE_FAIL || tInfo.status == FILE_SIZE_FAIL)
       {
        return( -1 );
       }
    else if (pInfo.dTotalBytes < 0.0 || tInfo.dTotalBytes < 0.0)
       {
        return( -1 );
       }

/***/
/* If the true and predicted sizes can be expressed as ints:
/*   Compare the int sizes to see if they are consistant.
/* Otherwise:
/*   Compare the double-precision float sizes instead.
/***/

    if (pInfo.status == FILE_SIZE_OK && tInfo.status == FILE_SIZE_OK)
       {
        mismatchFlag = (pInfo.iTotalBytes != tInfo.iTotalBytes);

        if (verbose && mismatchFlag)
           {
            FPR( stderr, "\n" );
            FPR( stderr, "File Size Check Error:" );
            FPR( stderr, " Insufficient Data in %s\n", path );
            FPR( stderr, "Predicted Size: %d bytes.\n", pInfo.iTotalBytes );
            FPR( stderr, "Available Size: %d bytes.\n", tInfo.iTotalBytes );
            FPR( stderr, "\n" );
           }
       }
    else
       {
        mismatchFlag = (pInfo.dTotalBytes != tInfo.dTotalBytes);

#ifndef LINT_CHECK
        if (verbose && mismatchFlag)
           {
            FPR( stderr, "\n" );
            FPR( stderr, "File Size Check Error:" );
            FPR( stderr, " Insufficient Data in %s\n", path );
            FPR( stderr, "Predicted Size: %.0lf bytes.\n", pInfo.dTotalBytes );
            FPR( stderr, "Available Size: %.0lf bytes.\n", tInfo.dTotalBytes );
            FPR( stderr, "\n" );
           }
#endif

       }

    return( mismatchFlag );
}

/***/
/* testNMRout: tests if predicted length of output exceeds space limits.
/*
/*            Returns: -1 if tests fail.
/*                      1 if predicted length is too large. 
/*                      0 if length is within limits.
/***/

int testNMRout( path, fdata, multiFile, verbose )

   char  *path;
   float fdata[FDATASIZE];
   int   verbose;
{
    struct FileSize pInfo, aInfo;
    int    noSpaceFlag;

/***/
/* Extract the predicted and available file sizes.
/* Stop if either extraction fails.
/***/

    (void) getNMRBytes( fdata, multiFile, &pInfo );
    (void) getFreeBytes( path, &aInfo );

    if (pInfo.status == FILE_SIZE_FAIL || aInfo.status == FILE_SIZE_FAIL)
       {
        return( -1 );
       }
    else if (pInfo.dTotalBytes < 0.0 || aInfo.dTotalBytes < 0.0)
       {
        return( -1 );
       }

/***/
/* If the predicted and available sizes can be expressed as ints:
/*   Compare the int sizes to see if they are consistant.
/* Otherwise:
/*   Compare the double-precision float sizes instead.
/***/

    if (pInfo.status == FILE_SIZE_OK && aInfo.status == FILE_SIZE_OK)
       {
        noSpaceFlag = (pInfo.iTotalBytes > aInfo.iTotalBytes);

        if (verbose && noSpaceFlag)
           {
            FPR( stderr, "\n" );
            FPR( stderr, "File Size Check Error:" );
            FPR( stderr, " Insufficient Space for %s\n", path );
            FPR( stderr, "Predicted Size: %d bytes.\n", pInfo.iTotalBytes );
            FPR( stderr, "Available Size: %d bytes.\n", aInfo.iTotalBytes );
            FPR( stderr, "\n" );
           }
       }
    else
       {
        noSpaceFlag = (pInfo.dTotalBytes > aInfo.dTotalBytes);

#ifndef LINT_CHECK
        if (verbose && noSpaceFlag)
           {
            FPR( stderr, "\n" );
            FPR( stderr, "File Size Check Error:" );
            FPR( stderr, " Insufficient Space for %s\n", path );
            FPR( stderr, "Predicted Size: %.0lf bytes.\n", pInfo.dTotalBytes );
            FPR( stderr, "Available Size: %.0lf bytes.\n", aInfo.dTotalBytes );
            FPR( stderr, "\n" );
           }
#endif

       }

    return( noSpaceFlag );
}

/***/
/* getNMRBytes: returns expected size of datafile based on header.
/***/

double getNMRBytes( fdata, multiFile, fInfo )

   struct FileSize *fInfo;
   float  fdata[FDATASIZE];
   int    multiFile;
{
    int   xSize, ySize, zSize, aSize, fileCount, dimCount, quadState;
    
    (void) fileSizeInit( fInfo );

    xSize  = getParm( fdata, NDSIZE, CUR_XDIM );
    ySize  = getParm( fdata, NDSIZE, CUR_YDIM );
    zSize  = getParm( fdata, NDSIZE, CUR_ZDIM );
    aSize  = getParm( fdata, NDSIZE, CUR_ADIM );

    fileCount  = getParm( fdata, FDFILECOUNT, 0 );
    dimCount   = getParm( fdata, FDDIMCOUNT,  0 );

    if (xSize < 1) xSize = 1;
    if (ySize < 1) ySize = 1;
    if (zSize < 1) zSize = 1;
    if (aSize < 1) aSize = 1;

    if (!multiFile || fileCount < 1) fileCount = 1;

    if (1 == (int)getParm( fdata, FDQUADFLAG, 0 ))
       quadState = 1;
    else
       quadState = 2;

    if (NULL_DIM != (int)getParm( fdata, FDPIPEFLAG, 0 ))
       {
        fInfo->dTotalBytes = sizeof(float) * 
           (FDATASIZE + (double)xSize*ySize*zSize*aSize*quadState);

        fInfo->iTotalBytes = sizeof(float) * 
           (FDATASIZE + xSize*ySize*zSize*aSize*quadState);

        if (fInfo->dTotalBytes > INT_MAX || fInfo->dTotalBytes < 0.0)
           {
            fInfo->status = FILE_SIZE_OVER;
           }
       }
    else
       {
        if (dimCount == 1)
           {
            fInfo->dTotalBytes = 
               (double)fileCount *
                  (sizeof(float)*(FDATASIZE + (double)xSize*quadState));

            fInfo->iTotalBytes = 
               fileCount*(sizeof(float)*(FDATASIZE + xSize*quadState));

            if (fInfo->dTotalBytes > INT_MAX || fInfo->dTotalBytes < 0.0)
               {
                fInfo->status = FILE_SIZE_OVER;
               }
           }
        else
           {
            fInfo->dTotalBytes =
               (double)fileCount * 
                 (sizeof(float)*(FDATASIZE + (double)xSize*ySize*quadState));

            fInfo->iTotalBytes =
               fileCount * 
                  (sizeof(float)*(FDATASIZE + xSize*ySize*quadState));

            if (fInfo->dTotalBytes > INT_MAX || fInfo->dTotalBytes < 0.0)
               {
                fInfo->status = FILE_SIZE_OVER;
               }
           }
       }

    if (fInfo->status == FILE_SIZE_OK)
       {
        fInfo->blockCount = 
           fInfo->iTotalBytes/fInfo->blockSize;

        fInfo->byteCount  = 
           fInfo->iTotalBytes - fInfo->blockCount*fInfo->blockSize;
       }
    else
       {
        fInfo->blockCount =
           fInfo->dTotalBytes/fInfo->blockSize;

        fInfo->byteCount  =
           fInfo->dTotalBytes - fInfo->blockCount*fInfo->blockSize;
       }

    return( fInfo->dTotalBytes );
}

/***/
/* getFreeBytes: returns space available on filesystem associated with path.
/*               Returns negative number if test fails.
/***/

double getFreeBytes( path, fInfo )

   struct FileSize *fInfo; 
   char   *path;
{
    struct STATFS buff;

    errno = 0;

    (void) fileSizeInit( fInfo );

#define USE_STATFS

#ifdef SGI
    if (statfs( path, &buff, sizeof( struct statfs ), 0 ))
       {
        if (errno) (void) perror( "File Status Notice" );

        fInfo->status = FILE_SIZE_FAIL;
        return( (double) -1.0 );
       }
    else
       {
        fInfo->blockSize  = buff.f_bsize;
        fInfo->blockCount = buff.f_bfree;
       }

#undef USE_STATFS
#endif

#ifdef CRAY
    if (statfs( path, &buff, sizeof( struct statfs ), 0 ))
       {
        if (errno) (void) perror( "File Status Notice" );

        fInfo->status = FILE_SIZE_FAIL;
        return( (double) -1.0 );
       }
    else
       {
        fInfo->blockSize  = buff.f_bsize;
        fInfo->blockCount = buff.f_bfree;
       }

#undef USE_STATFS
#endif

#ifdef ALPHA
    if (statfs( path, &buff, sizeof( buff ) ))
       {
        if (errno) (void) perror( "File Status Notice" );

        fInfo->status = FILE_SIZE_FAIL;
        return( (double) -1.0 );
       }
    else
       {
        fInfo->blockSize  = buff.f_bsize;
        fInfo->blockCount = buff.f_bavail;
       }

#undef USE_STATFS
#endif

#ifdef USE_STATFS
    if (STATFS( path, &buff ))
       {
        if (errno) (void) perror( "File Status Notice" );

        fInfo->status = FILE_SIZE_FAIL;
        return( (double) -1.0 );
       }
    else
       {
        fInfo->blockSize  = buff.f_bsize;
        fInfo->blockCount = buff.f_bavail;
       }
#endif

    fInfo->dTotalBytes = (double)fInfo->blockSize*fInfo->blockCount;
    fInfo->iTotalBytes = fInfo->blockSize*fInfo->blockCount;

    if (fInfo->dTotalBytes > INT_MAX || fInfo->dTotalBytes < 0.0)
       {
        fInfo->status = FILE_SIZE_OVER;
       }

    return( fInfo->dTotalBytes );
}

/***/
/* getFileBytes: returns space used by file associated with path.
/*               Returns negative number if test fails.
/***/

double getFileBytes( path, fInfo )

   struct FileSize *fInfo;
   char   *path;
{
    struct stat buff;

    errno = 0;

    (void) fileSizeInit( fInfo );

    if (stat( path, &buff ))
       {
        if (errno) (void) perror( "File Status Notice" );

        fInfo->status = FILE_SIZE_FAIL;
        return( (double) -1.0 );
       }
    else
       {
        fInfo->dTotalBytes = (double)buff.st_size;
        fInfo->iTotalBytes = buff.st_size;
       }

    fInfo->blockCount = buff.st_size/fInfo->blockSize;
    fInfo->byteCount  = buff.st_size - fInfo->blockSize*fInfo->blockCount;

    if (fInfo->dTotalBytes > INT_MAX || fInfo->dTotalBytes < 0.0)
       {
        fInfo->status = FILE_SIZE_OVER;
       }

    return( fInfo->dTotalBytes );
}

/***/
/* fileSizeInit: initialize a FileSize structure.
/***/

int fileSizeInit( fInfo )

   struct FileSize *fInfo;
{
    fInfo->status      = FILE_SIZE_OK;
    fInfo->blockSize   = DEF_BLOCK_SIZE;
    fInfo->blockCount  = 0;
    fInfo->byteCount   = 0;
    fInfo->iTotalBytes = 0;
    fInfo->dTotalBytes = 0.0;

    return( 0 );
}

/***/
/* Bottom.
/***/
