//  Copyright (c) 2012-2014, Brian E. Coggins.  All rights reserved.


#include <stdio.h>
#include <vector>
#include <string>
#include <iostream>
#include <sys/ioctl.h>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <bec_misc/becmisc.h>
#include "nmr_wash.h"
#include "console_support.h"
#include "pipewash.h"
#include "apod.h"

namespace po = boost::program_options;
using namespace BEC_NMRData;
using namespace nmr_wash;


const std::string version_info( "pipewash: CLEAN and SCRUB for NMRPipe, version " + nmr_wash::version + ", compiled " + __DATE__ "\n" + nmr_wash::copyright + "\n" );

struct pipewash_clean_handle
{
    pipewash_clean_handle( sampling_pattern *sp_, NMRData *data_, cleaner *c_, FILE *log_, int slices_per_array_, int array_size_, int num_of_arrays_ ) : sp( sp_ ), data( data_ ), c( c_ ), log( log_ ), slices_per_array( slices_per_array_ ), array_size( array_size_ ), array_num( 0 ), num_of_arrays( num_of_arrays_ ), ne( array_size_ > 32768 ? 32768 : array_size_ , array_size_ ) {  }
    ~pipewash_clean_handle()
    {
        delete sp;
        delete data;
        delete c;
        fclose( log );
        
        sp = 0;
        data = 0;
        c = 0;
        log = 0;
    }
    
    sampling_pattern *sp;
    NMRData *data;
    cleaner *c;
    FILE *log;
    int slices_per_array;
    int array_size;
    int array_num;
    int num_of_arrays;
    int char_count;
    details::noise_estimator ne;
    std::vector< float > initial_noise_levels;
    std::vector< float > final_noise_levels;
};

struct pipewash_scrub_handle
{
    pipewash_scrub_handle( sampling_pattern *sp_, NMRData *data_, scrubber *s_, FILE *log_, int slices_per_array_, int array_size_, int num_of_arrays_ ) : sp( sp_ ), data( data_ ), s( s_ ), log( log_ ), slices_per_array( slices_per_array_ ), array_size( array_size_ ), array_num( 0 ), num_of_arrays( num_of_arrays_ ), ne( array_size_ > 32768 ? 32768 : array_size_ , array_size_ ) {  }
    ~pipewash_scrub_handle()
    {
        delete sp;
        delete data;
        delete s;
        fclose( log );
        
        sp = 0;
        data = 0;
        s = 0;
        log = 0;
    }
    
    sampling_pattern *sp;
    NMRData *data;
    scrubber *s;
    FILE *log;
    int slices_per_array;
    int array_size;
    int array_num;
    int num_of_arrays;
    int char_count;
    details::noise_estimator ne;
    std::vector< float > initial_noise_levels;
    std::vector< float > final_noise_levels;
};

struct pipewash_options_handle
{
    pipewash_options_handle( boost::program_options::variables_map *vm_ ) : vm( vm_ ) {  }
    ~pipewash_options_handle()
    {
        delete vm;
        vm = 0;
    }
    
    boost::program_options::variables_map *vm;
};

dimension_apod make_dimension_apod( float pipe_apodcode, float pipe_q1, float pipe_q2, float pipe_q3, float pipe_apodsize, float pipe_c1 )
{
    dimension_apod da;

    if( pipe_apodcode == APOD_SP )
        da.method = sp;
    else if( pipe_apodcode == APOD_EM )
        da.method = em;
    else if( pipe_apodcode == APOD_GM )
        da.method = gm;
    else if( pipe_apodcode == APOD_TM )
        da.method = nmr_wash::tm;
    else if( pipe_apodcode == APOD_TRI )
        da.method = tri;
    else if( pipe_apodcode == APOD_GMB )
        da.method = gmb;
    else if( pipe_apodcode == APOD_JMOD )
        da.method = jmod;
    else
        da.method = none;

    da.p1 = pipe_q1;
    da.p2 = pipe_q2;
    da.p3 = pipe_q3;
    da.size = ( int ) BEC_Misc::closest_int( pipe_apodsize );
    da.first_point_correction = pipe_c1;
    
    return da;
}

pipewash_options_handle * pipewash_clean_params( int argc, char *argv[] )
{
    try
    {
        //  *********************************************************
        //  Define program options
        
        winsize ws;
        ioctl( 0, TIOCGWINSZ, &ws );
        int disp_cols = ws.ws_col > 100 ? 100 : ws.ws_col;
        
        po::options_description general_options( "General options", disp_cols, disp_cols - 24 );
        general_options.add_options()
        ( "help,h", "show this help message" )
        ( "version", "show version information" )
        ( "pattern,p", po::value< std::string >(), "sampling pattern file" )
        ( "quiet,q", "suppress information about the progress of the calculation" )
        ( "log", po::value< std::string >(), "record a log to a text file" )
        ( "verbose-log", "record complete details of the calculation to the log file" )
        ( "output-components-only", "write only the components to output" )
        ( "output-residuals-only", "write only the residuals to output" );
        
        po::options_description dim_options( "Dimension assignment", disp_cols, disp_cols - 24 );
        dim_options.add_options()
        ( "z", po::value< dimension_assignment >(), "interpretation of current Z dimension (U, V, W). Supply these to override the automatic assignment; if you override one, you will need to override the remaining ones up to the number of sparsely sampled dimensions in the experiment.  N.B. before calling this function, use transpose functions to shift the sparsely sampled dimensions so that they are in X for one sparse dimension, X and Y for two, and X/Y/Z for three." )
        ( "y", po::value< dimension_assignment >(), "interpretation of current Y dimension (U, V, W)" )
        ( "x", po::value< dimension_assignment >(), "interpretation of current X dimension (U, V, W)" );
        
        po::options_description pattern_parsing_options( "Sampling pattern parsing options", disp_cols, disp_cols - 24 );
        pattern_parsing_options.add_options()
        ( "u-col", po::value< int >(), "the column of the sampling pattern containing the U dimension (N.B. choosing any of these overrides automatic parsing of the sampling pattern)" )
        ( "v-col", po::value< int >(), "the column of the sampling pattern containing the V dimension" )
        ( "w-col", po::value< int >(), "the column of the sampling pattern containing the W dimension" )
        ( "weight-col", po::value< int >(), "the column of the sampling pattern containing the weighting information" )
        ( "off-grid", "the sampling pattern is NOT defined on a grid" )
        ( "ignore-weights", "ignore any weights found in the sampling pattern file" );
        
        po::options_description clean_options( "CLEAN options", disp_cols, disp_cols - 24 );
        clean_options.add_options()
        ( "gain,g", po::value< float >()->default_value( 10.0F ), "gain (%)" )
        ( "snr-threshold", po::value< float >()->default_value( 5.0F ), "CLEAN stops when the signal-to-noise ratio is less than or equal to this many standard deviations of the noise" )  //  N.B. This maps to threshold_intensity
        ( "noise-change-threshold", po::value< float >()->default_value( 5.0F ), "CLEAN stops when the average noise level has not changed more than this percentage for 25 consecutive iterations" )   //  N.B. This maps to threshold_noise
        ( "max-iter", po::value< int >(), "The maximum number of CLEAN iterations to allow for each position in the spectrum that is processed(the default is unlimited)" );
        
        po::options_description pure_comp_options( "Linearity and pure component calculation options", disp_cols, disp_cols - 24 );
        pure_comp_options.add_options()
        ( "linearity", po::value< linearity_modes >()->default_value( tde ), "linearity mode (fd, tde, td; default is tde)" )
        ( "pure-comp-mode", po::value< pure_component_modes >()->default_value( contour_ellipsoid ), "calculation mode (contour-irregular, contour-ellipsoid, fixed-ellipsoid, delta-function); default is contour-ellipsoid; N.B. TD linearity mode requires delta functions for pure components and will override this setting" )
        ( "pc-contour-irregular-level", po::value< float >()->default_value( 1.0F ), "contour level for contour-irregular calculations (% of central peak intensity)" )
        ( "pc-contour-irregular-margin", po::value< bool >()->default_value( false ), "for contour-irregular calculations, whether or not to add a one point margin around the irregular contour" )
        ( "pc-contour-ellipsoid-level", po::value< float >()->default_value( 1.0F ), "contour level for contour-ellipsoid calculations (% of central peak intensity)" )
        ( "pc-contour-ellipsoid-margin", po::value< float >()->default_value( 0.0F ), "margin width for contour-ellipsoid calculations (after ellipsoid is circumscribed around contour, it is enlarged by this % of its own size)" )
        ( "pc-fixed-ellipsoid-size", po::value< float >()->default_value( 2.0F ), "size of ellipsoid for fixed-ellipsoid calculations (each semiaxis is this % of the spectral width)" );
        
        po::options_description psf_options( "PSF and pure component output", disp_cols, disp_cols - 24 );
        psf_options.add_options()
        ( "psf", po::value< std::string >(), "save the calculated PSF to a spectrum file" )
        ( "pure-comp", po::value< std::string >(), "save the calculated pure component to a spectrum file" );
        
        po::options_description all_options;
        all_options.add( general_options ).add( dim_options ).add( pattern_parsing_options ).add( clean_options ).add( pure_comp_options ).add( psf_options );
        
        std::auto_ptr< po::variables_map > vm_p( new po::variables_map );
        po::variables_map &vm = *vm_p;
        
        //  *********************************************************
        //  Parse command line
        
        try
        {
            po::store( po::command_line_parser( argc, argv ).options( all_options ).style( ( po::command_line_style::unix_style | po::command_line_style::allow_long_disguise ) ^ po::command_line_style::allow_guessing ).run(), vm );
            po::notify( vm );
        }
        catch( exceptions::bad_dimension_assignment_string &e )
        {
            std::cerr << "CLEAN: An invalid dimension assignment was provided.\n";
            return 0;
        }
        catch( exceptions::bad_pure_component_mode_string &e )
        {
            std::cerr << "CLEAN: An invalid pure component calculation mode was provided.\n";
            return 0;
        }
        catch( po::unknown_option &e )
        {
            std::cerr << "CLEAN: Unrecognized option " << e.get_option_name() << ".\n";
            return 0;
        }
        catch( po::validation_error &e )
        {
            std::cerr << "CLEAN: Value for option " << e.get_option_name() << " is not valid: " << e.what() << "\n";
            return 0;
        }
        catch( po::invalid_option_value &e )
        {
            std::cerr << "CLEAN: An invalid option value was provided.\n";
            return 0;
        }
        catch( po::error &e )
        {
            std::cerr << "CLEAN: An error occurred while parsing the command line: " << e.what() << "\n";
            return 0;
        }
        
        //  *********************************************************
        //  Respond to information flags
        
        if( vm.count( "help" ) )
        {
            std::cerr << version_info << "\n";
            std::cerr << all_options << "\n";
            return 0;
        }
        if( vm.count( "version" ) )
        {
            std::cout << version_info << "\n";
            return 0;
        }
        
        //  *********************************************************
        //  Check for required parameters
        
        if( !vm.count( "pattern" ) )
        {
            std::cerr << "No sampling pattern.\n";
            return 0;
        }
        if( vm.count( "output-components-only" ) && vm.count( "output-residuals-only" ) )
        {
            std::cerr << "scrub: Both --output-components-only and --output-residuals-only were provided; please choose only one at a time.\n";
            return 0;
        }
        
        return new pipewash_options_handle( vm_p.release() );
    }
    catch( std::exception &e )
    {
        std::cerr << "SCRUB: An unexpected error occurred: " << e.what() << "\n";
        return 0;
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        std::cerr << "SCRUB: An unexpected error occurred: " << e.what() << "\n";
        return 0;
    }
    
}
pipewash_clean_handle * pipewash_clean_init( pipewash_options_handle *pwo, int dim_count, int pipe_x_size, int pipe_y_size, int pipe_z_size, int pipe_a_size, char *pipe_x_label, char *pipe_y_label, char *pipe_z_label, char *pipe_a_label, float pipe_x_sw, float pipe_y_sw, float pipe_z_sw, float pipe_x_apodcode, float pipe_x_q1, float pipe_x_q2, float pipe_x_q3, float pipe_x_apodsize, float pipe_x_c1, float pipe_y_apodcode, float pipe_y_q1, float pipe_y_q2, float pipe_y_q3, float pipe_y_apodsize, float pipe_y_c1, float pipe_z_apodcode, float pipe_z_q1, float pipe_z_q2, float pipe_z_q3, float pipe_z_apodsize, float pipe_z_c1 )
{
 
    try
    {
        po::variables_map &vm = *( pwo->vm );
        
        //  *********************************************************
        //  Get quiet flag
        bool quiet = vm.count( "quiet" ) ? true : false;
        
        FILE *log = 0;
        if( vm.count( "log" ) )
        {
            std::string log_filename( vm[ "log" ].as< std::string >() ); 
            log = fopen( log_filename.c_str(), "w+" );
            if( !log )
            {
                std::cerr << "CLEAN: The log file " << log_filename << "could not be created.\n";
                return 0;
            }
        }
        log_relay log_and_cerr( log, std::cerr, quiet );
        
        if( !quiet || log ) log_and_cerr << version_info << "\n" << std::flush;
        
        //  *********************************************************
        //  Load sampling pattern
        
        std::string pattern_filename = vm[ "pattern" ].as< std::string >();
        if( !quiet || log ) log_and_cerr << "Parsing sampling pattern file " << pattern_filename << "..." << std::flush;
        std::auto_ptr< sampling_pattern > sp_p( new sampling_pattern );
        sampling_pattern &sp = *sp_p;
        try
        {
            if( vm.count( "u-col" ) || vm.count( "v-col" ) || vm.count( "w-col" ) || vm.count( "weight-col" ) || vm.count( "off-grid" ) )
            {
                int u_col = vm.count( "u-col" ) ? vm[ "u-col" ].as< int >() : -1;
                int v_col = vm.count( "v-col" ) ? vm[ "v-col" ].as< int >() : -1;
                int w_col = vm.count( "v-col" ) ? vm[ "w-col" ].as< int >() : -1;
                int weight_col = vm.count( "weight-col" ) ? vm[ "weight-col" ].as< int >() : -1;
                bool on_grid = vm.count( "off-grid" ) ? false : true;
                
                sp.load( pattern_filename, u_col, v_col, w_col, weight_col, on_grid );
            }
            else sp.load( vm[ "pattern" ].as< std::string >() );
        }
        catch( exceptions::file_not_found &e )
        {
            std::cerr << "\nCLEAN: The pattern file " << e.filename << " could not be found.\n";
            return 0;
        }
        catch( exceptions::cant_open_file &e )
        {
            std::cerr << "\nCLEAN: The pattern file " << e.filename << " could not be opened.\n";
            return 0;
        }
        catch( exceptions::sampling_pattern::file_format_not_understood &e )
        {
            std::cerr << "\nCLEAN: The pattern file " << e.filename << " could not be interpreted automatically.\n";
            return 0;
        }
        catch( exceptions::sampling_pattern::file_format_not_consistent &e )
        {
            std::cerr << "\nCLEAN: The pattern file " << e.filename << " does not have a consistent format: one or more entries have extra or missing data fields.\n";
            return 0;
        }
        catch( exceptions::sampling_pattern::file_format_doesnt_match &e )
        {
            std::cerr << "\nCLEAN: The pattern file " << e.filename << " could not be processed according to the format options provided.\n";
            return 0;
        }
        catch( exceptions::sampling_pattern::bad_file_format_guidance &e )
        {
            std::cerr << "\nCLEAN: Invalid options for the pattern file format.\n";
            return 0;
        }
        
        if( !quiet || log )
        {
            log_and_cerr << "done.\n";
            log_and_cerr << "Sampling pattern has " << sp.get_num_of_dims() << " dimensions, ";
            log_and_cerr << sp.size() << " sampling points, ";
            if( sp.got_weights() ) log_and_cerr << "and weighting information.\n";
            else log_and_cerr << "and no weighting information.\n";
            if( vm.count( "ignore-weights" ) && sp.got_weights() )
                log_and_cerr << "Ignoring weights.\n";
            log_and_cerr << std::flush;
        }
        
        if( vm.count( "ignore-weights" ) && sp.got_weights() )
        {
            sp.clear_weights();
        }
        
        if( !quiet || log ) log_and_cerr << "\n" << std::flush;
        
        //  *********************************************************
        //  Check NMRPipe data parameters
        
        if( dim_count < sp.get_num_of_dims() )
        {
            std::cerr << "CLEAN: The sampling pattern has more dimensions than the data.\n";
            return 0;
        }
        
        //  *********************************************************
        //  Build NMRData to hold data for processing
        
        std::auto_ptr< NMRData > data( new NMRData( sp.get_num_of_dims() ) );
        switch( sp.get_num_of_dims() )
        {
            case 1:
                data->SetDimensionSize( 0, pipe_x_size );
                data->SetDimensionSW( 0, pipe_x_sw );
                break;
            case 2:
                data->SetDimensionSize( 0, pipe_y_size );
                data->SetDimensionSize( 1, pipe_x_size );
                data->SetDimensionSW( 0, pipe_y_sw );
                data->SetDimensionSW( 1, pipe_x_sw );
                break;
            case 3:
                data->SetDimensionSize( 0, pipe_z_size );
                data->SetDimensionSize( 1, pipe_y_size );
                data->SetDimensionSize( 2, pipe_x_size );
                data->SetDimensionSW( 0, pipe_z_sw );
                data->SetDimensionSW( 1, pipe_y_sw );
                data->SetDimensionSW( 2, pipe_x_sw );
                break;
        }
        data->BuildInMem();
        
        //  *********************************************************
        //  Build dimension map
        
        dimension_map dm;
        switch( sp.get_num_of_dims() )
        {
            case 1:
                dm.F1 = u;
                break;
            case 2:
                dm.F1 = u;
                dm.F2 = v;
                if( vm.count( "y" ) ) dm.F1 = vm[ "y" ].as< dimension_assignment >();
                if( vm.count( "x" ) ) dm.F2 = vm[ "x" ].as< dimension_assignment >();
                break;
            case 3:
                dm.F1 = u;
                dm.F2 = v;
                dm.F3 = w;
                if( vm.count( "z" ) ) dm.F1 = vm[ "z" ].as< dimension_assignment >();
                if( vm.count( "y" ) ) dm.F2 = vm[ "y" ].as< dimension_assignment >();
                if( vm.count( "x" ) ) dm.F3 = vm[ "x" ].as< dimension_assignment >();
                break;
        }
        if( !dm.is_valid() )
        {
            std::cerr << "CLEAN: The assignment of sampling pattern dimensions to NMRPipe dimensions is not valid.\n";
            return 0;
        }
        
        if( !quiet || log )
        {
            log_and_cerr << "Dimensions (-> indicates dimensions to be processed): \n";
            switch( sp.get_num_of_dims() )
            {
                case 1:
                    if( dim_count == 4 ) log_and_cerr << "     A: " << pipe_a_label << " (" << pipe_a_size << " points)\n";
                    if( dim_count >= 3 ) log_and_cerr << "     Z: " << pipe_z_label << " (" << pipe_z_size << " points)\n";
                    if( dim_count >= 2 ) log_and_cerr << "     Y: " << pipe_y_label << " (" << pipe_y_size << " points)\n";
                    log_and_cerr << "  -> X: " << pipe_x_label << " (" << pipe_x_size << " points) [" << dm.F1 << " dimension of sampling pattern]\n";
                    break;
                case 2:
                    if( dim_count == 4 ) log_and_cerr << "     A: " << pipe_a_label << " (" << pipe_a_size << " points)\n";
                    if( dim_count >= 3 ) log_and_cerr << "     Z: " << pipe_z_label << " (" << pipe_z_size << " points)\n";
                    if( dim_count >= 2 ) log_and_cerr << "  -> Y: " << pipe_y_label << " (" << pipe_y_size << " points) [" << dm.F1 << " dimension of sampling pattern]\n";
                    log_and_cerr << "  -> X: " << pipe_x_label << " (" << pipe_x_size << " points) [" << dm.F2 << " dimension of sampling pattern]\n";
                    break;
                case 3:
                    if( dim_count == 4 ) log_and_cerr << "     A: " << pipe_a_label << " (" << pipe_a_size << " points)\n";
                    if( dim_count >= 3 ) log_and_cerr << "  -> Z: " << pipe_z_label << " (" << pipe_z_size << " points) [" << dm.F1 << " dimension of sampling pattern]\n";
                    if( dim_count >= 2 ) log_and_cerr << "  -> Y: " << pipe_y_label << " (" << pipe_y_size << " points) [" << dm.F2 << " dimension of sampling pattern]\n";
                    log_and_cerr << "  -> X: " << pipe_x_label << " (" << pipe_x_size << " points) [" << dm.F3 << " dimension of sampling pattern]\n";
                    break;
            }
            log_and_cerr << "\n";
            log_and_cerr << std::flush;
        }
        
        //  *********************************************************
        //  Configure apodization information
        switch( sp.get_num_of_dims() )
        {
            case 1:
                dm.F1_apod = make_dimension_apod( pipe_x_apodcode, pipe_x_q1, pipe_x_q2, pipe_x_q3, pipe_x_apodsize, pipe_x_c1 );
                break;
            case 2:
                dm.F1_apod = make_dimension_apod( pipe_y_apodcode, pipe_y_q1, pipe_y_q2, pipe_y_q3, pipe_y_apodsize, pipe_y_c1 );
                dm.F2_apod = make_dimension_apod( pipe_x_apodcode, pipe_x_q1, pipe_x_q2, pipe_x_q3, pipe_x_apodsize, pipe_x_c1 );
                break;
            case 3:
                dm.F1_apod = make_dimension_apod( pipe_z_apodcode, pipe_z_q1, pipe_z_q2, pipe_z_q3, pipe_z_apodsize, pipe_z_c1 );
                dm.F2_apod = make_dimension_apod( pipe_y_apodcode, pipe_y_q1, pipe_y_q2, pipe_y_q3, pipe_y_apodsize, pipe_y_c1 );
                dm.F3_apod = make_dimension_apod( pipe_x_apodcode, pipe_x_q1, pipe_x_q2, pipe_x_q3, pipe_x_apodsize, pipe_x_c1 );
                break;
        }
        
        //  *********************************************************
        //  Construct cleaner
        
        std::auto_ptr< cleaner > c_p( new cleaner( sp, dm ) );
        cleaner &c = *c_p;
        
        if( vm.count( "gain" ) ) c.gain = vm[ "gain" ].as< float >();
        if( vm.count( "snr-threshold" ) ) c.threshold_intensity = vm[ "snr-threshold" ].as< float >();
        if( vm.count( "noise-change-threshold" ) ) c.threshold_noise = vm[ "noise-change-threshold" ].as< float >();
        if( vm.count( "max-iter" ) ) c.max_iter = vm[ "max-iter" ].as< int >();
        
        if( vm.count( "linearity" ) ) c.linearity_mode = vm[ "linearity" ].as< linearity_modes >();
        if( vm.count( "pure-comp-mode" ) ) c.pure_component_mode = vm[ "pure-comp-mode" ].as< pure_component_modes >();
        if( vm.count( "pc-contour-irregular-level" ) ) c.pure_comp_ci_level = vm[ "pc-contour-irregular-level" ].as< float >();
        if( vm.count( "pc-contour-irregular-margin" ) ) c.pure_comp_ci_margin = vm[ "pc-contour-irregular-margin" ].as< bool >();
        if( vm.count( "pc-contour-ellipsoid-level" ) ) c.pure_comp_ce_level = vm[ "pc-contour-ellipsoid-level" ].as< float >();
        if( vm.count( "pc-contour-ellipsoid-margin" ) ) c.pure_comp_ce_margin = vm[ "pc-contour-ellipsoid-margin" ].as< float >();
        if( vm.count( "pc-fixed-ellipsoid-size" ) ) c.pure_comp_fe_size = vm[ "pc-fixed-ellipsoid-size" ].as< float >();
        
        c.log_file = log;
        if( vm.count( "verbose-log" ) ) c.verbose_log = true;
        if( vm.count( "output-components-only" ) ) c.components_only = true;
        if( vm.count( "output-residuals-only" ) ) c.residuals_only = true;
        
        //  *********************************************************
        //  Construct and return handle
        
        int slices_per_array = 1;
        switch( sp.get_num_of_dims() )
        {
            case 1:
                break;
            case 2:
                slices_per_array *= pipe_y_size;
                break;
            case 3:
                slices_per_array *= pipe_z_size;
                slices_per_array *= pipe_y_size;
                break;
        }
        
        int array_size = ( int ) data->GetFullSize();
        
        int num_of_arrays = pipe_x_size * pipe_y_size * pipe_z_size * pipe_a_size / array_size;
        
        return new pipewash_clean_handle( sp_p.release(), data.release(), c_p.release(), log, slices_per_array, array_size, num_of_arrays );
    }
    catch( std::exception &e )
    {
        std::cerr << "CLEAN: An unexpected error occurred: " << e.what() << "\n";
        return 0;
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        std::cerr << "CLEAN: An unexpected error occurred: " << e.what() << "\n";
        return 0;
    }
}

int pipewash_clean_get_array_size( pipewash_clean_handle *pwh )
{
    return pwh->array_size;
}

int pipewash_clean_get_slices_per_array( pipewash_clean_handle *pwh )
{
    return pwh->slices_per_array;
}

int pipewash_clean_proc( pipewash_clean_handle *pwh, float *data )
{
    try
    {
        if( !pwh->array_num )
        {
            std::cerr << "CLEAN:          0%                                                100%\n";
            std::cerr << "Calculating...  " << std::flush;
            pwh->char_count = 0;
        }
        else
        {
            int done_chars = ( int ) floorf( ( float ) pwh->array_num * 50.0F / ( float ) pwh->num_of_arrays );
            for( ; pwh->char_count < done_chars; pwh->char_count++ )
                std::cerr << "X" << std::flush;            
        }
        
        cleaner &c = *( pwh->c );
        pwh->data->SetDataFromCopyPtr( data );
        pwh->ne( data );
        pwh->initial_noise_levels.push_back( pwh->ne.noise_floor );
        
        c( *( pwh->data ) );
        
        boost::scoped_ptr< float > out_data( pwh->data->GetDataCopyPtr() );
        memcpy( data, out_data.get(), pwh->array_size * sizeof( float ) );
        pwh->ne( data );
        pwh->final_noise_levels.push_back( pwh->ne.noise_floor );
        
        if( ++pwh->array_num == pwh->num_of_arrays )
        {
            std::cerr << "\n" << std::flush;
            
            details::vector_noise_estimator vne;
            vne( pwh->initial_noise_levels );
            float noise_level = vne.noise_mean + vne.noise_std_dev;
            
            float artifacts_initial = 0.0F, artifacts_final = 0.0F;
            for( std::vector< float >::iterator i = pwh->initial_noise_levels.begin(), j = pwh->final_noise_levels.begin(); i != pwh->initial_noise_levels.end(); ++i, ++j )
            {
                float nf_start = *i;
                float nf_end = *j;
                artifacts_initial += nf_start > noise_level ? nf_start - noise_level : 0;
                artifacts_final += nf_end > noise_level ? nf_end - noise_level : 0;
            }
            float suppression = ( artifacts_initial - artifacts_final ) * 100.0F / artifacts_initial;
            if( suppression > 100.0F ) suppression = 100.0F;
            std::cerr << "Approximate artifact suppression: " << std::fixed << std::setprecision( 0 ) << ( artifacts_initial - artifacts_final ) * 100.0F / ( artifacts_initial ) << "%\n";
        }

        return 0;
    }
    catch( std::exception &e )
    {
        std::cerr << "CLEAN: An unexpected error occurred: " << e.what() << "\n";
        return 1;
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        std::cerr << "CLEAN: An unexpected error occurred: " << e.what() << "\n";
        return 1;
    }
}

int pipewash_clean_done( pipewash_clean_handle *pwh )
{
    delete pwh;
    
    return 0;
}

pipewash_options_handle * pipewash_scrub_params( int argc, char *argv[] )
{
    try
    {
        //  *********************************************************
        //  Define program options
        
        winsize ws;
        ioctl( 0, TIOCGWINSZ, &ws );
        int disp_cols = ws.ws_col > 100 ? 100 : ws.ws_col;
        
        po::options_description general_options( "General options", disp_cols, disp_cols - 24 );
        general_options.add_options()
        ( "help,h", "show this help message" )
        ( "version", "show version information" )
        ( "pattern,p", po::value< std::string >(), "sampling pattern file" )
        ( "quiet,q", "suppress information about the progress of the calculation" )
        ( "log", po::value< std::string >(), "record a log to a text file" )
        ( "verbose-log", "record complete details of the calculation to the log file" )
        ( "output-components-only", "write only the components to output" )
        ( "output-residuals-only", "write only the residuals to output" );
        
        po::options_description dim_options( "Dimension assignment", disp_cols, disp_cols - 24 );
        dim_options.add_options()
        ( "z", po::value< dimension_assignment >(), "interpretation of current Z dimension (U, V, W). Supply these to override the automatic assignment; if you override one, you will need to override the remaining ones up to the number of sparsely sampled dimensions in the experiment.  N.B. before calling this function, use transpose functions to shift the sparsely sampled dimensions so that they are in X for one sparse dimension, X and Y for two, and X/Y/Z for three." )
        ( "y", po::value< dimension_assignment >(), "interpretation of current Y dimension (U, V, W)" )
        ( "x", po::value< dimension_assignment >(), "interpretation of current X dimension (U, V, W)" );
        
        po::options_description pattern_parsing_options( "Sampling pattern parsing options", disp_cols, disp_cols - 24 );
        pattern_parsing_options.add_options()
        ( "u-col", po::value< int >(), "the column of the sampling pattern containing the U dimension (N.B. choosing any of these overrides automatic parsing of the sampling pattern)" )
        ( "v-col", po::value< int >(), "the column of the sampling pattern containing the V dimension" )
        ( "w-col", po::value< int >(), "the column of the sampling pattern containing the W dimension" )
        ( "weight-col", po::value< int >(), "the column of the sampling pattern containing the weighting information" )
        ( "off-grid", "the sampling pattern is NOT defined on a grid" )
        ( "ignore-weights", "ignore any weights found in the sampling pattern file" );
        
        po::options_description scrub_options( "SCRUB options", disp_cols, disp_cols - 24 );
        scrub_options.add_options()
        ( "gain,g", po::value< float >()->default_value( 10.0F ), "gain (%)" )
        ( "base,b", po::value< float >()->default_value( 0.01F ), "base" );
        
        po::options_description pure_comp_options( "Linearity and pure component calculation options", disp_cols, disp_cols - 24 );
        pure_comp_options.add_options()
        ( "linearity", po::value< linearity_modes >()->default_value( tde ), "linearity mode (fd, tde, td; default is tde)" )
        ( "pure-comp-mode", po::value< pure_component_modes >()->default_value( contour_ellipsoid ), "calculation mode (contour-irregular, contour-ellipsoid, fixed-ellipsoid, delta-function); default is contour-ellipsoid; N.B. TD linearity mode requires delta functions for pure components and will override this setting" )
        ( "pc-contour-irregular-level", po::value< float >()->default_value( 1.0F ), "contour level for contour-irregular calculations (% of central peak intensity)" )
        ( "pc-contour-irregular-margin", po::value< bool >()->default_value( false ), "for contour-irregular calculations, whether or not to add a one point margin around the irregular contour" )
        ( "pc-contour-ellipsoid-level", po::value< float >()->default_value( 1.0F ), "contour level for contour-ellipsoid calculations (% of central peak intensity)" )
        ( "pc-contour-ellipsoid-margin", po::value< float >()->default_value( 0.0F ), "margin width for contour-ellipsoid calculations (after ellipsoid is circumscribed around contour, it is enlarged by this % of its own size)" )
        ( "pc-fixed-ellipsoid-size", po::value< float >()->default_value( 2.0F ), "size of ellipsoid for fixed-ellipsoid calculations (each semiaxis is this % of the spectral width)" );
        
        po::options_description psf_options( "PSF and pure component output", disp_cols, disp_cols - 24 );
        psf_options.add_options()
        ( "psf", po::value< std::string >(), "save the calculated PSF to a spectrum file" )
        ( "pure-comp", po::value< std::string >(), "save the calculated pure component to a spectrum file" );
        
        po::options_description all_options;
        all_options.add( general_options ).add( dim_options ).add( pattern_parsing_options ).add( scrub_options ).add( pure_comp_options ).add( psf_options );
        
        std::auto_ptr< po::variables_map > vm_p( new po::variables_map );
        po::variables_map &vm = *vm_p;
        
        //  *********************************************************
        //  Parse command line
        
        try
        {
            po::store( po::command_line_parser( argc, argv ).options( all_options ).style( po::command_line_style::default_style | po::command_line_style::allow_long_disguise ).allow_unregistered().run(), vm );
            po::notify( vm );
        }
        catch( exceptions::bad_dimension_assignment_string &e )
        {
            std::cerr << "SCRUB: An invalid dimension assignment was provided.\n";
            return 0;
        }
        catch( exceptions::bad_pure_component_mode_string &e )
        {
            std::cerr << "SCRUB: An invalid pure component calculation mode was provided.\n";
            return 0;
        }
        catch( po::unknown_option &e )
        {
            std::cerr << "SCRUB: Unrecognized option " << e.get_option_name() << ".\n";
            return 0;
        }
        catch( po::validation_error &e )
        {
            std::cerr << "SCRUB: Value for option " << e.get_option_name() << " is not valid: " << e.what() << "\n";
            return 0;
        }
        catch( po::invalid_option_value &e )
        {
            std::cerr << "SCRUB: An invalid option value was provided.\n";
            return 0;
        }
        catch( po::error &e )
        {
            std::cerr << "SCRUB: An error occurred while parsing the command line: " << e.what() << "\n";
            return 0;
        }
        
        //  *********************************************************
        //  Respond to information flags
        
        if( vm.count( "help" ) )
        {
            std::cerr << version_info << "\n";
            std::cerr << all_options << "\n";
            return 0;
        }
        if( vm.count( "version" ) )
        {
            std::cout << version_info << "\n";
            return 0;
        }
        
        //  *********************************************************
        //  Check for required parameters
        
        if( !vm.count( "pattern" ) )
        {
            std::cerr << "SCRUB: No sampling pattern.\n";
            return 0;
        }
        if( vm.count( "output-components-only" ) && vm.count( "output-residuals-only" ) )
        {
            std::cerr << "scrub: Both --output-components-only and --output-residuals-only were provided; please choose only one at a time.\n";
            return 0;
        }
        
        return new pipewash_options_handle( vm_p.release() );
    }
    catch( std::exception &e )
    {
        std::cerr << "SCRUB: An unexpected error occurred: " << e.what() << "\n";
        return 0;
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        std::cerr << "SCRUB: An unexpected error occurred: " << e.what() << "\n";
        return 0;
    }
            
}

pipewash_scrub_handle * pipewash_scrub_init( pipewash_options_handle *pwo, int dim_count, int pipe_x_size, int pipe_y_size, int pipe_z_size, int pipe_a_size, char *pipe_x_label, char *pipe_y_label, char *pipe_z_label, char *pipe_a_label, float pipe_x_sw, float pipe_y_sw, float pipe_z_sw, float pipe_x_apodcode, float pipe_x_q1, float pipe_x_q2, float pipe_x_q3, float pipe_x_apodsize, float pipe_x_c1, float pipe_y_apodcode, float pipe_y_q1, float pipe_y_q2, float pipe_y_q3, float pipe_y_apodsize, float pipe_y_c1, float pipe_z_apodcode, float pipe_z_q1, float pipe_z_q2, float pipe_z_q3, float pipe_z_apodsize, float pipe_z_c1 )
{
    try
    {
        po::variables_map &vm = *( pwo->vm );
        
        //  *********************************************************
        //  Get quiet flag
        bool quiet = vm.count( "quiet" ) ? true : false;
        
        FILE *log = 0;
        if( vm.count( "log" ) )
        {
            std::string log_filename( vm[ "log" ].as< std::string >() ); 
            log = fopen( log_filename.c_str(), "w+" );
            if( !log )
            {
                std::cerr << "SCRUB: The log file " << log_filename << "could not be created.\n";
                return 0;
            }
        }
        log_relay log_and_cerr( log, std::cerr, quiet );
        
        if( !quiet || log ) log_and_cerr << version_info << "\n" << std::flush;
        
        //  *********************************************************
        //  Load sampling pattern
        
        std::string pattern_filename = vm[ "pattern" ].as< std::string >();
        if( !quiet || log ) log_and_cerr << "Parsing sampling pattern file " << pattern_filename << "..." << std::flush;
        std::auto_ptr< sampling_pattern > sp_p( new sampling_pattern );
        sampling_pattern &sp = *sp_p;
        try
        {
            if( vm.count( "u-col" ) || vm.count( "v-col" ) || vm.count( "w-col" ) || vm.count( "weight-col" ) || vm.count( "off-grid" ) )
            {
                int u_col = vm.count( "u-col" ) ? vm[ "u-col" ].as< int >() : -1;
                int v_col = vm.count( "v-col" ) ? vm[ "v-col" ].as< int >() : -1;
                int w_col = vm.count( "v-col" ) ? vm[ "w-col" ].as< int >() : -1;
                int weight_col = vm.count( "weight-col" ) ? vm[ "weight-col" ].as< int >() : -1;
                bool on_grid = vm.count( "off-grid" ) ? false : true;
                
                sp.load( pattern_filename, u_col, v_col, w_col, weight_col, on_grid );
            }
            else sp.load( vm[ "pattern" ].as< std::string >() );
        }
        catch( exceptions::file_not_found &e )
        {
            std::cerr << "\nSCRUB: The pattern file " << e.filename << " could not be found.\n";
            return 0;
        }
        catch( exceptions::cant_open_file &e )
        {
            std::cerr << "\nSCRUB: The pattern file " << e.filename << " could not be opened.\n";
            return 0;
        }
        catch( exceptions::sampling_pattern::file_format_not_understood &e )
        {
            std::cerr << "\nSCRUB: The pattern file " << e.filename << " could not be interpreted automatically.\n";
            return 0;
        }
        catch( exceptions::sampling_pattern::file_format_not_consistent &e )
        {
            std::cerr << "\nSCRUB: The pattern file " << e.filename << " does not have a consistent format: one or more entries have extra or missing data fields.\n";
            return 0;
        }
        catch( exceptions::sampling_pattern::file_format_doesnt_match &e )
        {
            std::cerr << "\nSCRUB: The pattern file " << e.filename << " could not be processed according to the format options provided.\n";
            return 0;
        }
        catch( exceptions::sampling_pattern::bad_file_format_guidance &e )
        {
            std::cerr << "\nSCRUB: Invalid options for the pattern file format.\n";
            return 0;
        }
        
        if( !quiet || log )
        {
            log_and_cerr << "done.\n";
            log_and_cerr << "Sampling pattern has " << sp.get_num_of_dims() << " dimensions, ";
            log_and_cerr << sp.size() << " sampling points, ";
            if( sp.got_weights() ) log_and_cerr << "and weighting information.\n";
            else log_and_cerr << "and no weighting information.\n";
            if( vm.count( "ignore-weights" ) && sp.got_weights() )
                log_and_cerr << "Ignoring weights.\n";
            log_and_cerr << std::flush;
        }
        
        if( vm.count( "ignore-weights" ) && sp.got_weights() )
        {
            sp.clear_weights();
        }
        
        if( !quiet || log ) log_and_cerr << "\n" << std::flush;
        
        //  *********************************************************
        //  Check NMRPipe data parameters
        
        if( dim_count < sp.get_num_of_dims() )
        {
            std::cerr << "SCRUB: The sampling pattern has more dimensions than the data.\n";
            return 0;
        }
        
        //  *********************************************************
        //  Build NMRData to hold data for processing
        
        std::auto_ptr< NMRData > data( new NMRData( sp.get_num_of_dims() ) );
        switch( sp.get_num_of_dims() )
        {
            case 1:
                data->SetDimensionSize( 0, pipe_x_size );
                data->SetDimensionSW( 0, pipe_x_sw );
                break;
            case 2:
                data->SetDimensionSize( 0, pipe_y_size );
                data->SetDimensionSize( 1, pipe_x_size );
                data->SetDimensionSW( 0, pipe_y_sw );
                data->SetDimensionSW( 1, pipe_x_sw );
                break;
            case 3:
                data->SetDimensionSize( 0, pipe_z_size );
                data->SetDimensionSize( 1, pipe_y_size );
                data->SetDimensionSize( 2, pipe_x_size );
                data->SetDimensionSW( 0, pipe_z_sw );
                data->SetDimensionSW( 1, pipe_y_sw );
                data->SetDimensionSW( 2, pipe_x_sw );
                break;
        }
        data->BuildInMem();
        
        //  *********************************************************
        //  Build dimension map
        
        dimension_map dm;
        switch( sp.get_num_of_dims() )
        {
            case 1:
                dm.F1 = u;
                break;
            case 2:
                dm.F1 = u;
                dm.F2 = v;
                if( vm.count( "y" ) ) dm.F1 = vm[ "y" ].as< dimension_assignment >();
                if( vm.count( "x" ) ) dm.F2 = vm[ "x" ].as< dimension_assignment >();
                break;
            case 3:
                dm.F1 = u;
                dm.F2 = v;
                dm.F3 = w;
                if( vm.count( "z" ) ) dm.F1 = vm[ "z" ].as< dimension_assignment >();
                if( vm.count( "y" ) ) dm.F2 = vm[ "y" ].as< dimension_assignment >();
                if( vm.count( "x" ) ) dm.F3 = vm[ "x" ].as< dimension_assignment >();
                break;
        }
        if( !dm.is_valid() )
        {
            std::cerr << "SCRUB: The assignment of sampling pattern dimensions to NMRPipe dimensions is not valid.\n";
            return 0;
        }
        
        if( !quiet || log )
        {
            log_and_cerr << "Dimensions (-> indicates dimensions to be processed): \n";
            switch( sp.get_num_of_dims() )
            {
                case 1:
                    if( dim_count == 4 ) log_and_cerr << "     A: " << pipe_a_label << " (" << pipe_a_size << " points)\n";
                    if( dim_count >= 3 ) log_and_cerr << "     Z: " << pipe_z_label << " (" << pipe_z_size << " points)\n";
                    if( dim_count >= 2 ) log_and_cerr << "     Y: " << pipe_y_label << " (" << pipe_y_size << " points)\n";
                    log_and_cerr << "  -> X: " << pipe_x_label << " (" << pipe_x_size << " points) [" << dm.F1 << " dimension of sampling pattern]\n";
                    break;
                case 2:
                    if( dim_count == 4 ) log_and_cerr << "     A: " << pipe_a_label << " (" << pipe_a_size << " points)\n";
                    if( dim_count >= 3 ) log_and_cerr << "     Z: " << pipe_z_label << " (" << pipe_z_size << " points)\n";
                    if( dim_count >= 2 ) log_and_cerr << "  -> Y: " << pipe_y_label << " (" << pipe_y_size << " points) [" << dm.F1 << " dimension of sampling pattern]\n";
                    log_and_cerr << "  -> X: " << pipe_x_label << " (" << pipe_x_size << " points) [" << dm.F2 << " dimension of sampling pattern]\n";
                    break;
                case 3:
                    if( dim_count == 4 ) log_and_cerr << "     A: " << pipe_a_label << " (" << pipe_a_size << " points)\n";
                    if( dim_count >= 3 ) log_and_cerr << "  -> Z: " << pipe_z_label << " (" << pipe_z_size << " points) [" << dm.F1 << " dimension of sampling pattern]\n";
                    if( dim_count >= 2 ) log_and_cerr << "  -> Y: " << pipe_y_label << " (" << pipe_y_size << " points) [" << dm.F2 << " dimension of sampling pattern]\n";
                    log_and_cerr << "  -> X: " << pipe_x_label << " (" << pipe_x_size << " points) [" << dm.F3 << " dimension of sampling pattern]\n";
                    break;
            }
            log_and_cerr << "\n";
            log_and_cerr << std::flush;
        }
        
        //  *********************************************************
        //  Configure apodization information
        switch( sp.get_num_of_dims() )
        {
            case 1:
                dm.F1_apod = make_dimension_apod( pipe_x_apodcode, pipe_x_q1, pipe_x_q2, pipe_x_q3, pipe_x_apodsize, pipe_x_c1 );
                break;
            case 2:
                dm.F1_apod = make_dimension_apod( pipe_y_apodcode, pipe_y_q1, pipe_y_q2, pipe_y_q3, pipe_y_apodsize, pipe_y_c1 );
                dm.F2_apod = make_dimension_apod( pipe_x_apodcode, pipe_x_q1, pipe_x_q2, pipe_x_q3, pipe_x_apodsize, pipe_x_c1 );
                break;
            case 3:
                dm.F1_apod = make_dimension_apod( pipe_z_apodcode, pipe_z_q1, pipe_z_q2, pipe_z_q3, pipe_z_apodsize, pipe_z_c1 );
                dm.F2_apod = make_dimension_apod( pipe_y_apodcode, pipe_y_q1, pipe_y_q2, pipe_y_q3, pipe_y_apodsize, pipe_y_c1 );
                dm.F3_apod = make_dimension_apod( pipe_x_apodcode, pipe_x_q1, pipe_x_q2, pipe_x_q3, pipe_x_apodsize, pipe_x_c1 );
                break;
        }
        
        //  *********************************************************
        //  Construct scrubber
        
        std::auto_ptr< scrubber > s_p( new scrubber( sp, dm ) );
        scrubber &s = *s_p;
        
        if( vm.count( "gain" ) ) s.gain = vm[ "gain" ].as< float >();
        if( vm.count( "stopping-threshold" ) ) s.stopping_threshold = vm[ "stopping-threshold" ].as< float >();
        if( vm.count( "base" ) ) s.base = vm[ "base" ].as< float >();
        
        if( vm.count( "linearity" ) ) s.linearity_mode = vm[ "linearity" ].as< linearity_modes >();
        if( vm.count( "pure-comp-mode" ) ) s.pure_component_mode = vm[ "pure-comp-mode" ].as< pure_component_modes >();
        if( vm.count( "pc-contour-irregular-level" ) ) s.pure_comp_ci_level = vm[ "pc-contour-irregular-level" ].as< float >();
        if( vm.count( "pc-contour-irregular-margin" ) ) s.pure_comp_ci_margin = vm[ "pc-contour-irregular-margin" ].as< bool >();
        if( vm.count( "pc-contour-ellipsoid-level" ) ) s.pure_comp_ce_level = vm[ "pc-contour-ellipsoid-level" ].as< float >();
        if( vm.count( "pc-contour-ellipsoid-margin" ) ) s.pure_comp_ce_margin = vm[ "pc-contour-ellipsoid-margin" ].as< float >();
        if( vm.count( "pc-fixed-ellipsoid-size" ) ) s.pure_comp_fe_size = vm[ "pc-fixed-ellipsoid-size" ].as< float >();
        
        s.log_file = log;
        if( vm.count( "verbose-log" ) ) s.verbose_log = true;
        if( vm.count( "threads" ) ) s.num_of_threads = vm[ "threads" ].as< int >();
        if( vm.count( "output-components-only" ) ) s.components_only = true;
        if( vm.count( "output-residuals-only" ) ) s.residuals_only = true;
        
        //  *********************************************************
        //  Construct and return handle
        
        int slices_per_array = 1;
        switch( sp.get_num_of_dims() )
        {
            case 1:
                break;
            case 2:
                slices_per_array *= pipe_y_size;
                break;
            case 3:
                slices_per_array *= pipe_z_size;
                slices_per_array *= pipe_y_size;
                break;
        }
        
        int array_size = ( int ) data->GetFullSize();
        
        int num_of_arrays = pipe_x_size * pipe_y_size * pipe_z_size * pipe_a_size / array_size;
        
        return new pipewash_scrub_handle( sp_p.release(), data.release(), s_p.release(), log, slices_per_array, array_size, num_of_arrays );
    }
    catch( std::exception &e )
    {
        std::cerr << "SCRUB: An unexpected error occurred: " << e.what() << "\n";
        return 0;
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        std::cerr << "SCRUB: An unexpected error occurred: " << e.what() << "\n";
        return 0;
    }
}

int pipewash_scrub_get_array_size( pipewash_scrub_handle *pwh )
{
    return pwh->array_size;
}

int pipewash_scrub_get_slices_per_array( pipewash_scrub_handle *pwh )
{
    return pwh->slices_per_array;
}

int pipewash_scrub_proc( pipewash_scrub_handle *pwh, float *data )
{
    try
    {
        if( !pwh->array_num )
        {
            std::cerr << "SCRUB:          0%                                                100%\n";
            std::cerr << "Calculating...  " << std::flush;
            pwh->char_count = 0;
        }
        else
        {
            int done_chars = ( int ) floorf( ( float ) pwh->array_num * 50.0F / ( float ) pwh->num_of_arrays );
            for( ; pwh->char_count < done_chars; pwh->char_count++ )
                std::cerr << "X" << std::flush;            
        }
        
       
        scrubber &s = *( pwh->s );
        pwh->data->SetDataFromCopyPtr( data );
        pwh->ne( data );
        pwh->initial_noise_levels.push_back( pwh->ne.noise_floor );

        s( *( pwh->data ) );
        
        boost::scoped_ptr< float > out_data( pwh->data->GetDataCopyPtr() );
        memcpy( data, out_data.get(), pwh->array_size * sizeof( float ) );
        pwh->ne( data );
        pwh->final_noise_levels.push_back( pwh->ne.noise_floor );
        
        if( ++pwh->array_num == pwh->num_of_arrays )
        {
            std::cerr << "\n" << std::flush;
            
            details::vector_noise_estimator vne;
            vne( pwh->initial_noise_levels );
            float noise_level = vne.noise_mean + vne.noise_std_dev;
            
            float artifacts_initial = 0.0F, artifacts_final = 0.0F;
            for( std::vector< float >::iterator i = pwh->initial_noise_levels.begin(), j = pwh->final_noise_levels.begin(); i != pwh->initial_noise_levels.end(); ++i, ++j )
            {
                float nf_start = *i;
                float nf_end = *j;
                artifacts_initial += nf_start > noise_level ? nf_start - noise_level : 0;
                artifacts_final += nf_end > noise_level ? nf_end - noise_level : 0;
            }
            float suppression = ( artifacts_initial - artifacts_final ) * 100.0F / artifacts_initial;
            if( suppression > 100.0F ) suppression = 100.0F;
            std::cerr << "Approximate artifact suppression: " << std::fixed << std::setprecision( 0 ) << ( artifacts_initial - artifacts_final ) * 100.0F / ( artifacts_initial ) << "%\n";
        }
        
        return 0;
    }
    catch( std::exception &e )
    {
        std::cerr << "SCRUB: An unexpected error occurred: " << e.what() << "\n";
        return 1;
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        std::cerr << "SCRUB: An unexpected error occurred: " << e.what() << "\n";
        return 1;
    }
}

int pipewash_scrub_done( pipewash_scrub_handle *pwh )
{
    delete pwh;
    
    return 0;
}
