/*  Portions Copyright (c) 2012, Brian E. Coggins, Ph.D.  All rights reserved. */

/***/
/* userproc.h: function table for NMRPipe system.
/***/

/***/
/* The following function will be used by nmrPipe to extract
/* a pointer to the table of functions.
/***/

struct ProcFuncInfo *getFuncList();

/***/
/*-------------- Addition of New Functions ---------------------------------
/***/

/***/
/* Processing Function Codes, Names, and Descriptions.
/* Adjust the two declarations below to add a new function.
/*
/* The function should be of the form:
/*
/*   int procFunc( struct ProcDataInfo *dataInfo )
/*
/* It will be called repeatedly by the nmrPipe program to perform
/* initialization, processing, output (if needed) and cleanup procedures.
/* The action to be taken is indicated by dataInfo->sliceCode:
/*
/* FUNCTION RETURN VALUES:
/*
/*   Returns zero on sucessful completion.
/*   Returns non-zero on failure.
/*
/* INPUT/OUTPUT:
/*
/*   Use stderr to send all informational messages.
/*   Use command line arguments for extracting user input.
/*
/* FN_PARAMS:
/*
/*   Extract any arguments from command line when sliceCode = FN_PARAMS. 
/*   Display help text if "-help" flag is found in argument list.
/*   Return non-zero if "-help" flag is found.
/*
/* FN_INIT:
/*
/*   Adjust file header for post process state when sliceCode = FN_INIT.
/*   Perform pre-processing initialization when sliceCode = FN_INIT.
/*
/* FN_SHUTDOWN:
/*
/*   Perform shutdown and deallocation when sliceCode = FN_SHUTDOWN.
/*
/* PROCESSING:
/*
/*   Perform a 1D process when sliceCode is greater than zero.
/*   Write the 1D slice after 1D process when sliceCode is greater than zero.
/*   The sliceCode will start at 1 and end at dataInfo->sliceCount. 
/***/

/***/
/* Declaration 1: the function prototypes (if the function is not 
/*                defined somewhere else such as a header file):
/***/

int uNULL(), uCLEAN(), uSCRUB(), uFT(), uZF(), uXY2YX(), uXYZ2ZYX(), uFDM();

/***/
/* Declaration 2: the function descriptions:
/*
/*  C-Function-Name,        "Cmnd-Line-Invoke-Name",
/*   "Func-Descrption",     "[Cmnd-Line-Flags]", . . . 
/***/

static struct ProcFuncInfo uprocList[] = 
   {
    uNULL,                    "NULL",
     "Null Function",         "",
    uCLEAN,                   "CLEAN",
     "CLEAN",                 "[-pattern file ...]",
    uSCRUB,                   "SCRUB",
     "SCRUB",                 "[-pattern file ...]",
    uZF,                      "ZF",
     "Zero Fill",             "[-zf count | -size xSize -inv ETC...]",
    uFT,                      "FT",
     "Fourier Transform",     "[-inv -real -alt -neg]",
    uXY2YX,                   "TP YTP XY2YX",
     "2D Transpose XY->YX",   "[-hyper -nohyper -auto]",
    uXYZ2ZYX,                 "ZTP XYZ2ZYX",
     "3D Transpose XYZ->ZYX", "",
    NULL,                     (char *) NULL,
     (char *) NULL,           (char *) NULL
   };
