//  Copyright (c) 2012, Brian E. Coggins, Ph.D.  All rights reserved.

#include <vector>
#include <string>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"
#include "console_support.h"

std::vector< std::vector< int > > parse_position_list( std::vector< std::string > pos_list_str, int num_of_index_dims )
{
    using namespace boost::spirit::qi;
    typedef std::string::iterator it_t;
    rule< it_t, unused_type() >                 delimiter =         ascii::char_( ',' );
    rule< it_t, std::vector< int >() >          int_list =          int_ >> *( delimiter >> int_ );
    
    std::vector< std::vector< int > > pos_list;
    for( std::vector< std::string >::iterator i = pos_list_str.begin(); i != pos_list_str.end(); ++i )
    {
        std::string &current_pos_str = *i;
        std::vector< int > i_data;
        if( phrase_parse( current_pos_str.begin(), current_pos_str.end(), int_list, ascii::space, i_data ) )
        {
            if( i_data.size() == num_of_index_dims )
                pos_list.push_back( i_data );
            else
                throw invalid_pos_string( current_pos_str );
        }
        else
            throw invalid_pos_string( current_pos_str );
    }
    
    return pos_list;
}

std::vector< std::string > generate_position_filenames( std::vector< std::vector< int > > pos_list, std::string output_filename )
{
    boost::filesystem::path output_file_path( output_filename );
    boost::filesystem::path output_file_stem( output_file_path.stem() ), output_file_parent_path( output_file_path.parent_path() ), output_file_ext( output_file_path.extension() );
    if( output_file_stem.extension().string() == ".3d" || output_file_stem.extension().string() == ".3D" )
    {
        output_file_ext = boost::filesystem::path( ".3d" + output_file_ext.string() );
        output_file_stem = output_file_stem.stem();
    }
    std::vector< std::string > output_filenames;
    for( std::vector< std::vector< int > >::iterator i = pos_list.begin(); i != pos_list.end(); ++i )
    {
        std::string composed_filename( output_file_stem.string() );
        for( std::vector< int >::iterator j = i->begin(); j != i->end(); ++j )
            composed_filename += boost::str( boost::format( "_%03i" ) % *j );
        composed_filename += output_file_ext.string();
        boost::filesystem::path composed_path = output_file_parent_path / boost::filesystem::path( composed_filename );
        output_filenames.push_back( composed_path.string() );
    }
    return output_filenames;
}

void create_position_files( std::vector< std::string > output_filenames, BEC_Misc::owned_ptr_vector< BEC_NMRData::NMRData > &output_files, const BEC_NMRData::NMRData &input, nmr_wash::dimension_map dm, bool overwrite )
{
    using namespace BEC_NMRData;
    using namespace nmr_wash;
    using namespace BEC_Misc;
    
    std::vector< int > sparse_dims = dm.get_sparse_dim_nums();
    
    for( std::vector< std::string >::iterator i = output_filenames.begin(); i != output_filenames.end(); ++i )
    {
        std::string filename( *i );
        
        boost::filesystem::path file_path( filename );
        if( exists( file_path ) && !overwrite )
            throw file_exists( filename );
        
        stack_ptr< NMRData > out( NMRData::GetObjForFileType( filename ) );
        out->SetNumOfDims( ( int ) sparse_dims.size() );
        for( int j = 0; j < sparse_dims.size(); j++ )
        {
            int dim = sparse_dims[ j ];
            out->SetDimensionSize( j, input.GetDimensionSize( dim ) );
            out->SetDimensionLabel( j, input.GetDimensionLabelStr( dim ) );
            out->SetDimensionSW( j, input.GetDimensionSW( dim ) );
            out->SetDimensionSF( j, input.GetDimensionSF( dim ) );
            out->SetDimensionCenterPPM( j, input.GetDimensionCenterPPM( dim ) );
        }
        
        out->CreateFile( filename );
        
        output_files.push_back( out );
    }
}
