//  Copyright (c) 2006-2014, Brian E. Coggins, Ph.D.  All rights reserved.

#include <memory.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <scitbx/fftpack/complex_to_complex.h>
#include <scitbx/fftpack/complex_to_complex_3d.h>
#include <scitbx/array_family/ref.h>
#include <scitbx/array_family/accessors/c_grid.h>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"

using namespace BEC_Misc;
using namespace BEC_NMRData;
using namespace nmr_wash;
using namespace nmr_wash::details;


psf_calc::psf_calc( const sampling_pattern &pattern, const dimension_map dim_map, int xsize_, int ysize_, int zsize_, float xsw_, float ysw_, float zsw_ ) : sp( pattern ), dm( dim_map ), xsw( xsw_ ), ysw( ysw_ ), zsw( zsw_ ), comp_scale( 1.0F )
{
    xsize = xsize_ ? xsize_ : 1;
    ysize = ysize_ ? ysize_ : 1;
    zsize = zsize_ ? zsize_ : 1;
    
    xis = ysize * ( ysize > 1 ? 2 : 1 ) * zsize * ( zsize > 1 ? 2 : 1 );
    yis = zsize * ( zsize > 1 ? 2 : 1 );
    size = xsize * 2 * ysize * ( ysize > 1 ? 2 : 1 ) * zsize * ( zsize > 1 ? 2 : 1 );
    
    xtmax = dm.xf( sp.get_u_max_value(), sp.get_v_max_value(), sp.get_w_max_value() );
    ytmax = dm.yf( sp.get_u_max_value(), sp.get_v_max_value(), sp.get_w_max_value() );
    ztmax = dm.zf( sp.get_u_max_value(), sp.get_v_max_value(), sp.get_w_max_value() );
    
    if( !xtmax ) xtmax = 1.0F;
    if( !ytmax ) ytmax = 1.0F;
    if( !ztmax ) ztmax = 1.0F;
}

boost::shared_array< float > psf_calc::psf()
{
    if( stored_psf )
        return stored_psf;

    const std::vector< sampling_point > &points = sp.get_points();
    size_t pattern_size = points.size();
    
    if( sp.is_on_grid() )
    {
        grid_hc_fft gfft( xsize * 2, ysize > 1 ? ysize * 2 : 1, zsize > 1 ? zsize * 2 : 1 );
        
        for( size_t i = 0; i < pattern_size; i++ )
        {
            sampling_point p = points[ i ];
            
            int xe = dm.x( p.u, p.v, p.w ) * 2;
            int ye = dm.y( p.u, p.v, p.w ) * 2;
            int ze = dm.z( p.u, p.v, p.w ) * 2;
            
            //  The size of each dimension is doubled and points are spaced by a factor of two
            //  in the TD so that the FD will have double the width.  We could accommodate points
            //  up to xsize/ysize/zsize previously (which become xsize * 2, ysize * 2, and zsize * 2),
            //  but this is not possible now that we are inverse-transforming the pure component.
            //  That inverse transform mirrors points from 0 to xsize into xsize to xsize * 2, so any
            //  points beyond xsize / 2 before doubling (xsize after doubling) would corrupt the
            //  inverse transform and integral.  Users are advised to zero-fill by at least 2x, as
            //  always in NMR.
            //  if( xe >= xsize * 2 || ye >= ysize * 2 || ze >= zsize * 2 )
            if( xe >= xsize || ye >= ysize || ze >= zsize )
                continue;
            
            float x = dm.x( p.u, p.v, p.w );
            float y = dm.y( p.u, p.v, p.w );
            float z = dm.z( p.u, p.v, p.w );
            
            gfft( xe, ye, ze ) = p.weight * dm.x_apod( x ) * dm.y_apod( y ) * dm.z_apod( z );
        }
        
        gfft.ft_di_x();
        if( ysize > 1 ) gfft.ft_di_y();
        if( zsize > 1 ) gfft.ft_di_z();
        stored_psf = gfft.get_data();
    }
    else        //  off-grid
    {
        if( sp.get_num_of_dims() == 3 )
        {
            boost::shared_array< float > new_psf( new float[ size ] );
            float *npsfd = new_psf.get();
            memset( npsfd, 0, sizeof( float ) * size );
            size_t offset = 0;
            for( int xpos = 0; xpos < xsize * 2; xpos++ )
                for( int ypos = 0; ypos < ysize * 2; ypos++ )
                    for( int zpos = 0; zpos < zsize * 2; zpos++ )
                    {
                        for( size_t ipos = 0; ipos < pattern_size; ipos++ )
                        {
                            sampling_point p = points[ ipos ];
                            
                            float xt = dm.xf( p.uf, p.vf, p.wf );
                            float yt = dm.yf( p.uf, p.vf, p.wf );
                            float zt = dm.zf( p.uf, p.vf, p.wf );
                            
                            float xf = -xsw + ( float ) xpos * xsw / ( ( float ) xsize );
                            float yf = -ysw + ( float ) ypos * ysw / ( ( float ) ysize );
                            float zf = -zsw + ( float ) zpos * zsw / ( ( float ) zsize );
                            
                            float ft_coef_pppc = cosf( -2.0F * pi_f * (  xf * xt + yf * yt + zf * zt ) );
                            float ft_coef_mppc = cosf( -2.0F * pi_f * ( -xf * xt + yf * yt + zf * zt ) );
                            float ft_coef_pmpc = cosf( -2.0F * pi_f * (  xf * xt - yf * yt + zf * zt ) );
                            float ft_coef_mmpc = cosf( -2.0F * pi_f * ( -xf * xt - yf * yt + zf * zt ) );

                            float weight = p.weight * dm.x_apod( xt ) * dm.y_apod( yt ) * dm.z_apod( zt );
                            
                            npsfd[ offset ] += weight * ( ft_coef_pppc + ft_coef_mppc + ft_coef_pmpc + ft_coef_mmpc );
                        }
                        offset++;
                    }
            stored_psf = new_psf;
        }
        else if( sp.get_num_of_dims() == 2 )
        {
            boost::shared_array< float > new_psf( new float[ size ] );
            float *npsfd = new_psf.get();
            memset( npsfd, 0, sizeof( float ) * size );
            size_t offset = 0;
            for( int xpos = 0; xpos < xsize * 2; xpos++ )
                for( int ypos = 0; ypos < ysize * 2; ypos++ )
                {
                    for( size_t ipos = 0; ipos < pattern_size; ipos++ )
                    {
                        sampling_point p = points[ ipos ];
                        
                        float xt = dm.xf( p.uf, p.vf, p.wf );
                        float yt = dm.yf( p.uf, p.vf, p.wf );
                        
                        float xf = -xsw + ( float ) xpos * xsw / ( ( float ) xsize );
                        float yf = -ysw + ( float ) ypos * ysw / ( ( float ) ysize );
                        
                        float ft_coef_ppc = cosf( -2.0F * pi_f * (  xf * xt + yf * yt ) );
                        float ft_coef_mpc = cosf( -2.0F * pi_f * ( -xf * xt + yf * yt ) );
                        
                        float weight = p.weight * dm.x_apod( xt ) * dm.y_apod( yt );
                        
                        npsfd[ offset ] += weight * ( ft_coef_ppc + ft_coef_mpc );
                    }
                    offset++;
                }
            
            stored_psf = new_psf;
        }
        else if( sp.get_num_of_dims() == 1 )
        {
            boost::shared_array< float > new_psf( new float[ size ] );
            float *npsfd = new_psf.get();
            memset( npsfd, 0, sizeof( float ) * size );
            size_t offset = 0;
            for( int xpos = 0; xpos < xsize * 2; xpos++ )
            {
                for( size_t ipos = 0; ipos < pattern_size; ipos++ )
                {
                    sampling_point p = points[ ipos ];
                    float xt = dm.xf( p.uf, p.vf, p.wf );
                    float xf = -xsw + ( float ) xpos * xsw / ( ( float ) xsize );
                    float ft_coef_pc = cosf( -2.0F * pi_f * (  xf * xt ) );
                    float weight = p.weight * dm.x_apod( xt );
                    npsfd[ offset ] += weight * ( ft_coef_pc );
                }
                offset++;
            }

            stored_psf = new_psf;
        }
    }
    
    float *psf_direct = stored_psf.get();

    float max = 0.0F;
    for( size_t i = 0; i < size; i++ )
        if( fast_abs( psf_direct[ i ] ) > max )
            max = psf_direct[ i ];
    
    float scale = 1.0F / max;
    
    for( size_t i = 0; i < size; i++ )
        psf_direct[ i ] *= scale;
    
    return stored_psf;
}

boost::shared_array< float > psf_calc::pure_component_contour_irregular( float level, bool margin )
{
    boost::shared_array< float > pure_comp( new float[ size ] );
    memset( pure_comp.get(), 0, sizeof( float ) * size );
    
    if( !stored_psf )
        psf();
    
    const float *psf_direct = stored_psf.get();
    float *pc = pure_comp.get();
    
    details::noise_estimator ne( ( size_t ) size, ( size_t ) size );
    ne( psf_direct );
    float nf = ne.avg + ne.std_dev;
    if( level < nf * 100.0F ) level = nf * 100.0F;
    
    size_t center_offset = xsize * xis + ( ysize > 1 ? ysize : 0 ) * yis + ( zsize > 1 ? zsize : 0 );
    
    position_map candidate_positions, accepted_positions;
    std::queue< position > examination_queue;
    examination_queue.push( position( center_offset, xsize, ysize, zsize ) );
    
    while( !examination_queue.empty() )
    {
        position &p = examination_queue.front();
        if( accepted_positions.find( p.offset ) != accepted_positions.end() )
            continue;
        
        if( psf_direct[ p.offset ] >= level * 0.01F )
        {
            accepted_positions.insert( position_map_entry( p.offset, p ) );
            
            std::vector< position > neighbors = get_neighboring_positions( p );
            for( std::vector< position >::iterator np = neighbors.begin(); np != neighbors.end(); ++np )
            {
                if( accepted_positions.find( np->offset ) != accepted_positions.end() )
                    continue;
                if( candidate_positions.find( np->offset ) != candidate_positions.end() )
                    continue;
                
                candidate_positions.insert( position_map_entry( np->offset, *np ) );
                examination_queue.push( *np );
            }
        }
        
        examination_queue.pop();
    }
    
    if( margin )
        for( position_map::iterator i = candidate_positions.begin(); i != candidate_positions.end(); ++i )
            if( accepted_positions.find( i->first ) == accepted_positions.end() )
                accepted_positions.insert( *i );
    
    for( position_map::iterator i = accepted_positions.begin(); i != accepted_positions.end(); ++i )
        pc[ i->first ] = psf_direct[ i->first ];
    
    calc_pc_scale_factor( pure_comp );

    return pure_comp;
}

boost::shared_array< float > psf_calc::pure_component_contour_ellipsoid( float level, float margin )
{
    boost::shared_array< float > pure_comp( new float[ size ] );
    memset( pure_comp.get(), 0, sizeof( float ) * size );
    
    if( !stored_psf )
        psf();
    
    const float *psf_direct = stored_psf.get();
    float *pc = pure_comp.get();
    
    details::noise_estimator ne( ( size_t ) size, ( size_t ) size );
    ne( psf_direct );
    float nf = ne.avg + ne.std_dev;
    if( level < nf * 100.0F ) level = nf * 100.0F;
    
    size_t center_offset = xsize * xis + ( ysize > 1 ? ysize : 0 ) * yis + ( zsize > 1 ? zsize : 0 );
    
    position_map candidate_positions, accepted_positions;
    std::queue< position > examination_queue;
    examination_queue.push( position( center_offset, xsize, ysize > 1 ? ysize : 0, zsize > 1 ? zsize : 0 ) );
        
    while( !examination_queue.empty() )
    {
        position &p = examination_queue.front();
        if( accepted_positions.find( p.offset ) != accepted_positions.end() )
        {
            continue;
        }
        
        if( psf_direct[ p.offset ] >= level * 0.01F )
        {
            accepted_positions.insert( position_map_entry( p.offset, p ) );
            
            std::vector< position > neighbors = get_neighboring_positions( p );
            for( std::vector< position >::iterator np = neighbors.begin(); np != neighbors.end(); ++np )
            {
                if( accepted_positions.find( np->offset ) != accepted_positions.end() )
                    continue;
                if( candidate_positions.find( np->offset ) != candidate_positions.end() )
                    continue;
                
                candidate_positions.insert( position_map_entry( np->offset, *np ) );
                examination_queue.push( *np );
            }
        }
        
        examination_queue.pop();
    }
    
    float r_max = 0.0F;
    for( position_map::iterator i = accepted_positions.begin(); i != accepted_positions.end(); ++i )
    {
        float x_diff = ( float ) i->second.xpos - ( float ) xsize;
        float y_diff = ysize > 1 ? ( float ) i->second.ypos - ( float ) ysize : 0;
        float z_diff = zsize > 1 ? ( float ) i->second.zpos - ( float ) zsize : 0;
        float r = sqrtf( x_diff * x_diff + y_diff * y_diff + z_diff * z_diff );
        if( r > r_max ) r_max = r;
    }
    if( r_max <= 0.0F ) r_max = 0.0001F;
    
    float a_min = r_max, b_min = ysize > 1 ? r_max : 0.0F, c_min =  zsize > 1 ? r_max : 0.0F;
    for( float a = r_max; a >= 0.0F; a -= 0.1F )
        for( float b = ysize > 1 ? r_max : 0; b >= 0.0F; b -= 0.1F )
            for( float c = zsize > 1 ? r_max : 0; c >= 0.0F; c -= 0.1F )
            {
                bool all_enclosed = true;
                for( position_map::iterator i = accepted_positions.begin(); i != accepted_positions.end(); ++i )
                {
                    int x = i->second.xpos;
                    int y = i->second.ypos;
                    int z = i->second.zpos;
                    float xd = ( float ) ( x - xsize ) * ( float ) ( x - xsize ) / ( a * a );
                    float yd = b ? ( float ) ( y - ysize ) * ( float ) ( y - ysize ) / ( b * b ) : 0;
                    float zd = c ? ( float ) ( z - zsize ) * ( float ) ( z - zsize ) / ( c * c ) : 0;
                    if( sqrtf( xd + yd + zd ) > 1.0F )
                    {
                        all_enclosed = false;
                        break;
                    }
                }
                if( all_enclosed )
                {
                    if( a < a_min ) a_min = a;
                    if( b < b_min ) b_min = b;
                    if( c < c_min ) c_min = c;
                }
            }
    
    margin = 1.0F + margin * 0.01F;
    float a = a_min * margin;
    float b = b_min * margin;
    float c = c_min * margin;
    
    for( int x = xsize - ( int ) ceilf( a ); x < xsize + ( int ) ceilf( a ) + 1; x++ )
        for( int y = ( ysize > 1 ? ysize - ( int ) ceilf( b ) : 0 ); y < ( ysize > 1 ? ysize + ( int ) ceilf( b ) + 1 : 1 ); y++ )
            for( int z = ( zsize > 1 ? zsize - ( int ) ceilf( c ) : 0 ); z < ( zsize > 1 ? zsize + ( int ) ceilf( c ) + 1 : 1 ); z++ )
            {
                if( x < 0 || x >= xsize * 2 || y < 0 || y >= ysize * 2 || z < 0 || z >= zsize * 2 )
                    continue;
                float xd = ( float ) ( x - xsize ) * ( float ) ( x - xsize ) / ( a * a );
                float yd = b ? ( float ) ( y - ysize ) * ( float ) ( y - ysize ) / ( b * b ) : 0.0F;
                float zd = c ? ( float ) ( z - zsize ) * ( float ) ( z - zsize ) / ( c * c ) : 0.0F;
                if( sqrtf( xd + yd + zd ) <= 1.0F )
                {
                    size_t offset = x * xis + y * yis + z;
                    pc[ offset ] = psf_direct[ offset ];
                }
            }
    
    calc_pc_scale_factor( pure_comp );
    
    return pure_comp;
}

boost::shared_array< float > psf_calc::pure_component_fixed_ellipsoid( float radius )
{
    boost::shared_array< float > pure_comp( new float[ size ] );
    memset( pure_comp.get(), 0, sizeof( float ) * size );
    
    if( !stored_psf )
        psf();
    
    const float *psf_direct = stored_psf.get();
    float *pc = pure_comp.get();
    
    radius *= 0.01F;
    float a = radius * ( float ) xsize, b = radius * ( float ) ysize, c = radius * ( float ) zsize;
    
    for( int x = xsize - ceilf( a ); x < xsize + ceilf( a ); x++ )
        for( int y = ysize - ceilf( b ); y < ysize + ceilf( b ); y++ )
            for( int z = zsize - ceilf( c ); z < zsize + ceilf( c ); z++ )
                if( sqrtf( 
                          ( float ) ( x - xsize ) * ( float ) ( x - xsize ) / ( a * a ) +
                          ( float ) ( y - ysize ) * ( float ) ( y - ysize ) / ( b * b ) +
                          ( float ) ( z - zsize ) * ( float ) ( z - zsize ) / ( c * c ) ) <= 1.0F )
                {
                    size_t offset = x * xis + y * yis + z;
                    pc[ offset ] = psf_direct[ offset ];
                }
    
    
    calc_pc_scale_factor( pure_comp );
    
    return pure_comp;
}

boost::shared_array< float > psf_calc::pure_component_delta_function()
{
    boost::shared_array< float > pure_comp( new float[ size ] );
    memset( pure_comp.get(), 0, sizeof( float ) * size );
    
    if( !stored_psf )
        psf();
    
    const float *psf_direct = stored_psf.get();
    float *pc = pure_comp.get();

    size_t center_offset = xsize * xis + ( ysize > 1 ? ysize : 0 ) * yis + ( zsize > 1 ? zsize : 0 );
    pc[ center_offset ] = psf_direct[ center_offset ];
    
    calc_pc_scale_factor( pure_comp );
    
    return pure_comp;
}

void psf_calc::set_psf( boost::shared_array<float> psf_ )
{
    assert( psf_ );
    stored_psf = psf_;
}

size_t psf_calc::get_psf_size() const
{
    return size;
}

std::vector< position > psf_calc::get_neighboring_positions( position p )
{
    std::vector< position > list;
    
    for( int x = p.xpos - 1; x <= p.xpos + 1; x++ )
        for( int y = p.ypos - 1; y <= p.ypos + 1; y++ )
            for( int z = p.zpos - 1; z <= p.zpos + 1; z++ )
                if( x >= 0 && x < xsize * 2 &&
                   y >= 0 && y < ( ysize > 1 ? ysize * 2 : ysize ) &&
                   z >= 0 && z < ( zsize > 1 ? zsize * 2 : zsize ) &&
                   !( x == p.xpos && y == p.ypos && z == p.zpos ) )
                    list.push_back( position( x * xis + y * yis + z, x, y, z ) );
    
    return list;
}

float psf_calc::calc_pc_scale_factor( boost::shared_array< float > pure_comp )
{
    //  Compute inverse transform of the PC (should give envelope)
    grid_hc_fft gfft( xsize * 2, ysize > 1 ? ysize * 2 : 1, zsize > 1 ? zsize * 2 : 1, true );
    gfft.set_data( pure_comp );
    gfft.ift_ad_x();
    if( ysize > 1 ) gfft.ift_ad_y();
    if( zsize > 1 ) gfft.ift_ad_z();
    
    //  Compute increment sizes, with built-in scaling for complex components as needed
    int xisc = ( xsize > 1 ? 2 : 1 ) * ysize * ( ysize > 1 ? 4 : 1 ) * zsize * ( zsize > 1 ? 4 : 1 );
    int yisc = ( ysize > 1 ? 2 : 1 ) * zsize * ( zsize > 1 ? 4 : 1 );
    int zisc = ( zsize > 1 ? 2 : 1 );

    //  Rescale data
    float *d = gfft.get_data().get();
    float max = 0.0F;
    for( size_t i = 0; i < size; i++ )
        if( fast_abs( d[ i ] ) > max )
            max = d[ i ];
    float scale = 1.0F / max;
    gfft.scale_data( scale );
    
    //  Integrate real components of TD over x -> xsize, etc.
    //  TD extends to xsize * 2, etc., but the other quadrants are mirror-images.
    //  Simultaneously correct for increases in intensity on the edges due to overlap of mirror images
    float env_integral = 0.0F;
    for( int x = 0; x < xsize; x += 2 )
        for( int y = 0; y < ysize; y += 2 )
            for( int z = 0; z < zsize; z += 2 )
                env_integral += d[ x * xisc + y * yisc + z * zisc ] * ( xsize > 1 && !x ? 0.5F : 1.0F ) * ( ysize > 1 && !y ? 0.5F : 1.0F ) * ( zsize > 1 && !z ? 0.5F : 1.0F );

    //  Integrate sampling pattern weights
    float sp_integral = 0.0F;
    for( size_t i = 0; i < sp.size(); i++ )
        sp_integral += sp[ i ].weight;

    //  Calculate scaling factor
    comp_scale = env_integral / sp_integral;
    
    return comp_scale;
}

details::grid_hc_fft::grid_hc_fft( int xsize_, int ysize_, int zsize_, bool fd ) : xsize( xsize_ ), ysize( ysize_ ), zsize( zsize_ )
{
    if( fd )
    {
        xc = yc = zc = false;
        comp = 1;
    }
    else
    {
        xc = true;
        yc = ysize > 1 ? true : false;
        zc = zsize > 1 ? true : false;
        comp = zc ? 8 : yc ? 4 : 2;
    }

    data.reset( new float[ xsize * ysize * zsize * comp ] );
}

float &details::grid_hc_fft::operator()( int x, int y, int z )
{
    int xis = ysize * ( yc ? 2 : 1 ) * zsize * ( zc ? 2 : 1 );
    int yis = zsize * ( zc ? 2 : 1 );
    return data[ ( xc ? x * 2 : x ) * xis + ( yc ? y * 2 : y ) * yis + ( zc ? z * 2 : z ) ];
}

boost::shared_array< float > details::grid_hc_fft::get_data()
{
    return data;
}

void details::grid_hc_fft::set_data( boost::shared_array< float > input )
{
    memcpy( data.get(), input.get(), sizeof( float ) * xsize * ysize * zsize * comp );
}

void details::grid_hc_fft::scale_data( float scale_factor )
{
    float *d = data.get();
    for( size_t i = 0; i < xsize * ysize * zsize * comp; i++ )
        d[ i ] *= scale_factor;
}

void details::grid_hc_fft::ft_di_x()
{
    //  Assuming X is complex and time domain, compute FT in X dimension and discard imaginary values
    assert( xc );

    namespace fftpack = scitbx::fftpack;

    //  Calculate increment sizes for offsets into input (XYZ) and transposed (YZX) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    int xis_in = ysize * ( yc ? 2 : 1 ) * zsize * ( zc ? 2 : 1 );
    int yis_in = zsize * ( zc ? 2 : 1 );
    int yis_yzx = zsize * ( zc ? 2 : 1 ) * xsize * 2;
    int zis_yzx = xsize * 2;

    //  Transpose XYZ input data into new YZX array
    //  xe/ye/ze are "expanded" coordinates when the corresponding dimensions are complex and need to be expanded
    //  ycomp/zcomp select the correct components when corresponding dimensions are complex
    //  X must be complex, so both X components are copied explicitly
    boost::shared_array< float > yzx_data( new float[ xsize * ysize * zsize * comp ] );
    memset( yzx_data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xe = x * 2;
                        int ye = yc ? y * 2 : y;
                        int ze = zc ? z * 2 : z;
                        yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xe ] = data[ xe * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ];
                        yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xe + 1 ] = data[ ( xe + 1 ) * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ];
                    }

    //  Compute FT: iterate over Y and Z coordinates/components and process each complex X vector of xsize complex values
    fftpack::complex_to_complex< float > fft( xsize );
    for( int y = 0; y < ysize; y++ )
        for( int z = 0; z < zsize; z++ )
            for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                {
                    int ye = yc ? y * 2 : y;
                    int ze = zc ? z * 2 : z;
                    fft.forward( &( yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx ] ) );
                }
    
    //  Transpose FTed YZX data back to XYZ data
    //  xs = x coordinate in "source" (YZX), which requires a circular shift
    //  xd = x coordinate in "destination" (XYZ), which must be reversed
    //  ye/ze are "expanded" coordinates, and ycomp/zcomp select components, both applying only when corresponding dimensions are complex
    //  note that X imaginaries in FD are discarded, hence:
    //      (1) only a single copy step for each X coordinate,
    //      (2) xs coordinates must be scaled by 2, 
    //      (3) xd coordinates are NOT scaled by 2
    comp /= 2;
    xc = false;
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xs = ( x + xsize / 2 >= xsize ? x - xsize / 2 : x + xsize / 2 ) * 2;
                        int xd = x ? xsize - x : 0;
                        int ye = yc ? y * 2 : y;
                        int ze = zc ? z * 2 : z;
                        data[ xd * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ] = yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xs ];
                    }
    
}

void details::grid_hc_fft::ft_di_y()
{
    //  Assuming Y is complex and time domain, compute FT in Y dimension and discard imaginary values
    assert( yc );
    
    namespace fftpack = scitbx::fftpack;
    
    //  Calculate increment sizes for offsets into input (XYZ, Y complex), transposed (XZY), and output (XYZ, Y not complex) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    //  Between input and output, only the X increment size changes
    int xis_in = ysize * 2 * zsize * ( zc ? 2 : 1 );
    int yis_in = zsize * ( zc ? 2 : 1 );
    int xis_xzy = zsize * ( zc ? 2 : 1 ) * ysize * 2;
    int zis_xzy = ysize * 2;
    int xis_out = ysize * zsize * ( zc ? 2 : 1 );
    
    //  Transpose XYZ input data into new XZY array
    //  xe/ye/ze are "expanded" coordinates when the corresponding dimensions are complex and need to be expanded
    //  xcomp/zcomp select the correct components when corresponding dimensions are complex
    //  Y must be complex, so both Y components are copied explicitly
    boost::shared_array< float > xzy_data( new float[ xsize * ysize * zsize * comp ] );
    memset( xzy_data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xe = xc ? x * 2 : x;
                        int ye = y * 2;
                        int ze = zc ? z * 2 : z;
                        xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + ye ] = data[ ( xe + xcomp ) * xis_in + ye * yis_in + ze + zcomp ];
                        xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + ye + 1 ] = data[ ( xe + xcomp ) * xis_in + ( ye + 1 ) * yis_in + ze + zcomp ];
                    }
    
    //  Compute FT: iterate over X and Z coordinates/components and process each complex Y vector of ysize complex values
    fftpack::complex_to_complex< float > fft( ysize );
    for( int x = 0; x < xsize; x++ )
        for( int z = 0; z < zsize; z++ )
            for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                {
                    int xe = xc ? x * 2 : x;
                    int ze = zc ? z * 2 : z;
                    fft.forward( &( xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy ] ) );
                }
    
    //  Transpose FTed XZY data back to XYZ data
    //  ys = y coordinate in "source" (XZY), which requires a circular shift
    //  yd = y coordinate in "destination" (XYZ), which must be reversed
    //  xe/ze are "expanded" coordinates, and xcomp/zcomp select components, both applying only when corresponding dimensions are complex
    //  note that Y imaginaries in FD are discarded, hence:
    //      (1) only a single copy step for each Y coordinate,
    //      (2) ys coordinates must be scaled by 2, 
    //      (3) yd coordinates are NOT scaled by 2
    comp /= 2;
    yc = false;
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int ys = ( y + ysize / 2 >= ysize ? y - ysize / 2 : y + ysize / 2 ) * 2;
                        int yd = y ? ysize - y : 0;
                        int xe = xc ? x * 2 : x;
                        int ze = zc ? z * 2 : z;
                        data[ ( xe + xcomp ) * xis_out + yd * yis_in + ze + zcomp ] = xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + ys ];
                    }
    
}

void details::grid_hc_fft::ft_di_z()
{
    //  Assuming Z is complex and time domain, compute FT in Z dimension and discard imaginary values
    assert( zc );
    
    namespace fftpack = scitbx::fftpack;
    
    //  Calculate increment sizes for offsets into input (XYZ, Z complex) and output (XYZ, Z not complex) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    int xis_in = ysize * ( yc ? 2 : 1 ) * zsize * 2;
    int yis_in = zsize * 2;
    int xis_out = ysize * ( yc ? 2 : 1 ) * zsize;
    int yis_out = zsize;
    
    //  Compute FT: iterate over X and Y coordinates/components and process each complex Z vector of zsize complex values
    fftpack::complex_to_complex< float > fft( zsize );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                {
                    int xe = xc ? x * 2 : x;
                    int ye = yc ? y * 2 : y;
                    fft.forward( &( data[ ( xe + xcomp ) * xis_in + ( ye + ycomp ) * yis_in ] ) );
                }
    
    //  Reorder FTed Z points and discard Z imaginary values
    //  zs = z coordinate in "source", which requires a circular shift
    //  zd = z coordinate in "destination", which must be reversed
    //  xe/ye are "expanded" coordinates, and xcomp/ycomp select components, both applying only when corresponding dimensions are complex
    //  note that Z imaginaries in FD are discarded, hence:
    //      (1) only a single copy step for each Z coordinate,
    //      (2) zs coordinates must be scaled by 2, 
    //      (3) zd coordinates are NOT scaled by 2
    boost::shared_array< float > src( data );
    comp /= 2;
    zc = false;
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    {
                        int zs = ( z + zsize / 2 >= zsize ? z - zsize / 2 : z + zsize / 2 ) * 2;
                        int zd = z ? zsize - z : 0;
                        int xe = xc ? x * 2 : x;
                        int ye = yc ? y * 2 : y;
                        data[ ( xe + xcomp ) * xis_out + ( ye + ycomp ) * yis_out + zd ] = src[ ( xe + xcomp ) * xis_in + ( ye + ycomp ) * yis_in + zs ];
                    }
    
}

void details::grid_hc_fft::ift_ad_x()
{
    //  Assuming X is real and frequency domain, compute inverse FT in X dimension, expanding storage to hold imaginary values
    assert( !xc );
    
    namespace fftpack = scitbx::fftpack;
    
    //  Calculate increment sizes for offsets into input (XYZ) and transposed (YZX) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    int xis_in = ysize * ( yc ? 2 : 1 ) * zsize * ( zc ? 2 : 1 );
    int yis_in = zsize * ( zc ? 2 : 1 );
    int yis_yzx = zsize * ( zc ? 2 : 1 ) * xsize * 2;
    int zis_yzx = xsize * 2;
    
    //  Transpose real, frequency-domain XYZ input data into new complex YZX array
    //  xs = x coordinate in "source" (XYZ), which requires a circular shift
    //  xd = x coordinate in "destination" (YZX), which must be reversed
    //  ye/ze are "expanded" coordinates, and ycomp/zcomp select components, both applying only when corresponding dimensions are complex
    //  note that X imaginaries in FD must be added, but these are assumed to be zero, hence:
    //      (1) only a single copy step for each X coordinate,
    //      (2) xs coordinates are NOT scaled by 2,
    //      (3) xd coordinates must be scaled by 2
    comp *= 2;
    xc = true;
    boost::shared_array< float > yzx_data( new float[ xsize * ysize * zsize * comp ] );
    memset( yzx_data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xs = x - xsize / 2 < 0 ? x + xsize / 2 : x - xsize / 2;
                        int xd = x ? xsize - x : 0;
                        int ye = yc ? y * 2 : y;
                        int ze = zc ? z * 2 : z;
                        yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xd * 2 ] = data[ xs * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ];
                    }
    
    //  Compute inverse FT: iterate over Y and Z coordinates/components and process each complex X vector of xsize complex values
    fftpack::complex_to_complex< float > fft( xsize );
    for( int y = 0; y < ysize; y++ )
        for( int z = 0; z < zsize; z++ )
            for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                {
                    int ye = yc ? y * 2 : y;
                    int ze = zc ? z * 2 : z;
                    fft.backward( &( yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx ] ) );
                }
    
    //  Transpose inverse-FTed YZX data back to XYZ data
    //  xe/ye/ze are "expanded" coordinates when the corresponding dimensions are complex and need to be expanded
    //  ycomp/zcomp select the correct components when corresponding dimensions are complex
    //  X is complex, so both X components are copied explicitly
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xe = x * 2;
                        int ye = yc ? y * 2 : y;
                        int ze = zc ? z * 2 : z;
                        data[ xe * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ] = yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xe ];
                        data[ ( xe + 1 ) * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ] = yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xe + 1 ];
                    }
    
}

void details::grid_hc_fft::ift_ad_y()
{
    //  Assuming Y is real and frequency domain, compute inverse FT in Y dimension, expanding storage to hold imaginary values
    assert( !yc );
    
    namespace fftpack = scitbx::fftpack;
    
    //  Calculate increment sizes for offsets into input (XYZ, Y not complex), transposed (XZY), and output (XYZ, Y complex) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    //  Between input and output, only the X increment size changes
    int xis_in = ysize * zsize * ( zc ? 2 : 1 );
    int yis_in = zsize * ( zc ? 2 : 1 );
    int xis_xzy = zsize * ( zc ? 2 : 1 ) * ysize * 2;
    int zis_xzy = ysize * 2;
    int xis_out = ysize * 2 * zsize * ( zc ? 2 : 1 );
    
    //  Transpose real, frequency-domain XYZ input data into new complex XZY array
    //  ys = y coordinate in "source" (XYZ), which requires a circular shift
    //  yd = y coordinate in "destination" (XZY), which must be reversed
    //  xe/ze are "expanded" coordinates, and xcomp/zcomp select components, both applying only when corresponding dimensions are complex
    //  note that Y imaginaries in FD must be added, but these are assumed to be zero, hence:
    //      (1) only a single copy step for each Y coordinate,
    //      (2) ys coordinates are NOT scaled by 2,
    //      (3) yd coordinates must be scaled by 2
    comp *= 2;
    yc = true;
    boost::shared_array< float > xzy_data( new float[ xsize * ysize * zsize * comp ] );
    memset( xzy_data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int ys = y - ysize / 2 < 0 ? y + ysize / 2 : y - ysize / 2;
                        int yd = y ? ysize - y : 0;
                        int xe = xc ? x * 2 : x;
                        int ze = zc ? z * 2 : z;
                        xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + yd * 2 ] = data[ ( xe + xcomp ) * xis_in + ys * yis_in + ze + zcomp ];
                    }
    
    //  Compute inverse FT: iterate over X and Z coordinates/components and process each complex Y vector of ysize complex values
    fftpack::complex_to_complex< float > fft( ysize );
    for( int x = 0; x < xsize; x++ )
        for( int z = 0; z < zsize; z++ )
            for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                {
                    int xe = xc ? x * 2 : x;
                    int ze = zc ? z * 2 : z;
                    fft.backward( &( xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy ] ) );
                }
    
    //  Transpose inverse-FTed XZY data back to XYZ data
    //  xe/ye/ze are "expanded" coordinates when the corresponding dimensions are complex and need to be expanded
    //  xcomp/zcomp select the correct components when corresponding dimensions are complex
    //  Y must be complex, so both Y components are copied explicitly
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xe = xc ? x * 2 : x;
                        int ye = y * 2;
                        int ze = zc ? z * 2 : z;
                        data[ ( xe + xcomp ) * xis_out + ye * yis_in + ze + zcomp ] = xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + ye ];
                        data[ ( xe + xcomp ) * xis_out + ( ye + 1 ) * yis_in + ze + zcomp ] = xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + ye + 1 ];
                    }
    
}

void details::grid_hc_fft::ift_ad_z()
{
    //  Assuming Z is real and frequency domain, compute inverse FT in Z dimension, expanding storage to hold imaginary values
    assert( !zc );
    
    namespace fftpack = scitbx::fftpack;
    
    //  Calculate increment sizes for offsets into input (XYZ, Z not complex) and output (XYZ, Z complex) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    int xis_in = ysize * ( yc ? 2 : 1 ) * zsize;
    int yis_in = zsize;
    int xis_out = ysize * ( yc ? 2 : 1 ) * zsize * 2;
    int yis_out = zsize * 2;
    
    //  Reorder FD Z points, adding Z imaginary values
    //  zs = z coordinate in "source", which requires a circular shift
    //  zd = z coordinate in "destination", which must be reversed
    //  xe/ye are "expanded" coordinates, and xcomp/ycomp select components, both applying only when corresponding dimensions are complex
    //  note that Z imaginaries in FD must be added, but these are assumed to be zero, hence:
    //      (1) only a single copy step for each Z coordinate,
    //      (2) zs coordinates are NOT scaled by 2,
    //      (3) zd coordinates must be scaled by 2
    boost::shared_array< float > src( data );
    comp *= 2;
    zc = true;
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    {
                        int zs = z - zsize / 2 < 0 ? z + zsize / 2 : z - zsize / 2;
                        int zd = z ? zsize - z : 0;
                        int xe = xc ? x * 2 : x;
                        int ye = yc ? y * 2 : y;
                        data[ ( xe + xcomp ) * xis_out + ( ye + ycomp ) * yis_out + zd * 2 ] = src[ ( xe + xcomp ) * xis_in + ( ye + ycomp ) * yis_in + zs ];
                    }
    
    //  Compute inverse FT: iterate over X and Y coordinates/components and process each complex Z vector of zsize complex values
    fftpack::complex_to_complex< float > fft( zsize );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                {
                    int xe = xc ? x * 2 : x;
                    int ye = yc ? y * 2 : y;
                    fft.backward( &( data[ ( xe + xcomp ) * xis_out + ( ye + ycomp ) * yis_out ] ) );
                }
}

void details::grid_hc_fft::ift_ht_ad_x()
{
    //  Assuming X is real and frequency domain, compute inverse FT in X dimension, after expanding storage to hold imaginary values and after computing the Hilbert transform
    assert( !xc );
    
    namespace fftpack = scitbx::fftpack;
    
    //  Calculate increment sizes for offsets into input (XYZ) and transposed (YZX) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    int xis_in = ysize * ( yc ? 2 : 1 ) * zsize * ( zc ? 2 : 1 );
    int yis_in = zsize * ( zc ? 2 : 1 );
    int yis_yzx = zsize * ( zc ? 2 : 1 ) * xsize * 2;
    int zis_yzx = xsize * 2;
    
    //  Transpose real, frequency-domain XYZ input data into new complex YZX array
    //  xs = x coordinate in "source" (XYZ), which requires a circular shift
    //  xd = x coordinate in "destination" (YZX), which must be reversed
    //  ye/ze are "expanded" coordinates, and ycomp/zcomp select components, both applying only when corresponding dimensions are complex
    //  note that X imaginaries in FD must be added, but these are assumed to be zero, hence:
    //      (1) only a single copy step for each X coordinate,
    //      (2) xs coordinates are NOT scaled by 2,
    //      (3) xd coordinates must be scaled by 2
    comp *= 2;
    xc = true;
    boost::shared_array< float > yzx_data( new float[ xsize * ysize * zsize * comp ] );
    memset( yzx_data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xs = x - xsize / 2 < 0 ? x + xsize / 2 : x - xsize / 2;
                        int xd = x ? xsize - x : 0;
                        int ye = yc ? y * 2 : y;
                        int ze = zc ? z * 2 : z;
                        yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xd * 2 ] = data[ xs * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ];
                    }
    
    //  Compute inverse FT: iterate over Y and Z coordinates/components and process each complex X vector of xsize complex values
    //  Compute Hilbert transform before inverse FT
    fftpack::complex_to_complex< float > fft( xsize );
    for( int y = 0; y < ysize; y++ )
        for( int z = 0; z < zsize; z++ )
            for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                {
                    int ye = yc ? y * 2 : y;
                    int ze = zc ? z * 2 : z;
                    ht( &( yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx ] ), xsize );
                    fft.backward( &( yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx ] ) );
                }
    
    //  Transpose inverse-FTed YZX data back to XYZ data
    //  xe/ye/ze are "expanded" coordinates when the corresponding dimensions are complex and need to be expanded
    //  ycomp/zcomp select the correct components when corresponding dimensions are complex
    //  X must be complex, so both X components are copied explicitly
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xe = x * 2;
                        int ye = yc ? y * 2 : y;
                        int ze = zc ? z * 2 : z;
                        data[ xe * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ] = yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xe ];
                        data[ ( xe + 1 ) * xis_in + ( ye + ycomp ) * yis_in + ze + zcomp ] = yzx_data[ ( ye + ycomp ) * yis_yzx + ( ze + zcomp ) * zis_yzx + xe + 1 ];
                    }
    
}

void details::grid_hc_fft::ift_ht_ad_y()
{
    //  Assuming Y is real and frequency domain, compute inverse FT in Y dimension, after expanding storage to hold imaginary values and after computing the Hilbert transform
    assert( !yc );
    
    namespace fftpack = scitbx::fftpack;
    
    //  Calculate increment sizes for offsets into input (XYZ, Y not complex), transposed (XZY), and output (XYZ, Y complex) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    //  Between input and output, only the X increment size changes
    int xis_in = ysize * zsize * ( zc ? 2 : 1 );
    int yis_in = zsize * ( zc ? 2 : 1 );
    int xis_xzy = zsize * ( zc ? 2 : 1 ) * ysize * 2;
    int zis_xzy = ysize * 2;
    int xis_out = ysize * 2 * zsize * ( zc ? 2 : 1 );
    
    //  Transpose real, frequency-domain XYZ input data into new complex XZY array
    //  ys = y coordinate in "source" (XYZ), which requires a circular shift
    //  yd = y coordinate in "destination" (XZY), which must be reversed
    //  xe/ze are "expanded" coordinates, and xcomp/zcomp select components, both applying only when corresponding dimensions are complex
    //  note that Y imaginaries in FD must be added, but these are assumed to be zero, hence:
    //      (1) only a single copy step for each Y coordinate,
    //      (2) ys coordinates are NOT scaled by 2,
    //      (3) yd coordinates must be scaled by 2
    comp *= 2;
    yc = true;
    boost::shared_array< float > xzy_data( new float[ xsize * ysize * zsize * comp ] );
    memset( xzy_data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int ys = y - ysize / 2 < 0 ? y + ysize / 2 : y - ysize / 2;
                        int yd = y ? ysize - y : 0;
                        int xe = xc ? x * 2 : x;
                        int ze = zc ? z * 2 : z;
                        xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + yd * 2 ] = data[ ( xe + xcomp ) * xis_in + ys * yis_in + ze + zcomp ];
                    }
    
    //  Compute inverse FT: iterate over X and Z coordinates/components and process each complex Y vector of ysize complex values
    //  Compute Hilbert transform before inverse FT
    fftpack::complex_to_complex< float > fft( ysize );
    for( int x = 0; x < xsize; x++ )
        for( int z = 0; z < zsize; z++ )
            for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                {
                    int xe = xc ? x * 2 : x;
                    int ze = zc ? z * 2 : z;
                    ht( &( xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy ] ), ysize );
                    fft.backward( &( xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy ] ) );
                }
    
    //  Transpose inverse-FTed XZY data back to XYZ data
    //  xe/ye/ze are "expanded" coordinates when the corresponding dimensions are complex and need to be expanded
    //  xcomp/zcomp select the correct components when corresponding dimensions are complex
    //  Y must be complex, so both Y components are copied explicitly
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int zcomp = 0; zcomp <= ( zc ? 1 : 0 ); zcomp++ )
                    {
                        int xe = xc ? x * 2 : x;
                        int ye = y * 2;
                        int ze = zc ? z * 2 : z;
                        data[ ( xe + xcomp ) * xis_out + ye * yis_in + ze + zcomp ] = xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + ye ];
                        data[ ( xe + xcomp ) * xis_out + ( ye + 1 ) * yis_in + ze + zcomp ] = xzy_data[ ( xe + xcomp ) * xis_xzy + ( ze + zcomp ) * zis_xzy + ye + 1 ];
                    }
    
}

void details::grid_hc_fft::ift_ht_ad_z()
{
    //  Assuming Z is real and frequency domain, compute inverse FT in Z dimension, after expanding storage to hold imaginary values and after computing the Hilbert transform
    assert( !zc );
    
    namespace fftpack = scitbx::fftpack;
    
    //  Calculate increment sizes for offsets into input (XYZ, Z complex) and output (XYZ, Z not complex) data blocks
    //  Increment size takes into account whether subsequent dimensions are complex or not
    //      e.g. X increment size for XYZ data takes into account whether Y and Z are complex
    //  Increment size does NOT take into account whether current dimension is complex
    //      e.g. X increment size for XYZ data does not take into account whether X is complex
    //      if X is complex, X coordinates must be doubled and an offset added to select the component before multiplying by the increment
    int xis_in = ysize * ( yc ? 2 : 1 ) * zsize;
    int yis_in = zsize;
    int xis_out = ysize * ( yc ? 2 : 1 ) * zsize * 2;
    int yis_out = zsize * 2;
    
    //  Reorder FD Z points, adding Z imaginary values
    //  zs = z coordinate in "source", which requires a circular shift
    //  zd = z coordinate in "destination", which must be reversed
    //  xe/ye are "expanded" coordinates, and xcomp/ycomp select components, both applying only when corresponding dimensions are complex
    //  note that Z imaginaries in FD must be added, but these are assumed to be zero, hence:
    //      (1) only a single copy step for each Z coordinate,
    //      (2) zs coordinates are NOT scaled by 2,
    //      (3) zd coordinates must be scaled by 2
    boost::shared_array< float > src( data );
    comp *= 2;
    zc = true;
    data.reset( new float[ xsize * ysize * zsize * comp ] );
    memset( data.get(), 0, sizeof( float ) * xsize * ysize * zsize * comp );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                    for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                    {
                        int zs = z - zsize / 2 < 0 ? z + zsize / 2 : z - zsize / 2;
                        int zd = z ? zsize - z : 0;
                        int xe = xc ? x * 2 : x;
                        int ye = yc ? y * 2 : y;
                        data[ ( xe + xcomp ) * xis_out + ( ye + ycomp ) * yis_out + zd * 2 ] = src[ ( xe + xcomp ) * xis_in + ( ye + ycomp ) * yis_in + zs ];
                    }
    
    //  Compute inverse FT: iterate over X and Y coordinates/components and process each complex Z vector of zsize complex values
    //  Compute Hilbert transform before inverse FT
    fftpack::complex_to_complex< float > fft( zsize );
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int xcomp = 0; xcomp <= ( xc ? 1 : 0 ); xcomp++ )
                for( int ycomp = 0; ycomp <= ( yc ? 1 : 0 ); ycomp++ )
                {
                    int xe = xc ? x * 2 : x;
                    int ye = yc ? y * 2 : y;
                    ht( &( data[ ( xe + xcomp ) * xis_in + ( ye + ycomp ) * yis_in ] ), zsize );
                    fft.backward( &( data[ ( xe + xcomp ) * xis_out + ( ye + ycomp ) * yis_out ] ) );
                }
}

void details::grid_hc_fft::ht( float *data, int size )
{
    //  Compute Hilbert transform in-place using FT method: inverse FT, multiply by impulse function i sgn s, then FT
    //  Assume input data consists of (size) complex values, currently all real (imaginaries are zero)
    //  In the discrete context, "multiply by impulse function i sgn s" means:
    //      (1) Multiply imaginaries by -1 and put in real slots
    //      (2) Move reals to imaginary slots
    //      (3) Multiply both real and imaginary by:
    //          (a) 0 for point 0
    //          (b) 1 for points 1 to (size/2 - 1)
    //          (c) 0 for point size/2
    //          (d) -1 for points (size/2 + 1) to (size - 1)
    
    boost::shared_array< float > workspace( new float[ size * 2 ] );
    float *ws = workspace.get();
    memcpy( ws, data, sizeof( float ) * size * 2 );
    
    scitbx::fftpack::complex_to_complex< float > fft( size );
    fft.backward( ws );
    
    for( int i = 0; i < size; i++ )
    {
        float sgn = i == 0 ? 0.0F : i > 0 && i < size / 2 ? 1.0F : i == size / 2 ? 0.0F : -1.0F;
        float re = ws[ i * 2 + 1 ] * sgn * -1.0F;
        float im = ws[ i * 2 ] * sgn;
        
        ws[ i * 2 ] = re;
        ws[ i * 2 + 1 ] = im;
    }
    
    fft.forward( ws );
    
    for( int i = 0; i < size; i++ )
        data[ i * 2 + 1 ] = ws[ i * 2 + 1 ];
}

float details::grid_hc_fft::integrate_real()
{
    //  Compute increment sizes, with built-in scaling for complex components as needed
    int xisc = ( xc ? 2 : 1 ) * ysize * ( yc ? 2 : 1 ) * zsize * ( zc ? 2 : 1 );
    int yisc = ( yc ? 2 : 1 ) * zsize * ( zc ? 2 : 1 );
    int zisc = ( xc ? 2 : 1 );
    
    float integral = 0.0F;
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
                integral += data[ x * xisc + y * yisc + z * zisc ];
    
    return integral;
}

boost::shared_ptr< NMRData > nmr_wash::convert_psf_or_pure_comp_to_spectrum( const boost::shared_array< float > input, int xsize, int ysize, int zsize )
{
    int dims = 1;
    if( ysize > 1 ) dims++;
    if( zsize > 1 ) dims++;
    
    boost::shared_ptr< NMRData > d( new NMRData( dims ) );
    
    d->SetDimensionSize( 0, xsize * 2 );
    d->SetDimensionLabel( 0, "X" );
    d->SetDimensionCalibration( 0, 2000.0F, 100.0F, 10.0F );
    
    if( dims >= 2 )
    {
        d->SetDimensionSize( 1, ysize * 2 );
        d->SetDimensionLabel( 1, "Y" );
        d->SetDimensionCalibration( 1, 2000.0F, 100.0F, 10.0F );
    }
    
    if( dims == 3 )
    {
        d->SetDimensionSize( 2, zsize * 2 );
        d->SetDimensionLabel( 2, "Z" );
        d->SetDimensionCalibration( 2, 2000.0F, 100.0F, 10.0F );
    }
    
    d->BuildInMem();
    d->SetDataFromCopyPtr( input.get() );
    return d;
}

void psf_calc::debug_snapshot_xy( const float *data, const char *filename, int xs, int ys, bool xc, bool yc, float scale )
{
    using namespace BEC_NMRData;
    
    size_t new_size = xs * ys;
    boost::shared_array< float > new_data( new float[ new_size ] );
    memset( new_data.get(), 0, sizeof( float ) * new_size );
    int xisc = ( xs > 1 && xc ? 2 : 1 ) * ys * ( ys > 1 && yc ? 2 : 1 );
    int yisc = ( ys > 1 && yc ? 2 : 1 );
    for( int x = 0; x < xs; x++ )
        for( int y = 0; y < ys; y++ )
            new_data[ x * ys + y ] = data[ x * xisc + y * yisc ];
    NMRData_nmrPipe out;
    out.SetNumOfDims( 2 );
    out.SetDimensionSize( 0, xs );
    out.SetDimensionLabel( 0, "X" );
    out.SetDimensionSize( 1, ys );
    out.SetDimensionLabel( 1, "Y" );
    out.CreateFile( filename );
    out.SetDataFromCopyPtr( new_data.get() );
    if( scale != 1.0F )
        out *= scale;
    out.Close();
}

void psf_calc::debug_snapshot_xyz( const float *data, const char *filename, int xs, int ys, int zs, bool xc, bool yc, bool zc, float scale )
{
    using namespace BEC_NMRData;
    
    size_t new_size = xs * ys * zs;
    boost::shared_array< float > new_data( new float[ new_size ] );
    memset( new_data.get(), 0, sizeof( float ) * new_size );
    int xis = ys * ( ys > 1 && yc ? 2 : 1 ) * zs * ( zs > 1 && zc ? 2 : 1 );
    int yis = zs * ( zs > 1 && zc ? 2 : 1 );
    for( int x = 0; x < xs; x++ )
        for( int y = 0; y < ys; y++ )
            for( int z = 0; z < zs; z++ )
                new_data[ x * ys * zs + y * zs + z ] = data[ ( xc ? x * 2 : x ) * xis + ( yc ? y * 2 : y ) * yis + ( zc ? z * 2 : z ) ];
    NMRData_NMRView out;
    out.SetNumOfDims( 3 );
    out.SetDimensionSize( 0, xs );
    out.SetDimensionLabel( 0, "X" );
    out.SetDimensionSize( 1, ys );
    out.SetDimensionLabel( 1, "Y" );
    out.SetDimensionSize( 2, zs );
    out.SetDimensionLabel( 2, "Z" );
    out.CreateFile( filename );
    out.SetDataFromCopyPtr( new_data.get() );
    if( scale != 1.0F )
        out *= scale;
    out.Close();
}



