//  Copyright (c) 2006-2014 Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <boost/scoped_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/container/vector.hpp>
//#include <voro++.hh>
//#include <voro++_2d.hh>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"

float rpowf( float x, float y );

using namespace nmr_wash;

dimension_apod::dimension_apod( const BEC_NMRData::DimensionApodInfo &x )
{
    if( x.Method == BEC_NMRData::None )
        method = none;
    else if( x.Method == BEC_NMRData::SP )
        method = sp;
    else if( x.Method == BEC_NMRData::EM )
        method = em;
    else if( x.Method == BEC_NMRData::GM )
        method = gm;
    else if( x.Method == BEC_NMRData::GMB )
        method = gmb;
    else if( x.Method == BEC_NMRData::TM )
        method = tm;
    else if( x.Method == BEC_NMRData::TRI )
        method = tri;
    else if( x.Method == BEC_NMRData::JMOD )
        method = jmod;
    
    p1 = x.P1;
    p2 = x.P2;
    p3 = x.P3;
    size = x.Size;
    first_point_correction = x.FirstPointCorrection;
    
    sw = -1.0F;
}

float rpowf( float x, float y )
{
    if( y > 0.0F )
        return powf( x, y );
    else if( x == 0.0F && y < 0.0F )
        return 10.0E+16F;
    else if( x < 0.0F && y != ( int ) y )
        return powf( BEC_Misc::fast_abs( x ), y );
    else
        return powf( x, y );
}

float dimension_apod::operator()( float position ) const
{
    //  These are derived from Frank Delaglio's apod.c in NMRPipe
    //  Handling of out-of-range parameters, etc., is designed to exactly mirror NMRPipe's handling
    
    if( size < 0.0F || sw < 0.0F )
        return 1.0F;
    
    float swa = ( sw > 0.0F ? sw : 1.0F );
    
    if( position >= ( float ) size && method != none )
        return 0.0F;
    
    float w = ( position < 0.01F ) ? first_point_correction : 1.0F;
    
    float a, b, e, g, t, aq, l, r, loc;
    switch( method )
    {
        case none:
            return w;
        case sp:
            //  SINE-BELL
            //  p1 = starting offset of sine-bell (pi*rad), p2 = ending offset of sine-bell (pi*rad), p3 = exponent
            a = rpowf( sinf( ( p1 + position / ( float ) ( size - 1 ) * ( p2 - p1 ) ) * BEC_Misc::pi_f ), p3 );
            if( p1 >= 0.0F && p1 <= 1.0F && p2 >= 0.0F && p2 <= 1.0F )
                a = BEC_Misc::fast_abs( a );
            return w *= a;
        case em:
            //  EXPONENTIAL
            //  p1 = line broadening (Hz)
            return w *= expf( -BEC_Misc::pi_f * position * p1 / swa );
        case gm:
            //  LORENTZ-TO-GAUSS
            //  p1 = inverse exponential line sharpening (Hz), p2 = Gaussian line broadening (Hz), position of Gaussian function maximum (pts)
            e = BEC_Misc::pi_f * position * p1 / swa;
            g = 0.6F * BEC_Misc::pi_f * p2 * ( p3 * ( ( float ) ( size - 1 ) ) - position ) / swa;
            return w *= expf( e - g * g );
        case gmb:
            //  GAUSS BROADEN WINDOW
            //  p1 = "exponential factor", p2 = "Gaussian factor"
            t = position / swa;
            aq = ( float ) size / swa;
            a = BEC_Misc::pi_f * p1;
             b = -a / ( 2.0F * p2 * aq );
            return w *= expf( -a * t - b * t * t );
        case tm:
            //  TRAPEZOID
            //  p1 = length of left side, p2 = length of right side
            l = ( p1 < 0.0F ? 0.0F : p1 > ( float ) size ? ( float ) size : p1 );
            r = ( p2 < 0.0F ? 0.0F : p2 > ( float ) size ? ( float ) size : p2 );
            if( l > 1 && position < l )
            {
                float val = position / ( l - 1.0F );
                if( val < 0.0F ) val = 0.0F;
                return w *= val;
            }
            else if( r > 1 && position >= ( float ) size - r && position < ( float ) size )
            {
                float val = ( ( float ) size - position - 1.0F ) / ( r - 1.0F );
                if( val < 0.0F ) val = 0.0F;
                return w *= val;
            }
            return w;
        case tri:
            //  TRIANGLE
            //  p1 = location of apex (pts), p2 = left edge starting height, p3 = right edge ending height
            loc = ( p1 < 1.0F ? 1.0F : p1 > ( float ) size ? ( float ) size : p1 );
            if( position < loc )
            {
                float inc = ( loc - 1.0F == 0.0F ) ? 0.0F : ( 1.0F - p2 ) / ( loc - 1.0F );
                float val = p2 + position * inc;
                if( val < 0.0F ) val = 0.0F;
                return w *= val;
            }
            else
            {
                float inc = ( ( float ) size - loc == 0.0F ) ? 0.0F : ( p3 - 1.0F ) / ( ( float ) size - loc );
                float val = 1.0F + position * inc;
                if( val < 0.0F ) val = 0.0F;
                return w *= val;
            }
            return w;
        case jmod:
            //  EXPONENTIALLY DAMPED J-MODULATION PROFILE
            //  p1 = starting offset of J modulation (pi*rad), p2 = J modulation frequency (Hz), p3 = line broadening (Hz)
            e = BEC_Misc::pi_f * position * p3 / swa;
            float end = p1 + p2 * ( ( float ) ( size - 1 ) ) / swa;
            return w *= expf( -e ) * sinf( BEC_Misc::pi_f * p1 + BEC_Misc::pi_f * ( end - p1 ) * position / ( ( float ) ( size - 1 ) ) );
    }
}

int dimension_map::get_num_of_dims() const
{
    int dims = 0;
    
    if( F1 != unused ) dims++;
    if( F2 != unused ) dims++;
    if( F3 != unused ) dims++;
    if( F4 != unused ) dims++;
    
    return dims;
}

int dimension_map::get_num_of_index_dims() const
{
    int id = 0;
    
    if( F1 == index_dim ) id++;
    if( F2 == index_dim ) id++;
    if( F3 == index_dim ) id++;
    if( F4 == index_dim ) id++;
    
    return id;
}

int dimension_map::get_num_of_sparse_dims() const
{
    int sd = 0;
    
    if( F1 == u || F1 == v || F1 == w ) sd++;
    if( F2 == u || F2 == v || F2 == w ) sd++;
    if( F3 == u || F3 == v || F3 == w ) sd++;
    if( F4 == u || F4 == v || F4 == w ) sd++;
    
    return sd;
}

expt_dimension dimension_map::get_x_expt_dim() const
{
    if( F1 != unused && F1 != index_dim )
        return nmr_wash::F1;
    if( F2 != unused && F2 != index_dim )
        return nmr_wash::F2;
    if( F3 != unused && F3 != index_dim )
        return nmr_wash::F3;
    if( F4 != unused && F4 != index_dim )
        return nmr_wash::F4;
    return nmr_wash::not_present;
}

expt_dimension dimension_map::get_y_expt_dim() const
{
    if( get_x_expt_dim() == nmr_wash::F1 )
    {
        if( F2 != unused && F2 != index_dim )
            return nmr_wash::F2;
        if( F3 != unused && F3 != index_dim )
            return nmr_wash::F3;
        if( F4 != unused && F4 != index_dim )
            return nmr_wash::F4;
    }
    if( get_x_expt_dim() == nmr_wash::F2 )
    {
        if( F3 != unused && F3 != index_dim )
            return nmr_wash::F3;
        if( F4 != unused && F4 != index_dim )
            return nmr_wash::F4;
    }
    if( get_x_expt_dim() == nmr_wash::F3 )
    {
        if( F4 != unused && F4 != index_dim )
            return nmr_wash::F4;
    }
    return nmr_wash::not_present;
}

expt_dimension dimension_map::get_z_expt_dim() const
{
    if( get_y_expt_dim() == nmr_wash::F2 )
    {
        if( F3 != unused && F3 != index_dim )
            return nmr_wash::F3;
        if( F4 != unused && F4 != index_dim )
            return nmr_wash::F4;
    }
    if( get_y_expt_dim() == nmr_wash::F3 )
    {
        if( F4 != unused && F4 != index_dim )
            return nmr_wash::F4;
    }
    return nmr_wash::not_present;
}

expt_dimension dimension_map::get_u_expt_dim() const
{
    if( F1 == u )
        return nmr_wash::F1;
    if( F2 == u )
        return nmr_wash::F2;
    if( F3 == u )
        return nmr_wash::F3;
    if( F4 == u )
        return nmr_wash::F4;
    return nmr_wash::not_present;
}

expt_dimension dimension_map::get_v_expt_dim() const
{
    if( F1 == v )
        return nmr_wash::F1;
    if( F2 == v )
        return nmr_wash::F2;
    if( F3 == v )
        return nmr_wash::F3;
    if( F4 == v )
        return nmr_wash::F4;
    return nmr_wash::not_present;
}

expt_dimension dimension_map::get_w_expt_dim() const
{
    if( F1 == w )
        return nmr_wash::F1;
    if( F2 == w )
        return nmr_wash::F2;
    if( F3 == w )
        return nmr_wash::F3;
    if( F4 == w )
        return nmr_wash::F4;
    return nmr_wash::not_present;
}

int dimension_map::get_x_expt_dim_num() const
{
    switch( get_x_expt_dim() )
    {
        case nmr_wash::F1:
            return 0;
        case nmr_wash::F2:
            return 1;
        case nmr_wash::F3:
            return 2;
        case nmr_wash::F4:
            return 3;
        default:
            throw exceptions::dimension_not_present();
    }
}

int dimension_map::get_y_expt_dim_num() const
{
    switch( get_y_expt_dim() )
    {
        case nmr_wash::F1:
            return 0;
        case nmr_wash::F2:
            return 1;
        case nmr_wash::F3:
            return 2;
        case nmr_wash::F4:
            return 3;
        default:
            throw exceptions::dimension_not_present();
    }
}

int dimension_map::get_z_expt_dim_num() const
{
    switch( get_z_expt_dim() )
    {
        case nmr_wash::F1:
            return 0;
        case nmr_wash::F2:
            return 1;
        case nmr_wash::F3:
            return 2;
        case nmr_wash::F4:
            return 3;
        default:
            throw exceptions::dimension_not_present();
    }
}

std::vector< int > dimension_map::get_index_dim_nums() const
{
    std::vector< int > dims;

    if( F1 == index_dim )
        dims.push_back( 0 );
    if( F2 == index_dim )
        dims.push_back( 1 );
    if( F3 == index_dim )
        dims.push_back( 2 );
    if( F4 == index_dim )
        dims.push_back( 3 );
    
    return dims;
}

std::vector< int > dimension_map::get_sparse_dim_nums() const
{
    std::vector< int > dims;
    
    if( F1 == u || F1 == v || F1 == w )
        dims.push_back( 0 );
    if( F2 == u || F2 == v || F2 == w )
        dims.push_back( 1 );
    if( F3 == u || F3 == v || F3 == w )
        dims.push_back( 2 );
    if( F4 == u || F4 == v || F4 == w )
        dims.push_back( 3 );
    
    return dims;
}

bool dimension_map::is_valid() const
{
    if( F1 == u && F2 == u )
        return false;
    if( F1 == u && F3 == u )
        return false;
    if( F1 == u && F4 == u )
        return false;
    if( F2 == u && F3 == u )
        return false;
    if( F2 == u && F4 == u )
        return false;
    if( F3 == u && F4 == u )
        return false;

    if( F1 == v && F2 == v )
        return false;
    if( F1 == v && F3 == v )
        return false;
    if( F1 == v && F4 == v )
        return false;
    if( F2 == v && F3 == v )
        return false;
    if( F2 == v && F4 == v )
        return false;
    if( F3 == v && F4 == v )
        return false;

    if( F1 == w && F2 == w )
        return false;
    if( F1 == w && F3 == w )
        return false;
    if( F1 == w && F4 == w )
        return false;
    if( F2 == w && F3 == w )
        return false;
    if( F2 == w && F4 == w )
        return false;
    if( F3 == w && F4 == w )
        return false;
    
    return true;
}

int dimension_map::x( int u, int v, int w ) const
{
    switch( get_x_expt_dim() )
    {
        case nmr_wash::F1:
            if( F1 == nmr_wash::u )
                return u;
            else if( F1 == nmr_wash::v )
                return v;
            else if( F1 == nmr_wash::w )
                return w;
            else return 0;
        case nmr_wash::F2:
            if( F2 == nmr_wash::u )
                return u;
            else if( F2 == nmr_wash::v )
                return v;
            else if( F2 == nmr_wash::w )
                return w;
            else return 0;
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return u;
            else if( F3 == nmr_wash::v )
                return v;
            else if( F3 == nmr_wash::w )
                return w;
            else return 0;
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return u;
            else if( F4 == nmr_wash::v )
                return v;
            else if( F4 == nmr_wash::w )
                return w;
            else return 0;
        default:
            return 0;
    }
}

int dimension_map::y( int u, int v, int w ) const
{
    switch( get_y_expt_dim() )
    {
        case nmr_wash::F2:
            if( F2 == nmr_wash::u )
                return u;
            else if( F2 == nmr_wash::v )
                return v;
            else if( F2 == nmr_wash::w )
                return w;
            else return 0;
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return u;
            else if( F3 == nmr_wash::v )
                return v;
            else if( F3 == nmr_wash::w )
                return w;
            else return 0;
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return u;
            else if( F4 == nmr_wash::v )
                return v;
            else if( F4 == nmr_wash::w )
                return w;
            else return 0;
        default:
            return 0;
    }
}

int dimension_map::z( int u, int v, int w ) const
{
    switch( get_z_expt_dim() )
    {
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return u;
            else if( F3 == nmr_wash::v )
                return v;
            else if( F3 == nmr_wash::w )
                return w;
            else return 0;
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return u;
            else if( F4 == nmr_wash::v )
                return v;
            else if( F4 == nmr_wash::w )
                return w;
            else return 0;
        default:
            return 0;
    }
}

int dimension_map::get_x_grid_size( const sampling_pattern &sp ) const
{
    switch( get_x_expt_dim() )
    {
        case nmr_wash::F1:
            if( F1 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F1 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F1 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F2:
            if( F2 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F2 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F2 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F3 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F3 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F4 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F4 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::not_present:
            throw exceptions::dimension_not_present();
    }
}

int dimension_map::get_y_grid_size( const sampling_pattern &sp ) const
{
    switch( get_y_expt_dim() )
    {
        case nmr_wash::F1:
            if( F1 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F1 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F1 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F2:
            if( F2 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F2 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F2 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F3 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F3 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F4 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F4 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::not_present:
            throw exceptions::dimension_not_present();
    }
}

int dimension_map::get_z_grid_size( const sampling_pattern &sp ) const
{
    switch( get_z_expt_dim() )
    {
        case nmr_wash::F1:
            if( F1 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F1 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F1 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F2:
            if( F2 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F2 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F2 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F3 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F3 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return sp.get_u_grid_size();
            else if( F4 == nmr_wash::v )
                return sp.get_v_grid_size();
            else if( F4 == nmr_wash::w )
                return sp.get_w_grid_size();
            else throw exceptions::dimension_not_present();
        case nmr_wash::not_present:
            throw exceptions::dimension_not_present();
    }
}

float dimension_map::xf( float u, float v, float w ) const
{
    switch( get_x_expt_dim() )
    {
        case nmr_wash::F1:
            if( F1 == nmr_wash::u )
                return u;
            else if( F1 == nmr_wash::v )
                return v;
            else if( F1 == nmr_wash::w )
                return w;
            else return 0.0F;
        case nmr_wash::F2:
            if( F2 == nmr_wash::u )
                return u;
            else if( F2 == nmr_wash::v )
                return v;
            else if( F2 == nmr_wash::w )
                return w;
            else return 0.0F;
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return u;
            else if( F3 == nmr_wash::v )
                return v;
            else if( F3 == nmr_wash::w )
                return w;
            else return 0.0F;
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return u;
            else if( F4 == nmr_wash::v )
                return v;
            else if( F4 == nmr_wash::w )
                return w;
            else return 0.0F;
        default:
            return 0.0F;
    }
}

float dimension_map::yf( float u, float v, float w ) const
{
    switch( get_y_expt_dim() )
    {
        case nmr_wash::F2:
            if( F2 == nmr_wash::u )
                return u;
            else if( F2 == nmr_wash::v )
                return v;
            else if( F2 == nmr_wash::w )
                return w;
            else return 0.0F;
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return u;
            else if( F3 == nmr_wash::v )
                return v;
            else if( F3 == nmr_wash::w )
                return w;
            else return 0.0F;
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return u;
            else if( F4 == nmr_wash::v )
                return v;
            else if( F4 == nmr_wash::w )
                return w;
            else return 0.0F;
        default:
            return 0.0F;
    }
}

float dimension_map::zf( float u, float v, float w ) const
{
    switch( get_z_expt_dim() )
    {
        case nmr_wash::F3:
            if( F3 == nmr_wash::u )
                return u;
            else if( F3 == nmr_wash::v )
                return v;
            else if( F3 == nmr_wash::w )
                return w;
            else return 0.0F;
        case nmr_wash::F4:
            if( F4 == nmr_wash::u )
                return u;
            else if( F4 == nmr_wash::v )
                return v;
            else if( F4 == nmr_wash::w )
                return w;
            else return 0.0F;
        default:
            return 0.0F;
    }
}

float dimension_map::x_apod( float position ) const
{
    switch( get_x_expt_dim() )
    {
        case nmr_wash::F1:
            return F1_apod( position );
        case nmr_wash::F2:
            return F2_apod( position );
        case nmr_wash::F3:
            return F3_apod( position );
        case nmr_wash::F4:
            return F4_apod( position );
        default:
            return 1.0F;
    }    
}

float dimension_map::y_apod( float position ) const
{
    switch( get_y_expt_dim() )
    {
        case nmr_wash::F1:
            return F1_apod( position );
        case nmr_wash::F2:
            return F2_apod( position );
        case nmr_wash::F3:
            return F3_apod( position );
        case nmr_wash::F4:
            return F4_apod( position );
        default:
            return 1.0F;
    }    
}

float dimension_map::z_apod( float position ) const
{
    switch( get_z_expt_dim() )
    {
        case nmr_wash::F1:
            return F1_apod( position );
        case nmr_wash::F2:
            return F2_apod( position );
        case nmr_wash::F3:
            return F3_apod( position );
        case nmr_wash::F4:
            return F4_apod( position );
        default:
            return 1.0F;
    }    
}

sampling_pattern::sampling_pattern()
{
    clear();
}

sampling_pattern::sampling_pattern( std::string filename )
{
    using namespace boost::spirit::qi;
    
    clear();

    boost::filesystem::path file_path( filename );
    std::string ext = file_path.extension().string();
    if( phrase_parse( ext.begin(), ext.end(), lit( ".ft" ) | lit( ".ft2" ) | lit( ".dat" ) | lit( ".nv" ) | lit( ".ucsf" ) | lit( ".param" ), space ) )
        build_from_spectrum_representation( filename );
    else
        build_from_text_file( filename );
}

sampling_pattern::sampling_pattern( std::string filename, int u_is_col, int v_is_col, int w_is_col, int weight_is_col, bool is_on_grid )
{
    clear();
    build_from_text_file_with_guidance( filename, u_is_col, v_is_col, w_is_col, weight_is_col, is_on_grid );
}

void sampling_pattern::load( std::string filename )
{
    using namespace boost::spirit::qi;

    boost::filesystem::path file_path( filename );
    std::string ext = file_path.extension().string();
    if( phrase_parse( ext.begin(), ext.end(), lit( ".ft" ) | lit( ".ft2" ) | lit( ".ft3" ) | lit( ".ft4" ) | lit( ".dat" ) | lit( ".nv" ) | lit( ".ucsf" ) | lit( ".param" ), space ) )
        build_from_spectrum_representation( filename );
    else
        build_from_text_file( filename );   
}

void sampling_pattern::load( std::string filename, int u_is_col, int v_is_col, int w_is_col, int weight_is_col, bool is_on_grid )
{
    build_from_text_file_with_guidance( filename, u_is_col, v_is_col, w_is_col, weight_is_col, is_on_grid );
}

void sampling_pattern::save( std::string filename, bool number_points, bool include_col_headings, char comment_char, const char *delimiter_string ) const
{
    using namespace boost::spirit::qi;
    using namespace BEC_NMRData;

    if( !dims )
        return;
    if( points.empty() )
        return;
    
    boost::filesystem::path file_path( filename );
    std::string ext = file_path.extension().string();
    if( phrase_parse( ext.begin(), ext.end(), lit( ".ft" ) | lit( ".ft2" ) | lit( ".dat" ) | lit( ".nv" ) | lit( ".ucsf" ) | lit( ".param" ), space ) )
    {
        if( !on_grid )
            throw exceptions::sampling_pattern::cant_use_spectrum_representation_for_off_grid_pattern( filename );
        
        boost::scoped_ptr< NMRData > dp( NMRData::GetObjForFileType( filename ) );
        NMRData &d = *dp;
        
        d.SetNumOfDims( dims );
        
        d.SetDimensionLabel( 1, "u" );
        d.SetDimensionSize( 1, u_max + 1 );

        int vsize = 1;
        if( dims >= 2 )
        {
            d.SetDimensionLabel( 2, "v" );
            d.SetDimensionSize( 2, v_max + 1 );
            vsize = v_max + 1;
        }
        
        int wsize = 1;
        if( dims == 3 )
        {
            d.SetDimensionLabel( 3, "w" );
            d.SetDimensionSize( 3, w_max + 1 );
            wsize = w_max + 1;
        }
        
        d.CreateFile( filename );
        if( !d.GotData )
            throw exceptions::cant_open_file( filename );
        
        for( std::vector< sampling_point >::const_iterator i = points.begin(); i != points.end(); ++i )
        {
            size_t offset = i->u * vsize * wsize + i->v * wsize + i->w;
            d[ offset ] = 1.0F;
        }
    }
    else
    {
        FILE *f = fopen( filename.c_str(), "w+" );
        if( !f )
            throw exceptions::cant_open_file( filename );
        
        if( include_col_headings )
        {
            fprintf( f, "%c", comment_char );
            if( number_points )
                fprintf( f, "No. %s", delimiter_string );
            fprintf( f, "u  " );
            if( dims >= 2 )
                fprintf( f, "%sV  ", delimiter_string );
            if( dims == 3 )
                fprintf( f, "%sW  ", delimiter_string );
            if( weights )
                fprintf( f, "%sWeight", delimiter_string );
            fprintf( f, "\n" );
        }

        int count = 1;
        for( std::vector< sampling_point >::const_iterator i = points.begin(); i != points.end(); ++i )
        {
            if( number_points )
                fprintf( f, "%04i%s", count++, delimiter_string );
            
            if( on_grid )
                fprintf( f, "%03i", i->u );
            else
                fprintf( f, "%f", i->uf );
            
            if( dims >= 2 && on_grid )
                fprintf( f, "%s%03i", delimiter_string, i->v );
            else if( dims >= 2 )
                fprintf( f, "%s%f", delimiter_string, i->vf );

            if( dims == 3 && on_grid )
                fprintf( f, "%s%03i", delimiter_string, i->w );
            else if( dims == 3 )
                fprintf( f, "%s%f", delimiter_string, i->wf );
            
            if( weights )
                fprintf( f, "%s%f", delimiter_string, i->weight );
            
            fprintf( f, "\n" );
        }
        
        fclose( f );
    }
}

int sampling_pattern::get_num_of_dims() const
{
    return dims;
}

bool sampling_pattern::got_weights() const
{
    return weights;
}

bool sampling_pattern::is_on_grid() const
{
    return on_grid;
}

int sampling_pattern::get_u_grid_size() const
{
    assert( on_grid );
    return u_max;
}

int sampling_pattern::get_v_grid_size() const
{
    assert( on_grid );
    return v_max;
}

int sampling_pattern::get_w_grid_size() const
{
    assert( on_grid );
    return w_max;
}

float sampling_pattern::get_u_max_value() const
{
    if( !on_grid )
        return uf_max;
    else
        return ( float ) u_max;
}

float sampling_pattern::get_v_max_value() const
{
    if( !on_grid )
        return vf_max;
    else
        return ( float ) v_max;
}

float sampling_pattern::get_w_max_value() const
{
    if( !on_grid )
        return wf_max;
    else
        return ( float ) w_max;
}

void sampling_pattern::add_point( sampling_point new_point )
{
    assert( dims );
    assert( !( ( dims < 2 && ( new_point.v || new_point.vf ) ) ||
       ( dims < 3 && ( new_point.w || new_point.wf ) ) ||
       ( !weights && new_point.weight != 1.0F ) ||
       ( on_grid && ( new_point.uf || new_point.vf || new_point.wf ) ) ||
              ( !on_grid && ( new_point.u || new_point.v || new_point.w ) ) ) );
    
    if( on_grid )
    {
        if( new_point.u > u_max )
            u_max = new_point.u;
        if( new_point.v > v_max )
            v_max = new_point.v;
        if( new_point.w > w_max )
            w_max = new_point.w;
    }
    else
    {
        if( new_point.uf > uf_max )
            uf_max = new_point.uf;
        if( new_point.vf > vf_max )
            vf_max = new_point.vf;
        if( new_point.wf > wf_max )
            wf_max = new_point.wf;
    }

    points.push_back( new_point );
}

void sampling_pattern::clear()
{
    dims = 0;
    weights = false;
    on_grid = true;
    
    u_max = 0;
    v_max = 0;
    w_max = 0;
    uf_max = 0;
    vf_max = 0;
    wf_max = 0;

    points.clear();
}

void sampling_pattern::clear_weights()
{
    for( std::vector< sampling_point >::iterator i = points.begin(); i != points.end(); ++i )
        i->weight = 1.0F;
}

const std::vector< sampling_point > & sampling_pattern::get_points() const
{
    return points;
}

std::vector< sampling_point > & sampling_pattern::get_points()
{
    return points;
}

std::vector< sampling_point > sampling_pattern::get_points_copy() const
{
    return points;
}

void sampling_pattern::set_options( int num_of_dims, bool weights, bool on_grid_ )
{
    dims = num_of_dims;
    weights = weights;
    on_grid = on_grid_;
}

size_t sampling_pattern::size() const
{
    return points.size();
}

sampling_point & sampling_pattern::operator[]( size_t index )
{
    return points[ index ];
}

const sampling_point &sampling_pattern::operator[]( size_t index ) const
{
    return points[ index ];
}

//void sampling_pattern::calculate_voronoi_weights( std::string graphical_output_file )
//{
//    using namespace voro;
//    
//    if( dims == 1 )
//    {
//        pre_container pc( 0.0, on_grid ? ( double ) u_max : uf_max, -0.5, 0.5, -0.5, 0.5, false, false, false );
//        
//        int count = 0;
//        for( std::vector< sampling_point >::iterator i = points.begin(); i != points.end(); ++i )
//            if( on_grid )
//                pc.put( count++, ( double ) i->u, 0.0, 0.0 );
//            else pc.put( count++, i->uf, 0.0, 0.0 );
//        
//        particle_order po;
//        int nu, nv, nw;
//        pc.guess_optimal( nu, nv, nw );
//        container c( 0.0, on_grid ? ( double ) u_max : uf_max, -0.5, 0.5, -0.5, 0.5, nu, nv, nw, false, false, false, 8 );
//        pc.setup( po, c );
//        
//        c_loop_order cl( c, po );
//        voronoicell vc;
//        if( cl.start() )
//            do
//            {
//                if( c.compute_cell( vc, cl ) )
//                    points[ cl.pid() ].weight = vc.volume();
//            } while( cl.inc() );
//        
//        if( !graphical_output_file.empty() )
//        {
//            boost::filesystem::path file_path( graphical_output_file );
//            std::string ext = file_path.extension().string();
//            if( !ext.compare( ".pov" ) )
//            {
//                std::string file_v = file_path.stem().string();
//                file_v += "_v.pov";
//                c.draw_cells_pov( file_v.c_str() );
//                
//                std::string file_p = file_path.stem().string();
//                file_p += "_p.pov";
//                c.draw_particles_pov( file_p.c_str() );
//            }
//            else if( !ext.compare( ".gnu" ) )
//                c.draw_cells_gnuplot( graphical_output_file.c_str() );
//        }
//    }   // 1-D
//    else if( dims == 2 )
//    {
//        pre_container pc( 0.0, on_grid ? ( double ) u_max : uf_max, 0.0, on_grid ? ( double ) v_max : vf_max, -0.5, 0.5, false, false, false );
//        
//        int count = 0;
//        for( std::vector< sampling_point >::iterator i = points.begin(); i != points.end(); ++i )
//            if( on_grid )
//                pc.put( count++, ( double ) i->u, ( double ) i->v, 0.0 );
//            else pc.put( count++, ( double ) i->uf, ( double ) i->vf, 0.0 );
//        
//        particle_order po;
//        int nu, nv, nw;
//        pc.guess_optimal( nu, nv, nw );
//        container c( 0.0, on_grid ? ( double ) u_max : uf_max, 0.0, on_grid ? ( double ) v_max : vf_max, -0.5, 0.5, nu, nv, nw, false, false, false, 8 );
//        pc.setup( po, c );
//        
//        c_loop_order cl( c, po );
//        voronoicell vc;
//        if( cl.start() )
//            do
//            {
//                if( c.compute_cell( vc, cl ) )
//                    points[ cl.pid() ].weight = vc.volume();
//            } while( cl.inc() );
//        
//        if( !graphical_output_file.empty() )
//        {
//            boost::filesystem::path file_path( graphical_output_file );
//            std::string ext = file_path.extension().string();
//            if( !ext.compare( ".pov" ) )
//            {
//                std::string file_v = file_path.stem().string();
//                file_v += "_v.pov";
//                c.draw_cells_pov( file_v.c_str() );
//                
//                std::string file_p = file_path.stem().string();
//                file_p += "_p.pov";
//                c.draw_particles_pov( file_p.c_str() );
//            }
//            else if( !ext.compare( ".gnu" ) )
//                c.draw_cells_gnuplot( graphical_output_file.c_str() );
//        }
//    }   // 2-D
//    else if( dims == 3 )
//    {
//        pre_container pc( 0.0, on_grid ? ( double ) u_max : uf_max, 0.0, on_grid ? ( double ) v_max : vf_max, 0.0, on_grid ? ( double ) w_max : wf_max, false, false, false );
//        
//        int count = 0;
//        for( std::vector< sampling_point >::iterator i = points.begin(); i != points.end(); ++i )
//            if( on_grid )
//                pc.put( count++, ( double ) i->u, ( double ) i->v, ( double ) i->w );
//            else pc.put( count++, ( double ) i->uf, ( double ) i->vf, ( double ) i->wf );
//        
//        particle_order po;
//        int nu, nv, nw;
//        pc.guess_optimal( nu, nv, nw );
//        container c( 0.0, on_grid ? ( double ) u_max : uf_max, 0.0, on_grid ? ( double ) v_max : vf_max, 0.0, on_grid ? ( double ) w_max : wf_max, nu, nv, nw, false, false, false, 8 );
//        pc.setup( po, c );
//        
//        c_loop_order cl( c, po );
//        voronoicell vc;
//        if( cl.start() )
//            do
//            {
//                if( c.compute_cell( vc, cl ) )
//                    points[ cl.pid() ].weight = vc.volume();
//            } while( cl.inc() );
//        
//        if( !graphical_output_file.empty() )
//        {
//            boost::filesystem::path file_path( graphical_output_file );
//            std::string ext = file_path.extension().string();
//            if( !ext.compare( ".pov" ) )
//            {
//                std::string file_v = file_path.stem().string();
//                file_v += "_v.pov";
//                c.draw_cells_pov( file_v.c_str() );
//                
//                std::string file_p = file_path.stem().string();
//                file_p += "_p.pov";
//                c.draw_particles_pov( file_p.c_str() );
//            }
//            else if( !ext.compare( ".gnu" ) )
//                c.draw_cells_gnuplot( graphical_output_file.c_str() );
//        }
//    }   // 3-D
//    
//    weights = true;
//}

void sampling_pattern::build_from_spectrum_representation( std::string filename )
{
    using namespace BEC_NMRData;

    if( !boost::filesystem::exists( boost::filesystem::path( filename ) ) )
        throw exceptions::file_not_found( filename );
    
    boost::scoped_ptr< NMRData > dp( NMRData::GetObjForFile( filename ) );
    if( !dp->GotData )
        throw exceptions::cant_open_file( filename );
    NMRData &d = *dp;

    clear();
    on_grid = true;
    weights = false;
    
    if( d.GetNumOfDims() == 1 )
    {
        dims = 1;
        u_max = d.GetDimensionSize( 0 );
        
        for( int u = 0; u < u_max; u++ )
            if( d[ u ] )
                add_point( sampling_point( u ) );
    }
    else if( d.GetNumOfDims() == 2 )
    {
        dims = 2;
        u_max = d.GetDimensionSize( 0 );
        v_max = d.GetDimensionSize( 1 );
        
        for( int u = 0; u < u_max; u++ )
            for( int v = 0; v < v_max; v++ )
            {
                size_t offset = u * v_max + v;
                if( d[ offset ] )
                    add_point( sampling_point( u, v ) );
            }
    }
    else if( d.GetNumOfDims() == 3 )
    {
        dims = 3;
        u_max = d.GetDimensionSize( 0 );
        v_max = d.GetDimensionSize( 1 );
        w_max = d.GetDimensionSize( 2 );
        
        for( int u = 0; u < u_max; u++ )
            for( int v = 0; v < v_max; v++ )
                for( int w = 0; w < w_max; w++ )
                {
                    size_t offset = u * v_max * w_max + v * w_max + w;
                    if( d[ offset ] )
                        add_point( sampling_point( u, v, w ) );
                }
    }
}

void sampling_pattern::build_from_text_file( std::string filename )
{
    using namespace boost::spirit::qi;
    using namespace boost::fusion;
    
    typedef std::string::iterator it_t;
    typedef boost::fusion::vector< int, float > i1f;
    typedef boost::fusion::vector< int, int, float > i2f;
    typedef boost::fusion::vector< int, int, int, float > i3f;
    
    enum formats
    {
        not_known,
        ui,
        ui_w,
        ui_vi,
        ui_vi_w,
        ui_vi_wi,
        ui_vi_wi_w,
        idx_ui_vi_wi,
        idx_ui_vi_wi_w,
        idx_ui_vi_w,
        uf,
        uf_vf,
        uf_vf_wf,
        uf_vf_wf_w
    };
    
    real_parser< float, strict_real_policies< float > > strict_float;
    
    //  Parsers
    rule< it_t, unused_type() >                 delimiter =         -ascii::char_( ',' ) >> +ascii::blank;
    rule< it_t, unused_type() >                 comment =           ascii::char_( '#' ) | ascii::char_( ';' );
    rule< it_t, std::vector< int >() >          int_list =          int_ >> *( delimiter >> int_ );
    rule< it_t, i1f() >                         i1f_list =          int_ >> delimiter >> strict_float;
    rule< it_t, i2f() >                         i2f_list =          int_ >> delimiter >> int_ >> delimiter >> strict_float;
    rule< it_t, i3f() >                         i3f_list =          int_ >> delimiter >> int_ >> delimiter >> int_ >> delimiter >> strict_float;
    rule< it_t, i3f() >                         idx_i3f_list =      omit[ int_ ] >> delimiter >> int_ >> delimiter >> int_ >> delimiter >> int_ >> delimiter >> strict_float;
    rule< it_t, i2f() >                         idx_i2f_list =      omit[ int_ ] >> delimiter >> int_ >> delimiter >> int_ >> delimiter >> strict_float;
    rule< it_t, std::vector< float >() >        float_list =        strict_float >> *( delimiter >> strict_float );
//    rule< it_t, unused_type() >                 cmt_header =        ascii::char_( '#' ) >> *delimiter >> lit( "nmr_sampling_pattern_grid" );
//    rule< it_t, unused_type() >                 cmt_header_3d =     ascii::char_( '#' ) >> *delimiter >> lit( "nmr_sampling_pattern_grid_3d" );
    
    formats format = not_known;
    
    if( !boost::filesystem::exists( boost::filesystem::path( filename ) ) )
        throw exceptions::file_not_found( filename );
    
    FILE *pf = fopen( filename.c_str(), "r" );
    if( !pf )
        throw exceptions::cant_open_file( filename );

    clear();
    
    std::vector< int > i_data;
    i1f i1f_data;
    i2f i2f_data;
    i3f i3f_data;
    std::vector< float > f_data;
    
    char buffer[ 2048 ];
    while( fgets( buffer, 2048, pf ) )
    {
        std::string line( buffer );
        
        if( format == not_known )
        {
            if( line.empty() )
                continue;
//            else if( phrase_parse( line.begin(), line.end(), cmt_header_3d, ascii::space ) )
//                //  Supports our earlier grid pattern files, which have a point index in the first column.
//            {
//                format = idx_ui_vi_w;
//                dims = 2;
//                weights = true;
//            }
//            else if( phrase_parse( line.begin(), line.end(), cmt_header, ascii::space ) )
//                //  Supports our earlier grid pattern files, which have a point index in the first column.
//            {
//                format = idx_ui_vi_wi_w;
//                dims = 3;
//                weights = true;
//            }
            else if( phrase_parse( line.begin(), line.end(), comment, ascii::space ) )
                continue;
            else if( phrase_parse( line.begin(), line.end(), lit( "nmr_sampling_pattern_grid_3d" ), ascii::space ) )
                continue;
//                //  Supports our earlier grid pattern files, which have a point index in the first column.
//            {
//                format = idx_ui_vi_w;
//                dims = 2;
//                weights = true;
//            }
            else if( phrase_parse( line.begin(), line.end(), lit( "nmr_sampling_pattern_grid" ), ascii::space ) )
                continue;
//                //  Supports our earlier grid pattern files, which have a point index in the first column.
//            {
//                format = idx_ui_vi_wi_w;
//                dims = 3;
//                weights = true;
//            }
            else if( phrase_parse( line.begin(), line.end(), float_list, ascii::space, f_data ) )
            {
                switch( f_data.size() )
                {
                    case 1:
                        format = uf;
                        dims = 1;
                        on_grid = false;
                        add_point( sampling_point( f_data[ 0 ] ) );
                        break;
                    case 2:
                        format = uf_vf;
                        dims = 2;
                        on_grid = false;
                        add_point( sampling_point( f_data[ 0 ], f_data[ 1 ] ) );
                        break;
                    case 3:
                        format = uf_vf_wf;
                        dims = 3;
                        on_grid = false;
                        add_point( sampling_point( f_data[ 0 ], f_data[ 1 ], f_data[ 2 ] ) );
                        break;
                    case 4:
                        format = uf_vf_wf_w;
                        dims = 2;
                        on_grid = false;
                        weights = true;
                        add_point( sampling_point( f_data[ 0 ], f_data[ 1 ], f_data[ 2 ], f_data[ 3 ] ) );
                        break;
                    default:
                        throw exceptions::sampling_pattern::file_format_not_understood( filename );
                }
            }
            else if( phrase_parse( line.begin(), line.end(), idx_i3f_list, ascii::space, i3f_data ) )
            {
                format = idx_ui_vi_wi_w;
                dims = 3;
                weights = true;
                on_grid = true;
                add_point( sampling_point( at_c< 0 >( i3f_data ), at_c< 1 >( i3f_data ), at_c< 2 >( i3f_data ), at_c< 3 >( i3f_data ) ) );
            }
            else if( phrase_parse( line.begin(), line.end(), i3f_list, ascii::space, i3f_data ) )
            {
                format = ui_vi_wi_w;
                dims = 3;
                weights = true;
                on_grid = true;
                add_point( sampling_point( at_c< 0 >( i3f_data ), at_c< 1 >( i3f_data ), at_c< 2 >( i3f_data ), at_c< 3 >( i3f_data ) ) );
            }
            else if( phrase_parse( line.begin(), line.end(), i2f_list, ascii::space, i2f_data ) )
            {
                format = ui_vi_w;
                dims = 2;
                weights = true;
                on_grid = true;
                add_point( sampling_point( at_c< 0 >( i3f_data ), at_c< 1 >( i3f_data ), 0, at_c< 2 >( i3f_data ) ) );
            }
            else if( phrase_parse( line.begin(), line.end(), i1f_list, ascii::space, i1f_data ) )
            {
                format = ui_w;
                dims = 1;
                weights = true;
                on_grid = true;
                add_point( sampling_point( at_c< 0 >( i1f_data ), 0, 0, at_c< 1 >( i1f_data ) ) );
            }
            else if( phrase_parse( line.begin(), line.end(), int_list, ascii::space, i_data ) )
            {
                switch( i_data.size() )
                {
                    case 1:
                        format = ui;
                        dims = 1;
                        on_grid = true;
                        add_point( sampling_point( i_data[ 0 ] ) );
                        break;
                    case 2:
                        format = ui_vi;
                        dims = 2;
                        on_grid = true;
                        add_point( sampling_point( i_data[ 0 ], i_data[ 1 ] ) );
                        break;
                    case 3:
                        format = ui_vi_wi;
                        dims = 3;
                        on_grid = true;
                        add_point( sampling_point( i_data[ 0 ], i_data[ 1 ], i_data[ 2 ] ) );
                        break;
                    case 4:
                        format = idx_ui_vi_wi;
                        dims = 3;
                        on_grid = true;
                        add_point( sampling_point( i_data[ 1 ], i_data[ 2 ], i_data[ 3 ] ) );
                        break;
                    default:
                        throw exceptions::sampling_pattern::file_format_not_understood( filename );
                }
            }
            else
                throw exceptions::sampling_pattern::file_format_not_understood( filename );
        }   //  if format not known
        else
        {
            if( line.empty() )
                continue;
            if( phrase_parse( line.begin(), line.end(), comment, ascii::space ) )
                continue;
            switch( format )
            {
                case not_known:
                    assert( 0 );
                    break;
                case ui:
                    if( phrase_parse( line.begin(), line.end(), int_list, ascii::space, i_data ) )
                    {
                        if( i_data.size() == 1 )
                            add_point( sampling_point( i_data[ 0 ] ) );
                        else
                            throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case ui_w:
                    if( phrase_parse( line.begin(), line.end(), i1f_list, ascii::space, i1f_data ) )
                    {
                        int u = at_c< 0 >( i1f_data );
                        float weight = at_c< 1 >( i1f_data );
                        add_point( sampling_point( u, 0, 0, weight ) );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case ui_vi:
                    if( phrase_parse( line.begin(), line.end(), int_list, ascii::space, i_data ) )
                    {
                        if( i_data.size() == 2 )
                            add_point( sampling_point( i_data[ 0 ], i_data[ 1 ] ) );
                        else
                            throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case ui_vi_w:
                    if( phrase_parse( line.begin(), line.end(), i2f_list, ascii::space, i2f_data ) )
                    {
                        int u = at_c< 0 >( i2f_data ), v = at_c< 1 >( i2f_data );
                        float weight = at_c< 2 >( i2f_data );
                        add_point( sampling_point( u, v, 0, weight ) );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                   break;
                case ui_vi_wi:
                    if( phrase_parse( line.begin(), line.end(), int_list, ascii::space, i_data ) )
                    {
                        if( i_data.size() == 3 )
                            add_point( sampling_point( i_data[ 0 ], i_data[ 1 ], i_data[ 2 ] ) );
                        else
                            throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                   break;
                case ui_vi_wi_w:
                    if( phrase_parse( line.begin(), line.end(), i3f_list, ascii::space, i3f_data ) )
                    {
                        int u = at_c< 0 >( i3f_data ), v = at_c< 1 >( i3f_data ), w = at_c< 2 >( i3f_data );
                        float weight = at_c< 3 >( i3f_data );
                        add_point( sampling_point( u, v, w, weight ) );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case idx_ui_vi_wi:
                    if( phrase_parse( line.begin(), line.end(), int_list, ascii::space, i_data ) )
                    {
                        if( i_data.size() == 4 )
                            add_point( sampling_point( i_data[ 1 ], i_data[ 2 ], i_data[ 3 ] ) );
                        else
                            throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case idx_ui_vi_wi_w:
                    if( phrase_parse( line.begin(), line.end(), idx_i3f_list, ascii::space, i3f_data ) )
                        add_point( sampling_point( at_c< 0 >( i3f_data ), at_c< 1 >( i3f_data ), at_c< 2 >( i3f_data ), at_c< 3 >( i3f_data ) ) );
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case idx_ui_vi_w:
                    if( phrase_parse( line.begin(), line.end(), idx_i2f_list, ascii::space, i2f_data ) )
                        add_point( sampling_point( at_c< 0 >( i2f_data ), at_c< 1 >( i2f_data ), 0, at_c< 2 >( i2f_data ) ) );
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case uf:
                    if( phrase_parse( line.begin(), line.end(), float_list, ascii::space, f_data ) )
                    {
                        if( f_data.size() == 1 )
                            add_point( sampling_point( f_data[ 0 ] ) );
                        else
                            throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case uf_vf:
                    if( phrase_parse( line.begin(), line.end(), float_list, ascii::space, f_data ) )
                    {
                        if( f_data.size() == 2 )
                            add_point( sampling_point( f_data[ 0 ], f_data[ 1 ] ) );
                        else
                            throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case uf_vf_wf:
                    if( phrase_parse( line.begin(), line.end(), float_list, ascii::space, f_data ) )
                    {
                        if( f_data.size() == 3 )
                            add_point( sampling_point( f_data[ 0 ], f_data[ 1 ], f_data[ 2 ] ) );
                        else
                            throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
                case uf_vf_wf_w:
                    if( phrase_parse( line.begin(), line.end(), float_list, ascii::space, f_data ) )
                    {
                        if( f_data.size() == 4 )
                            add_point( sampling_point( f_data[ 0 ], f_data[ 1 ], f_data[ 2 ], f_data[ 3 ] ) );
                        else
                            throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    }
                    else
                        throw exceptions::sampling_pattern::file_format_not_consistent( filename );
                    break;
            }   //  which format
        }   //  if format is known
        i_data.clear();
        f_data.clear();
    }   //  line fetching loop
    
    fclose( pf );
}

void sampling_pattern::build_from_text_file_with_guidance( std::string filename, int u_is_col, int v_is_col, int w_is_col, int weight_is_col, bool is_on_grid )
{
    using namespace boost::spirit::qi;
    using namespace boost::spirit::ascii;
    
    typedef std::string::iterator it_t;
    
    //  Parsers
    rule< it_t, unused_type() >                 comment =       ascii::char_( '#' ) | ascii::char_( ';' );
    rule< it_t, unused_type() >                 delimiter =     -ascii::char_( ',' ) >> +ascii::blank;
    rule< it_t, std::vector< float >() >        float_list =    float_ >> *( delimiter >> float_ );
    
    if( !boost::filesystem::exists( boost::filesystem::path( filename ) ) )
        throw exceptions::file_not_found( filename );
    std::ifstream ifs( filename.c_str() );
    if( ifs.fail() )
        throw exceptions::cant_open_file( filename );
    
    clear();
    
    if( u_is_col >= 0 && v_is_col >= 0 && w_is_col >= 0 )
        dims = 3;
    else if( u_is_col >= 0 && v_is_col >= 0 )
        dims = 2;
    else if( u_is_col >= 0 )
        dims = 1;
    else
        throw exceptions::sampling_pattern::bad_file_format_guidance( filename );
    
    on_grid = is_on_grid;
    weights = weight_is_col >= 0;
    
    std::string line;
    std::vector< float > f_data;
    
    while( ifs.good() )
    {
        getline( ifs, line );
        
        if( line.empty() )
            continue;
        if( phrase_parse( line.begin(), line.end(), comment, ascii::space ) )
            continue;
        if( phrase_parse( line.begin(), line.end(), float_list, ascii::blank, f_data ) )
        {
            if( u_is_col > f_data.size() || v_is_col > f_data.size() || w_is_col > f_data.size() || weight_is_col > f_data.size() )
                throw exceptions::sampling_pattern::file_format_doesnt_match( filename );
            
            if( on_grid )
                add_point( sampling_point( u_is_col >= 0 ? BEC_Misc::closest_int( f_data[ u_is_col ] ) : 0,
                                         v_is_col >= 0 ? BEC_Misc::closest_int( f_data[ v_is_col ] ) : 0,
                                         w_is_col >= 0 ? BEC_Misc::closest_int( f_data[ w_is_col ] ) : 0,
                                         weight_is_col >= 0 ? f_data[ weight_is_col ] : 1.0F ) );
            else
                add_point( sampling_point( u_is_col >= 0 ? f_data[ u_is_col ] : 0,
                                         v_is_col >= 0 ? f_data[ v_is_col ] : 0,
                                         w_is_col >= 0 ? f_data[ w_is_col ] : 0,
                                         weight_is_col >= 0 ? f_data[ weight_is_col ] : 1.0F ) );
        }
        else
            throw exceptions::sampling_pattern::file_format_doesnt_match( filename );
        
        f_data.clear();

    }   //  line fetching loop
}

std::istream & nmr_wash::operator>>( std::istream &in, dimension_assignment &da )
{
    std::string token;
    in >> token;
    
    boost::to_lower( token );
    
    if( token == "u" )
        da = nmr_wash::u;
    else if( token == "v" )
        da = nmr_wash::v;
    else if( token == "w" )
        da = nmr_wash::w;
    else if( token == "index" )
        da = nmr_wash::index_dim;
    else if( token == "unused" )
        da = nmr_wash::unused;
    else
        throw exceptions::bad_dimension_assignment_string();
    
    return in;
}

std::ostream & nmr_wash::operator<<( std::ostream &out, dimension_assignment &da )
{
    switch( da )
    {
        case u:
            out << "u";
            break;
        case v:
            out << "v";
            break;
        case w:
            out << "w";
            break;
        case index_dim:
            out << "index";
            break;
        case unused:
            out << "unused";
            break;
    }
    
    return out;
}

std::istream & nmr_wash::operator>>( std::istream &in, apod_method &am )
{
    std::string token;
    in >> token;
    
    boost::to_lower( token );
    
    if( token == "none" )
        am = nmr_wash::none;
    else if( token == "sp" )
        am = nmr_wash::sp;
    else if( token == "em" )
        am = nmr_wash::em;
    else if( token == "gm" )
        am = nmr_wash::gm;
    else if( token == "gmb" )
        am = nmr_wash::gmb;
    else if( token == "tm" )
        am = nmr_wash::tm;
    else if( token == "tri" )
        am = nmr_wash::tri;
    else if( token == "jmod" )
        am = nmr_wash::jmod;
    else
        throw exceptions::bad_apod_method_string();
    
    return in;
}

std::ostream & nmr_wash::operator<<( std::ostream &out, apod_method &am )
{
    switch( am )
    {
        case none:
            out << "None";
            break;
        case sp:
            out << "SP";
            break;
        case em:
            out << "EM";
            break;
        case gm:
            out << "GM";
            break;
        case gmb:
            out << "GMB";
            break;
        case tm:
            out << "TM";
            break;
        case tri:
            out << "TRI";
            break;
        case jmod:
            out << "JMOD";
            break;
    }
    
    return out;
}

std::ostream & nmr_wash::operator<<( std::ostream &out, dimension_apod &da )
{
    switch( da.method )
    {
        case none:
            out << "None, first point correction = " << std::fixed << std::setprecision( 2 ) << da.first_point_correction;
            break;
        case sp:
            out << "SP off = " << std::fixed << std::setprecision( 2 ) << da.p1 << ", end = " << da.p2 << ", pow = " << da.p3 << ", size = " << da.size << " pts, first point correction = " << da.first_point_correction;
            break;
        case em:
            out << "EM lb = " << std::fixed << std::setprecision( 2 ) << da.p1 << " Hz, sw = " << da.sw << " Hz, size = " << da.size << " pts, first point correction = " << da.first_point_correction;
            break;
        case gm:
            out << "GM g1 = " << std::fixed << std::setprecision( 2 ) << da.p1 << " Hz, g2 = " << da.p2 << " Hz, g3 = " << da.p3 << ", sw = " << da.sw << " Hz, size = " << da.size << " pts, first point correction = " << da.first_point_correction;
            break;
        case gmb:
            out << "GMB lb = " << std::fixed << std::setprecision( 2 ) << da.p1 << " Hz, gb = " << da.p2 << ", sw = " << da.sw << " Hz, size = " << da.size << " pts, first point correction = " << da.first_point_correction;
            break;
        case tm:
            out << "TM t1 = " << std::fixed << std::setprecision( 2 ) << da.p1 << ", t2 = " << da.p2 << ", size = " << da.size << " pts, first point correction = " << da.first_point_correction;
            break;
        case tri:
            out << "TRI loc = " << std::fixed << std::setprecision( 2 ) << da.p1 << ", lHi = " << da.p2 << ", rHi = " << da.p3 << ", size = " << da.size << " pts, first point correction = " << da.first_point_correction;
            break;
        case jmod:
            out << "JMOD off = " << std::fixed << std::setprecision( 2 ) << da.p1 << " rad, j = " << da.p2 << " Hz, lb = " << da.p3 << " Hz, sw = " << da.sw << " Hz, size = " << da.size << " pts, first point correction = " << da.first_point_correction;
            break;
    }
    
    return out;
}


