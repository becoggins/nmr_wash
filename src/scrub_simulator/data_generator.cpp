//  Copyright (c) 2006-2012 Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <memory.h>
#include <complex>
#include <boost/shared_array.hpp>
#include <boost/random.hpp>
#include <boost/format.hpp>
#include <scitbx/fftpack/complex_to_complex_3d.h>
#include <scitbx/array_family/ref.h>
#include <scitbx/array_family/accessors/c_grid.h>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"
#include "data_generator.h"

using namespace BEC_Misc;
using namespace nmr_wash;
namespace fftpack = scitbx::fftpack;
namespace af = scitbx::af;


data_generator::data_generator( const sampling_pattern &sp_, dimension_map dm_, int xsize_, int ysize_, int zsize_, unsigned int seed_, pure_component_modes pure_component_mode, float pc_param_a, float pc_param_b ) : sp( sp_ ), dm( dm_ ), xsize( xsize_ ), ysize( ysize_ ), zsize( zsize_ ), size( xsize_ * ysize_ * zsize_ ), seed( seed_ )
{
    sparse_input = new std::complex< float >[ size ];
    memset( sparse_input, 0, sizeof( std::complex< float > ) * size );

    full_input = new std::complex< float >[ size ];
    memset( full_input, 0, sizeof( std::complex< float > ) * size );
    
    psf_calc psfc( sp, dm, xsize, ysize, zsize );
    psfc.psf();
    
    boost::shared_array< float > pure_comp;
    switch( pure_component_mode )
    {
        case contour_irregular:
            pure_comp = psfc.pure_component_contour_irregular( pc_param_a, pc_param_b );
            break;
        case contour_ellipsoid:
            pure_comp = psfc.pure_component_contour_ellipsoid( pc_param_a, pc_param_b );
            break;
        case fixed_ellipsoid:
            pure_comp = psfc.pure_component_fixed_ellipsoid( pc_param_a );
            break;
    }
    
    env.reset( new float[ size ] );
    memset( env.get(), 0, sizeof( float ) * xsize * ysize * zsize );
    
    boost::scoped_array< std::complex< float > > env_cartesian_input( new std::complex< float >[ xsize * ysize * zsize * 8 ] );
    memset( env_cartesian_input.get(), 0, sizeof( std::complex< float > ) * xsize * ysize * zsize * 8 );
    
    af::ref< std::complex< float >, af::c_grid< 3 > > env_cdata( env_cartesian_input.get(), af::c_grid< 3 >( xsize * 2, ysize * 2, zsize * 2 ) );
    
    for( int x = 0; x < xsize * 2; x++ )
        for( int y = 0; y < ysize * 2; y++ )
            for( int z = 0; z < zsize * 2; z++ )
            {
                int x_fft = x + xsize;
                if( x_fft >= xsize * 2 ) x_fft -= xsize * 2;
                
                int y_fft = y + ysize;
                if( y_fft >= ysize * 2 ) y_fft -= ysize * 2;
                
                int z_fft = z + zsize;
                if( z_fft >= zsize * 2 ) z_fft -= zsize * 2;
                
                int rev_x = x ? xsize * 2 - x : 0;
                int rev_y = y ? ysize * 2 - y : 0;
                int rev_z = z ? zsize * 2 - z : 0;
                
                env_cartesian_input[ x_fft * ysize * zsize * 4 + y_fft * zsize * 2 + z_fft ] = 
                std::complex< float >( pure_comp[ rev_x * ysize * zsize * 4 + rev_y * zsize * 2 + rev_z ], 0.0F );
            };
    
    fftpack::complex_to_complex_3d< float > env_fft( xsize * 2, ysize * 2, zsize * 2 );
    env_fft.backward( env_cdata );
    
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
            {
                env[ x * ysize * zsize + y * zsize + z ] = env_cartesian_input[ x * 2 * ysize * zsize * 4 + y * 2 * zsize * 2 + z * 2 ].real();
            }
    
    float max = 0.0F;
    for( size_t i = 0; i < xsize * ysize * zsize; i++ )
        if( fast_abs( env[ i ] ) > max )
            max = env[ i ];
    
    float scale = 1.0F / max;
    
    for( size_t i = 0; i < xsize * ysize * zsize; i++ )
        env[ i ] *= scale; 
}

data_generator::data_generator( const sampling_pattern &sp_, dimension_map dm_, int xsize_, int ysize_, int zsize_, unsigned int seed_, boost::shared_array< float > env_ ) : sp( sp_ ), dm( dm_ ), xsize( xsize_ ), ysize( ysize_ ), zsize( zsize_ ), size( xsize_ * ysize_ * zsize_ ), seed( seed_ ), env( env_ )
{
    sparse_input = new std::complex< float >[ size ];
    memset( sparse_input, 0, sizeof( std::complex< float > ) * size );
    
    full_input = new std::complex< float >[ size ];
    memset( full_input, 0, sizeof( std::complex< float > ) * size );
}

data_generator::~data_generator()
{
    delete sparse_input;
    delete full_input;
    
    sparse_input = 0;
    full_input = 0;
}

boost::shared_array< float > data_generator::get_env() const
{
    return env;
}

void data_generator::add_signal( float xpos, float ypos, float zpos, float intensity, float xtau, float ytau, float ztau )
{
    af::ref< std::complex< float >, af::c_grid< 3 > > sdata( sparse_input, af::c_grid< 3 >( xsize, ysize, zsize ) );
    af::ref< std::complex< float >, af::c_grid< 3 > > fdata( full_input, af::c_grid< 3 >( xsize, ysize, zsize ) );
    
    xpos = floorf( xpos * xsize ) / ( float ) xsize;
    ypos = floorf( ypos * ysize ) / ( float ) ysize;
    zpos = floorf( zpos * zsize ) / ( float ) zsize;
    
    float xf, yf, zf;
    xf = ( xpos > 0.5F ? 1.0F + 0.5F - xpos : 0.5F - xpos ) * ( float ) xsize / 2.0F;
    yf = ( ypos > 0.5F ? 1.5F - ypos : 0.5F - ypos ) * ( float ) ysize / 2.0F;
    zf = ( zpos > 0.5F ? 1.5F - zpos : 0.5F - zpos ) * ( float ) zsize / 2.0F;
    
    const std::vector< sampling_point > points = sp.get_points();
    for( std::vector< sampling_point >::const_iterator i = points.begin(); i != points.end(); ++i )
    {
        const sampling_point &p = *i;
        
        int x = dm.x( p.u, p.v, p.w );
        int y = dm.y( p.u, p.v, p.w );
        int z = dm.z( p.u, p.v, p.w );
        int min_x = xsize - x;
        int min_y = ysize - y;
        
        if( x >= xsize / 2 || y >= ysize / 2 || z >= zsize )
            continue;
        
        float tx = ( float ) x * 2.0F / ( float ) xsize;
        float ty = ( float ) y * 2.0F / ( float ) ysize;
        float tz = ( float ) z * 2.0F / ( float ) zsize;
        
        float amp = intensity * p.weight * expf( -xtau * tx ) * expf( -ytau * ty ) * expf( -ztau * tz );
        if( !z ) amp *= 0.5F;
        
        float ccc = cosf( 2.0F * pi_f * xf * tx ) * cosf( 2.0F * pi_f * yf * ty ) * cosf( 2.0F * pi_f * zf * tz ) * amp;
        float ccs = cosf( 2.0F * pi_f * xf * tx ) * cosf( 2.0F * pi_f * yf * ty ) * sinf( 2.0F * pi_f * zf * tz ) * amp;
        float scc = sinf( 2.0F * pi_f * xf * tx ) * cosf( 2.0F * pi_f * yf * ty ) * cosf( 2.0F * pi_f * zf * tz ) * amp;
        float scs = sinf( 2.0F * pi_f * xf * tx ) * cosf( 2.0F * pi_f * yf * ty ) * sinf( 2.0F * pi_f * zf * tz ) * amp;
        float csc = cosf( 2.0F * pi_f * xf * tx ) * sinf( 2.0F * pi_f * yf * ty ) * cosf( 2.0F * pi_f * zf * tz ) * amp;
        float css = cosf( 2.0F * pi_f * xf * tx ) * sinf( 2.0F * pi_f * yf * ty ) * sinf( 2.0F * pi_f * zf * tz ) * amp;
        float ssc = sinf( 2.0F * pi_f * xf * tx ) * sinf( 2.0F * pi_f * yf * ty ) * cosf( 2.0F * pi_f * zf * tz ) * amp;
        float sss = sinf( 2.0F * pi_f * xf * tx ) * sinf( 2.0F * pi_f * yf * ty ) * sinf( 2.0F * pi_f * zf * tz ) * amp;
        
        float cppp =  ccc - ssc - scs - css;
        float sppp =  scc + csc + ccs - sss;
        float cmpp =  ccc + ssc + scs - css;
        float smpp = -scc + csc + ccs + sss;
        float cpmp =  ccc + ssc - scs + css;
        float spmp =  scc - csc + ccs + sss;
        float cmmp =  ccc - ssc + scs + css;
        float smmp = -scc - csc + ccs - sss;
        
            sdata( x,     y,     z ) += std::complex< float >( cppp, sppp );
        if( x )
            sdata( min_x, y,     z ) += std::complex< float >( cmpp, smpp );
        if( y )
            sdata( x,     min_y, z ) += std::complex< float >( cpmp, spmp );
        if( x && y )
            sdata( min_x, min_y, z ) += std::complex< float >( cmmp, smmp );
        
    }
    
    for( int x = 0; x < xsize / 2; x++ )
        for ( int y = 0; y < ( ysize > 1 ? ysize / 2 : 1 ); y++ )
            for( int z = 0; z < ( zsize > 1 ? zsize / 2 : 1 ); z++ )
            {
                int min_x = xsize - x;
                int min_y = ysize - y;
                
                float tx = ( float ) x * 2.0F / ( float ) xsize;
                float ty = ( float ) y * 2.0F / ( float ) ysize;
                float tz = ( float ) z * 2.0F / ( float ) zsize;
                
                float amp = intensity * expf( -xtau * tx ) * expf( -ytau * ty ) * expf( -ztau * tz ) * env[ x * ysize * zsize + y * zsize + z ];
                if( z <= 0.0000000001F ) amp *= 0.5F;
                
                float ccc = cosf( 2.0F * pi_f * xf * tx ) * cosf( 2.0F * pi_f * yf * ty ) * cosf( 2.0F * pi_f * zf * tz ) * amp;
                float ccs = cosf( 2.0F * pi_f * xf * tx ) * cosf( 2.0F * pi_f * yf * ty ) * sinf( 2.0F * pi_f * zf * tz ) * amp;
                float scc = sinf( 2.0F * pi_f * xf * tx ) * cosf( 2.0F * pi_f * yf * ty ) * cosf( 2.0F * pi_f * zf * tz ) * amp;
                float scs = sinf( 2.0F * pi_f * xf * tx ) * cosf( 2.0F * pi_f * yf * ty ) * sinf( 2.0F * pi_f * zf * tz ) * amp;
                float csc = cosf( 2.0F * pi_f * xf * tx ) * sinf( 2.0F * pi_f * yf * ty ) * cosf( 2.0F * pi_f * zf * tz ) * amp;
                float css = cosf( 2.0F * pi_f * xf * tx ) * sinf( 2.0F * pi_f * yf * ty ) * sinf( 2.0F * pi_f * zf * tz ) * amp;
                float ssc = sinf( 2.0F * pi_f * xf * tx ) * sinf( 2.0F * pi_f * yf * ty ) * cosf( 2.0F * pi_f * zf * tz ) * amp;
                float sss = sinf( 2.0F * pi_f * xf * tx ) * sinf( 2.0F * pi_f * yf * ty ) * sinf( 2.0F * pi_f * zf * tz ) * amp;
                
                float cppp =  ccc - ssc - scs - css;
                float sppp =  scc + csc + ccs - sss;
                float cmpp =  ccc + ssc + scs - css;
                float smpp = -scc + csc + ccs + sss;
                float cpmp =  ccc + ssc - scs + css;
                float spmp =  scc - csc + ccs + sss;
                float cmmp =  ccc - ssc + scs + css;
                float smmp = -scc - csc + ccs - sss;
                
                    fdata( x,     y,     z ) += std::complex< float >( cppp, sppp );
                if( x )
                    fdata( min_x, y,     z ) += std::complex< float >( cmpp, smpp );
                if( y )
                    fdata( x,     min_y, z ) += std::complex< float >( cpmp, spmp );
                if( x && y )
                    fdata( min_x, min_y, z ) += std::complex< float >( cmmp, smmp );
            }
    
}

void data_generator::add_noise( float mean, float std_dev )
{
    boost::mt19937 random_engine( ( boost::mt19937::result_type ) seed );
    boost::normal_distribution< float > random_dist( mean, std_dev );
    boost::variate_generator< boost::mt19937, boost::normal_distribution< float > > random_gen( random_engine, random_dist );

    af::ref< std::complex< float >, af::c_grid< 3 > > sdata( sparse_input, af::c_grid< 3 >( xsize, ysize, zsize ) );
    const std::vector< sampling_point > points = sp.get_points();
    for( std::vector< sampling_point >::const_iterator i = points.begin(); i != points.end(); ++i )
    {
        const sampling_point &p = *i;
        
        int x = dm.x( p.u, p.v, p.w );
        int y = dm.y( p.u, p.v, p.w );
        int z = dm.z( p.u, p.v, p.w );
        int min_x = xsize - x;
        int min_y = ysize - y;
        
            sdata( x,     y,     z ) += std::complex< float >( random_gen(), random_gen() );
        if( x )
            sdata( min_x, y,     z ) += std::complex< float >( random_gen(), random_gen() );
        if( y )
            sdata( x,     min_y, z ) += std::complex< float >( random_gen(), random_gen() );
        if( x && y )
            sdata( min_x, min_y, z ) += std::complex< float >( random_gen(), random_gen() );
    }
    
    for( size_t i = 0; i < size; i++ )
        full_input[ i ] += std::complex< float >( random_gen(), random_gen() );
}

boost::shared_array< float > data_generator::fft_sparse()
{
    boost::shared_array< float > fft_result( new float[ size ] );
    
    af::ref< std::complex< float >, af::c_grid< 3 > > sdata( sparse_input, af::c_grid< 3 >( xsize, ysize, zsize ) );
    
    fftpack::complex_to_complex_3d< float > fft( xsize, ysize, zsize );
    fft.forward( sdata );

    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
            {
                int x_fft = x + xsize / 2;
                if( x_fft >= xsize ) x_fft -= xsize;
                
                int y_fft = y + ysize / 2;
                if( y_fft >= ysize ) y_fft -= ysize;
                
                int z_fft = z + zsize / 2;
                if( z_fft >= zsize ) z_fft -= zsize;
                
                int rev_x = x ? xsize - x : 0;
                int rev_y = y ? ysize - y : 0;
                int rev_z = z ? zsize - z : 0;
                
                fft_result[ rev_x * ysize * zsize + rev_y * zsize + rev_z ] = sparse_input[ x_fft * ysize * zsize + y_fft * zsize + z_fft ].real();
            };
    
    return fft_result;
}

boost::shared_array< float > data_generator::fft_full()
{
    boost::shared_array< float > fft_result( new float[ size ] );
    
    af::ref< std::complex< float >, af::c_grid< 3 > > fdata( full_input, af::c_grid< 3 >( xsize, ysize, zsize ) );
    
    fftpack::complex_to_complex_3d< float > fft( xsize, ysize, zsize );
    fft.forward( fdata );
    
    for( int x = 0; x < xsize; x++ )
        for( int y = 0; y < ysize; y++ )
            for( int z = 0; z < zsize; z++ )
            {
                int x_fft = x + xsize / 2;
                if( x_fft >= xsize ) x_fft -= xsize;
                
                int y_fft = y + ysize / 2;
                if( y_fft >= ysize ) y_fft -= ysize;
                
                int z_fft = z + zsize / 2;
                if( z_fft >= zsize ) z_fft -= zsize;
                
                int rev_x = x ? xsize - x : 0;
                int rev_y = y ? ysize - y : 0;
                int rev_z = z ? zsize - z : 0;
                
                fft_result[ rev_x * ysize * zsize + rev_y * zsize + rev_z ] = full_input[ x_fft * ysize * zsize + y_fft * zsize + z_fft ].real();
            };
    
    return fft_result;
}

void make_snapshot( const float *data, std::string filename, int dims, int xs, int ys, int zs, float scale )
{
    using namespace BEC_NMRData;
    
    if( dims == 1 )
    {
        NMRData_NMRView out;
        out.SetNumOfDims( 1 );
        out.SetDimensionSize( 0, xs );
        out.SetDimensionLabel( 0, "F1" );
        out.SetDimensionCalibration( 0, 2000.0F, 100.0F, 10.0F );
        out.CreateFile( filename.c_str() );
        out.SetDataFromCopyPtr( data );
        if( scale != 1.0F )
            out *= scale;
        out.Close();
    }
    else if( dims == 2 )
    {
        NMRData_NMRView out;
        out.SetNumOfDims( 2 );
        out.SetDimensionSize( 0, xs );
        out.SetDimensionLabel( 0, "F1" );
        out.SetDimensionCalibration( 0, 2000.0F, 100.0F, 10.0F );
        out.SetDimensionSize( 1, ys );
        out.SetDimensionLabel( 1, "F2" );
        out.SetDimensionCalibration( 1, 2000.0F, 100.0F, 10.0F );
        out.CreateFile( filename.c_str() );
        out.SetDataFromCopyPtr( data );
        if( scale != 1.0F )
            out *= scale;
        out.Close();
    }
    else
    {
        NMRData_NMRView out;
        out.SetNumOfDims( 3 );
        out.SetDimensionSize( 0, xs );
        out.SetDimensionLabel( 0, "F1" );
        out.SetDimensionCalibration( 0, 2000.0F, 100.0F, 10.0F );
        out.SetDimensionSize( 1, ys );
        out.SetDimensionLabel( 1, "F2" );
        out.SetDimensionCalibration( 1, 2000.0F, 100.0F, 10.0F );
        out.SetDimensionSize( 2, zs );
        out.SetDimensionLabel( 2, "F3" );
        out.SetDimensionCalibration( 2, 2000.0F, 100.0F, 10.0F );
        out.CreateFile( filename.c_str() );
        out.SetDataFromCopyPtr( data );
        if( scale != 1.0F )
            out *= scale;
        out.Close();
    }
}

z_snapshot_recorder::z_snapshot_recorder( std::string filename_stem_, int xsize_, int ysize_, int zsize_, int ypos_, int interval_ ) : filename_stem( filename_stem_ ), xsize( xsize_ ), ysize( ysize_ ), zsize( zsize_ ), ypos( ypos_ ), count( 0 ), outer( 0 ), middle( 0 ), interval( interval_ )
{
    if( ysize > 1 && zsize > 1 ) dims = 3;
    else if( ysize > 1 ) dims = 2;
    else dims = 1;
}

void z_snapshot_recorder::operator()( bool new_outer, float current_threshold, float noise_floor, float noise_std_dev, float clean_level, size_t accepted_positions, size_t components, const float * data )
{
    if( new_outer )
    {
        outer++;
        middle = 0;
    }
    else middle++;
    
    if( count % interval == 0 )
    {
        std::string filename = filename_stem + ( boost::format( "_%04i.txt" ) % ( int ) count ).str();
        FILE *f = fopen( filename.c_str(), "w+" );
        fprintf( f, "outer, %i\n", ( int ) outer );
        fprintf( f, "middle, %i\n", ( int ) middle );
        fprintf( f, "current_threshold, %f\n", current_threshold );
        fprintf( f, "noise_floor, %f\n", noise_floor );
        fprintf( f, "noise_std_dev, %f\n", noise_std_dev );
        fprintf( f, "clean_level, %f\n", clean_level );
        fprintf( f, "accepted_positions, %i\n", ( int ) accepted_positions );
        fprintf( f, "components, %i\n", ( int ) components );
        switch( dims )
        {
            case 1:
                for( size_t i = 0; i < xsize; i++ )
                {
                    fprintf( f, "%i, %f\n", ( int ) i, data[ i ] );
                }
                break;
            case 2:
                for( size_t i = 0; i < ysize; i++ )
                {
                    size_t offset = ypos * xsize + i;
                    fprintf( f, "%i, %f\n", ( int ) i, data[ offset ] );
                }
                break;
            case 3:
                for( size_t i = 0; i < zsize; i++ )
                {
                    size_t offset = ( xsize / 2 ) * ysize * zsize + ypos * zsize + i;
                    fprintf( f, "%i, %f\n", ( int ) i, data[ offset ] );
                }
                break;
        }
        fclose( f );
    }
    count++;
}

void z_snapshot_recorder::set_y_pos( int new_y_pos )
{
    ypos = new_y_pos;
}
