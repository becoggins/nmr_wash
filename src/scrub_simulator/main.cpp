//  Copyright (c) 2011-2012, Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <vector>
#include <string>
#include <iostream>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"
#include "console_support.h"
#include "data_generator.h"

const std::string usage_info( "scrub_simulator pattern_file output_file [options]\n" );
const std::string version_info( "scrub_simulator " + nmr_wash::version + " compiled " + __DATE__ "\n" + nmr_wash::copyright + "\n" );

std::vector< std::vector< int > > parse_position_list( std::vector< std::string > pos_list_str, int num_of_index_dims );
std::vector< std::string > generate_position_filenames( std::vector< std::vector< int > > pos_list, std::string output_filename );

int main( int argc, const char * argv[] )
{
    namespace po = boost::program_options;
    using namespace BEC_NMRData;
    using namespace nmr_wash;
    using namespace BEC_Misc;
    
    //  *********************************************************
    //  Define program options
    
    po::options_description general_options( "General options" );
    general_options.add_options()
    ( "help,h", "show this help message" )
    ( "version", "show version information" )
    ( "pattern,p", po::value< std::string >(), "sampling pattern file" )
    ( "quiet,q", "suppress information about the progress of the calculation" )
    ( "log", po::value< std::string >(), "record a log to a text file" )
    ( "verbose-log", "record complete details of the calculation to the log file" );
    
    po::options_description general_simulation_options( "General simulation options" );
    general_simulation_options.add_options()
    ( "size", po::value< int >()->default_value( 128 ), "the simulated spectrum will have this many points per axis" )
    ( "seed", po::value< unsigned int >()->default_value( 42u ), "seed for random number generator" );
    
    po::options_description predefined_simulation_options( "Predefined simulations" );
    predefined_simulation_options.add_options()
    ( "64-signal-test", "64 signals of varying intensity." )
    ( "wide-signal-test", "A very wide signal and a very narrow signal." )
    ( "dynamic-range-test", "Five signals at five orders of magnitude of SNR." );
    
    po::options_description custom_simulation_options( "Custom simulation" );
    custom_simulation_options.add_options()
    ( "next-last-signals", po::value< int >(), "the number of signals in the next to last dimension of the signal grid" )
    ( "last-signals", po::value< int >(), "the number of signals in the last dimension of the signal grid" )
    ( "negative-signals", "mirror the positively valued signal grid with a symmetric negatively valued grid" )
    ( "pow-scale", "set the heights of the signals to differ by orders of magnitude instead of linearly" )
    ( "variable-linewidth", "vary the linewidths of the signals, as well as the intensities" )
    ( "decay-function-height-factor", "for variable linewidths, multiply decay function by this amplitude" )
    ( "decay-function-width-factor", "for variable linewidths, scale time argument to decay function by this factor" )
    ( "noise", "include white noise in the simulated data" )
    ( "noise-mean", po::value< float >()->default_value( 0.0F ), "mean of the added white noise" )
    ( "noise-sigma", po::value< float >()->default_value( 0.0001F ), "standard deviation of the added white noise" );
    
    po::options_description simulation_output_options( "Simulation output options" );
    simulation_output_options.add_options()
    ( "input", po::value< std::string >(), "destination to save a copy of the simulated frequency domain before SCRUB processing" )
    ( "input-signals", po::value< std::string >(), "destination to save a copy of the simulated frequency domain signals before SCRUB processing" )
    ( "input-noise", po::value< std::string >(), "destination to save a copy of the simulated frequency domain noise before SCRUB processing" )
    ( "output", po::value< std::string >(), "destination for processed simulated frequency domain" )
    ( "control", po::value< std::string >(), "generate a control to simulate an equivalent conventional dataset" )
    ( "clean", po::value< std::string >(), "generate results for CLEAN on the same simulated input" )
    ( "correlation-input", po::value< std::string >(), "generate a text file comparing the output values to the input values" )
    ( "correlation-input-2d", po::value< std::string >(), "generate a text file comparing the output values for the YZ plane alone to the control values" )
    ( "correlation-control", po::value< std::string >(), "generate a text file comparing the output values to the input values" )
    ( "correlation-control-2d", po::value< std::string >(), "generate a text file comparing the output values for the YZ plane alone to the control values" )
    ( "snapshots", po::value< std::string >(), "make snapshots along the Z centerline during the run (snapshot text files are named by adding an underscore, an integer, and extension txt to the supplied name stem; each snapshot contains one entry per line)" )
    ( "snapshot-interval", po::value< int >()->default_value( 1 ), "take a snapshot every arg iterations" )
    ( "overwrite", "overwrite existing output files with the same names" );
    
    po::options_description pattern_parsing_options( "Sampling pattern parsing options (choosing any of these overrides automatic parsing of the sampling pattern)" );
    pattern_parsing_options.add_options()
    ( "u-col", po::value< int >(), "the column of the sampling pattern containing the U dimension" )
    ( "v-col", po::value< int >(), "the column of the sampling pattern containing the V dimension" )
    ( "w-col", po::value< int >(), "the column of the sampling pattern containing the W dimension" )
    ( "weight-col", po::value< int >(), "the column of the sampling pattern containing the weighting information" )
    ( "off-grid", "the sampling pattern is NOT defined on a grid" )
    ( "ignore-weights", "ignore any weights found in the sampling pattern file" );
    
    po::options_description scrub_options( "SCRUB options" );
    scrub_options.add_options()
    ( "gain", po::value< float >()->default_value( 10.0F ), "gain (%)" )
    ( "base", po::value< float >()->default_value( 0.01F ), "base" )
    ( "stopping-threshold", po::value< float >()->default_value( 2.0F ), "stopping threshold (std dev of the noise)" );
    
    po::options_description clean_options( "CLEAN options" );
    clean_options.add_options()
    ( "snr-threshold", po::value< float >()->default_value( 5.0F ), "CLEAN stops when the signal-to-noise ratio is less than or equal to this many standard deviations of the noise" )  //  N.B. This maps to threshold_intensity
    ( "noise-change-threshold", po::value< float >()->default_value( 5.0F ), "CLEAN stops when the average noise level has not changed more than this percentage for 25 consecutive iterations" )   //  N.B. This maps to threshold_noise
    ( "max-iter", po::value< int >(), "The maximum number of CLEAN iterations to allow for each position in the spectrum that is processed(the default is unlimited)" );
    
    po::options_description pure_comp_options( "Pure component calculation options" );
    pure_comp_options.add_options()
    ( "pure-comp-mode", po::value< pure_component_modes >()->default_value( contour_ellipsoid ), "calculation mode (contour-irregular, contour-ellipsoid, fixed-ellipsoid)" )
    ( "pc-contour-irregular-level", po::value< float >()->default_value( 1.0F ), "contour level for contour-irregular calculations (% of central peak intensity)" )
    ( "pc-contour-irregular-margin", po::value< bool >()->default_value( false ), "for contour-irregular calculations, whether or not to add a one point margin around the irregular contour" )
    ( "pc-contour-ellipsoid-level", po::value< float >()->default_value( 1.0F ), "contour level for contour-ellipsoid calculations (% of central peak intensity)" )
    ( "pc-contour-ellipsoid-margin", po::value< float >()->default_value( 0.0F ), "margin width for contour-ellipsoid calculations (after ellipsoid is circumscribed around contour, it is enlarged by this % of its own size)" )
    ( "pc-fixed-ellipsoid-size", po::value< float >()->default_value( 2.0F ), "size of ellipsoid for fixed-ellipsoid calculations (each semiaxis is this % of the spectral width)" );
    
    po::options_description psf_options( "PSF and pure component output" );
    psf_options.add_options()
    ( "psf", po::value< std::string >(), "save the calculated PSF to a spectrum file" )
    ( "pure-comp", po::value< std::string >(), "save the calculated pure component to a spectrum file" );
    
    po::options_description all_options;
    all_options.add( general_options ).add( general_simulation_options ).add( predefined_simulation_options ).add( custom_simulation_options ).add( simulation_output_options ).add( pattern_parsing_options ).add( scrub_options ).add( pure_comp_options ).add( psf_options );
    
    po::positional_options_description positional_options;
    positional_options.add( "pattern", 1 );
    positional_options.add( "output", 1 );
    
    po::variables_map vm;
    
    //  *********************************************************
    //  Parse command line
    
    try
    {
        po::store( po::command_line_parser( argc, argv).options( all_options ).positional( positional_options ).run(), vm );
        po::notify( vm );
    }
    catch( exceptions::bad_pure_component_mode_string &e )
    {
        std::cerr << "scrub_simulator: An invalid pure component calculation mode was provided.\n";
        return 1;
    }
    catch( po::unknown_option &e )
    {
        std::cerr << "scrub_simulator: Unrecognized option " << e.get_option_name() << ".\n";
        return 1;
    }
    catch( po::validation_error &e )
    {
        std::cerr << "scrub_simulator: Value for option " << e.get_option_name() << " is not valid: " << e.what() << "\n";
        return 1;
    }
    catch( po::invalid_option_value &e )
    {
        std::cerr << "scrub_simulator: An invalid option value was provided.\n";
        return 1;
    }
    catch( po::error &e )
    {
        std::cerr << "scrub_simulator: The command line options were not valid.\n";
        return 1;
    }
    
    //  *********************************************************
    //  Respond to information flags
    
    if( vm.count( "help" ) )
    {
        std::cout << usage_info << all_options << "\n";
        return 1;
    }
    if( vm.count( "version" ) )
    {
        std::cout << version_info << "\n";
        return 1;
    }
    
    //  *********************************************************
    //  Check for required parameters
    
    if( !vm.count( "pattern" ) )
    {
        std::cerr << "scrub_simulator: No sampling pattern.\n";
        return 1;
    }
    
    //  *********************************************************
    //  Get quiet flag
    bool quiet = vm.count( "quiet" ) ? true : false;
    
    FILE *log = 0;
    if( vm.count( "log" ) )
    {
        std::string log_filename( vm[ "log" ].as< std::string >() ); 
        log = fopen( log_filename.c_str(), "w+" );
        if( !log )
        {
            std::cerr << "scrub_simulator: The log file " << log_filename << "could not be created.\n";
            return 1;
        }
    }
    log_relay log_and_cout( log, std::cout, quiet );
    
    if( !quiet || log ) log_and_cout << version_info << "\n" << std::flush;
    
    //  *********************************************************
    //  Load sampling pattern
    
    std::string pattern_filename = vm[ "pattern" ].as< std::string >();
    if( !quiet || log ) log_and_cout << "Parsing sampling pattern file " << pattern_filename << "..." << std::flush;
    sampling_pattern sp;
    try
    {
        if( vm.count( "u-col" ) || vm.count( "v-col" ) || vm.count( "w-col" ) || vm.count( "weight-col" ) || vm.count( "off-grid" ) )
        {
            int u_col = vm.count( "u-col" ) ? vm[ "u-col" ].as< int >() - 1 : -1;
            int v_col = vm.count( "v-col" ) ? vm[ "v-col" ].as< int >() - 1 : -1;
            int w_col = vm.count( "v-col" ) ? vm[ "w-col" ].as< int >() - 1 : -1;
            int weight_col = vm.count( "weight-col" ) ? vm[ "weight-col" ].as< int >() - 1 : -1;
            bool on_grid = vm.count( "off-grid" ) ? false : true;
            
            sp.load( pattern_filename, u_col, v_col, w_col, weight_col, on_grid );
        }
        else sp.load( vm[ "pattern" ].as< std::string >() );
    }
    catch( exceptions::file_not_found &e )
    {
        std::cerr << "\nscrub_simulator: The pattern file " << e.filename << " could not be found.\n";
        return 1;
    }
    catch( exceptions::cant_open_file &e )
    {
        std::cerr << "\nscrub_simulator: The pattern file " << e.filename << " could not be opened.\n";
        return 1;
    }
    catch( exceptions::sampling_pattern::file_format_not_understood &e )
    {
        std::cerr << "\nscrub_simulator: The pattern file " << e.filename << " could not be interpreted automatically.\n";
        return 1;
    }
    catch( exceptions::sampling_pattern::file_format_not_consistent &e )
    {
        std::cerr << "\nscrub_simulator: The pattern file " << e.filename << " does not have a consistent format: one or more entries have extra or missing data fields.\n";
        return 1;
    }
    catch( exceptions::sampling_pattern::file_format_doesnt_match &e )
    {
        std::cerr << "\nscrub_simulator: The pattern file " << e.filename << " could not be processed according to the format options provided.\n";
        return 1;
    }
    catch( exceptions::sampling_pattern::bad_file_format_guidance &e )
    {
        std::cerr << "\nscrub_simulator: Invalid options for the pattern file format.\n";
        return 1;
    }
    
    if( !quiet || log )
    {
        log_and_cout << "done.\n";
        log_and_cout << "Sampling pattern has " << sp.get_num_of_dims() << " dimensions, ";
        log_and_cout << sp.size() << " sampling points, ";
        if( sp.got_weights() ) log_and_cout << "and weighting information.\n";
        else log_and_cout << "and no weighting information.\n";
        if( vm.count( "ignore-weights" ) && sp.got_weights() )
            log_and_cout << "Ignoring weights.\n";
        log_and_cout << std::flush;
    }
    
    if( vm.count( "ignore-weights" ) && sp.got_weights() )
    {
        sp.clear_weights();
    }
    
    if( !quiet || log ) log_and_cout << "\n" << std::flush;
    
    //  *********************************************************
    //  Build dimension map
    
    int dims = sp.get_num_of_dims();
    
    dimension_map dm;
    dm.F1 = u;
    if( dims >= 2 ) dm.F2 = v;
    if( dims == 3 ) dm.F3 = w;
    
    //  *********************************************************
    //  Construct status gatherer
    boost::scoped_ptr< status_reporter_scrub > sr;
    boost::scoped_ptr< console_status_scrub > cs;
    if( !quiet )
    {
        sr.reset( new status_reporter_scrub );
        cs.reset( new console_status_scrub( *sr ) );
    }
    
    //  *********************************************************
    //  Construct scrubber
    
    scrubber s( sp, dm, sr.get() );
    
    if( vm.count( "gain" ) ) s.gain = vm[ "gain" ].as< float >();
    if( vm.count( "stopping-threshold" ) ) s.stopping_threshold = vm[ "stopping-threshold" ].as< float >();
    if( vm.count( "base" ) ) s.base = vm[ "base" ].as< float >();
    
    if( vm.count( "pure-comp-mode" ) ) s.pure_component_mode = vm[ "pure-comp-mode" ].as< pure_component_modes >();
    if( vm.count( "pc-contour-irregular-level" ) ) s.pure_comp_ci_level = vm[ "pc-contour-irregular-level" ].as< float >();
    if( vm.count( "pc-contour-irregular-margin" ) ) s.pure_comp_ci_margin = vm[ "pc-contour-irregular-margin" ].as< bool >();
    if( vm.count( "pc-contour-ellipsoid-level" ) ) s.pure_comp_ce_level = vm[ "pc-contour-ellipsoid-level" ].as< float >();
    if( vm.count( "pc-contour-ellipsoid-margin" ) ) s.pure_comp_ce_margin = vm[ "pc-contour-ellipsoid-margin" ].as< float >();
    if( vm.count( "pc-fixed-ellipsoid-size" ) ) s.pure_comp_fe_size = vm[ "pc-fixed-ellipsoid-size" ].as< float >();
    
    s.log_file = log;
    if( vm.count( "verbose-log" ) ) s.verbose_log = true;
    
    int xsize, ysize, zsize;
    xsize = ysize = zsize = vm[ "size" ].as< int >();
    if( dims < 2 ) ysize = 1;
    if( dims < 3 ) zsize = 1;
    size_t size = xsize * ysize * zsize;
    
    boost::scoped_ptr< z_snapshot_recorder > zsr;
    if( vm.count( "snapshots" ) )
    {
        zsr.reset( new z_snapshot_recorder( vm[ "snapshots" ].as< std::string >(), xsize, ysize, zsize, ysize / 3, vm.count( "snapshot-interval" ) ? vm[ "snapshot-interval" ].as< int >() : 1 ) );
        boost::function< void ( bool, float, float, float, float, size_t, size_t, const float * ) > f( *zsr );
        s.snapshot_recorder = f;
    }
    
    pure_component_modes pcm = s.pure_component_mode;
    float pc_param_a, pc_param_b;
    switch( pcm )
    {
        case nmr_wash::contour_irregular:
            pc_param_a = s.pure_comp_ci_level;
            pc_param_b = s.pure_comp_ci_margin;
            break;
        case nmr_wash::contour_ellipsoid:
            pc_param_a = s.pure_comp_ce_level;
            pc_param_b = s.pure_comp_ce_margin;
            break;
        case nmr_wash::fixed_ellipsoid:
            pc_param_a = s.pure_comp_fe_size;
    }
    
    //  *********************************************************
    //  Generate simulated data
    
    if( !quiet || log ) log_and_cout << "Generating simulated data..." << std::flush;
    
    unsigned int seed = vm[ "seed" ].as< unsigned int >();
    
    boost::shared_array< float > input_signals, input_noise, input, control;
    
    if( vm.count( "64-signal-test" ) )
    {
        if( dims < 2 )
        {
            std::cerr << "\nscrub_simulator: The 64-signal test requires at least 2 dimensions.  Please choose a sampling pattern with either 2 or 3 dimensions.";
            return 1;
        }

        data_generator sdg( sp, dm, xsize, ysize, zsize, seed, pcm, pc_param_a, pc_param_b );
        
        int signals_y = 4;
        int signals_z = 8;
        for( int signal_y = 0; signal_y < signals_y; signal_y++ )
            for( int signal_z = 0; signal_z < signals_z; signal_z++ )
            {
                float intensity = 0.4F - ( float ) signal_z * ( 0.4 / ( float ) signals_z ) - ( float ) signal_y * ( 0.4 / ( ( float ) signals_z * ( float ) signals_y ) );

                float pxf = 0.5F;
                float pyf = ( ( float ) ( signal_y + 1 ) ) / ( ( float ) ( signals_y * 2 + 1 ) );
                float pzf = ( ( float ) ( signal_z + 1 ) ) / ( ( float ) ( signals_z + 1 ) );
                if( dims == 2 ) sdg.add_signal( pyf, pzf, 0.0F, intensity );
                else sdg.add_signal( pxf, pyf, pzf, intensity );
                
                float nxf = 0.5F;
                float nyf = ( ( float ) ( signal_y + signals_y + 1 ) ) / ( ( float ) ( signals_y * 2 + 1 ) );
                float nzf = ( ( float ) ( signal_z + 1 ) ) / ( ( float ) ( signals_z + 1 ) );
                if( dims == 2 ) sdg.add_signal( nyf, nzf, 0.0F, -intensity );
                sdg.add_signal( nxf, nyf, nzf, -intensity );
             }
        
        input_signals = sdg.fft_sparse();
        
        data_generator ndg( sp, dm, xsize, ysize, zsize, seed, sdg.get_env() );
        ndg.add_noise( 0.0F, 1.0F );     //  2.0
        input_noise = ndg.fft_sparse();
        
        control = sdg.fft_full();
        
        if( zsr ) zsr->set_y_pos( floorf( ( float ) ysize / ( ( float ) ( signals_y * 2 + 1 ) ) ) );
    }
    else if( vm.count( "wide-signal-test" ) )
    {
        data_generator sdg( sp, dm, xsize, ysize, zsize, seed, pcm, pc_param_a, pc_param_b );
        
        int signals_y = 1;
        int signals_z = 2;
        for( int signal_y = 0; signal_y < signals_y; signal_y++ )
            for( int signal_z = 0; signal_z < signals_z; signal_z++ )
            {
                float intensity = 0.4F - ( float ) signal_z * ( 0.4 / ( float ) signals_z ) - ( float ) signal_y * ( 0.4 / ( ( float ) signals_z * ( float ) signals_y ) );     //  Use standard intensity formula, then adjust when adding the signal
                
                float pxf = 0.5F;
                float pyf = ( ( float ) ( signal_y + 1 ) ) / ( ( float ) ( signals_y * 2 + 1 ) );
                float pzf = ( ( float ) ( signal_z + 1 ) ) / ( ( float ) ( signals_z + 1 ) );
                
                if( dims == 1 ) sdg.add_signal( pzf, 0.0F, 0.0F, intensity * powf( 10.0F, ( float ) signal_z ), 0.0F, 0.0F, ( float ) signal_z * 50.0F );
                else if( dims == 2 ) sdg.add_signal( pyf, pzf, 0.0F, intensity * powf( 10.0F, ( float ) signal_z ), 0.0F, 0.0F, ( float ) signal_z * 50.0F );
                else sdg.add_signal( pxf, pyf, pzf, intensity * powf( 10.0F, ( float ) signal_z ), 0.0F, 0.0F, ( float ) signal_z * 50.0F );
            }
        
        input_signals = sdg.fft_sparse();
        
        data_generator ndg( sp, dm, xsize, ysize, zsize, seed, sdg.get_env() );
        ndg.add_noise( 0.0F, 0.0001F );
        input_noise = ndg.fft_sparse();
        
        control = sdg.fft_full();

        if( zsr ) zsr->set_y_pos( floorf( ( float ) ysize / ( ( float ) ( signals_y * 2 + 1 ) ) ) );
    }
    else if( vm.count( "dynamic-range-test" ) )
    {
        data_generator sdg( sp, dm, xsize, ysize, zsize, seed, pcm, pc_param_a, pc_param_b );
        
        int signals_y = 1;
        int signals_z = 5;
        for( int signal_y = 0; signal_y < signals_y; signal_y++ )
            for( int signal_z = 0; signal_z < signals_z; signal_z++ )
            {
                //if( signal_z != 0 ) continue;
                float intensity = 0.4F * powf( 0.1F, ( float ) signal_z );
                
                float pxf = 0.5F;
                float pyf = ( ( float ) ( signal_y + 1 ) ) / ( ( float ) ( signals_y * 2 + 1 ) );
                float pzf = ( ( float ) ( signal_z + 1 ) ) / ( ( float ) ( signals_z + 1 ) );
                
                if( dims == 1 ) sdg.add_signal( pzf, 0.0F, 0.0F, intensity );
                else if( dims == 2 ) sdg.add_signal( pyf, pzf, 0.0F, intensity );
                else sdg.add_signal( pxf, pyf, pzf, intensity );
            }
        
        input_signals = sdg.fft_sparse();
        
        data_generator ndg( sp, dm, xsize, ysize, zsize, seed, sdg.get_env() );
        ndg.add_noise( 0.0F, 0.0001F );
        input_noise = ndg.fft_sparse();
        
        control = sdg.fft_full();

        if( zsr ) zsr->set_y_pos( floorf( ( float ) ysize / ( ( float ) ( signals_y * 2 + 1 ) ) ) );
    }
    else
    {
        data_generator sdg( sp, dm, xsize, ysize, zsize, seed, pcm, pc_param_a, pc_param_b );
        
        if( dims >= 2 && ( !vm.count( "next-last-signals" ) || !vm.count( "last-signals" ) ) )
        {
            std::cerr << "\nscrub_simulator: If you are not using a predefined simulation, you must specify the size of the signal grid by supplying the number of signals for the last dimension (--last-signals) and the number for the next to last dimension (--next-last-signals).";
            return 1;
        }
        if( dims == 1 && !vm.count( "last-signals" ) )
        {
            std::cerr << "\nscrub_simulator: If you are not using a predefined simulation, you must specify the number of signals using --last-signals.";
            return 1;
        }
        if( dims == 1 && vm.count( "negative-signals" ) )
        {
            std::cerr << "\nscrub_simulator: The --negative-signals option is not supported for 1-D simulations.";
            return 1;
        }
        
        int signals_y = vm.count( "next-last-signals" ) ? vm[ "next-last-signals" ].as< int >() : 1;
        int signals_z = vm[ "last-signals" ].as< int >();
        bool negative_signals = vm.count( "negative-signals" );
        bool pow_scale = vm.count( "pow-scale" );
        bool variable_linewidth = vm.count( "variable-linewidth" );
        float decay_h_factor = vm.count( "decay-function-height-factor" ) ? vm[ "decay-function-height-factor" ].as< float >() : 10.0F;
        float decay_w_factor = vm.count( "decay-function-width-factor" ) ? vm[ "decay-function-width-factor" ].as< float >() : 50.0F;
        
        for( int signal_y = 0; signal_y < signals_y; signal_y++ )
            for( int signal_z = 0; signal_z < signals_z; signal_z++ )
            {
                float intensity = pow_scale ? 0.4F * powf( 0.1F, ( float ) signal_z ): 0.4F - ( float ) signal_z * ( 0.4 / ( float ) signals_z ) - ( float ) signal_y * ( 0.4 / ( ( float ) signals_z * ( float ) signals_y ) );
                
                float pxf = 0.5F;
                float pyf = ( ( float ) ( signal_y + 1 ) ) / ( ( float ) ( signals_y * 2 + 1 ) );
                float pzf = ( ( float ) ( signal_z + 1 ) ) / ( ( float ) ( signals_z + 1 ) );
                if( dims == 1 )
                {
                    if( variable_linewidth ) sdg.add_signal( pzf, 0.0F, 0.0F, intensity * powf( decay_h_factor, ( float ) signal_z ), 0.0F, 0.0F, ( float ) signal_z * decay_w_factor );
                    else sdg.add_signal( pzf, 0.0F, 0.0F, intensity );
                }
                else if( dims == 2 )
                {
                    if( variable_linewidth ) sdg.add_signal( pyf, pzf, 0.0F, intensity * powf( decay_h_factor, ( float ) signal_z ), 0.0F, 0.0F, ( float ) signal_z * decay_w_factor );
                    else sdg.add_signal( pyf, pzf, 0.0F, intensity );                   
                }
                else
                {
                    if( variable_linewidth ) sdg.add_signal( pxf, pyf, pzf, intensity * powf( decay_h_factor, ( float ) signal_z ), 0.0F, 0.0F, ( float ) signal_z * decay_w_factor );
                    else sdg.add_signal( pxf, pyf, pzf, intensity );
                }
                
                if( negative_signals )
                {
                    float nxf = 0.5F;
                    float nyf = ( ( float ) ( signal_y + signals_y + 1 ) ) / ( ( float ) ( signals_y * 2 + 1 ) );
                    float nzf = ( ( float ) ( signal_z + 1 ) ) / ( ( float ) ( signals_z + 1 ) );
                    if( dims == 2 )
                    {
                        if( variable_linewidth ) sdg.add_signal( nyf, nzf, 0.0F, -intensity * powf( decay_h_factor, ( float ) signal_z ), 0.0F, 0.0F, ( float ) signal_z * decay_w_factor );
                        else sdg.add_signal( nyf, nzf, 0.0F, -intensity );
                    }
                    else
                    {
                        if( variable_linewidth ) sdg.add_signal( nxf, nyf, nzf, -intensity * powf( decay_h_factor, ( float ) signal_z ), 0.0F, 0.0F, ( float ) signal_z * decay_w_factor );
                        else sdg.add_signal( nxf, nyf, nzf, -intensity );
                    }
                }
            }
        
        input_signals = sdg.fft_sparse();
        
        data_generator ndg( sp, dm, xsize, ysize, zsize, seed, sdg.get_env() );
        if( vm.count( "noise" ) )
            ndg.add_noise( vm[ "noise-mean" ].as< float >(), vm[ "noise-sigma" ].as< float >() );
        input_noise = ndg.fft_sparse();
        
        control = sdg.fft_full();

        if( zsr ) zsr->set_y_pos( floorf( ( float ) ysize / ( ( float ) ( signals_y * 2 + 1 ) ) ) );
    }
    
    float input_signal_height = 0.0F, control_signal_height = 0.0F, control_signal_scale;
    for( size_t i = 0; i < size; i++ )
    {
        if( fast_abs( input_signals[ i ] ) >= input_signal_height ) input_signal_height = fast_abs( input_signals[ i ] );
        if( fast_abs( control[ i ] ) >= control_signal_height ) control_signal_height = fast_abs( control[ i ] );
    }
    control_signal_scale = input_signal_height / control_signal_height;
    
    input.reset( new float[ size ] );
    for( size_t i = 0; i < size; i++ )
    {
        input[ i ] = input_noise[ i ] + input_signals[ i ];
        control[ i ] *= control_signal_scale;
        control[ i ] += input_noise[ i ];
    }
    
    if( vm.count( "input" ) )
    {
        std::string filename( vm[ "input" ].as< std::string >() );
        boost::filesystem::path file_path( filename );
        if( boost::filesystem::exists( file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "\nscrub_simulator: The requested output file " << filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        make_snapshot( input.get(), filename, dims, xsize, ysize, zsize, 1000.0F );
    }

    if( vm.count( "input-signals" ) )
    {
        std::string filename( vm[ "input-signals" ].as< std::string >() );
        boost::filesystem::path file_path( filename );
        if( boost::filesystem::exists( file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "\nscrub_simulator: The requested output file " << filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        make_snapshot( input_signals.get(), filename, dims, xsize, ysize, zsize, 1000.0F );
    }
    
    if( vm.count( "input-noise" ) )
    {
        std::string filename( vm[ "input-noise" ].as< std::string >() );
        boost::filesystem::path file_path( filename );
        if( boost::filesystem::exists( file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "\nscrub_simulator: The requested output file " << filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        make_snapshot( input_noise.get(), filename, dims, xsize, ysize, zsize, 1000.0F );
    }
    
    if( vm.count( "control" ) )
    {
        std::string filename( vm[ "control" ].as< std::string >() );
        boost::filesystem::path file_path( filename );
        if( boost::filesystem::exists( file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "\nscrub_simulator: The requested output file " << filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        make_snapshot( control.get(), filename, dims, xsize, ysize, zsize, 1000.0F );
    }
    
    if( !quiet || log )
    {
        log_and_cout << "done.\n";
        if( vm.count( "64-signal-test" ) )
        {
            if( dims == 2 )
                log_and_cout << "64-signal test: The 2-D plane has been populated with 64 signals, half positive and half negative, of varying intensities, with significant white noise.\n\n";
            else
                log_and_cout << "64-signal test: The central YZ plane (i.e. that plane where X = size/2) has been populated with 64 signals, half positive and half negative, of varying intensities, with significant white noise.\n\n";
        }
        else if( vm.count( "wide-signal-test" ) )
        {
            if( dims == 1 )
                log_and_cout << "Wide signal test: The 1-D axis has been populated with two signals, one extremely narrow and one extremely wide, along with a very low level of white noise.\n\n";
            else if( dims == 2 )
                log_and_cout << "Wide signal test: The central F2 slice (i.e. that slice where F1 = size/2) has been populated with two signals, one extremely narrow and one extremely wide, along with a very low level of white noise.\n\n";
            else
                log_and_cout << "Wide signal test: The central F3 slice (i.e. that slice where F1 = size/2, F2 = size/2) has been populated with two signals, one extremely narrow and one extremely wide, along with a very low level of white noise.\n\n";
        }
        else if( vm.count( "dynamic-range-test" ) )
        {
            if( dims == 1 )
                log_and_cout << "Dynamic range test: The 1-D axis has been populated with five signals, each one order of magnitude weaker than the previous, with white noise at a level of about 1/3 the intensity of the weakest peak.\n\n";
            else if( dims == 2 )
                log_and_cout << "Dynamic range test: The central F2 slice (i.e. that slice where F1 = size/2) has been populated with five signals, each one order of magnitude weaker than the previous, with white noise at a level of about 1/3 the intensity of the weakest peak.\n\n";
            else
                log_and_cout << "Dynamic range test: The central F3 slice (i.e. that slice where F1 = size/2, F2 = size/2) has been poplated with five signals, each one order of magnitude weaker than the previous, with white noise at a level of about 1/3 the intensity of the weakest peak.\n\n";
        }
        else
        {
            int signals_y = vm[ "y-signals" ].as< int >();
            int signals_z = vm[ "z-signals" ].as< int >();
            bool negative_signals = vm.count( "negative-signals" );
            bool pow_scale = vm.count( "pow-scale" );
            bool variable_linewidth = vm.count( "variable-linewidth" );
            float decay_h_factor = vm.count( "decay-function-height-factor" ) ? vm[ "decay-function-height-factor" ].as< float >() : 10.0F;
            float decay_w_factor = vm.count( "decay-function-width-factor" ) ? vm[ "decay-function-width-factor" ].as< float >() : 50.0F;

            if( dims == 1 )
                log_and_cout << "The 1-D space has been populated with an array of " << signals_z << " positive signals";
            else if( dims == 2 )
                log_and_cout << "The 2-D space has been populated with a grid of " << signals_y << " x " << signals_z << " positive signals";
            else
                log_and_cout << "The central F2/F3 plane (i.e. that plane where F1 = size/2) has been populated with a grid of " << signals_y << " x " << signals_z << " positive signals";
            
            if( pow_scale ) log_and_cout << ", each one order of magnitude weaker than the previous";
            else log_and_cout << " of linearly varying intensity";
            
            if( negative_signals ) log_and_cout << ", as well as a corresponding grid of negatively valued signals.";
            else log_and_cout << ".";
            
            if( variable_linewidth ) log_and_cout << "  The linewidths and intensities of the signals have been adjusted using the decay height factor of " << decay_h_factor << " and the decay width factor of " << decay_w_factor << " (see the manual for more information).";
            
            if( vm.count( "noise" ) ) log_and_cout << "  White noise has been added with a mean of " << vm[ "noise-mean" ].as< float >() << " and a standard deviation of " << vm[ "noise-sigma" ].as< float >() << ".";
            
            log_and_cout << "\n\n";
                                                                                                                                                
        }
    }
    
    //  *********************************************************
    //  Process simulated data using SCRUB
    
    NMRData input_nd;
    if( dims == 1 )
    {
        input_nd.SetNumOfDims( 1 );
        input_nd.SetDimensionSize( 0, xsize );
        input_nd.SetDimensionLabel( 0, "F1" );
        input_nd.BuildInMem();
        input_nd.SetDataFromCopyPtr( input.get() );
    }
    else if( dims == 2 )
    {
        input_nd.SetNumOfDims( 2 );
        input_nd.SetDimensionSize( 0, xsize );
        input_nd.SetDimensionLabel( 0, "F1" );
        input_nd.SetDimensionSize( 1, ysize );
        input_nd.SetDimensionLabel( 1, "F2" );
        input_nd.BuildInMem();
        input_nd.SetDataFromCopyPtr( input.get() );
    }
    else
    {
        input_nd.SetNumOfDims( 3 );
        input_nd.SetDimensionSize( 0, xsize );
        input_nd.SetDimensionLabel( 0, "F1" );
        input_nd.SetDimensionSize( 1, ysize );
        input_nd.SetDimensionLabel( 1, "F2" );
        input_nd.SetDimensionSize( 2, zsize );
        input_nd.SetDimensionLabel( 2, "F3" );
        input_nd.BuildInMem();
        input_nd.SetDataFromCopyPtr( input.get() );
    }
    
    NMRData output_nd;
    output_nd.CopyParameters( &input_nd );
    output_nd.BuildInMem();
    
    if( !quiet || log ) log_and_cout << "Processing with SCRUB...\n";
    
    boost::scoped_ptr< boost::thread > cst;
    if( !quiet ) cst.reset( new boost::thread( *cs ) );
    
    try
    {
        s( input_nd, output_nd );
    }
    catch( exceptions::base_exception &e )
    {
        std::cerr << "scrub_simulator: An unexpected error occurred: " << e.what() << "\n";
        return 1;
    }
    
    if( !quiet ) cst->interrupt();
    
    if( !quiet || log ) log_and_cout << "\nCalculations completed.\n";
    
    boost::scoped_array< float > output( output_nd.GetDataCopyPtr() );
    
    if( vm.count( "output" ) )
    {
        std::string output_filename( vm[ "output" ].as< std::string >() );
        boost::filesystem::path output_file_path( output_filename );
        if( boost::filesystem::exists( output_file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "scrub_simulator: The requested output file " << output_filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        make_snapshot( output.get(), output_filename, dims, xsize, ysize, zsize, 1000.0F );
    }
    
    //  *********************************************************
    //  Process also with CLEAN (as a control, if requested)
    
    if( vm.count( "clean" ) )
    {
        if( !quiet || log ) log_and_cout << "\nProcessing with CLEAN (as requested, as a control)...\n";
        
        boost::scoped_ptr< status_reporter_clean > sr;
        boost::scoped_ptr< console_status_clean > cs;
        if( !quiet || log )
        {
            sr.reset( new status_reporter_clean );
            cs.reset( new console_status_clean( *sr ) );
        }
        
        cleaner c( sp, dm, sr.get() );
        
        if( vm.count( "gain" ) ) c.gain = vm[ "gain" ].as< float >();
        if( vm.count( "snr-threshold" ) ) c.threshold_intensity = vm[ "snr-threshold" ].as< float >();
        if( vm.count( "noise-change-threshold" ) ) c.threshold_noise = vm[ "noise-change-threshold" ].as< float >();
        if( vm.count( "max-iter" ) ) c.max_iter = vm[ "max-iter" ].as< int >();
        
        if( vm.count( "pure-comp-mode" ) ) s.pure_component_mode = vm[ "pure-comp-mode" ].as< pure_component_modes >();
        if( vm.count( "pc-contour-irregular-level" ) ) s.pure_comp_ci_level = vm[ "pc-contour-irregular-level" ].as< float >();
        if( vm.count( "pc-contour-irregular-margin" ) ) s.pure_comp_ci_margin = vm[ "pc-contour-irregular-margin" ].as< bool >();
        if( vm.count( "pc-contour-ellipsoid-level" ) ) s.pure_comp_ce_level = vm[ "pc-contour-ellipsoid-level" ].as< float >();
        if( vm.count( "pc-contour-ellipsoid-margin" ) ) s.pure_comp_ce_margin = vm[ "pc-contour-ellipsoid-margin" ].as< float >();
        if( vm.count( "pc-fixed-ellipsoid-size" ) ) s.pure_comp_fe_size = vm[ "pc-fixed-ellipsoid-size" ].as< float >();
        
        s.log_file = log;
        if( vm.count( "verbose-log" ) ) s.verbose_log = true;
        
        NMRData clean_output_nd;
        clean_output_nd.CopyParameters( &input_nd );
        clean_output_nd.BuildInMem();
        
        if( !quiet || log ) log_and_cout << "Processing with CLEAN...\n";
        
        boost::scoped_ptr< boost::thread > cst;
        if( !quiet ) cst.reset( new boost::thread( *cs ) );
        
        c( input_nd, clean_output_nd );
        
        if( !quiet ) cst->interrupt();
        
        if( !quiet || log ) log_and_cout << "\nCalculations completed.\n";
       
        std::string clean_output_filename( vm[ "clean" ].as< std::string >() );
        boost::filesystem::path clean_output_file_path( clean_output_filename );
        if( boost::filesystem::exists( clean_output_file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "scrub_simulator: The requested output file " << clean_output_filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
            
        boost::scoped_array< float > clean_output( clean_output_nd.GetDataCopyPtr() );
        
        make_snapshot( clean_output.get(), clean_output_filename, dims, xsize, ysize, zsize, 1000.0F );
    }
    
    //  *********************************************************
    //  Write correlation data (if requested)

    if( vm.count( "correlation-input" ) )
    {
        std::string filename( vm[ "correlation-input" ].as< std::string >() );
        boost::filesystem::path file_path( filename );
        
        if( boost::filesystem::exists( file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "scrub_simulator: The requested output file " << filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        FILE *f = fopen( filename.c_str(), "w+" );
        if( !f )
        {
            std::cerr << "scrub_simulator: The requested output file " << filename << " could not be opened.\n";
            return 1;
        }
        
        for( size_t i = 0; i < size; i++ )
            fprintf( f, "%f, %f\n", input[ i ], output[ i ] );
        fclose( f );
    }
    
    if( vm.count( "correlation-input-2d" ) )
    {
        if( dims < 3 )
        {
            std::cerr << "scrub_simulator: The 2-D correlation options are only applicable for 3-D simulations.  For simulations with 1 or 2 dimensions, please use the normal correlation option.";
            return 1;
        }
        
        std::string filename( vm[ "correlation-input-2d" ].as< std::string >() );
        boost::filesystem::path file_path( filename );
        
        if( boost::filesystem::exists( file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "scrub_simulator: The requested output file " << filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        FILE *f = fopen( filename.c_str(), "w+" );
        if( !f )
        {
            std::cerr << "scrub_simulator: The requested output file " << filename << " could not be opened.\n";
            return 1;
        }
        
        for( size_t i = 0; i < ysize * zsize; i++ )
        {
            size_t offset = ( xsize / 2 ) * ysize * zsize + i;
            //fprintf( f, "%f, %f\n", control[ offset ], output[ offset ] );
            fprintf( f, "%f, %f\n", input[ offset ], output[ offset ] );
        }
        fclose( f );
    }
    
    if( vm.count( "correlation-control" ) )
    {
        std::string filename( vm[ "correlation-control" ].as< std::string >() );
        boost::filesystem::path file_path( filename );
        
        if( boost::filesystem::exists( file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "scrub_simulator: The requested output file " << filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        FILE *f = fopen( filename.c_str(), "w+" );
        if( !f )
        {
            std::cerr << "scrub_simulator: The requested output file " << filename << " could not be opened.\n";
            return 1;
        }
        
        for( size_t i = 0; i < size; i++ )
            fprintf( f, "%f, %f\n", control[ i ], output[ i ] );
        fclose( f );
    }
    
    if( vm.count( "correlation-control-2d" ) )
    {
        if( dims < 3 )
        {
            std::cerr << "scrub_simulator: The 2-D correlation options are only applicable for 3-D simulations.  For simulations with 1 or 2 dimensions, please use the normal correlation option.";
            return 1;
        }

        std::string filename( vm[ "correlation-control-2d" ].as< std::string >() );
        boost::filesystem::path file_path( filename );
        
        if( boost::filesystem::exists( file_path ) && !vm.count( "overwrite" ) )
        {
            std::cerr << "scrub_simulator: The requested output file " << filename << " already exists.  Use the --overwrite option to overwrite.\n";
            return 1;
        }
        
        FILE *f = fopen( filename.c_str(), "w+" );
        if( !f )
        {
            std::cerr << "scrub_simulator: The requested output file " << filename << " could not be opened.\n";
            return 1;
        }
        
        for( size_t i = 0; i < ysize * zsize; i++ )
        {
            size_t offset = ( xsize / 2 ) * ysize * zsize + i;
            //fprintf( f, "%f, %f\n", control[ offset ], output[ offset ] );
            fprintf( f, "%f, %f\n", control[ offset ], output[ offset ] );
        }
        fclose( f );
    }
    
    //  *********************************************************
    //  Collect and save PSF and/or pure component (if requested)
    
    if( vm.count( "psf" ) )
    {
        boost::shared_ptr< NMRData > psf( convert_psf_or_pure_comp_to_spectrum( s.get_psf(), s.get_xsize(), s.get_ysize(), s.get_zsize() ) );
        
        boost::scoped_ptr< NMRData > psf_file;
        try
        {
            psf_file.reset( NMRData::GetObjForFileType( vm[ "psf" ].as< std::string >() ) );
        }
        catch( Exceptions::FileTypeNotRecognized &e )
        {
            std::cerr << "scrub_simulator: The file type for the PSF output file could not be recognized from its extension or is not a supported file type.\n";
            return 1;
        }
        try
        {
            psf_file->CopyParameters( psf.get() );
            psf_file->CreateFile( vm[ "psf" ].as< std::string >() );
        }
        catch( Exceptions::CantOpenFile &e )
        {
            std::cerr << "\nscrub_simulator: The PSF output file could not be created.\n";
            return 1;
        }
        catch( Exceptions::FileError &e )
        {
            std::cerr << "\nscrub_simulator: An error occurred while writing to the PSF output file.\n";
            return 1;
        }
        
        *psf_file = *psf;
    }
    
    if( vm.count( "pure-comp" ) )
    {
        boost::shared_ptr< NMRData > pc( convert_psf_or_pure_comp_to_spectrum( s.get_pure_component(), s.get_xsize(), s.get_ysize(), s.get_zsize() ) );
        
        boost::scoped_ptr< NMRData > pc_file;
        try
        {
            pc_file.reset( NMRData::GetObjForFileType( vm[ "pure-comp" ].as< std::string >() ) );
        }
        catch( Exceptions::FileTypeNotRecognized &e )
        {
            std::cerr << "scrub_simulator: The file type for the pure component output file could not be recognized from its extension or is not a supported file type.\n";
            return 1;
        }
        try
        {
            pc_file->CopyParameters( pc.get() );
            pc_file->CreateFile( vm[ "pure-comp" ].as< std::string >() );
        }
        catch( Exceptions::CantOpenFile &e )
        {
            std::cerr << "\nscrub_simulator: The pure component output file could not be created.\n";
            return 1;
        }
        catch( Exceptions::FileError &e )
        {
            std::cerr << "\nscrub_simulator: An error occurred while writing to the pure component output file.\n";
            return 1;
        }
        
        *pc_file = *pc;
    }
    
    if( log ) fclose( log );
    return 0;
}

