//  Copyright (c) 2006-2014, Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <memory.h>
#include <vector>
#include <boost/function.hpp>
#include <boost/shared_array.hpp>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"

using namespace BEC_Misc;
using namespace BEC_NMRData;
using namespace nmr_wash;
using namespace nmr_wash::details;

scrubber::scrubber( const sampling_pattern &pattern, const dimension_map dim_map, status_reporter_scrub *reporter ) : washer_base< scrubber, job_info_scrub, job_status_relay_scrub, job_callback_wrapper_scrub, status_reporter_scrub >( pattern, dim_map, reporter ), gain( 10.0F ), stopping_threshold( 2.0F ), base( 0.01F )
{
    //
}

void scrubber::scrub( job &j )
{
    try
    {
        job_status_relay_scrub *jsr = sri ? sri->start_job( j.get_job_id() ) : 0;
        
        float scrub_gain = gain / 100.0F;
        float scrub_base = base / 100.0F;
        
        FILE *log = j.get_log();
        if( log )
            j.write_log_section_header();
        
        size_t size = xsize * ysize * zsize;
        
        float *out = j.get_input();
        
        std::vector< component > components;
        
        noise_estimator ne( size > 32768 ? 32768 : size, size );
        ne( out );
        if( ne.noise_floor == 0.0F )
            return;
        
        std::vector< float > noise_record_raw;
        float nf_minimum = ne.noise_floor;
        size_t comp_index_at_nf_min = 0;
        size_t nf_index_at_nf_min = 0;
        position_list excluded_positions;
        
        for( float current_threshold = ne.noise_floor; ; current_threshold = ne.noise_floor )
        {
            if( log )
                fprintf( log, "  Beginning outer loop iteration...\n    At start: noise_floor = %f    noise_std_dev = %f    components = %i\n", ne.noise_floor, ne.noise_std_dev, ( int ) components.size() );
            
            if( jsr ? jsr->post( current_threshold, ne.noise_floor, ne.noise_std_dev, 0.0F, 0, ( int ) components.size() ) : false )
                return;
            if( worker_exception )
                return;
            
            position_list accepted_positions;
            float clean_level;
            
            size_t offset = 0;
            for( size_t i = 0; i < size; i++ )
                if( fast_abs( out[ i ] ) > fast_abs( out[ offset ] ) && !excluded_positions.is_on_list( i ) )
                    offset = i;
            
            if( !offset && excluded_positions.is_on_list( offset ) )
                break;
            
            if( fast_abs( out[ offset ] ) >= current_threshold )
            {
                accepted_positions.add( get_position( offset ) );
                clean_level = fast_abs( out[ offset ] );
                if( log && verbose_log )
                    fprintf( log, "    Starting with position %i, value = %f (clean_level = value)\n", ( int ) offset, out[ offset ] );
            }
            else break;
            
            if( jsr ? jsr->post( current_threshold, ne.noise_floor, ne.noise_std_dev, clean_level, ( int ) accepted_positions.size(), ( int ) components.size() ) : false )
                return;
            if( worker_exception )
                return;
            bool new_outer = true;
            
            int last_comp_size = 0;
            
            for( ; clean_level * ( float ) accepted_positions.size() > ne.noise_floor * scrub_base; clean_level -= scrub_gain * clean_level )
            {
                if( log && verbose_log )
                    fprintf( log, "    clean_level = %f     noise_floor = %f\n", clean_level, ne.noise_floor );

                if( snapshot_recorder )
                    snapshot_recorder( new_outer, current_threshold, ne.noise_floor, ne.noise_std_dev, clean_level, accepted_positions.size(), components.size(), out );
                new_outer = false;
                
                if( jsr ? jsr->post( current_threshold, ne.noise_floor, ne.noise_std_dev, clean_level, ( int ) accepted_positions.size(), ( int ) components.size() ) : false )
                    return;
                if( worker_exception )
                    return;
                
                sorted_data_vector sorted_data = sort_data( out, clean_level, ne.noise_floor, accepted_positions, excluded_positions );
                
                bool reset_inner;
                do
                {
                    reset_inner = false;
                    for( sorted_data_vector::iterator i = sorted_data.begin(); i != sorted_data.end() && i->first > clean_level; i++ )
                    {
                        position p = get_position( i->second );
                        
                        if( log && verbose_log )
                            fprintf( log, "      Testing position %03i ( x = %03i, y = %03i, z = %03i, value = %f )\n", ( int ) p.offset, p.xpos, p.ypos, p.zpos, out[ p.offset ] );
                        
                        if( accepted_positions.is_on_list( p.offset ) )
                        {
                            while( fast_abs( out[ p.offset ] ) > clean_level )
                            {
                                if( log && verbose_log )
                                    fprintf( log, "      On list and above clean_level (value = %f, clean_level = %f: cleaning...\n", out[ p.offset ], clean_level );
                                
                                components.push_back( component( p, out[ p.offset ] * scrub_gain * comp_scale ) );
                                subtract_psf( p, out, out[ p.offset ] * scrub_gain );
                            }
                            continue;
                        }
                        else if( i->first > ne.noise_floor )
                        {
                            if( log && verbose_log )
                                fprintf( log, "      Above cutoff: adding to list and cleaning...\n" );
                            accepted_positions.add( p );
                            clean_level = i->first;
                            
                            components.push_back( component( p, out[ p.offset ] * scrub_gain * comp_scale ) );
                            subtract_psf( p, out, out[ p.offset ] * scrub_gain );
                            reset_inner = true;
                        }
                        
                        if( reset_inner )
                        {
                            sorted_data = sort_data( out, clean_level, ne.noise_floor, accepted_positions, excluded_positions );
                            ne( out );
                            break;
                        }
                    }	//	inner loop
                }	while( reset_inner );	//	inner loop restart
                if( components.size() - last_comp_size > 10 )
                {
                    last_comp_size = ( int ) components.size();

                    ne( out );
                    noise_record_raw.push_back( ne.noise_floor );
                    
                    float nf_averaged = 0.0F;
                    if( noise_record_raw.size() >= 10 )
                    {
                        for( std::vector< float >::reverse_iterator i = noise_record_raw.rbegin(); i < noise_record_raw.rbegin() + 10; ++i )
                            nf_averaged += *i;
                    }
                    else continue;
                    nf_averaged /= 10.0F;
                    
                    if( nf_averaged < nf_minimum )
                    {
                        nf_minimum = nf_averaged;
                        comp_index_at_nf_min = components.size() - 1;
                        nf_index_at_nf_min = noise_record_raw.size() - 1;
                    }
                    else if( nf_averaged > 1.05F * nf_minimum && components.size() - comp_index_at_nf_min > 50 )
                    {
                        if( log )
                            fprintf( log, "   Backing out: noise_floor = %f, noise_floor_minimum = %f, components = %i, components at NFM = %i\n", nf_averaged, nf_minimum, ( int ) components.size(), ( int ) comp_index_at_nf_min );
                        std::vector< component >::iterator comp_min_it = components.begin() + comp_index_at_nf_min;
                        for( std::vector< component >::iterator i = components.end() - 1; i > comp_min_it; --i )
                        {
                            subtract_psf( i->pos, out, -i->scale / comp_scale );
                            if( !excluded_positions.is_on_list( i->pos ) )
                                excluded_positions.add( i->pos );
                            if( log && verbose_log )
                                fprintf( log, "      Reversed: position %i (x = %i, y = %i, z = %i), adding back %f.\n", ( int ) i->pos.offset, i->pos.xpos, i->pos.ypos, i->pos.zpos, i->scale );
                        }
                        components.erase( comp_min_it + 1, components.end() );
                        noise_record_raw.erase( noise_record_raw.begin() + nf_index_at_nf_min + 1, noise_record_raw.end() );
                        last_comp_size = ( int ) components.size();
                        break;
                    }
                    
                }
            }	//	middle loop
            ne( out );
        }	//	outer loop

        if( log )
        {
            ne( out );
            fprintf( log, "  Final:    noise_floor = %f    noise_std_dev = %f    components = %i\n", 
                    ne.noise_floor, ne.noise_std_dev, ( int ) components.size() );
        }
        
        if( components_only )
            memset( out, 0, sizeof( float ) * size );
        
        if( !residuals_only )
            add_pure_components( out, components );
        
        j.post_output( out );
        
        if( sri )
            sri->end_job( j.get_job_id() );
    }	//	try
    catch( std::exception &e )
    {
        boost::lock_guard< boost::mutex > lock( worker_exception_mutex );
        worker_exception = boost::current_exception();
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        boost::lock_guard< boost::mutex > lock( worker_exception_mutex );
        worker_exception = boost::copy_exception( nmr_wash::exceptions::nmrdata_library_exception( e.what() ) );
    }
}


//  SCRUB v1 (as published in JACS)
//
//  This is outstanding for 4-D data, but the thresholds are insufficiently aggressive to work well on 3-D
//  data, where the gap between the signals and the noise floor can be extremely small.

void scrubber::scrub_v1( job &j )
{
    try
    {
        job_status_relay_scrub *jsr = sri ? sri->start_job( j.get_job_id() ) : 0;
        
        float scrub_gain = gain / 100.0F;
        float scrub_base = base / 100.0F;
        float scrub_end = stopping_threshold;
        
        FILE *log = j.get_log();
        if( log )
            j.write_log_section_header();
        
        size_t size = xsize * ysize * zsize;
        
        float *out = j.get_input();
        float *sub = new float[ xsize * ysize * zsize ];
        memset( sub, 0, sizeof( float ) * xsize * ysize * zsize );
        
        size_t offset = 0;
        
        std::vector< component > components;
        position_list excluded_positions;
        
        noise_estimator ne( size, size );
        ne( out );
        if( log )
            fprintf( log, "  Initial\n    noise_floor = %f\n    noise_std_dev = %f\n", ne.noise_floor, ne.noise_std_dev );
         
        for( float current_threshold = ne.noise_floor; 
            current_threshold > scrub_end * ne.noise_std_dev; 
            current_threshold -= ne.noise_std_dev * 0.5F )
        {
            if( log )
                fprintf( log, "  Beginning next outer loop iteration...\n    noise_floor = %f\n    noise_std_dev = %f\n    current_threshold = %f\n    components = %i\n", 
                        ne.noise_floor, ne.noise_std_dev, current_threshold, ( int ) components.size() );
            
            if( jsr ? jsr->post( current_threshold, ne.noise_floor, ne.noise_std_dev, 0.0F, 0, ( int ) components.size() ) : false )
                return;
            if( worker_exception )
                return;
           
            position_list accepted_positions;
            float clean_level;
            
            for( size_t i = 0; i < size; i++ )
                if( fast_abs( out[ i ] ) > fast_abs( out[ offset ] ) )
                    offset = i;
            
            if( fast_abs( out[ offset ] ) >= ne.noise_floor + current_threshold )
            {
                accepted_positions.add_note_neighbors( get_position( offset ), xsize, ysize, zsize );
                clean_level = fast_abs( out[ offset ] );
            }
            else continue;
            
            if( jsr ? jsr->post( current_threshold, ne.noise_floor, ne.noise_std_dev, clean_level, ( int ) accepted_positions.size(), ( int ) components.size() ) : false )
                return;
            if( worker_exception )
                return;
            bool new_outer = true;
            
            bool recalc_noise;
            
            for( ; clean_level * ( float ) accepted_positions.size() > ne.noise_floor * scrub_base; clean_level -= scrub_gain * clean_level )
            {
                if( log && verbose_log )
                    fprintf( log, "    clean_level = %f\n", clean_level );
                
                if( snapshot_recorder )
                    snapshot_recorder( new_outer, current_threshold, ne.noise_floor, ne.noise_std_dev, clean_level, accepted_positions.size(), components.size(), out );
                new_outer = false;
                
                if( jsr ? jsr->post( current_threshold, ne.noise_floor, ne.noise_std_dev, clean_level, ( int ) accepted_positions.size(), ( int ) components.size() ) : false )
                    return;
                if( worker_exception )
                    return;
                
                sorted_data_vector sorted_data = sort_data( out, clean_level, ne.noise_floor + 0.5F * current_threshold, accepted_positions, excluded_positions );
                
                bool reset_inner;
                do
                {
                    reset_inner = false;
                    recalc_noise = false;
                    for( sorted_data_vector::iterator i = sorted_data.begin(); i != sorted_data.end() && i->first > clean_level; i++ )
                    {
                        position p = get_position( i->second );
                        
                        if( log && verbose_log )
                            fprintf( log, "      Testing position %03i ( x = %03i, y = %03i, z = %03i, value = %f )\n", ( int ) p.offset, p.xpos, p.ypos, p.zpos, out[ p.offset ] );
                        
                        if( accepted_positions.is_on_list( p.offset ) )
                        {
                            while( fast_abs( out[ p.offset ] ) > clean_level )
                            {
                                if( log && verbose_log )
                                    fprintf( log, "      On list and above clean_level (value = %f, clean_level = %f: cleaning...\n", out[ p.offset ], clean_level );
                                
                                components.push_back( component( p, out[ p.offset ] * scrub_gain ) );
                                subtract_psf( p, out, out[ p.offset ] * scrub_gain );
                                memset( sub, 0, sizeof( float ) * xsize * ysize * zsize );
                                subtract_psf( p, sub, out[ p.offset ] * scrub_gain );
                                recalc_noise = true;
                            }
                            continue;
                        }
                        else if( accepted_positions.is_neighbor( p ) )
                        {
                            if( i->first > ne.noise_floor + 0.5F * current_threshold && i->first > clean_level + 0.5F * current_threshold )
                            {
                                if( log && verbose_log )
                                    fprintf( log, "      Neighbor of listed positions and above cutoff: adding to list and cleaning...\n" );
                                
                                accepted_positions.add_note_neighbors( p, xsize, ysize, zsize );
                                clean_level = i->first;
                                
                                components.push_back( component( p, out[ p.offset ] * scrub_gain ) );
                                subtract_psf( p, out, out[ p.offset ] * scrub_gain );
                                memset( sub, 0, sizeof( float ) * xsize * ysize * zsize );
                                subtract_psf( p, sub, out[ p.offset ] * scrub_gain );
                                reset_inner = true;
                            }
                            else continue;
                        }
                        else if( i->first > ne.noise_floor + current_threshold && i->first > clean_level + current_threshold )
                        {
                            if( log && verbose_log )
                                fprintf( log, "      Above cutoff: adding to list and cleaning...\n" );
                            accepted_positions.add_note_neighbors( p, xsize, ysize, zsize );
                            clean_level = i->first;
                            
                            components.push_back( component( p, out[ p.offset ] * scrub_gain ) );
                            subtract_psf( p, out, out[ p.offset ] * scrub_gain );
                            memset( sub, 0, sizeof( float ) * xsize * ysize * zsize );
                            subtract_psf( p, sub, out[ p.offset ] * scrub_gain );
                            reset_inner = true;
                        }
                        
                        if( reset_inner )
                        {
                            sorted_data = sort_data( out, clean_level, ne.noise_floor + 0.5F * current_threshold, accepted_positions, excluded_positions );
                            ne( out );
                            break;
                        }
                    }	//	inner loop
                }	while( reset_inner );	//	inner loop restart
            }	//	middle loop
            if( recalc_noise ) ne( out );
        }	//	outer loop
        
        if( log )
            fprintf( log, "  Final\n    noise_floor = %f\n    noise_std_dev = %f\n    components = %i\n", 
                    ne.noise_floor, ne.noise_std_dev, ( int ) components.size() );
       
        add_pure_components( out, components );

        j.post_output( out );
        
        if( sri )
            sri->end_job( j.get_job_id() );
    }	//	try
    catch( std::exception &e )
    {
        boost::lock_guard< boost::mutex > lock( worker_exception_mutex );
        worker_exception = boost::current_exception();
    }
    catch( BEC_NMRData::Exceptions::BaseException &e )
    {
        boost::lock_guard< boost::mutex > lock( worker_exception_mutex );
        worker_exception = boost::copy_exception( nmr_wash::exceptions::nmrdata_library_exception( e.what() ) );
    }
}

sorted_data_vector scrubber::sort_data( const float *data, float clean_level, float threshold, const position_list &accepted_positions, const position_list &excluded_positions )
{
    sorted_data_vector sd;
    size_t size = xsize * ysize * zsize;
    for( size_t i = 0; i < size; i++ )
        if( !excluded_positions.is_on_list( i ) &&
           ( fast_abs( data[ i ] ) >= threshold || 
           ( fast_abs( data[ i ] ) > clean_level && accepted_positions.is_on_list( i ) ) ) )
            sd.push_back( std::pair< float, size_t >( fast_abs( data[ i ] ), i ) );
    
    std::sort( sd.begin(), sd.end(), sorted_data_entry_greater() );
    
    return sd;
}


