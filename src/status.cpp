//  Copyright (c) 2012, Brian E. Coggins, Ph.D.  All rights reserved.

#include <assert.h>
#include <map>
#include <boost/thread.hpp>
#include "nmr_wash.h"

using namespace nmr_wash;
using namespace nmr_wash::details;


status_reporter_clean::status_reporter_clean()
{
    impl = new status_reporter_impl< job_info_clean, job_status_relay_clean >();
}

status_reporter_clean::~status_reporter_clean()
{
    delete impl;
}

int status_reporter_clean::get_num_of_workers() const
{
    assert( impl );
    return impl->get_num_of_workers();
}

const std::map< int, job_info_clean > status_reporter_clean::get_snapshot() const
{
    assert( impl );
    return impl->get_snapshot();
}

void status_reporter_clean::cancel()
{
    assert( impl );
    impl->cancel();
}

status_reporter_scrub::status_reporter_scrub()
{
    impl = new status_reporter_impl< job_info_scrub, job_status_relay_scrub >();
}

status_reporter_scrub::~status_reporter_scrub()
{
    delete impl;
}

int status_reporter_scrub::get_num_of_workers() const
{
    assert( impl );
    return impl->get_num_of_workers();
}

const std::map< int, job_info_scrub > status_reporter_scrub::get_snapshot() const
{
    assert( impl );
    return impl->get_snapshot();
}

void status_reporter_scrub::cancel()
{
    assert( impl );
    impl->cancel();
}

template< typename JobInfoClass, typename JobStatusRelayClass >
status_reporter_impl< JobInfoClass, JobStatusRelayClass >::~status_reporter_impl()
{
    if( relays.size() )
    {
        for( relay_map_iterator i = relays.begin(); i != relays.end(); ++i )
            delete i->second;
        relays.clear();
    }
}

template< typename JobInfoClass, typename JobStatusRelayClass >
int status_reporter_impl< JobInfoClass, JobStatusRelayClass >::get_num_of_workers() const
{
    boost::shared_lock< boost::shared_mutex > lock( mtx );
    return num_of_workers;
}

template< typename JobInfoClass, typename JobStatusRelayClass >
std::map< int, JobInfoClass > status_reporter_impl< JobInfoClass, JobStatusRelayClass >::get_snapshot() const
{
    std::map< int, JobInfoClass > snapshot;
    
    boost::shared_lock< boost::shared_mutex > main_lock( mtx );
    
    for( const_job_map_iterator i = jobs.begin(); i != jobs.end(); ++i )
    {
        const_relay_map_iterator relay_it = relays.find( i->first );
        if( relay_it != relays.end() )
        {
            boost::shared_lock< boost::shared_mutex > relay_lock( relay_it->second->get_mutex() );
            snapshot.insert( *i );
        }
        else
            snapshot.insert( *i );
    }
    
    return snapshot;
}

template< typename JobInfoClass, typename JobStatusRelayClass >
void status_reporter_impl< JobInfoClass, JobStatusRelayClass >::cancel()
{
    boost::unique_lock< boost::shared_mutex > main_lock( mtx );
    cancel_signal = true;
    
    for( relay_map_iterator i = relays.begin(); i != relays.end(); ++i )
        i->second->cancel();
}

template< typename JobInfoClass, typename JobStatusRelayClass >
void status_reporter_impl< JobInfoClass, JobStatusRelayClass >::clear()
{
    assert( !num_of_workers );
    
    boost::unique_lock< boost::shared_mutex > lock( mtx );

    jobs.clear();
    
    if( relays.size() )
    {
        for( relay_map_iterator i = relays.begin(); i != relays.end(); ++i )
            delete i->second;
        relays.clear();
    }
}

template< typename JobInfoClass, typename JobStatusRelayClass >
int status_reporter_impl< JobInfoClass, JobStatusRelayClass >::get_new_job_id()
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );
    
    return next_job_num++;
}

template< typename JobInfoClass, typename JobStatusRelayClass >
void status_reporter_impl< JobInfoClass, JobStatusRelayClass >::register_job( int job_id, job j )
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );
    
    if( j.is_full() )
    {
        JobInfoClass info( job_id, j.get_input_src() );
        jobs.insert( std::pair< int, JobInfoClass >( job_id, info ) );
    }
    else
    {
        JobInfoClass info( job_id, j.get_input_src(), *( j.get_position() ) );
        jobs.insert( std::pair< int, JobInfoClass >( job_id, info ) );
    }
}

template< typename JobInfoClass, typename JobStatusRelayClass >
void status_reporter_impl< JobInfoClass, JobStatusRelayClass >::register_jobs( std::vector< job > &jobs )
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );
    
    for( std::vector< job >::iterator i = jobs.begin(); i != jobs.end(); ++i )
    {
        job &j = *i;
        
        if( j.is_full() )
        {
            JobInfoClass info( j.get_job_id(), j.get_input_src() );
            this->jobs.insert( std::pair< int, JobInfoClass >( j.get_job_id(), info ) );
        }
        else
        {
            JobInfoClass info( j.get_job_id(), j.get_input_src(), *( j.get_position() ) );
            this->jobs.insert( std::pair< int, JobInfoClass >( j.get_job_id(), info ) );
        }
    }
}

template< typename JobInfoClass, typename JobStatusRelayClass >
JobStatusRelayClass * status_reporter_impl< JobInfoClass, JobStatusRelayClass >::start_job( int job_id )
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );
    
    job_map_iterator i = jobs.find( job_id );
    i->second.started = true;
    num_of_workers++;
    
    JobStatusRelayClass *relay = new JobStatusRelayClass( i );
    relays.insert( std::pair< int, JobStatusRelayClass * >( job_id, relay ) );
    
    return relay;
}

template< typename JobInfoClass, typename JobStatusRelayClass >
void status_reporter_impl< JobInfoClass, JobStatusRelayClass >::end_job( int job_id )
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );
    
    relay_map_iterator relay_it = relays.find( job_id );
    delete relay_it->second;
    relays.erase( relay_it );
    
    job_map_iterator i = jobs.find( job_id );
    i->second.done = true;
    num_of_workers--;

    return;
}

bool job_status_relay_clean::post( int iteration, float signal, float noise_floor, float noise_std_dev, float criteria_a, float criteria_b )
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );
    
    it->second.iteration = iteration;
    it->second.signal = signal;
    it->second.noise_floor = noise_floor;
    it->second.noise_std_dev = noise_std_dev;
    it->second.criteria_a = criteria_a;
    it->second.criteria_b = criteria_b;
    if( it->second.noise_floor_initial == 0.0F ) it->second.noise_floor_initial = noise_floor;
    
    return cancel_signal;
}

void job_status_relay_clean::cancel()
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );
    
    cancel_signal = true;
}

boost::shared_mutex & job_status_relay_clean::get_mutex()
{
    return mtx;
}

bool job_status_relay_scrub::post( float current_threshold, float noise_floor, float noise_std_dev, float clean_level, int accepted_positions, int components )
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );
    
    it->second.current_threshold = current_threshold;
    it->second.noise_floor = noise_floor;
    it->second.noise_std_dev = noise_std_dev;
    it->second.clean_level = clean_level;
    it->second.num_of_accepted_positions = accepted_positions;
    it->second.num_of_clean_components = components;
    if( it->second.noise_floor_initial == 0.0F ) it->second.noise_floor_initial = noise_floor;
    
    return cancel_signal;
}

void job_status_relay_scrub::cancel()
{
    boost::unique_lock< boost::shared_mutex > lock( mtx );

    cancel_signal = true;
}

boost::shared_mutex & job_status_relay_scrub::get_mutex()
{
    return mtx;
}

namespace nmr_wash
{
    namespace details
    {
        template class status_reporter_impl< job_info_scrub, job_status_relay_scrub >;
        template class status_reporter_impl< job_info_clean, job_status_relay_clean >;
    }
}

