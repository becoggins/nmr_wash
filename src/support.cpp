//  Copyright (c) 2011-2012, Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <vector>
#include <set>
#include <boost/math/special_functions/erf.hpp>
#include <bec_misc/becmisc.h>
#include <bec_misc/bec_minpack.h>
#include "nmr_wash.h"

using namespace BEC_Misc;
using namespace nmr_wash;
using namespace nmr_wash::details;

logger::~logger()
{
    if( log_file )
    {
        char buffer[ 2048 ];
        for( std::map< int, FILE * >::iterator i = tmp_files.begin(); i != tmp_files.end(); ++i )
        {
            fprintf( log_file, "\n" );
            FILE *tmp = i->second;
            fseek( tmp, 0, SEEK_SET );
            while( fgets( buffer, 2048, tmp ) )
                fputs( buffer, log_file );
            fclose( tmp );            
        }
    }
}

FILE *logger::register_job( int id )
{
    if( log_file )
    {
        FILE *tmp = tmpfile();
        tmp_files.insert( std::pair< int, FILE * >( id, tmp ) );
        return tmp;
    }
    else return 0;
}

noise_estimator::noise_estimator( size_t size, int total_size, int bins_ ) : n( size ), total_n( total_size ), noise_floor( 0.0F ), noise_avg( 0.0F ), noise_std_dev( 0.0F ), min( 0.0F ), max( 0.0F ), avg( 0.0F ), std_dev( 0.0F ), bins( bins_ ), histogram( bins_ ), xvalues( bins_ ), yvalues( bins_ ), guess( 6 ), fit( 6 ), fitter( bins_, 6, eval, 10000 )
{
    double ratio = ( ( double ) total_n - 1.0 ) / ( double ) total_n;
    if( ratio >= 1.0 )
        nf_factor = ceilf( sqrtf( 2.0F ) * 4.0F );
    else
        nf_factor = ceilf( sqrtf( 2.0F ) * ( float ) boost::math::erf_inv( ratio ) );
}

void noise_estimator::operator()( const float *data )
{
    using namespace BEC_MINPACK;
    
    min = 0.0F, max = 0.0F, avg = 0.0, std_dev = 0.0;
    for( int i = 0; i < n; i++ )
    {
        avg += data[ i ] / ( float ) n;
        if( data[ i ] < min ) min = data[ i ];
        if( data[ i ] > max ) max = data[ i ];
    }
    if( min == 0.0F && max == 0.0F )
    {
        noise_floor = 0.0F;
        return;
    }
    float bin_width = ( max - min ) / ( float ) bins;
    
    for( int i = 0; i < bins; i++ )
        histogram[ i ] = bin_element( min + i * bin_width, 0 );
    
    for( int i = 0; i < n; i++ )
    {
        float value = data[ i ];
        std_dev += fast_sqr( value - avg );
        int bin = ( int ) ( value - min ) / bin_width;
        if( bin < 0 ) bin = 0;
        if( bin >= bins ) bin = bins - 1;
        histogram[ bin ].second++;
    }
    
    std_dev = sqrtf( std_dev / ( float ) n );
    
    int histogram_max_point = 0;
    for( int i = 0; i < bins; i++ )
        if( histogram[ i ].second > histogram[ histogram_max_point ].second )
            histogram_max_point = i;
    
    for( int i = 0; i < bins; i++ )
    {
        xvalues[ i ] = ( double ) ( histogram[ i ].first + 0.5 * bin_width );
        yvalues[ i ] = ( double ) histogram[ i ].second;
    }
    
    guess[ 0 ] = ( double ) histogram[ histogram_max_point ].second;							//	a1
    guess[ 1 ] = ( double ) ( histogram[ histogram_max_point ].first + 0.5 * bin_width );		//	mu1
    guess[ 2 ] = std_dev;																		//	sigma1
    guess[ 3 ] = 1.0;																			//	a2
    guess[ 4 ] = 1.0;																			//	mu2
    guess[ 5 ] = 1.0;																			//	sigma2

    double tol = sqrt( std::numeric_limits< double >::epsilon() );
    
    FitDataToModel_LM_AnalyticJacobian< DualGaussianFunctionEvaluator >::Result result = 
    fitter( xvalues, yvalues, guess, tol, fit );

    if( fast_abs( fast_abs( fit[ 2 ] ) - std_dev ) <= fast_abs( fast_abs( fit[ 5 ] ) - std_dev ) )
    {
        noise_avg = ( float ) fit[ 1 ];
        noise_std_dev = fast_abs( ( float ) fit[ 2 ] );
    }
    else
    {
        noise_avg = ( float ) fit[ 4 ];
        noise_std_dev = fast_abs( ( float ) fit[ 5 ] );
    }
    noise_floor = fast_abs( noise_avg ) + nf_factor * noise_std_dev;
}

void vector_noise_estimator::operator()( const std::vector< float > data )
{
    using namespace BEC_MINPACK;
    
    int n = data.size();
    
    float min = 0.0F, max = 0.0F, avg = 0.0, std_dev = 0.0;
    for( int i = 0; i < n; i++ )
    {
        avg += data[ i ] / ( float ) n;
        if( data[ i ] < min ) min = data[ i ];
        if( data[ i ] > max ) max = data[ i ];
    }
    float bin_width = ( max - min ) / ( float ) bins;
    
    for( int i = 0; i < bins; i++ )
        histogram[ i ] = bin_element( min + i * bin_width, 0 );
    
    for( int i = 0; i < n; i++ )
    {
        float value = data[ i ];
        std_dev += fast_sqr( value - avg );
        int bin = ( int ) ( value - min ) / bin_width;
        if( bin < 0 ) bin = 0;
        if( bin >= bins ) bin = bins - 1;
        histogram[ bin ].second++;
    }
    
    std_dev = sqrtf( std_dev / ( float ) n );
    
    int histogram_max_point = 0;
    for( int i = 0; i < bins; i++ )
        if( histogram[ i ].second > histogram[ histogram_max_point ].second )
            histogram_max_point = i;
    
    for( int i = 0; i < bins; i++ )
    {
        xvalues[ i ] = ( double ) ( histogram[ i ].first + 0.5 * bin_width );
        yvalues[ i ] = ( double ) histogram[ i ].second;
    }
    
    guess[ 0 ] = ( double ) histogram[ histogram_max_point ].second;							//	a1
    guess[ 1 ] = ( double ) ( histogram[ histogram_max_point ].first + 0.5 * bin_width );		//	mu1
    guess[ 2 ] = std_dev;																		//	sigma1
    guess[ 3 ] = 1.0;																			//	a2
    guess[ 4 ] = 1.0;																			//	mu2
    guess[ 5 ] = 1.0;																			//	sigma2
    
    double tol = sqrt( std::numeric_limits< double >::epsilon() );
    
    FitDataToModel_LM_AnalyticJacobian< DualGaussianFunctionEvaluator >::Result result = 
    fitter( xvalues, yvalues, guess, tol, fit );
    
    float mu, sigma;
    if( fast_abs( fast_abs( fit[ 2 ] ) - std_dev ) <= fast_abs( fast_abs( fit[ 5 ] ) - std_dev ) )
    {
        mu = ( float ) fit[ 1 ];
        sigma = fast_abs( ( float ) fit[ 2 ] );
    }
    else
    {
        mu = ( float ) fit[ 4 ];
        sigma = fast_abs( ( float ) fit[ 5 ] );
    }

    noise_mean = mu;
    noise_std_dev = sigma;
}

//  PLANE SNAPSHOT RECORDER: record 2-D snapshots during the processing of a 2-D data space
//
//  This facility records 2-D snapshots from a 2-D SCRUB/CLEAN calculation and saves them as a
//  pseudo-3-D spectrum with F1 = iteration, F2 = X, F3 = Y.  This could easily be extended
//  to save the deltas between iterations; see the vector_snapshot_recorder below.  Recording
//  2-D planes within a 3-D space would require some more work in terms of specifying the
//  position and extracting X/Y or X/Z rather than the easier Y/Z planes.
//
//  To use, paste the code below into the main procedure shortly before the call to start the calculation:
//
//      details::plane_snapshot_recorder psr( filename, xsize, ysize, interval );
//      boost::function< void ( bool, float, float, float, float, size_t, size_t, const float * ) > f( psr );
//      s.snapshot_recorder = f;
//  
//  where:
//      filename = name of the output file to generate (must end in an extension recognized by the NMRData library)
//      xsize, ysize = sizes of these dimensions (obtain from input object)
//      interval = how often to record a sample
//      s = scrubber or cleaner object

plane_snapshot_recorder::plane_snapshot_recorder( std::string filename_, int xsize_, int ysize_, int interval_ ) : filename( filename_ ), xsize( xsize_ ), ysize( ysize_ ), interval( interval_ ), count( 0 )  {  }

plane_snapshot_recorder::~plane_snapshot_recorder()
{
    boost::shared_ptr< BEC_NMRData::NMRData > out( BEC_NMRData::NMRData::GetObjForFileType( filename ) );
    out->SetNumOfDims( 3 );
    out->SetDimensionSize( 0, ( int ) data_record.size() );
    out->SetDimensionLabel( 0, "Iteration" );
    out->SetDimensionCalibration( 0, 2000.0F, 100.0F, 10.0F );
    out->SetDimensionSize( 1, xsize );
    out->SetDimensionLabel( 1, "X" );
    out->SetDimensionCalibration( 1, 2000.0F, 100.0F, 10.0F );
    out->SetDimensionSize( 2, ysize );
    out->SetDimensionLabel( 2, "Y" );
    out->SetDimensionCalibration( 2, 2000.0F, 100.0F, 10.0F );
    out->CreateFile( filename );
    
    int current_index = 0;
    for( std::vector< boost::shared_array< float > >::iterator i = data_record.begin(); i != data_record.end(); ++i, current_index++ )
    {
        std::vector< int > coord1( 3 ), coord2( 3 );
        coord1[ 0 ] = current_index;
        coord1[ 1 ] = 0;
        coord1[ 2 ] = 0;
        coord2[ 0 ] = current_index;
        coord2[ 1 ] = xsize;
        coord2[ 2 ] = ysize;
        
        BEC_NMRData::Subset s( ( *out )( coord1, coord2 ) );
        s.SetDataFromCopyPtr( i->get() );
    }
}

void plane_snapshot_recorder::operator()( int outer_iteration_num, float current_threshold, float noise_floor, float noise_std_dev, float clean_level, size_t accepted_positions, size_t components, const float *current_data )
{
    if( count++ % interval != 0 )
        return;
    
    boost::shared_array< float > new_data( new float[ xsize * ysize ] );
    memcpy( new_data.get(), current_data, xsize * ysize * sizeof( float ) );
    data_record.push_back( new_data );
}

//  VECTOR SNAPSHOT RECORDER: record 1-D snapshots during the processing of a 2-D data space
//
//  This facility records 1-D snapshots from a 2-D SCRUB/CLEAN calculation and saves them as
//  CSV text files suitable for visualization in Mathematica.  X and Y snapshots are taken
//  simultaneously at the specified positions, and the deltas between iterations in X and Y
//  are also recorded.  Currently, there is no option to turn off deltas or one of the
//  dimensions.  Recording X/Y/Z 1-D snapshots within a 3-D space would be a trivial extension.
//  The output file contains one timepoint per line of text.
//
//  To use, paste the code below into the main procedure shortly before the call to start the calculation:
//
//      details::vector_snapshot_recorder vsr( xsize, ysize, xpos, x_filename, x_diff_filename, ypos, y_filename, y_diff_filename, interval );
//      boost::function< void ( bool, float, float, float, float, size_t, size_t, const float * ) > f( vsr );
//      s.snapshot_recorder = f;
//  
//  where:
//      x_filename, y_filename = names for the output files to generate, for the X and Y dimensions, respectively
//      x_diff_filename, y_diff_filename = names for the output files to generate, for the X/Y deltas, respectively
//      xsize, ysize = sizes of these dimensions (obtain from input object)
//      xpos, ypos = positions to record
//      interval = how often to record a sample
//      s = scrubber or cleaner object

vector_snapshot_recorder::vector_snapshot_recorder( int xsize_, int ysize_, int xpos_, std::string x_filename, std::string x_diff_filename, int ypos_, std::string y_filename, std::string y_diff_filename, int interval_ ) : xsize( xsize_ ), ysize( ysize_ ), xpos( xpos_ ), ypos( ypos_ ), interval( interval_ ), count( 0 )
{
    x_file = fopen( x_filename.c_str(), "w+" );
    x_diff_file = fopen( x_diff_filename.c_str(), "w+" );
    y_file = fopen( y_filename.c_str(), "w+" );
    y_diff_file = fopen( y_diff_filename.c_str(), "w+" );
    
    prev_data.reset( new float[ xsize * ysize ] );
    memset( prev_data.get(), 0, sizeof( float ) * xsize * ysize );
}

vector_snapshot_recorder::~vector_snapshot_recorder()
{
    fclose( x_file );
    fclose( x_diff_file );
    fclose( y_file );
    fclose( y_diff_file );
}

void vector_snapshot_recorder::operator()( int outer_iteration_num, float current_threshold, float noise_floor, float noise_std_dev, float clean_level, size_t accepted_positions, size_t components, const float *current_data )
{
    if( count++ % interval != 0 )
        return;

    fprintf( x_file, "%f", current_data[ xpos * ysize ] );
    for( int y = 1; y < ysize; y++ )
        fprintf( x_file, ", %f", current_data[ xpos * ysize + y ] );
    fprintf( x_file, "\n" );
    
    fprintf( y_file, "%f", current_data[ ypos ] );
    for( int x = 1; x < xsize; x++ )
        fprintf( y_file, ", %f", current_data[ x * ysize + ypos ] );
    fprintf( y_file, "\n" );

    if( count )
    {
        fprintf( x_diff_file, "%f", current_data[ xpos * ysize ] - prev_data[ xpos * ysize ] );
        for( int y = 1; y < ysize; y++ )
            fprintf( x_diff_file, ", %f", current_data[ xpos * ysize + y ] - prev_data[ xpos * ysize + y ] );
        fprintf( x_diff_file, "\n" );
        
        fprintf( y_diff_file, "%f", current_data[ ypos ] - prev_data[ ypos ] );
        for( int x = 1; x < xsize; x++ )
            fprintf( y_diff_file, ", %f", current_data[ x * ysize + ypos ] - prev_data[ x * ysize + ypos ] );
        fprintf( y_diff_file, "\n" );
    }

    memcpy( prev_data.get(), current_data, sizeof( float ) * xsize * ysize );
}



