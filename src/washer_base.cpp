//  Copyright (c) 2006-2014, Brian E. Coggins, Ph.D.  All rights reserved.

#include <stdio.h>
#include <memory.h>
#include <vector>
#include <boost/shared_array.hpp>
#include <boost/algorithm/string.hpp>
#include <blitz/array.h>
#include <bec_misc/becmisc.h>
#include <nmrdata/nmrdata.h>
#include "nmr_wash.h"

using namespace BEC_Misc;
using namespace BEC_NMRData;
using namespace nmr_wash;
using namespace nmr_wash::details;

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::washer_base( const sampling_pattern &pattern, const dimension_map dim_map, StatusReporter *reporter ) : sp( pattern ), dm( dim_map ), xsize( 0 ), ysize( 0 ), zsize( 0 ), linearity_mode( tde ), pure_component_mode( contour_ellipsoid ), pure_comp_ci_level( 1.0F ), pure_comp_ci_margin( false ), pure_comp_ce_level( 1.0F ), pure_comp_ce_margin( 0.0F ), pure_comp_fe_size( 2.0F ), components_only( false ), residuals_only( false ), num_of_threads( -1 ), log_file( 0 ), verbose_log( false )
{
    if( reporter ) sri = reporter->impl;
    else sri = 0;
    
    if( dm.get_num_of_sparse_dims() != sp.get_num_of_dims() )
        throw nmr_wash::exceptions::inconsistent_parameters();
    
    if( !dm.is_valid() )
        throw exceptions::bad_parameters();
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::operator()( NMRData &input )
{
    setup( input );
    
    if( dm.get_num_of_index_dims() )
    {
        logger l( log_file );
        job_queue jq( num_of_threads );
        boost::shared_ptr< job_callback_wrapper > jcw( new JobCallbackWrapper( static_cast< Derived * >( this ) ) );
        boost::mutex d_mtx;
        
        make_job_pos_inplace mj( &input, &d_mtx, dm );
        std::vector< job > jobs = make_jobs_all_pos( &input, mj, dm, jcw, &jq, l );
        if( sri ) sri->register_jobs( jobs );
        
        for( std::vector< job >::iterator i = jobs.begin(); i != jobs.end(); ++i )
            jq.add_job( *i );
        
        jq();
        
        if( worker_exception )
            boost::rethrow_exception( worker_exception );
    }
    else
    {
        boost::shared_ptr< job_callback_wrapper > jcw( new JobCallbackWrapper( static_cast< Derived * >( this ) ) );
        boost::shared_ptr< job_impl > ji( new job_full_inplace( jcw->get_new_job_id(), log_file, &input, 0 ) );
        job j( ji, jcw );
        if( sri ) sri->register_job( j.get_job_id(), j );
        
        j();
        
        if( worker_exception )
            boost::rethrow_exception( worker_exception );
    }
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::operator()( const NMRData &input, NMRData &output )
{
    if( input.GetNumOfDims() != output.GetNumOfDims() )
        throw nmr_wash::exceptions::in_and_out_not_compatible();
    for( int i = 0; i < input.GetNumOfDims(); i++ )
        if( input.GetDimensionSize( i ) != output.GetDimensionSize( i ) )
            throw nmr_wash::exceptions::in_and_out_not_compatible();
    
    setup( input );
    
    if( dm.get_num_of_index_dims() )
    {
        logger l( log_file );
        job_queue jq( num_of_threads );
        boost::shared_ptr< job_callback_wrapper > jcw( new JobCallbackWrapper( static_cast< Derived * >( this ) ) );
        boost::mutex in_mtx, out_mtx;
        
        make_job_pos mj( &input, &in_mtx, &output, &out_mtx, dm );
        std::vector< job > jobs = make_jobs_all_pos( &input, mj, dm, jcw, &jq, l );
        if( sri ) sri->register_jobs( jobs );
        
        for( std::vector< job >::iterator i = jobs.begin(); i != jobs.end(); ++i )
            jq.add_job( *i );
        
        jq();
        
        if( worker_exception )
            boost::rethrow_exception( worker_exception );
    }
    else
    {
        boost::shared_ptr< job_callback_wrapper > jcw( new JobCallbackWrapper( static_cast< Derived * >( this ) ) );
        boost::shared_ptr< job_impl > ji( new job_full( jcw->get_new_job_id(), log_file, &input, 0, &output, 0 ) );
        job j( ji, jcw );
        if( sri ) sri->register_job( j.get_job_id(), j );
        
        j();
        
        if( worker_exception )
            boost::rethrow_exception( worker_exception );
    }
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::operator()( NMRData &input, std::vector< std::vector< int > > position_list )
{
    if( !dm.get_num_of_index_dims() )
        throw nmr_wash::exceptions::bad_parameters();
    
    setup( input );
    
    logger l( log_file );
    job_queue jq( num_of_threads );
    boost::shared_ptr< job_callback_wrapper > jcw( new JobCallbackWrapper( static_cast< Derived * >( this ) ) );
    boost::mutex d_mtx;
    
    make_job_pos_inplace mj( &input, &d_mtx, dm );
    std::vector< job > jobs = make_jobs_pos_list( position_list, &input, mj, dm, jcw, &jq, l );
    if( sri ) sri->register_jobs( jobs );
    
    for( std::vector< job >::iterator i = jobs.begin(); i != jobs.end(); ++i )
        jq.add_job( *i );
    
    jq();
    
    if( worker_exception )
        boost::rethrow_exception( worker_exception );
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::operator()( const NMRData &input, NMRData &output, std::vector< std::vector< int > > position_list )
{
    if( !dm.get_num_of_index_dims() )
        throw nmr_wash::exceptions::bad_parameters();
    
    setup( input );
    
    logger l( log_file );
    job_queue jq( num_of_threads );
    boost::shared_ptr< job_callback_wrapper > jcw( new JobCallbackWrapper( static_cast< Derived * >( this ) ) );
    boost::mutex in_mtx, out_mtx;
    
    make_job_pos mj( &input, &in_mtx, &output, &out_mtx, dm );
    std::vector< job > jobs = make_jobs_pos_list( position_list, &input, mj, dm, jcw, &jq, l );
    if( sri ) sri->register_jobs( jobs );
    
    for( std::vector< job >::iterator i = jobs.begin(); i != jobs.end(); ++i )
        jq.add_job( *i );
    
    jq();
    
    if( worker_exception )
        boost::rethrow_exception( worker_exception );
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::operator()( const NMRData &input, ptr_vector< NMRData > outputs, std::vector< std::vector< int > > position_list )
{
    if( !dm.get_num_of_index_dims() )
        throw nmr_wash::exceptions::bad_parameters();
    if( outputs.size() != position_list.size() )
        throw nmr_wash::exceptions::bad_parameters();
    
    setup( input );
    
    logger l( log_file );
    job_queue jq( num_of_threads );
    boost::shared_ptr< job_callback_wrapper > jcw( new JobCallbackWrapper( static_cast< Derived * >( this ) ) );
    boost::mutex in_mtx;
    
    make_job_pos_to_full mj( &input, &in_mtx, outputs, dm );
    std::vector< job > jobs = make_jobs_pos_list( position_list, &input, mj, dm, jcw, &jq, l );
    if( sri ) sri->register_jobs( jobs );
    
    for( std::vector< job >::iterator i = jobs.begin(); i != jobs.end(); ++i )
        jq.add_job( *i );
    
    jq();
    
    if( worker_exception )
        boost::rethrow_exception( worker_exception );
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::set_reporter( StatusReporter *new_reporter )
{
    assert( new_reporter );
    sri = new_reporter->impl;
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
boost::shared_array< float > washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::get_psf()
{
    if( psf )
    {
        boost::shared_array< float > psf_copy( new float[ psf_size ] );
        memcpy( psf_copy.get(), psf.get(), sizeof( float ) * psf_size );
        return psf_copy;
    }
    else return boost::shared_array< float >();
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
boost::shared_array< float > washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::get_pure_component()
{
    if( pure_comp )
    {
        boost::shared_array< float > pure_comp_copy( new float[ psf_size ] );
        memcpy( pure_comp_copy.get(), pure_comp.get(), sizeof( float ) * psf_size );
        return pure_comp_copy;
    }
    else return boost::shared_array< float >();
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
int washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::get_xsize()
{
    return xsize;
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
int washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::get_ysize()
{
    return ysize;
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
int washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::get_zsize()
{
    return zsize;
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::setup( const BEC_NMRData::NMRData &input )
{
    if( input.GetNumOfDims() != dm.get_num_of_dims() )
        throw nmr_wash::exceptions::inconsistent_parameters();
    if( dm.get_num_of_index_dims() != input.GetNumOfDims() - sp.get_num_of_dims() )
        throw nmr_wash::exceptions::inconsistent_parameters();

    if( linearity_mode == td )
        pure_component_mode = delta_function;
    
    int new_xsize = 1, new_ysize = 1, new_zsize = 1;
    float xsw = 0.0F, ysw = 0.0F, zsw = 0.0F;   //  N.B. SWs are only needed for off-grid cases, but we pull them from input and pass to psf_calc for both cases as it is easier than trying to distinguish
    
    switch( dm.get_num_of_sparse_dims() )
    {
        case 0:
            break;
        case 1:
            new_xsize = input.GetDimensionSize( dm.get_x_expt_dim_num() );
            xsw = input.GetDimensionSW( dm.get_x_expt_dim_num() );
            break;
        case 2:
            new_xsize = input.GetDimensionSize( dm.get_x_expt_dim_num() );
            new_ysize = input.GetDimensionSize( dm.get_y_expt_dim_num() );
            xsw = input.GetDimensionSW( dm.get_x_expt_dim_num() );
            ysw = input.GetDimensionSW( dm.get_y_expt_dim_num() );
            break;
        case 3:
            new_xsize = input.GetDimensionSize( dm.get_x_expt_dim_num() );
            new_ysize = input.GetDimensionSize( dm.get_y_expt_dim_num() );
            new_zsize = input.GetDimensionSize( dm.get_z_expt_dim_num() );
            xsw = input.GetDimensionSW( dm.get_x_expt_dim_num() );
            ysw = input.GetDimensionSW( dm.get_y_expt_dim_num() );
            zsw = input.GetDimensionSW( dm.get_z_expt_dim_num() );
            break;
    }
    
    if( new_xsize != xsize ||
       new_ysize != ysize ||
       new_zsize != zsize )
    {
        psf_calc psfc( sp, dm, new_xsize, new_ysize, new_zsize, xsw, ysw, zsw );
        psf = psfc.psf();
        psf_size = psfc.get_psf_size();
        
        switch( pure_component_mode )
        {
            case contour_irregular:
                pure_comp = psfc.pure_component_contour_irregular( pure_comp_ci_level, pure_comp_ci_margin );
                break;
            case contour_ellipsoid:
                pure_comp = psfc.pure_component_contour_ellipsoid( pure_comp_ce_level, pure_comp_ce_margin );
                break;
            case fixed_ellipsoid:
                pure_comp = psfc.pure_component_fixed_ellipsoid( pure_comp_fe_size );
                break;
            case delta_function:
                pure_comp = psfc.pure_component_delta_function();
                break;
        }
        
        if( linearity_mode != fd )
            comp_scale = psfc.comp_scale;
        else
            comp_scale = 1.0F;
        
        cached_linearity_mode = linearity_mode;
        cached_pure_component_mode = pure_component_mode;
        cached_pure_comp_ci_level = pure_comp_ci_level;
        cached_pure_comp_ci_margin = pure_comp_ci_margin;
        cached_pure_comp_ce_level = pure_comp_ce_level;
        cached_pure_comp_ce_margin = pure_comp_ce_margin;
        cached_pure_comp_fe_size = pure_comp_fe_size;
        
        xsize = new_xsize;
        ysize = new_ysize;
        zsize = new_zsize;
    }
    else if( cached_linearity_mode != linearity_mode ||
            cached_pure_component_mode != pure_component_mode ||
            cached_pure_comp_ci_level != pure_comp_ci_level ||
            cached_pure_comp_ci_margin != pure_comp_ci_margin ||
            cached_pure_comp_ce_level != pure_comp_ce_level ||
            cached_pure_comp_ce_margin != pure_comp_ce_margin ||
            cached_pure_comp_fe_size != pure_comp_fe_size )
    {
        psf_calc psfc( sp, dm, xsize, ysize, zsize );
        psfc.set_psf( psf );
        
        switch( pure_component_mode )
        {
            case contour_irregular:
                pure_comp = psfc.pure_component_contour_irregular( pure_comp_ci_level, pure_comp_ci_margin );
                break;
            case contour_ellipsoid:
                pure_comp = psfc.pure_component_contour_ellipsoid( pure_comp_ce_level, pure_comp_ce_margin );
                break;
            case fixed_ellipsoid:
                pure_comp = psfc.pure_component_fixed_ellipsoid( pure_comp_fe_size );
                break;
            case delta_function:
                pure_comp = psfc.pure_component_delta_function();
                break;
        }
        
        if( linearity_mode != fd )
            comp_scale = psfc.comp_scale;
        else
            comp_scale = 1.0F;
        
        cached_linearity_mode = linearity_mode;
        cached_pure_component_mode = pure_component_mode;
        cached_pure_comp_ci_level = pure_comp_ci_level;
        cached_pure_comp_ci_margin = pure_comp_ci_margin;
        cached_pure_comp_ce_level = pure_comp_ce_level;
        cached_pure_comp_ce_margin = pure_comp_ce_margin;
        cached_pure_comp_fe_size = pure_comp_fe_size;
    }
    
    if( sri ) sri->clear();
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
details::position washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::get_position( size_t offset )
{
    int x = ( int ) offset / ( ysize * zsize );
    size_t xm = offset % ( ysize * zsize );
    int y = ( int ) xm / zsize;
    int z = ( int ) xm % zsize;
    return position( offset, x, y, z );
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::subtract_psf( position p, float *out, float scale )
{
    const float *psf_direct = psf.get();
    
    if( ysize == 1 && zsize == 1 )  //  one indirect dimension
    {
        for( int x = 0; x < xsize; x++ )
        {
            int psf_x = xsize - p.xpos + x;
            out[ x ] -= psf_direct[ psf_x ] * scale;
        }        
    }
    else if( zsize == 1 )           //  two indirect dimensions
    {
//        size_t out_offset = 0;
//        for( int x = 0; x < xsize; x++ )
//            for( int y = 0; y < ysize; y++ )
//            {
//                int psf_x = xsize - p.xpos + x;
//                int psf_y = ysize - p.ypos + y;
//                size_t psf_offset = psf_x * ysize * 2 + psf_y;
//                
//                out[ out_offset ] -= psf_direct[ psf_offset ] * scale;
//                
//                out_offset++;
//            }        

        blitz::Array< float, 2 > b_out( out, blitz::shape( xsize, ysize ), blitz::neverDeleteData );
        blitz::Array< float, 2 > b_psf( psf.get(), blitz::shape( xsize * 2, ysize * 2 ), blitz::neverDeleteData );
        blitz::Array< float, 2 > b_psf_subset = b_psf( blitz::Range( xsize - p.xpos, xsize - p.xpos + xsize - 1 ),
                                                      blitz::Range( ysize - p.ypos, ysize - p.ypos + ysize - 1 ) );
        b_out -= b_psf_subset * scale;
    }
    else                            //  three indirect dimensions
    {
//        size_t out_offset = 0;
//        for( int x = 0; x < xsize; x++ )
//            for( int y = 0; y < ysize; y++ )
//                for( int z = 0; z < zsize; z++ )
//                {
//                    int psf_x = xsize - p.xpos + x;
//                    int psf_y = ysize - p.ypos + y;
//                    int psf_z = zsize - p.zpos + z;
//                    size_t psf_offset = psf_x * ysize * zsize * 4 + psf_y * zsize * 2 + psf_z;
//                    
//                    out[ out_offset ] -= psf_direct[ psf_offset ] * scale;
//                    
//                    out_offset++;
//                }
        blitz::Array< float, 3 > b_out( out, blitz::shape( xsize, ysize, zsize ), blitz::neverDeleteData );
        blitz::Array< float, 3 > b_psf( psf.get(), blitz::shape( xsize * 2, ysize * 2, zsize * 2 ), blitz::neverDeleteData );
        blitz::Array< float, 3 > b_psf_subset = b_psf( blitz::Range( xsize - p.xpos, xsize - p.xpos + xsize - 1 ),
                                                       blitz::Range( ysize - p.ypos, ysize - p.ypos + ysize - 1 ),
                                                      blitz::Range( zsize - p.zpos, zsize - p.zpos + zsize - 1 ) );
        b_out -= b_psf_subset * scale;
    }
}

template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::add_pure_components( float *out, std::vector< component > &components )
{
    if( !components.size() )
        return;
    
    const float *pc_direct = pure_comp.get();
    
    std::sort( components.begin(), components.end(), component_offset_less() );
    std::vector< component > cc;
    cc.reserve( components.size() );
    for( std::vector< component >::iterator c = components.begin(); c != components.end(); )
    {
        std::vector< component >::iterator a = std::adjacent_find( c, components.end(), component_offset_equal() );
        std::copy( c, a, std::back_inserter( cc ) );
        if( a != components.end() )
        {
            component new_comp = *( a++ );
            while( a != components.end() && a->pos.offset == new_comp.pos.offset )
                new_comp.scale += ( a++ )->scale;
            cc.push_back( new_comp );
        }
        c = a;
    }
    
    if( ysize == 1 && zsize == 1 )  //  one indirect dimension
    {
        for( std::vector< component >::iterator c = cc.begin(); c != cc.end(); ++c )
        {
            for( int x = 0; x < xsize; x++ )
            {
                int pure_comp_x = xsize - c->pos.xpos + x;
                out[ x ] += pc_direct[ pure_comp_x ] * c->scale;
            }
        }
    }
    else if( zsize == 1 )           //  two indirect dimensions
    {
        blitz::Array< float, 2 > b_out( out, blitz::shape( xsize, ysize ), blitz::neverDeleteData );
        blitz::Array< float, 2 > b_pc( pure_comp.get(), blitz::shape( xsize * 2, ysize * 2 ), blitz::neverDeleteData );
        
        for( std::vector< component >::iterator c = cc.begin(); c != cc.end(); ++c )
        {
//            size_t out_offset = 0;
//            for( int x = 0; x < xsize; x++ )
//                for( int y = 0; y < ysize; y++ )
//                {
//                    int pure_comp_x = xsize - c->pos.xpos + x;
//                    int pure_comp_y = ysize - c->pos.ypos + y;
//                    size_t pure_comp_offset = pure_comp_x * ysize * 2 + pure_comp_y;
//                    
//                    out[ out_offset ] += pc_direct[ pure_comp_offset ] * c->scale;
//                    
//                    out_offset++;
//                }
            
            blitz::Array< float, 2 > b_pc_subset = b_pc( blitz::Range( xsize - c->pos.xpos, xsize - c->pos.xpos + xsize - 1 ),
                                                         blitz::Range( ysize - c->pos.ypos, ysize - c->pos.ypos + ysize - 1 ) );
            b_out += b_pc_subset * c->scale;
        }        
    }
    else                            //  three indirect dimensions
    {
        blitz::Array< float, 3 > b_out( out, blitz::shape( xsize, ysize, zsize ), blitz::neverDeleteData );
        blitz::Array< float, 3 > b_pc( pure_comp.get(), blitz::shape( xsize * 2, ysize * 2, zsize * 2 ), blitz::neverDeleteData );

        for( std::vector< component >::iterator c = cc.begin(); c != cc.end(); ++c )
        {
//            size_t out_offset = 0;
//            for( int x = 0; x < xsize; x++ )
//                for( int y = 0; y < ysize; y++ )
//                    for( int z = 0; z < zsize; z++ )
//                    {
//                        int pure_comp_x = xsize - c->pos.xpos + x;
//                        int pure_comp_y = ysize - c->pos.ypos + y;
//                        int pure_comp_z = zsize - c->pos.zpos + z;
//                        size_t pure_comp_offset = pure_comp_x * ysize * zsize * 4 + pure_comp_y * zsize * 2 + pure_comp_z;
//                        
//                        out[ out_offset ] += pc_direct[ pure_comp_offset ] * c->scale;
//                        
//                        out_offset++;
//                    }

            blitz::Array< float, 3 > b_pc_subset = b_pc( blitz::Range( xsize - c->pos.xpos, xsize - c->pos.xpos + xsize - 1 ),
                                                         blitz::Range( ysize - c->pos.ypos, ysize - c->pos.ypos + ysize - 1 ),
                                                         blitz::Range( zsize - c->pos.zpos, zsize - c->pos.zpos + zsize - 1 ) );
            b_out += b_pc_subset * c->scale;
        }        
    }
}


template< typename Derived, typename JobInfo, typename JobStatusRelay, typename JobCallbackWrapper, typename StatusReporter >
void washer_base< Derived, JobInfo, JobStatusRelay, JobCallbackWrapper, StatusReporter >::debug_snapshot( const float *data, const char *filename, int xs, int ys, int zs, float scale )
{
    using namespace BEC_NMRData;
    
    NMRData_NMRView out;
    out.SetNumOfDims( 3 );
    out.SetDimensionSize( 0, xs );
    out.SetDimensionLabel( 0, "X" );
    out.SetDimensionSize( 1, ys );
    out.SetDimensionLabel( 1, "Y" );
    out.SetDimensionSize( 2, zs );
    out.SetDimensionLabel( 2, "Z" );
    out.CreateFile( filename );
    out.SetDataFromCopyPtr( data );
    if( scale != 1.0F )
        out *= scale;
    out.Close();
}

std::istream & nmr_wash::operator>>( std::istream &in, nmr_wash::linearity_modes &lm )
{
    std::string token;
    in >> token;
    
    boost::to_lower( token );
    
    if( token == "fd")
        lm = nmr_wash::fd;
    else if( token == "tde" )
        lm = nmr_wash::tde;
    else if( token == "td" )
        lm = nmr_wash::td;
    else
        throw exceptions::bad_linearity_mode_string();
    
    return in;
}

std::istream & nmr_wash::operator>>( std::istream &in, nmr_wash::pure_component_modes &pcm )
{
    std::string token;
    in >> token;
    
    boost::to_lower( token );
    
    if( token == "contour-irregular")
        pcm = nmr_wash::contour_irregular;
    else if( token == "contour-ellipsoid" )
        pcm = nmr_wash::contour_ellipsoid;
    else if( token == "fixed-ellipsoid" )
        pcm = nmr_wash::fixed_ellipsoid;
    else if( token == "delta-function" )
        pcm = nmr_wash::delta_function;
    else
        throw exceptions::bad_pure_component_mode_string();
    
    return in;
}

namespace nmr_wash
{
    template class washer_base< nmr_wash::cleaner, nmr_wash::job_info_clean, nmr_wash::details::job_status_relay_clean, nmr_wash::details::job_callback_wrapper_clean, nmr_wash::status_reporter_clean >;
    template class washer_base< nmr_wash::scrubber, nmr_wash::job_info_scrub, nmr_wash::details::job_status_relay_scrub, nmr_wash::details::job_callback_wrapper_scrub, nmr_wash::status_reporter_scrub >;
}

